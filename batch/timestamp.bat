@echo off
REM Displays current date and time.
REM This is for a simple format of %date% %time% == 2015-04-18 17:10:55,94
REM Other settings may give different results and require more clean-up (e.g. Sat, May 4, 2015 5:10:55,94 PM)

REM Replace leading whitespace with a zero.
set _time=%time: =0%

REM Output date and time; cut off miliseconds.
echo %date% %_time:~0,-3%
