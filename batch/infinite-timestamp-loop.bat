REM Appends info to a log file every n seconds in an infinite loop.
REM
REM The random decimal number is between 0 and 32767, but know the limitations:
REM http://blogs.msdn.com/b/oldnewthing/archive/2010/06/17/10026183.aspx
@echo off
:again
echo %DATE% %TIME% - %COMPUTERNAME% - %RANDOM% >> log.txt
timeout /t 5 /nobreak
goto again