REM Usage: KillAndRestartVNC.bat XXX
REM XXX can be a remote computer. -- Sascha Offe, 2015-01-29.

@ECHO OFF
%WINDIR%\System32\sc.exe \\%1 stop uvnc_service
%WINDIR%\System32\tasklist.exe /S \\%1 /FI "IMAGENAME eq winvnc.exe" REM Show processes; not really necessary.
%WINDIR%\System32\taskkill.exe /S \\%1 /FI "IMAGENAME eq winvnc.exe" /F
%WINDIR%\System32\timeout.exe /T 10 REM For versions < Windows 7 use %WINDIR%\System32\ping.exe -n 10 localhost > nul
%WINDIR%\System32\sc.exe \\%1 start uvnc_service