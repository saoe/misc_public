@echo off
REM Expects output as shown below (YYYY-MM-DD); adjust token count, delimiter symbols and/or token order if necessary.
REM
REM C:\temp>echo %DATE%
REM 2015-02-19
REM
REM C:\temp>echo %TIME%
REM 10:15:05,77

For /f "tokens=1-3 delims=/- " %%a in ("%DATE%") do (
    SET YEAR=%%a
    SET MONTH=%%b
    SET DAY=%%c
)

For /f "tokens=1-4 delims=/:," %%a in ("%TIME: =0%") do (
    SET HOUR=%%a
    SET MIN=%%b
    SET SEC=%%c
    SET MSEC=%%d
)

echo Raw Date = %DATE%
echo Raw Time = %TIME%
echo YEAR = %YEAR%
echo MONTH = %MONTH%
echo DAY = %DAY%
echo HOUR = %HOUR%
echo MIN = %MIN%
echo SEC = %SEC%
echo MSEC = %MSEC%
echo %YEAR%-%MONTH%-%DAY% %HOUR%:%MIN%:%SEC% 
