@echo off

REM For example, this batch file is in C:\directory1\directory2\file.bat
REM So starting it from a prompt there (or double clicking it),
REM the current working diretory will also be C:\directory1\directory2.

REM --- The Problem ---
REM But if you 'Run it as Administrator' (using UAC), or if the working directory is changed in a shortcut,
REM the current working directory will be C:\Windows\System32 (or whatever is set in the shortcut).
echo The current working directory is: %cd%

REM --- The Workaround ---
REM This way, we will change the working directory to where the file is really located.
REM (The parameter /d makes sure that also the drive is changed, if neccessary.)
cd /d %~d0%~p0
echo The current working directory is now: %cd%

REM --- The Explanation ---
REM %0 is the path to the batch file.
REM * When called from C:\directory1\directory2 directly, it's "file.bat".
REM * When called from C:\, it's "directory1\directory2\file.bat".

REM --- More information and modiefiers ---

REM %~f0 - The full path, ("C:\directory1\directory2\file.bat")
echo Full path     : %~f0

REM %~d0 - Just the drive ("C:").
echo Drive         : %~d0

REM %~p0 - Only the directories ("\directory1\directory2\").
echo Directory     : %~p0

REM %~n0 - Just the base name of the file ("file").
echo File basename : %~n0

REM %~x0 - The extension of the file, inclusive the dot (".bat").
echo File extension: %~x0

REM Combination is also possible: %~dpnx0 will give the same result as %~f0
echo %~dpnx0 = %~f0

REM Wait for input.
pause