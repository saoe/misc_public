# Sascha's repository of _miscellaneous public_ source code 

Assorted scripts, snippets, small programs and auxiliary utilities.  

This repo's home: <https://bitbucket.org/saoe/misc_public>.  
Author's website: <http://www.saoe.net/>

Please check the particular files for copyright notices and license terms.

--------------------------------------------------------------------------------

## Batch

* [date-and-time.bat](batch/date-and-time.bat)  
  Displays the current date and time, complete and split in components.  

* [kill-and-restart-vnc.bat](batch/kill-and-restart-vnc.bat)  
  Kills and restarts the WinVNC.exe process on a (remote) Windows computer.  

* [timestamp.bat](batch/timestamp.bat)  
  Displays current date and time.  

* [infinite-timestamp-loop.bat](batch/timestamp.bat)  
  Appends info to a log file every n seconds in an infinite loop.  

* [current-working-directory.bat](batch/current-working-directory.bat)  
  Example on how to get/use the location of the current working directory.  

## C or C++

See [ReadMe in the C++ and C section](c_cpp/ReadMe.md) for details...

## CMake

* [CMakeLists-Template.txt](cmake/CMakeLists-Template.txt)  
  Basic CMakeLists.txt template file for a new project.  

* [CMake-Snippets.cmake](cmake/CMake-Snippets.cmake)  
  Small CMake code snippets, notes and reminders.  

* [FindVCRedist.cmake](cmake/FindVCRedist.cmake)  
  Function to return the full path to the installation file for the Microsoft Visual C++ Redistributable Package.  

* [version.cmake](cmake/version.cmake)  
  Script to get the timestamp (BUILDTIME) and the Git commit hash of 'head' (GIT_COMMIT_HASH) for a version.h file.  

## Powershell

See [ReadMe in the Powershell section](powershell/ReadMe.md) for details...
  
## Python

See [ReadMe in the Python section](python/ReadMe.md) for details...

## Visual Basic Script (VBScript)

* [modify-computer-group-membership.vbs](vbscript/modify-computer-group-membership.vbs)  
  Adds/removes computer object to/from ActiveDirectory group(s).  

* [move-activedirectory-computerobject-to-organizationalunit.vbs](vbscript/move-activedirectory-computerobject-to-organizationalunit.vbs)  
  Script to let a client (computer object) move itself to another Organizational Unit in an ActiveDirectory.  
