' ==================================================================================================
' Moves computer object to a different organizational unit (OU) in an Active Directory (AD).
'
' The script should be executed on the computer itself that needs to be moved.
' Note that the caller of the script therefore needs the privileges to move objects from one domain OU to another.
' Usually some method of "RunAs <privileged_user> cmd.exe /c cscript.exe moveToOU.vbs" works.
'
' As it is, the mechanics of this script expect a certain setup and layout to work:
'    The computer's hostname must have a specific naming convention (see below),
'    the target OU must have an appropriate description text (the location code).
'    the target OU must have certain sub-OUs ("Workstations", "Notebooks").
' So, you might have to adapt the script to your environment to be useful.
'
' [!] This script was not originally written by me!
'     I just inherited it and modified it (not even sure if my long-gone precursor at work wrote it...).
'     So kudos to whoever!
'
' 2017-10-02 Sascha Offe: The script can now be executed by a user that is a member of a different
'                         domain than the one to which the computer object belongs (as long as the
'                         user has the required privileges in both ADs, of course.)
' ==================================================================================================
 
set objNetwork = CreateObject("Wscript.Network")

computername = objNetwork.Computername

' Get the domain controller, on which the computer authenticated (e.g. 'dc01.de.example.com').
set Domain = GetObject("LDAP://" & GetComputerDomain() & "/rootDse")
DomainController = Domain.Get("dnsHostName")

'wscript.echo "* Domain Controller: " & vbCrLf & DomainController & vbCrLf & vbCrLf

OUPath = GetLocationOUADPath(GetLocation())
ComputerPath = GetComputerADPath()

if Instr(UCase(computername),"WS") >= 5 then
	OUPath = Replace(OUPath,"LDAP://", "LDAP://OU=Workstations,")
end if
 
if Instr(UCase(computername),"NB") >= 5 then
	OUPath = Replace(OUPath,"LDAP://", "LDAP://OU=Notebooks,")
end if

MoveObject OUPath,ComputerPath

'___________________________________________________________________________________________________

' --------------------------------------------------------------------------------------------------
' Get the domain of the computer object (e.g. 'de.example.com').

function GetComputerDomain ()
	set objWMISvc = GetObject( "winmgmts:\\.\root\cimv2" )
	set colItems = objWMISvc.ExecQuery( "Select * from Win32_ComputerSystem" )
		for each objItem in colItems
			strComputerDomain = objItem.Domain
			if objItem.PartOfDomain then
				GetComputerDomain = strComputerDomain
			else
				GetComputerDomain = ""
			end if
				next
end function

' --------------------------------------------------------------------------------------------------
' Extract from the hostname the location code (CCLLL).
' (Relies on our special naming convention: CCLLLTT#####: CC = Country code; LLL = Location code;
'                                                         TT = Machine type; ##### = Inventory number).

function GetLocation()
	set objNetwork = CreateObject("Wscript.Network")
	computername = objNetwork.Computername
	
	if Instr(UCase(computername),"WS") >= 5 then          ' Workstation
		intEndPos = Instr(UCase(computername),"WS")
	else
		if Instr(UCase(computername),"NB") >= 5 then
			intEndPos = Instr(UCase(computername),"NB")   ' Notebook
		else
			Wscript.echo "Location not found"
			Wscript.quit
		end if
	end if

	GetLocation = Left(computername,intEndPos - 1)
end function

' --------------------------------------------------------------------------------------------------
' Return the path to the location OU (e.g. 'LDAP://dc01.de.example.com/OU=Location,DC=de,DC=example,DC=com').
'
' Note: Expects the same location code in the description of the OU!
' I.e. the computer has the name CCLLLTT12345, the the target OU's description text must be LLL!

function GetLocationOUADPath (Location)
	const ADS_SCOPE_SUBTREE = 2
	set Connection = CreateObject("ADODB.Connection")
	Connection.Open "Provider=ADsDSOObject;"
	set Command = CreateObject("ADODB.Command")
	Command.ActiveConnection = Connection
	Command.Properties("searchscope") = ADS_SCOPE_SUBTREE
	
	' Add domain to the OpenDSObject argument, so that the context of the computer object is used, not the one from the user running the script.
	' (Example: Computer is member of domain 'de.example.com', but the user is member of another domain, e.g. root 'example.com').
	set objNS = GetObject("LDAP:")
	set objRootDSE = objNS.OpenDSObject("LDAP://" & GetComputerDomain() & "/RootDSE", "", "", 0)
	DNSDomain = objRootDSE.Get("DefaultNamingContext")
	
	' Add specific domain controller; otherwise the result will be an error ("A referral was returned from the server").
	' From https://stackoverflow.com/a/6974224
	' > A referral is sent by an AD server when it doesn't have the information requested itself, but know that another server have the info.
	' > It usually appears in trust environment where a DC can refer to a DC in trusted domain.
	' > In your case you are only specifying a domain, relying on automatic lookup of what domain controller to use.
	' > I think that you should try to find out what domain controller is used for the query and look if that one really holds the requested information.
	' > If you provide more information on your AD setup, including any trusts/subdomains, global catalogues and the DNS resource records for the domain controllers it will be easier to help you.
	Command.CommandText = "SELECT Adspath FROM 'LDAP://" & DomainController & "/" & DNSDomain & "' Where objectClass='organizationalUnit' and description='" & Location & "'"
	set RecordSet = Command.Execute
	GetLocationOUADPath = RecordSet.Fields("Adspath")
	Connection.Close
end function

' --------------------------------------------------------------------------------------------------
' Return the current path in the Active Directory (AD) of the computer object (e.g. 'LDAP://CN=machine12345,CN=Computers,DC=de,DC=example,DC=com').

function GetComputerADPath ()
	set ADSysInfo = CreateObject("ADSystemInfo")
	GetComputerADPath = "LDAP://" & ADSysInfo.Computername
end function

' --------------------------------------------------------------------------------------------------
' Move computer object to a different organizational unit (OU) in the Active Directory (AD).

function MoveObject (OUPath, ComputerPath)
	OUPath = Replace(OUPath, DomainController & "/", "")     ' Delete name of the DC from the path (otherwise MoveHere won't work).
	set OUObject = GetObject(OUPath)
	OUObject.MoveHere ComputerPath,vbNullString
end function
