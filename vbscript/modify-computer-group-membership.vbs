' ------------------------------------------------------------------------------
' Adds/removes the computer object on which the script is invoked to/from the
' given group(s); requires an ActiveDirectory domain!
'
' Usage: cscript.exe scriptname.vbs OPERATOR "Group Name 1" "Group Name 2"
'        OPERATOR = ADD: Add the computer object to the group(s).
'                   DEL: Delete (remove) the computer object from the group(s).
'
' Based on https://gallery.technet.microsoft.com/scriptcenter/Auto-adding-computer-to-AD-dd4b4df5
' Modified by Sascha Offe (2015-11-09).
'
' Note: A similar, built-in version of this is the command line command "net group".
' ------------------------------------------------------------------------------

Set WshShell = WScript.CreateObject("WScript.Shell")

OPERATOR = UCase(Wscript.Arguments(0))

if Not OPERATOR = "ADD" And Not OPERATOR = "DEL" Then
	WScript.Echo "*** Error ***"
	WScript.Echo "Usage: cscript.exe " & Wscript.ScriptName & " <ADD|DEL> ""Group Name 1"" ""Group Name 2""."
	WScript.Quit
End if

' Get Distinguished Name (DN) of the Computer.
Set objADSysInfo = CreateObject("ADSystemInfo")
ComputerDN       = objADSysInfo.ComputerName
strComputerDN    = "LDAP://" & ComputerDN
Set objADSysInfo = Nothing

' Connect with the Active Directory.
Set oRoot            = GetObject("LDAP://rootDSE")
strDomainPath        = oRoot.Get("defaultNamingContext")
Set oConnection      = CreateObject("ADODB.Connection")
oConnection.Provider = "ADsDSOObject"
oConnection.Open "Active Directory Provider"

count = WScript.Arguments.Count

For i = 1 To count-1          
	Group = WScript.Arguments(i)
	modifyGroupMembership(Group)
Next


' -----------------------------------------------------------------------------------
' Add/remove computer object to/from a group, depending on the given operator on the command line (ADD or DEL).

Function modifyGroupMembership (groupname)
	' Note that the 'names' in the AD might differ: <http://www.rlmueller.net/Name_Attributes.htm>
	' Example: I thought, the space in a group name was the problem -- not true:
	'          "Name" was using a hyphen (-), while "sAMAccountName" was using an underscore (_)... :-/
	Set oRs = oConnection.Execute("SELECT adspath FROM 'LDAP://" & strDomainPath & "'" & "WHERE objectCategory='group' AND " & "sAMAccountName='" & groupname & "'")

	If Not oRs.EOF Then
		strAdsPath = oRs("adspath")
	End If

	If IsEmpty(strAdsPath) = False Then
		Const ADS_SECURE_AUTHENTICATION = 1
		Set objGroup = GetObject(strAdsPath)
		Set objComputer = GetObject(strComputerDN)

		If OPERATOR = "ADD" Then
			If (objGroup.IsMember(objComputer.AdsPath) = False) Then
				objGroup.PutEx 3, "member", Array(ComputerDN)           ' 3 = ADS_PROPERTY_APPEND
				objGroup.SetInfo
			End If
		ElseIf OPERATOR = "DEL" Then
			If (objGroup.IsMember(objComputer.AdsPath)) Then
				objGroup.PutEx 4, "member", Array(ComputerDN)           ' 4 = ADS_PROPERTY_DELETE
				objGroup.SetInfo
			End If
		End If
	End If
End Function