# ==================================================================================================
# Update all local Mercurial repositories at once.
# Expects all clone repos to be below a common root directory (from which the script will be invoked);
# e.g.:
# 
#     hgdirectory
#     +-- repo1
#     +-- repo2
#     +-- ...
#     +-- script.py
#
#     C:\hgdirectory> script.py pull
#
# 2016-06-12 Sascha Offe: Created script; implemented hg_pull_and_update().
# 2018-04-19 Sascha Offe: 'print' must be used as a function under Python3, not as a statement.
#                         'subprocess.check_output()' removes the newline character at the end.
# 
# Copyright � 2016, 2018 Sascha Offe <so@saoe.net>
# SPDX-License-Identifier: MIT-0
# ==================================================================================================

import sys
import os
import subprocess

helptext = (
	"Usage: " + os.path.basename(sys.argv[0]) + " argument\n"
	"Argument(s):\n"
	"\tpull - Pull data from remote repository (and update the local clone)."
)


def hg_pull_and_update (dir):
	os.chdir(dir)
	
	RemoteRepository = subprocess.check_output("hg paths default", universal_newlines=True)
	print("*** Pulling data from " + RemoteRepository + "...")
	subprocess.call("hg pull --verbose " + RemoteRepository, shell=True)
	
	print("*** Updating local repository...")
	subprocess.call("hg update --config ui.merge=internal:fail --rev default --check", shell=True)
	
	os.chdir(os.path.normpath(os.pardir))


# --------------------------------------------------------------------------------------------------
# The main body of the script.
# Walk over and act on the top-level sub-directories in the current working directory of the script.

def main (argv):
	cwd = os.getcwd()
	
	if len(argv) == 2:
		if argv[1] == "pull":
			for directory in os.listdir(cwd):
				if os.path.exists(os.path.join(directory, ".hg")):
					print("----------------------------------------")
					print("*** Found a Hg repository in " + directory)
					hg_pull_and_update(directory)
		else:
			print(helptext)
	else:
		print(helptext)


if __name__ == "__main__":
	main(sys.argv) # Execute only if run as a script.
