# Script to calculate or verify a file's checksum/hash/message digest.
# Copyright © 2019 Sascha Offe <so@saoe.net>.
# Distributed under the terms of the MIT license <https://opensource.org/licenses/MIT>.

import sys
import os
import argparse
import hashlib

# Check whether Python 3+ is used.
if sys.version_info[0] < 3:
	sys.exit("You seem to be running an older version of Python:\n" + sys.version + "\n\nSorry, but you need Python 3 or higher for this script.")


# --------------------------------------------------------------------------------------------------
# Calculates the checksum of (a path to) a file, with the provided algorithm.
# 
# Returns a tuple of multiple values (index/order):
# [0] the calculated checksum
# [1] the used alorithm
# [2] the file the calculation is based on.
# [3] the size of the file (in bytes).

def calculate (input, algorithm):

	
	if (algorithm == "MD5"):
		hash_func = hashlib.md5()
	elif (algorithm == "SHA-1"):
		hash_func = hashlib.sha1()
	elif (algorithm == "SHA-224"):
		hash_func = hashlib.sha224()
	elif (algorithm == "SHA-256"):
		hash_func = hashlib.sha256()
	elif (algorithm == "SHA-384"):
		hash_func = hashlib.sha384()
	elif (algorithm == "SHA-512"):
		hash_func = hashlib.sha512()
	else:
		raise Exception("Error: " + algorithm + " is an unknown algorithm!\n")
	
	# Read the file (in chunks; to prevent to choking the RAM when reading large files at once).
	if (os.path.isfile(input)):
		with open(input, "rb") as f:           # 'with...' automatically closes the file at the end.
			while True:
				file_data = f.read(8192)       # Block size = 8192 Bytes (8 KB); just a good guess.
				if not file_data:
					break
				hash_func.update(file_data)
	else:
		raise FileNotFoundError("Error: File \"" + input + "\" not found!")
	
	return hash_func.hexdigest(),\
	       algorithm,\
	       input,\
	       os.path.getsize(input)


# --------------------------------------------------------------------------------------------------
# Check whether checksum and fingerprint match.

def verify (checksum, fingerprint):
	if (checksum == (fingerprint.lower() or fingerprint.upper())):
		return True
	else:
		return False
	

# --------------------------------------------------------------------------------------------------
# The main part of the script.

if __name__ == '__main__':	

	parser = argparse.ArgumentParser(description     = "Script to calculate or verify a file's checksum/hash/message digest.\nVersion 1.1.",
									 prefix_chars    = '-',
									 add_help        = True,   # We rely on the provided help by argparse.
									 epilog          = "Copyright (c) 2019 Sascha Offe <so@saoe.net>.\nPublished under the terms of the MIT license (see http://opensource.org/licenses/MIT for details).",
									 formatter_class = argparse.RawTextHelpFormatter   # To enable linebreaks (\n) in the strings.
									)

	parser.add_argument("file",
						action   = "store",
						default  = False,
						help     = "Path to the file for which the checksum will be calculated,\nor against which the fingerprint will be verified."
					   )

	parser.add_argument("-a",
						required = False,
						action   = "store",
						choices  = ["MD5", "SHA-1", "SHA-224", "SHA-256", "SHA-384", "SHA-512"],
						default  = "SHA-256",
						dest     = "algorithm",
						help     = "The algorithm used for calculation and verification.\nDefault is SHA-256."
					   )

	parser.add_argument("-f",
						required = False,
						nargs    = 1,
						action   = "store",
						default  = None,
						dest     = "fingerprint",
						help     = "The fingerprint against which the file should be verfified.\nMake sure that the same algorithm has been used!"
					   )

	args = parser.parse_args()
	
	if (args.fingerprint):
		args.fingerprint = ''.join(args.fingerprint) # The 'narg' is a list, but we need a string.
	
	try:
		hash, algo, filepath, fsize = calculate(args.file, args.algorithm)
	except (FileNotFoundError):
		print("{0}".format(sys.exc_info()[1]))
		sys.exit()
	
	print("")
	print("File       : " + filepath)
	print("Size       : " + str(fsize) + " Bytes")
	print("Algorithm  : " + algo)
	print("Checksum   : " + hash)
	
	if (args.fingerprint):
		print("Fingerprint: " + args.fingerprint)

		if (verify(hash, args.fingerprint)):
			print("\n\t √ PASS - Checksum & Fingerprint match.")
		else:
			print("\n\t X FAIL - Checksum & Fingerprint don't match!")
