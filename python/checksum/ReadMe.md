# Checksum.py

A Python script to calculate or verify a file's checksum/hash/message digest (MD5, SHA-1, SHA-256, etc.).  
(Plus an additional script that acts as a simple Tkinter GUI.)

## GUI

Call `C:\checksum> python gui.pyw` or just double-click it, if you have associated the `\*.pyw`
filetype with the _PythonW.exe_, that will bring up the [Tkinter](https://docs.python.org/3/library/tkinter.html)-based GUI for the script:

### Select a file

![Select a file](gui-browse.png)

### Calculate the checksum for the file

You can also easily copy the content of the message box by the usual Copy-&-Paste shortcuts under Microsoft Windows: _Ctrl+C_, _Ctrl+V_.

![Calculate the checksum for the file](gui-calculate.png)

### Check against a checksum-fingerprint

![Verify/Check/Compare checksum with another checksum-fingerprint](gui-verify.png)

----------------------------------------------------------------------------------------------------

## Command Line

### Show help

	C:\checksum> checksum.py -h

	usage: checksum.py [-h] [-a {MD5,SHA-1,SHA-224,SHA-256,SHA-384,SHA-512}]
	                   [-f FINGERPRINT]
	                   file

	Script to calculate or verify a file's checksum/hash/message digest.
	Version 1.1.

	positional arguments:
	  file                  Path to the file for which the checksum will be calculated,
	                        or against which the fingerprint will be verified.

	optional arguments:
	  -h, --help            show this help message and exit
	  -a {MD5,SHA-1,SHA-224,SHA-256,SHA-384,SHA-512}
	                        The algorithm used for calculation and verification.
	                        Default is SHA-256.
	  -f FINGERPRINT        The fingerprint against which the file should be verfified.
	                        Make sure that the same algorithm has been used!

	Copyright (c) 2019 Sascha Offe <so@saoe.net>.
	Published under the terms of the MIT license (see http://opensource.org/licenses/MIT for details).


### Calculate the checksum for the file

	C:\checksum> checksum.py "C:\temp\2016-01-04_025432.png"

	File       : C:\temp\2016-01-04_025432.png
	Size       : 655948 Bytes
	Algorithm  : SHA-256
	Checksum   : 47686ca43e038a35976d9fd08e3ab8f2a28a3a57074eb8674531cc0efccb51db


### Check against a checksum-fingerprint

	C:\checksum> checksum.py "C:\temp\2016-01-04_025432.png" -f 47686ca43e038a35976d9fd08e3ab8f2a28a3a57074eb8674531cc0efccb51db

	File       : C:\temp\2016-01-04_025432.png
	Size       : 655948 Bytes
	Algorithm  : SHA-256
	Checksum   : 47686ca43e038a35976d9fd08e3ab8f2a28a3a57074eb8674531cc0efccb51db
	Fingerprint: 47686ca43e038a35976d9fd08e3ab8f2a28a3a57074eb8674531cc0efccb51db

	             √ PASS - Checksum & Fingerprint match.

