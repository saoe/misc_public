# Simple Tkinter GUI frontend for the Python checksum script.
# Copyright © 2019-2020 Sascha Offe <so@saoe.net>.
# Distributed under the terms of the MIT license <https://opensource.org/licenses/MIT>.
# ------------------------------------------------------------------------------
# "Python scripts (files with the extension .py) will be executed by python.exe
# by default. This executable opens a terminal, which stays open even if the
# program uses a GUI. If you do not want this to happen, use the extension .pyw
# which will cause the script to be executed by pythonw.exe by default (both
# executables are located in the top-level of your Python installation
# directory). This suppresses the terminal window on startup."
#                 https://docs.python.org/2/using/windows.html#executing-scripts
# ------------------------------------------------------------------------------

import sys

sys.path.append('.')
import checksum

# --- Check whether Python 3+ is used; exit if not. ------------------------------------------------
if sys.version_info[0] < 3:
    sys.exit("Sorry, you need Python 3 or higher for this script.\nYou seem to be running an older version of Python:\n" + sys.version)

from tkinter import filedialog
from tkinter import messagebox
from tkinter import *
    
master = Tk()
master.title("GUI for Checksum Calculation/Verification")

# --------------------------------------------------------------------------------------------------

def browse_file ():
    global input_file_path
    filename = filedialog.askopenfilename()
    if filename:
        input_file_path.set(filename)

def calculation ():
    try:
        # Surrounding double quotes (as it happens when one uses Copy-as-path on Windows) are stripped, so that one can also copy an input path, instead of browsing.
        hash, algo, path, size = checksum.calculate(input_file_path_entry.get().strip("\""), algorithm_option_variable.get())
    except FileNotFoundError:
        messagebox.showerror(title="Exception/Error", message="{0}".format(sys.exc_info()[1]))
    messagebox.showinfo(title="Calculation Result", message="{2} ({3} bytes)\n\n {1}\n {0}".format(hash, algo, path, size))

def verification ():
    v = verify_fingerprint.get("1.0", 'end-1c') # Using 'end-1c' instead of END to get rid of the '\n' which is by default added to any Tkinter textbox.
                                                # (That '\n' at the end of the fingerprint would also be bad for the comparion with the checksum...)
    
    v = v.strip() # Trim (or strip) leading/left and trailing/right whitespace characters from the string.

    if v is '':
        messagebox.showerror(title="Exception/Error", message="No fingerprint text for comparison provided!")
    else:
        try:
            hash, algo, path, size = checksum.calculate(input_file_path_entry.get(), algorithm_option_variable.get())

            if checksum.verify(hash, v):
                messagebox.showinfo(title="Checksum/Fingerprint Comparison", message="Match")
            else:
                messagebox.showwarning(title="Checksum/Fingerprint Comparison", message="No match")
        
        except (FileNotFoundError, Exception):
            messagebox.showerror(title="Exception/Error", message="{0}".format(sys.exc_info()[1]))

# --------------------------------------------------------------------------------------------------
    
input_file_path = StringVar(master)
input_file_path.set(None)

input_file_label = Label(master, text='Input file:')
input_file_label.grid(row=0, column=0, padx=5, pady=5, sticky=W)

input_file_path_entry = Entry(master, textvariable=input_file_path)
input_file_path_entry.grid(row=0, column=1, columnspan=2, padx=5, pady=5, sticky=EW)
input_file_path_entry.configure()

input_file_browse_button = Button(master, text='Browse...', command=browse_file)
input_file_browse_button.grid(row=0, column=3, padx=5, pady=5, sticky=EW)

# --------------------------------------------------------------------------------------------------

algorithm_label = Label(master, text='Algorithm:')
algorithm_label.grid(row=1, column=0, padx=5, pady=5, sticky=W)

algorithm_option_variable = StringVar(master)
algorithm_option_variable.set("SHA-256") # Default value.
algorithm_option = OptionMenu(master, algorithm_option_variable, "MD5", "SHA-1", "SHA-224", "SHA-256", "SHA-384", "SHA-512")
algorithm_option.grid(row=1, column=1, columnspan=2, padx=5, pady=5, sticky=EW)
algorithm_option.configure(anchor=W)

# --------------------------------------------------------------------------------------------------

calculate_button = Button(master, text='Calculate', command=calculation)
calculate_button.grid(row=1, column=3, padx=5, pady=5, sticky=EW)

# --------------------------------------------------------------------------------------------------

verify_label = Label(master, text='Fingerprint for verification:')
verify_label.grid(row=2, column=0, padx=5, pady=5, sticky=NW)

verify_fingerprint = Text(master, height=6, width=20)
verify_fingerprint.grid(row=2, column=1, padx=0, pady=5, sticky=W)

scroll = Scrollbar(master)
scroll.grid(row=2, column=2, padx=0, pady=0, sticky=NS)
scroll.config(command=verify_fingerprint.yview)

verify_fingerprint.config(yscrollcommand=scroll.set)

verify_button = Button(master, text='Verify', command=verification)
verify_button.grid(row=2, column=3, padx=5, pady=5, sticky=EW+N)


# --------------------------------------------------------------------------------------------------

master.mainloop()
