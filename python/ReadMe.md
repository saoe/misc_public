# Python section

* [Package _saoe_](Packages/saoe/)  
  My Python toolkit; a collection of helper functions for diverse areas.  

* [setup_package.py](Packages/setup_package.py)  
  Prepare and install a package directory to the user site-packages directory of the local Python installation.  
  
* [build_readme.py](Packages/build_readme.py)  
  My Python toolkit; a collection of helper functions for diverse areas.  

* [backup-svn-repos.py](backup-svn-repos.py)  
  Creating backup files of Subversion repositories.  

* [catbinf.py](catbinf.py)  
  Catenates multiple binary files; works only on Windows (Shell)!  

* [get-ip-addresses-for-hostnames.py](get-ip-addresses-for-hostnames.py)  
  Reads hostnames from an input file (one host per line); gets the IPv4 address, prints the info to the screen and writes it to a file.  

* [shuffle-files-into-playlist.py](shuffle-files-into-playlist.py)  
  Writes a shuffled playlist file from the content of a directory.  

* [sitebuilder.py](sitebuilder.py)  
  Contains several utility code for managing website content.  

* [text-to-html.py](text-to-html.py)  
  Converts a raw text file to a HTML-formatted file.  

* [increment-buildnumber.py](increment-buildnumber.py)  
  Increments and replaces the value for a key(word) on each invocation of this script.  

* [hg-helper.py](hg-helper.py)  
  Updates all local Mercurial (Hg) repositories found in subdirectories at once.  

* [git-helper.py](git-helper.py)  
  Updates all local Git repositories found in subdirectories at once.  

* [tkinter-test.py](tkinter-test.py)  
  Simple Tkinter GUI for a Python script.  

* [checksum](checksum/)  
  Script to calculate or verify a file's checksum/hash/message digest (MD5, SHA-1, SHA-256, etc.).  
  (Plus an additional script that acts as a simple Tkinter GUI.)

* [deduplicate-and-sort-lines-in-textfile.py](deduplicate-and-sort-lines-in-textfile.py)  
  Script to detect duplicate lines in a text file, sort the remaining unique lines and save the result to another text file.  