#!/usr/bin/env python

# Creating backup files of Subversion repositories.
#
# Requries Python 1.6 or higher, due to module "zipfile".
#
# $Id$
#
# 2006-06-26 Sascha Offe - Start.

import sys, os, string, time, datetime, shutil
# import zipfile

cur_path   = os.getcwd()
repo_dir   = cur_path + os.path.sep + "svn"
backup_dir = cur_path + os.path.sep + "svn-backup"

entries = os.listdir(repo_dir)   # Determine how many items are in this directory.

today = datetime.date.today().isoformat()

if not os.path.exists(backup_dir):
	print "-- Suggested back-up directory does not exist yet. Creating it..."
	os.mkdir(backup_dir)
	print "-- Created " + backup_dir

for i in entries:
	svn_cmd = "svnadmin dump " + repo_dir + os.path.sep + i + " > " + backup_dir + os.path.sep + i + "_" + today
	os.system(svn_cmd)
	print "-- Done with " + i

# TODO: Compress files, [test], then remove original back-ups.
# LATER: Auto-purge old files?

#backup_entries = os.listdir(backup_dir)   # Determine how many items are in this directory.

#for i in backup_entries:
	# ...

