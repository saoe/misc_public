# ==================================================================================================
# Increments and replaces the value for a key(word) on each invocation of this script.
#
# Usage    : script.py <input/output file>
# Parameter: File in which the key(word) is located and where it will be replaced with the updated value.
# Example  : A C header file with "#define BUILDNUMBER 123" in it.
# Note     : If the key(word) is found, but without any digits on that line, a default value of '1' will be set.
#
# ~ Sascha Offe (2016-06-01)
# ==================================================================================================

import sys
import os
import codecs
import re

KEYWORD = ur"BUILDNUMBER"

if len(sys.argv) >= 2:
	File = codecs.open(sys.argv[1], 'r+', 'utf-8')
	FileBuffer = File.readlines()
else:
	print "*** Failure: Missing filename!"
	sys.exit(1)

File.seek(0)
File.truncate()

for line in FileBuffer:	
	found_key = re.search(r"\b" + KEYWORD + r"\b", line)    # \b = Word boundaries, e.g. spaces or dots.
	
	if found_key:
		current_buildnumber = re.search(r"[0-9]+", line)
				
		if current_buildnumber:
			line = re.sub(r"[0-9]+", str(int(current_buildnumber.group()) + 1), line, count=1)
		else:
			line = line.rstrip(os.linesep) + " 1" + os.linesep
	
	File.write(line)

File.close()
