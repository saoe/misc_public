# --------------------------------------------------------------------------------------------------
# Detect duplicate lines in a text file, then sort the remaining unique lines and save the result to another text file.
#
# Copyright (c) 2019 Sascha Offe <so@saoe.net>.
# Published under the terms of the MIT license (see http://opensource.org/licenses/MIT for details).
# --------------------------------------------------------------------------------------------------

import sys
import os
import argparse
from pathlib import Path

if sys.version_info[0] < 3:
	sys.exit("You seem to be running an older version of Python:\n" + sys.version + "\n\nSorry, but you need Python 3 or higher for this script.")

parser = argparse.ArgumentParser(description     = "Detect duplicate lines in a text file, then sort the remaining unique lines and save the result to another text file.",
                                 prefix_chars    = '-',
                                 add_help        = True,   # We rely on the provided help by argparse.
                                 epilog          = "Version 1.0\nCopyright (c) 2019 Sascha Offe <so@saoe.net>.\nPublished under the terms of the MIT license (see http://opensource.org/licenses/MIT for details).",
                                 formatter_class = argparse.RawTextHelpFormatter   # To enable linebreaks (\n) in the strings.
                                )

parser.add_argument("inputfile",
                    action   = "store",
					type     = Path,
                    help     = "Name of the input text file."
                   )

parser.add_argument("-o",
                    required = False,
                    action   = "store",
                    type     = Path,
                    dest     = "outputfile",
                    help     = "Name of the output text file.\nIf non is given here, '[<Path>\]<InputFileName>-Result.<ext>' will be used."
                   )

args = parser.parse_args()

# --------------------------------------------------------------------------------------------------

path, filename      = os.path.split(args.inputfile)
filename_w_ext      = os.path.basename(args.inputfile)
basename, extension = os.path.splitext(filename_w_ext)

if (args.outputfile):
	output_file = args.outputfile
else:
	output_file = os.path.join(path, basename+"-Result"+extension)

try:
	in_file  = open(args.inputfile, "r")
	out_file = open(output_file   , "w+")
	
	lines_seen = set()
	
	for line in in_file.read().split('\n'):
		if line not in lines_seen:
			lines_seen.add(line)

	for line in sorted(lines_seen):
		out_file.write(line+'\n')

finally:
	in_file.close()
	out_file.close()