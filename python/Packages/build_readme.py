# Generate a new (or update an existing) ReadMe.md file for a module/package with the synopsis of the public functions.
#
# This script loads a module/package from a given (local) path and extracts (parts of) the function's
# docstrings to build out of this information an overview ReadMe file (formatted as a Markdown text file).
# 
# Important note:
# A package must export its functions (via its __init__.py), otherwise no functions will be detected here!
# 
# Copyright © 2022 Sascha Offe <so@saoe.net>
# SPDX-License-Identifier: MIT-0
#
# 2022-03-03 (so): First release.

import os
import sys
import argparse
import datetime
import importlib
import inspect

# --------------------------------------------------------------------------------------------------
# Check some prerequisites and handle command line options:

if sys.version_info[0] < 3:
	sys.exit("You seem to be running an older version of Python:\n" + sys.version + "\n\nSorry, but you need Python 3 or higher for this script.")

parser = argparse.ArgumentParser(description  = "Build a ReadMe.md for a module/package",
                                 prefix_chars = '-/',   # Accepting the Unix version ("-") and also the Windows prefix ("/").
                                 add_help     = True,   # We rely on the provided help by argparse.
                                )

# Note: There doesn't seem to be a way to configure argparse to accept case insensitive arguments on the CLI; so -f and -F are different!
# Note: By default, argparse accepts abbrevations, if they are unambiguously identifiable: -version == -ver == -v.

parser.add_argument("-p", "--path", "/p", "/path",
                    required = True,
                    action   = "store",
                    help     = "The basepath to the module/package for which the ReadMe.md should be generated.",
                   )

parser.add_argument("-o", "--out", "/o", "/out",
                    required = False,
                    action   = "store",
                    help     = "The basepath to where the generated ReadMe.md should be saved; if ommited, it will be saved in the basepath of the module/package.",
                   )

args = parser.parse_args()
module_input_path = args.path

if not os.path.exists(module_input_path) and os.path.isdir(module_input_path):
    sys.exit("Error:" + module_input_path + " can't be found or is not a directory!")

# --------------------------------------------------------------------------------------------------
# Find and import the specified module for docstring-extraction:

module_basepath = os.path.realpath(os.path.normpath(module_input_path))
module_name = os.path.basename(module_basepath)

# Append its path (for this session) to sys.path and import it manually:
sys.path.append(module_basepath)
mod = importlib.import_module(module_name)

if (inspect.ismodule(mod)):
    print("Found module '" + mod.__name__ + "'")
else:
    sys.exit("Error: '" + mod.__name__ + "' is not a module or package!")

# --------------------------------------------------------------------------------------------------
# Get all functions of the module and extract the first line of each function's docstring; then
# construct a Markdown table out of it:

all_functions = inspect.getmembers(mod, inspect.isfunction)
function_list = []

for function_name, function_address in all_functions:
    function_doc_synopsis = inspect.getdoc(getattr(mod, function_name)).split("\n")[0]
    function_list.append((function_name, function_doc_synopsis))
    
function_table = """Name | Synopsis
----|----""" # Don't put a linebreak here: That would break the Markdown table composition!

for f in function_list:
    function_table += "\n{func} | {synopsis}".format(func=f[0], synopsis=f[1])

# --------------------------------------------------------------------------------------------------
# Compose the actual text for the new ReadMe from the preconfigured text blocks and then update or
# create and then save the file:

NewReadMeContent = """# Python package _{module_name}_

~ This file was generated on {utc_timestamp} by {generator_script} ~"

## Functions 

{function_text}
""".format(module_name = module_name, 
           function_text = function_table,
           utc_timestamp = datetime.datetime.now(datetime.timezone.utc).strftime("%Y-%m-%d %H:%M:%S %z (%Z)"),
           generator_script = os.path.basename(__file__))

if (args.out == None):
    output_path = module_basepath
else:
    output_path = args.out

with open(os.path.join(output_path, 'ReadMe.md'), 'w+') as output_file:
    output_file.write(NewReadMeContent)
    print("Saved ReadMe.md to {out}".format(out=os.path.realpath(os.path.normpath(output_file.name))))
