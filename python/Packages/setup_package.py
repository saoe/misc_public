# Prepare and install a package directory to the user site-packages directory of the local Python installation.
# 
# The user site-packages directory is on Windows normally %APPDATA%\Python\PythonXY\site-packages (where XY is the Python version).
#
# Copyright © 2022 Sascha Offe <so@saoe.net>
# SPDX-License-Identifier: MIT
#
# 2022-02-19 (so): First version is ready.

import sys
import os.path
import argparse
import site
import shutil
import winreg # Windows Registry

if sys.version_info[0] < 3:
	sys.exit("You seem to be running an older version of Python:\n" + sys.version + "\n\nSorry, but you need Python 3 or higher for this script.")

# -- Handle command line options -------------------------------------------------------------------
# Note: There doesn't seem to be a way to configure argparse to accept case insensitive arguments on the CLI; so -f and -F are different!
# Note: By default, argparse accepts abbrevations, if they are unambiguously identifiable: -version == -ver == -v.

parser = argparse.ArgumentParser(description  = "Install a package directory to the user site-packages directory of the local Python installation.",
                                 prefix_chars = '-/',   # Accepting the Unix version ("-") and also the Windows prefix ("/").
                                 add_help     = True,   # We rely on the provided help by argparse.
                                 epilog       = "Copyright (c) 2022 Sascha Offe <so@saoe.net>. Published under the terms of the MIT license (see http://opensource.org/licenses/ for details)."
                                )

parser.add_argument("-version", "/version", action="version", version="Version 1.0")

parser.add_argument("-i", "/i", "--input-directory",
                    required = True,
                    action   = "store",
                    dest     = "input_dir",
                    help     = "The directory where the script should start looking for the package files."
                   )

parser.add_argument("-documentation", "/documentation",
                    required = False,
                    action   = "store_true",
                    help     = "Set this flag if documentation files should also be generated."
                   )

parser.add_argument("-clear", "/clear",
                    required = False,
                    action   = "store_true",
                    help     = "Clear (i.e. remove) the destination directory first (if it exists), to get rid of old content."
                   )

args = parser.parse_args()

# --------------------------------------------------------------------------------------------------
# Helper function: Get full path to doxygen.exe
def findDoxygen():
    try:
        # Method on how to find the executable adapted from CMake's FindDoxygen macro:
        with winreg.ConnectRegistry(None, winreg.HKEY_LOCAL_MACHINE) as LocalHKLM:
            with winreg.OpenKey(LocalHKLM, r"SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\doxygen_is1") as DoxygenRegKey:
                DoxygenBasepath = winreg.QueryValueEx(DoxygenRegKey, "Inno Setup: App Path")[0]
                DoxygenVersion  = winreg.QueryValueEx(DoxygenRegKey, "DisplayVersion")[0]
                DoxygenExe      = os.path.join(DoxygenBasepath, 'bin', 'doxygen.exe')
                
                if (os.path.exists(DoxygenExe)):
                    #print("Doxygen {v} was found at {l}".format(l=DoxygenExe, v=DoxygenVersion))
                    return DoxygenExe
                else:
                    #print("Error: Doxygen executable was not found!")
                    return None

    except FileNotFoundError:
        print("Error: Registry entry for Doxygen was not found!")

# --------------------------------------------------------------------------------------------------
# The main entry point for this script.

if __name__ == '__main__':
    # Get the path to the base directory for the user site-packages:
    install_destination_path = os.path.normpath(site.getusersitepackages())
    
    if (os.path.isdir(args.input_dir)):
        package_dir_name = os.path.split(args.input_dir)[-1]
        destination_dir  = os.path.join(install_destination_path, package_dir_name)
        
        # Clear old files first, if requested (note: rmtree() deletes the whole folder, not just its content).
        if (args.clear):
            if (os.path.exists(destination_dir)):
                print("Clear option detected: Removing \'{d}\'".format(d=destination_dir))
                shutil.rmtree(destination_dir)
        
        # Copy the package files (except those on the ignore list).
        result_dir = shutil.copytree(args.input_dir, destination_dir, dirs_exist_ok = True, ignore = shutil.ignore_patterns('Doxyfile*'))
        
        # Create the documentation last, because the destination directory must be created before this step.
        if (args.documentation):
            print("Documentation will be generated...")
            
            # Modify/Prepare Doxyfile
            #
            # Ugly kludge: Doxygen accepts no command line arguments, only a Doxfile.
            # To read the Doxyfile template into a temporary file (where the output path could be replaced), didn't wore due to
            # issues with closing/removing temporary files on Windows with Python (known issues).
            # So, workaround: Read template file, modify content, write it back, run doxygen, write saved template data back into template file.
            with open(os.path.join('.', args.input_dir, 'Doxyfile'), mode='r+') as Doxyfile:
                DoxygenExe = findDoxygen()
                
                backup_data = Doxyfile.read()
                new_data = backup_data.replace("OUTPUT_DIRECTORY =", str("OUTPUT_DIRECTORY = " + '"' + destination_dir + '"'))
                
                Doxyfile.seek(0)
                Doxyfile.write(new_data)
                Doxyfile.truncate()
                
                if (DoxygenExe):
                    os.system('"' + DoxygenExe + '" ' + Doxyfile.name)
                else:
                    print("Doxygen not OK")
                
                Doxyfile.seek(0)
                Doxyfile.write(backup_data)
                Doxyfile.truncate()
        
        print("Package installed to \'{r}\'".format(r=result_dir))
    else:
        print("Error: Is a file")
