# Copyright © 2015, 2019, 2022 Sascha Offe <so@saoe.net>
# SPDX-License-Identifier: MIT

import sys

def timestamp (utc=False):
    """Print the current date and time as a formatted string.
    
    By default as the local time; if 'utc' is true, the as UTC time.
    
    > timestamp() or timestamp(False) or saoe.timestamp(0) or saoe.timestamp(utc=False)
    Local Time
    
    > timestamp(True) or saoe.timestamp(1) or saoe.timestamp(utc=True)
    UTC time
    """
    import datetime

    UTC_Timestamp = datetime.datetime.now(datetime.timezone.utc)

    if utc:
        print(UTC_Timestamp.strftime("%Y-%m-%d %H:%M:%S %z (%Z)"))
    else:
        print(UTC_Timestamp.astimezone().strftime("%Y-%m-%d %H:%M:%S %z (%Z)"))

if __name__ == '__main__':
    # Call it like this from the CLI: timestamp.py <anything-will-be-resolved-to-true>
    if len(sys.argv) < 2:
        timestamp()
    else:
        timestamp(bool(sys.argv[1]))