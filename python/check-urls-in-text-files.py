"""
Check the status of URLs that are found in text files

Copyright © 2022 Sascha Offe <so@saoe.net>  
SPDX-License-Identifier: MIT

2022-02-16 (so): It's far from perfect, but for a first draft, it worked alright:
                 Found several dead links in the Markdown source files for my Hugo website.
"""

import os
import sys
import re
import argparse
import fnmatch
import urllib.request
from pathlib import Path

if sys.version_info[0] < 3:
	sys.exit("You seem to be running an older version of Python:\n" + sys.version + "\n\nSorry, but you need Python 3 or higher for this script.")

# Handle command line options. ---------------------------------------------------------------------
parser = argparse.ArgumentParser(description  = "Check the status of URLs that are found in text files",
                                 prefix_chars = '-/',   # Accepting the Unix version ("-") and also the Windows prefix ("/").
                                 add_help     = True,   # We rely on the provided help by argparse.
                                 epilog       = "Copyright (c) 2022 Sascha Offe <so@saoe.net>. Published under the terms of the MIT license (see http://opensource.org/licenses/MIT for details)."
                                )

parser.add_argument("-v", "/v", "--version", action="version", version="Version 1.0")

parser.add_argument("-i", "/i", "--input-directory",
                    required = True,
                    action   = "store",
                    dest     = "input_dir",
                    help     = "The directory where the script should start looking for files."
                   )

parser.add_argument("-f", "/f", "--filename-pattern",
                    required = False,
                    action   = "store",
                    dest     = "filename_pattern",
                    default  = "*.*",
                    help     = "The matching pattern for the filenames.\n Default pattern is \"%(default)s\""
                   )

args = parser.parse_args()

input_dir        = args.input_dir
filename_pattern = args.filename_pattern
# ------------------------------------------------------------------- Handle command line options --

problematic_urls = []
    # Will contain all problematic (i.e. all non-HTTPStatusCode200/reachable) addresses at the end of the run.

# --------------------------------------------------------------------------------------------------
def checkURL (url, in_file):
    # HTTP response status codes <https://developer.mozilla.org/en-US/docs/Web/HTTP/Status>
    #   Informational responses (100–199)
    #   Successful responses    (200–299)
    #   Redirection messages    (300–399)
    #   Client error responses  (400–499)
    #   Server error responses  (500–599)
    
    # [FIXME] Some sites respond with errors, or weird status codes, like '403 Forbidden',
    # although it's reachable via a browser without any problems; maybe I need to supply some
    # custom headers in the request:
    # - https://stackoverflow.com/questions/16778435/python-check-if-website-exists
    # - https://stackoverflow.com/a/26275998/17589673
   
    try:
        req = urllib.request.urlopen(url, timeout=5)
        print("  Status: ["+ str(req.status) +"] for: " + url)

    except urllib.error.HTTPError as e:
        details = (url, in_file, e.code, e.reason)
        problematic_urls.append(details)
        print("  Error ["+ str(e.code) +"] for: " + url)

    except urllib.error.URLError as e:
        details = (url, in_file, "", e.reason)
        problematic_urls.append(details)

    except BaseException as e:
        details = (url, in_file, type(e), e)
        problematic_urls.append(details)

# --------------------------------------------------------------------------------------------------
def extractURLs (string):
    url_regex = r"""(?i)(\bhttps?:\/\/[a-zA-Z0-9/+=\-?_.#%;]*)"""
        # Need to be more specific: \S* also matched with all the markup stuff and other text,
        # which then cannot be trimmed:        ...)**
    
    urls = re.findall(url_regex, string)
    stripped_urls = []

    for url in urls:
        stripped_urls.append(url.rstrip(')]}>"'))
    
    return stripped_urls

# --------------------------------------------------------------------------------------------------
def findFilesWithURLs (dirpath, filenamefilter):
    if os.path.exists(dirpath):
        for dirpath, dirs, files in os.walk(dirpath):
            for file in files:
                if fnmatch.fnmatch(file, filenamefilter):
                    with open(os.path.join(dirpath, file), mode='rt', errors='strict', encoding="utf-8") as f:
                        try:
                            urls = extractURLs(f.read())
                            print("File: " + os.path.normpath(os.path.join(dirpath, file)))
                            if urls:
                                for url in urls:
                                    checkURL(url, os.path.normpath(os.path.join(dirpath, file)))
                                print("")
                            else:
                                print("No URLs found")
                                print("")
                        except Exception as ex:
                            print("[!] Exception for file " + os.path.normpath(os.path.join(dirpath, file)))
                            print(type(ex))    # the exception instance
                            #print(ex.args)     # arguments stored in .args
                            print(ex)          # __str__ allows args to be printed directly,
                            pass 
    else:
        print("Error: " + dirpath + "is not a valid path to a directory!")
        return

# --------------------------------------------------------------------------------------------------
# The main entry point for this script.
if __name__ == '__main__':
    findFilesWithURLs(input_dir, filename_pattern)
    
    print("==== Show all problematic URLs ================================================")
    
    for i in problematic_urls:
        print("Error\n  File.: {file}\n  URL..: {url}\n  Error: {errorcode} {errortext}\n".format(url=i[0], file=i[1], errorcode=i[2], errortext=i[3]))
    