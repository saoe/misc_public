# Python script "sitebuilder.py"
# Contains several utility code for managing website content.

import os, string, sys, datetime, time


# ------------------------------------------------------------------------------
# Generates a RSS feed in XML format from an inputfile.
# Originally written by Sascha Offe, on 2006-03-17.
#
# Format of input:
#    Time (format: yyyy-mm-dd)
#    Short one line summary
#    Longer descriptive message text (else use "-")
#    URL to any additional information  (else use "-")
#    < blankline | newline >
# 
# Note: The inputfile must end (= after final entry) with exactly one "newline"!
# 
# The parameter 'data' is a dictionary with the following keys:
# 
# feed_title       (string)
# feed_description (string)
# site_url         (string, e.g. "http://www.example.com/")
# email_address    (string, e.g. "mail@example.com")
# max_items        (int   , use maximal this many items for the newsfeed.
# 
# Create: data = {'feed_title': "Text", ... }

def createRSSfile (inputfile, outputfile, data):

	inputdata = open(inputfile, 'r')

	msg_short = []
	msg_body = []
	times = []
	links = []

	while 1:
		times.append(inputdata.readline().rstrip('\n'))
		msg_short.append(inputdata.readline().rstrip('\n'))
		msg_body.append(inputdata.readline().rstrip('\n'))
		links.append(inputdata.readline().rstrip('\n'))
		if not inputdata.readline():
			break

	inputdata.close()

	# Compose feed.
	rssfeed = open(outputfile, 'w')

	rssfeed.write("<?xml version=\"1.0\" encoding=\"ISO-8859-15\" ?>\n")
	rssfeed.write("<rss version=\"2.0\">\n")
	rssfeed.write("<channel>\n")
	rssfeed.write("\t<title>" + data['feed_title'] + "</title>\n")
	rssfeed.write("\t<link>" + data['site_url'] + "</link>\n")
	rssfeed.write("\t<description>" + data['feed_description'] + "</description>\n")
	rssfeed.write("\t<pubDate>" + time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime()) + "</pubDate>\n")
	rssfeed.write("\t<lastBuildDate>" + time.strftime("%a, %d %b %Y %H:%M:%S +0000", time.gmtime()) + "</lastBuildDate>\n")
	rssfeed.write("\t<webMaster>" + data['email_address'] + "</webMaster>\n")

	j = 0
	i = len(msg_short) - 1

	if data['max_items'] > len(msg_short):
		j = len(msg_short)
	else:
		j = data['max_items']

	while j is not 0:
		rssfeed.write("\t<item>\n")
		rssfeed.write("\t\t<title>" + msg_short[i] + "</title>\n")
		rssfeed.write("\t\t<pubDate>" + time.strftime("%a, %d %b %Y 00:00:00 +0000", time.strptime(times[i], "%Y-%m-%d")) + "</pubDate>\n")
		if msg_body[i] is not "-":
			rssfeed.write("\t\t<description>" + msg_body[i] + "</description>\n")
		if links[i] is not "-":
			rssfeed.write("\t\t<link>" + links[i] + "</link>\n")
		rssfeed.write("\t</item>\n")
		i = i - 1
		j = j - 1

	rssfeed.write("</channel>\n")
	rssfeed.write("</rss>\n")

	rssfeed.close()


# ------------------------------------------------------------------------------
# Composes news from inputfile and returns a HTML-formatted string.
# Originally written by Sascha Offe, on 2006-03-17.

import os, string, sys, datetime, time

def NewsAsHTMLCode (inputfile, max_items):
	"""	Composes news from inputfile and returns a HTML-formatted string.

	Arguments:
	  inputfile - source of the news, plain text.
	  max_items - Use maximal this many entries.

	Format for inputfile:
	  Time (format: yyyy-mm-dd)
	  Short one line summary
	  Longer descriptive message text (else use "-")
	  URL to any additional information  (else use "-")
	  < blankline | newline >

	Note: Inputfile must end (= after final entry) with exactly one "newline"!
	"""

	# Cache data from the source file.
	inputdata  = open(inputfile, 'r')

	msg_short = []
	msg_body = []
	times = []
	links = []

	while 1:
		times.append(inputdata.readline().rstrip('\n'))
		msg_short.append(inputdata.readline().rstrip('\n'))
		msg_body.append(inputdata.readline().rstrip('\n'))
		links.append(inputdata.readline().rstrip('\n'))
		if not inputdata.readline():
			break

	# Build the code.
	buf = ""
	j = 0
	i = len(msg_short) - 1

	if max_items > len(msg_short):
		j = len(msg_short)
	else:
		j = max_items

	while j is not 0:
		buf += "\n<p class=\"news_entry\">\n<span class=\"news_time\">" + times[i] + ":</span>\n"

		buf += "<span class=\"news_title\">" + msg_short[i] + "</span>"

		if msg_body[i] is not "-":
			buf += "<br>\n<span class=\"news_text\">" + msg_body[i] + "</span>\n"

		if links[i] is not "-":
			buf += "(<span class=\"news_link\"><a href=\"" + links[i] + "\">more info&hellip;</a></span>)\n"

		buf += "</p>\n"
		i = i - 1
		j = j - 1

	return buf


# ------------------------------------------------------------------------------
# Replaces string between a keyword-guarded section with a new string.
# Originally written by Sascha Offe, on 2006-03-20.

def updatePage (inputfile, outputfile, opening_tag, closing_tag, new_text):
	"""Replaces the text between keyword-guarded sections with another text and writes the re-composed string to an outputfile.

	Example of use: Update the timestamp in a document:
	  <html>
	  ...
	  <p>Last modified: <!--DATE-->2006-03-28<!--/DATE--></p>
	  ...
	  </html>
	"""

	inbuffer  = ""
	outbuffer = ""

	# Read file's content into memory buffer.
	ifile = file(inputfile, 'r')
	inbuffer += ifile.read()
	ifile.close()


	# Save the starting and ending indices of the original text as a tuple: s0, e1, s1, e1, ...
	# The new/replacement text will be inserted between these tuples.

	indices = [0]  # Must have an initial first entry, set to the beginning.
	start = 0

	# Find all occurences of the keyword-guarded sections.
	while start >= 0:
		start = inbuffer.find(opening_tag, start)
		if start >= 0:
			start += len(opening_tag)               # skip the id-string itself.
			end = inbuffer.find(closing_tag, start)
			indices.append(start)
			indices.append(end)
			start = end + len(closing_tag)

	indices.append(len(inbuffer)) # Final entry is the end of the original text.


	# Copy the original string to a temporary one and along the way replace/insert
	# the keyword-guarded sections with the new string.

	loop_counter = len(indices) / 2
	c = 0
	i = 0

	while c < loop_counter:
		outbuffer += inbuffer[indices[i]:indices[i+1]]
		if c is not loop_counter - 1:
			outbuffer += new_text
		c += 1
		i += 2

	# Write the newly composed string to file.
	ofile = file(outputfile, 'w+')
	ofile.write(outbuffer)
	ofile.close()


# ==============================================================================
# The main segment.

if len(sys.argv) > 1:
	if len(sys.argv) > 2:
		print "\nToo many arguments; max. one optional allowed (news-inputfile.txt)\n"
		sys.exit(0)
	else:
		datafile = sys.argv[1]
else:
	datafile = "news.txt"


newspage = "index.html"

rss_data = {
'feed_title'      : "Title goes here",
'feed_description': "Foo!",
'site_url'        : "http://www.example.com/",
'email_address'   : "mail@example.com",
'max_items'       : 10}

createRSSfile("news.txt", "rss.xml", rss_data)

updatePage (newspage, newspage, "<!--NEWS-->", "<!--/NEWS-->", NewsAsHTMLCode(datafile, 10))