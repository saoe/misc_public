# Writes a shuffled playlist file from the content of a directory.
# Copyright (c) 2014, 2018 Sascha Offe <so@saoe.net>.
# Published under the terms of the MIT license (see http://opensource.org/licenses/MIT for details).

import sys, os, argparse, socket
from random import shuffle

# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - Handle command line options.
parser = argparse.ArgumentParser(description  = "Writes a shuffled playlist file from the content of a directory. ",
                                 prefix_chars = '-/',   # Accepting the Unix version ("-") and also the Windows prefix ("/").
                                 add_help     = True,   # We rely on the provided help by argparse.
                                 epilog       = "Copyright (c) 2014, 2018 Sascha Offe <so@saoe.net>. Published under the terms of the MIT license (see http://opensource.org/licenses/MIT for details)."
                                )

parser.add_argument("-v", "/v", "--version", action="version", version="Version 2.0")

parser.add_argument("-i", "/i", "--input",
                    required = False,
                    action   = "store",
                    dest     = "source_dir",
                    default  = os.path.dirname(os.path.realpath(__file__)),
                    help     = "The source directory of the files for the playlist. Default value is the same folder where the script is (in this case: %(default)s)."
                   )

parser.add_argument("-o", "/o", "--output",
                    required = False,
                    action   = "store",
                    dest     = "playlist_filename",
                    default  = "_Playlist.m3u",
                    help     = "The name (or path) of the playlist.\n Default value is \"%(default)s\" in the same folder where the script is."
                   )

parser.add_argument("-u", "/u", "--file_URI_scheme",
                    required = False,
                    action   = "store_true",
                    dest     = "file_URI_scheme",
                    default  = True,
                    help     = "Use 'file://' URI paths instead of just the filenames."
                   )

args = parser.parse_args()

input_dir           = args.source_dir
outfilename         = args.playlist_filename
use_file_URI_scheme = args.file_URI_scheme
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - (end) Handle command line options.

output_file = open(os.path.join(os.path.dirname(os.path.realpath(__file__)), outfilename), "w", encoding="utf-8") # File will be created or overwritten!

filenamelist = []

for full_path, dirs, files in os.walk(input_dir):
	for f in files:
		if (use_file_URI_scheme == True):
			(drive, path_only) = os.path.splitdrive(full_path)
			f = f.replace("#", "%23")
			y = "file://" + socket.gethostname() + path_only.replace("\\","/") + '/' + f
		else:
			y = f
		
		filenamelist.append(y)

shuffle(filenamelist)

# Write the shuffled list of files into the playlist file, but don't include this script or the playlist itself.
for f in filenamelist:
	if (f != os.path.basename(__file__)) and (f != output_file.name):
		output_file.write(f + '\n')

output_file.close()

print("[OK] Saved a shuffled playlist of the directory " + input_dir + " to file " + output_file.name)
