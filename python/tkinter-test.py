# Simple Tkinter GUI for a Python script (2016-07).
# ------------------------------------------------------------------------------
# "Python scripts (files with the extension .py) will be executed by python.exe
# by default. This executable opens a terminal, which stays open even if the
# program uses a GUI. If you do not want this to happen, use the extension .pyw
# which will cause the script to be executed by pythonw.exe by default (both
# executables are located in the top-level of your Python installation
# directory). This suppresses the terminal window on startup."
#                 https://docs.python.org/2/using/windows.html#executing-scripts
# ------------------------------------------------------------------------------
# To wrap this in a single executable file for the Microsoft Windows platform,
# use PyInstaller, as described on my blog: http://www.saoe.net/blog/1394/
# Summary: C:\MyScript> "C:\Program Files (x86)\Python\2.7\python.exe"
#          "C:\Program Files (x86)\Python\2.7\Scripts\pyinstaller-script.py"
#          script.py --onefile --windowed
# ------------------------------------------------------------------------------

import sys

if sys.version_info[0] < 3:
        from Tkinter import * # for Python 2.
else:
        from tkinter import * # for Python 3.

def addValues():
	label_value.config(text=float(entry_1.get()) + float(entry_2.get()))

root_widget = Tk()
root_widget.title("Addition")

label_1 = Label(master=root_widget, text='Value 1:')
label_1.grid(row=0, column=0, padx=5, pady=5, sticky=W)

entry_1 = Entry(master=root_widget)
entry_1.grid(row=0, column=1, padx=5, pady=5)

label_2 = Label(master=root_widget, text='Value 2:')
label_2.grid(row=1, column=0, padx=5, pady=5, sticky=W)

entry_2 = Entry(master=root_widget)
entry_2.grid(row=1, column=1, padx=5, pady=5)

button_action = Button(master=root_widget, text='Add both values', command=addValues)
button_action.grid(row=2, columnspan=2, padx=5, pady=5)

label_result = Label(master=root_widget, text='Sum:')
label_result.grid(row=3, column=0, padx=5, pady=5, sticky=W	)

label_value = Label(master=root_widget, text='')
label_value.grid(row=3, column=1, padx=5, pady=5)

root_widget.mainloop()
