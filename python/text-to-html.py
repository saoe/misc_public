# This Python file uses the following encoding: utf-8

# Converts a raw text file to a HTML-formatted file.
#
# Copyright 2006 Sascha Offe.
#
# $Id$
#
# Usage: scriptname file1.txt [...] [-program_option]
# Returns (in a sub-directory) the corresponding HTML file(s).
# Note: The very first line of the text file determines the <title>.
#
# 2006-06-28 (so) - Start.

import os, string, sys

# ------------------------------------------------------------------------------

def convertGlyphs (line):
	
	# Replace special characters.
	line = line.replace("&", "&amp;") # Must come first!
	line = line.replace("<", "&lt;")
	line = line.replace(">", "&gt;")
	
	# Replace german Umlaute.
	line = line.replace("�", "&auml;")
	line = line.replace("�", "&Auml;")
	line = line.replace("�", "&ouml;")
	line = line.replace("�", "&Ouml;")
	line = line.replace("�", "&uuml;")
	line = line.replace("�", "&Uuml;")
	line = line.replace("�", "&szlig;")
	
	return line


# ------------------------------------------------------------------------------
# URLs like http://... to clickable HTML anchor tags like <a href="..."></a>.
#
# Caveat: Does not recoginze mailto:... or news:... (no double-slashes) or svn+ssh:// (Due 'Plus' symbol).

def convertLinks (line):
	
	new_line = ""
	
	start = 0
	end = len(line)
	
	while start < end:
	
		url_start = line.find("://", start)
		
		# Match.
		if url_start >= 0:
		
			while (not line[url_start-1].isspace()) and (line[url_start-1].isalnum()):
				url_start -= 1
			
			new_line += line[start:url_start]
			
			url_end = url_start
			
			# Getting rid of unwanted characters.
			while not line[url_end].isspace():
				url_end += 1
			
			while not (line[url_end-1].isalnum() or line[url_end-1] == "/"):
				if line[url_end-4:url_end] == "&gt;":   # Handling <...>-enclosed URLs.
					url_end -= 4
					break
				else:
					url_end -= 1
				
			URL = line[url_start:url_end]
			hyperlink = "<a href=\"" + URL + "\">" + URL + "</a>"

			new_line += hyperlink
			start = url_end-1
		else:
			new_line += line[start]
		
		start += 1

	return new_line

# ------------------------------------------------------------------------------

def convertEMailLinks (line):
	# --- Convert MailTo-links to clickable hyperlinks. ---
	
	new_line = ""
	
	start = 0
	end = len(line)
	
	while start < end:
	
		url_start = line.find("mailto:", start)
		
		if url_start >= 0:             # Found URL.
			
			new_line += line[start:url_start]
			
			url_end = url_start + 9 # N.B.: Length of "mailto:"!
			
			# Getting rid of unwanted trailing characters.
			while not line[url_end].isspace():
				url_end += 1
			while not line[url_end-1].isalnum():
				url_end -= 1
				
			URL = line[url_start:url_end]
			hyperlink = "<a href=\"" + URL + "\">" + URL[7:] + "</a>"  # Make the "mailto:" invisible.

			new_line += hyperlink
			start = url_end-1
		else:
			new_line += line[start]
		
		start += 1

	return new_line

# ------------------------------------------------------------------------------
# Paragraph tags <p>...</p>.

def convertParagraphs (line):
	
	new_line = ""
	
	i = 0
	end = len(line)
	
	new_line += "<h1>"
	while not line[i] == "\n":
		new_line += line[i]
		i += 1
	i += 1
	new_line += "</h1>\n\n"	

	toggle = False
	begin_p = False
	linebreaks = True
	
	while i < end:
		
		if line[i-1] == '\n' and line[i] == '\n':
			if toggle:
				new_line = new_line.rstrip('\n') # We don't want a linebreak before the </p> tag.
				new_line += "</p>\n\n"
				toggle = False
				continue
			else:
				new_line += "<p>"
				toggle = True
				begin_p = True
		
		# Within paragraph: Respect linebreaks.
		if prog_opt_break and line[i-2] != '\n' and line[i-1] == '\n' and line[i] != '\n':
			new_line = new_line.rstrip('\n')
			new_line += "<br/>\n"
		
		new_line += line[i]
		
		if begin_p:   # We don't want a linebreak after the <p> tag.
			new_line = new_line.rstrip('\n')
			begin_p = False
			
		i += 1
	
	new_line = new_line.rstrip('\n')
	new_line += "</p>\n"
		
	return new_line


# ------------------------------------------------------------------------------	
# Convert each file (that is given as a command line argument).

output_directory = "HTML"

prog_opt_flow  = False # Using flowing text and <p> tags (instead of pre-formatted text)?
prog_opt_break = False # Using <p> tags (as desc. above), plus adding <br> tags to linebreaks within paragraphs (!).

filelist = []

if len(sys.argv) > 1:
 	for x in sys.argv[1:]:
 		
 		# Program options.
 		if x[0] == "-":
 			if x[1:] == "p":
 				prog_opt_flow = True
 			elif x[1:] == "b":
 				prog_opt_flow = True
 				prog_opt_break = True
 			elif x[1:] == "h":
 				print "  Available options (Default is to use <pre> tags only):\n    -p: Use <p> tags instead.\n    -b: Use <p> tags, and respect linebreaks within paragraph.\n"
 			else:
 				print "  Unknown option:", x
 				print "  Type -h for help!"
 		
 		else:
			filelist.append(x)
else:
	print "Error: Input file missing."
	sys.exit(0)

filelist.sort() # Alphabetically by name.

for n in filelist:
	input_file  = open(n, 'r')
	
	basename = os.path.splitext(n) # Getting rid of the filename-extension.
	
	if not os.path.exists(output_directory):
		os.mkdir(output_directory)
	
	# The header section.
	output_buffer = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\n"
	output_buffer += "<html>\n"
	output_buffer += "<head>\n"
	
	content_buffer = page_title = input_file.readline()
	page_title = page_title.rstrip('\n')
	content_buffer += input_file.read()
	input_file.close()
	
	output_buffer += "<title>" + page_title + "</title>\n"
	
	output_buffer += "<style type=\"text/css\">\n"
	output_buffer += "<!--\n"
	if prog_opt_flow:
		output_buffer += "h1 {font-size:1.5em;}\n"
	#output_buffer += "a:link {color:#0000c0; text-decoration:none;}\n"
	#output_buffer += "a:visited {color:#a000c0; text-decoration:none;}\n"
	output_buffer += "-->\n"
	output_buffer += "</style>\n"
	
	output_buffer += "</head>\n"
	output_buffer += "<body>\n\n"
	
	if not prog_opt_flow:
		output_buffer += "<pre>\n"

	# Process and insert the actual content.
	content_buffer = convertGlyphs(content_buffer)
	content_buffer = convertLinks(content_buffer)
	content_buffer = convertEMailLinks(content_buffer)
	
	if prog_opt_flow:
		content_buffer = convertParagraphs(content_buffer)
	
	output_buffer += content_buffer
	
	if not prog_opt_flow:
		output_buffer += "</pre>\n\n"

	# The footer section.	
	#output_buffer += "\n<hr>\n\n"
	#output_buffer += "<div style=\"text-align:center; font-size:0.75em;\"><a href=\""+toc_file_name+"\">Back to the overview</a></div>\n\n"
	output_buffer += "</body>\n"
	output_buffer += "</html>\n"
	
	output_file = open(output_directory+"/"+basename[0]+".html", 'w')
	output_file.write(output_buffer)
	output_file.close()
