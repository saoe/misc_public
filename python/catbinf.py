#!/usr/bin/env python

# Catenates multiple binary files
# Works only on Windows (Shell)!
# -------------------------------
# 2006-01-22 18:56 so Total rewrite.

import sys, os, string, fileinput, time

full_path   = os.getcwd()                            # Determine in which directory we are currently (full path, e.g. C:\foo\bar).
current_dir = string.rsplit(full_path, os.sep, 1)[1] # We're only interested in the name of the directory in which we are.
items = os.listdir(full_path)                        # Determine how many entries/items are in this directory.
extension = ""

movie_files = []
movie_name = current_dir

for i in range(len(items)):	
	if os.path.isfile(items[i]):
		ext = os.path.splitext(full_path + os.sep + items[i])
		if ext[1] == ".mpg" or ".mpeg":                  # <-- MPG or MPEG
			extension = ext[1]
			movie_files.append(items[i])

movie_files.sort()


# ---- Building the command line ----

win32_shell_line = "copy "

for j in range(len(movie_files)):	
	win32_shell_line += movie_files[j] + " /b "
	
	if j != len(movie_files)-1:
		win32_shell_line += "+ "

win32_shell_line += movie_name + extension + " /b"


# ---- Executing the command line ----

#print win32_shell_line
os.system(win32_shell_line)