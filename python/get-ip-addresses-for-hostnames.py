# Reads hostnames from an input file (one host per line!).
# Gets the IPv4 address of each host.
# Prints the information to the screen and also writes it to a file.
# -- Sascha Offe, 2014-05-19/20

import socket
import sys

if len(sys.argv) is not 2:
	print ("Error: No input file.")
	sys.exit(0)
else:
	outfile = open("result.txt", 'w')

	with open(sys.argv[1]) as infile:
		lines = infile.readlines()
			for line in lines:
				line = line.strip()
					try:
						IP = socket.gethostbyname(line)
						print (line + "," + IP)
						outfile.write(line + "," + IP + "\n")
					except (socket.gaierror):
						print (line + ",ERROR")
						outfile.write(line + ",ERROR" + "\n")

	print ("\nAlso saved to " + outfile.name)

	outfile.close()
