# ==================================================================================================
# Run Git commands over all local Git repositories that are found in subdirectories.
# See Help text below for more information.
#
# 2016-06-12 Sascha Offe: Created script for use with Mercurial/Hg; implemented hg_pull_and_update().
# 2019-07-05 Sascha Offe: Created copy of Mercurial-oriented script and adjusted it for use with Git.
# 2020-12-19 Sascha Offe: Added option to run a short status check over the detected repositories,
#                         plus some other minor modifications and improvements.
# 2021-02-21 Sascha Offe: Added check for Python version (since a feature of subprocess.run() requires
#                         at least v3.8; added in git_status() a check whether the local branch is
#                         ahead or behind the remote; reworked output a bit.
# 2021-12-14 Sascha Offe: Added check/comparison with the remote repository for 'status'.
#
# Copyright © 2016, 2019-2021 Sascha Offe <so@saoe.net>
# SPDX-License-Identifier: MIT-0
# ==================================================================================================

import sys
import os
import subprocess

import sys

if not (sys.version_info.major == 3 and sys.version_info.minor >= 8):
    print("This script requires Python 3.8 or higher!")
    print("You are using Python {}.{}.".format(sys.version_info.major, sys.version_info.minor))
    sys.exit(1)

os.system('')
    # Workaround for Windows 10, to enable ANSI escape codes
    # <https://stackoverflow.com/questions/12492810/python-how-can-i-make-the-ansi-escape-codes-to-work-also-in-windows/51524239#51524239>

# --------------------------------------------------------------------------------------------------
helptext = ("""Usage: """ + os.path.basename(sys.argv[0]) + """ <command>

Run Git commands over all local Git repositories that are found in subdirectories.

Expects all clone repos to be below the directory from which the script will be invoked; e.g.:

  <git-directory>/
  +-- <repo1>/
  +-- <repo2>/
  +-- ...
  +-- """ + os.path.basename(sys.argv[0]) + """

  C:\\<git-directory>> """ + os.path.basename(sys.argv[0]) + """ pull

Available commands:
  * pull   - Fetch data from remote repository and update working directory.
  * status - Check the (short) status of the local repositories.""")

# --------------------------------------------------------------------------------------------------
# In Mercurial/Hg, "pull" (changes from repo) and "update" (current working directory) were two seperate steps.
# "Git pull" is a convenience command, that combines Git's "fetch" (changes from repo) and "merge" (with current working directory)
def git_pull (dir):
    os.chdir(dir)
    
    RemoteRepository = subprocess.check_output("git remote get-url --all origin", universal_newlines=True)
    print("* Pulling data from '" + RemoteRepository.strip() + "'..." + os.linesep)
    subprocess.call("git pull --verbose", shell=True)
    print('')
    
    os.chdir(os.path.normpath(os.pardir))

# --------------------------------------------------------------------------------------------------
# Short status check/overview of a local repository.
def git_status (dir):
    os.chdir(dir)

    print("* Checking status of '" + dir + "'...")
    status_output = subprocess.run("git status --short", capture_output=True, text=True)
    if (status_output.stdout):
        print("\033[093m" + "[!] Local branch has uncommitted changes" + "\033[0m")
        print(status_output.stdout.rstrip())
    
    # Fetch changes from remote repository, for comparison (i.e. just downloading the data).
    remote_output1 = subprocess.run("git remote update", capture_output=True, text=True)
    
    remote_output2 = subprocess.run("git status --short --branch", capture_output=True, text=True)
    if ('ahead' in remote_output2.stdout):
        print("\033[093m" + "[!] Local branch is ahead remote branch" + "\033[0m")
    elif ('behind' in remote_output2.stdout):
        print("\033[093m" + "[!] Local branch is behind remote branch (maybe the changes are already fetched; try 'git pull' for the repo)" + "\033[0m")
    
    os.chdir(os.path.normpath(os.pardir))

# --------------------------------------------------------------------------------------------------
# The main body of the script.
# Walk over all top-level subdirectories and check if it's a Git repository; if yes, then execute
# the requested command.
def main (argv):
    cwd = os.getcwd()
    
    if len(argv) == 2:
        if argv[1] == "pull":
            for directory in os.listdir(cwd):
                if os.path.exists(os.path.join(directory, ".git")):
                    print("* Found a Git repository in '" + directory + "'")
                    git_pull(directory)
                    print('')
        elif argv[1] == "status":
            for directory in os.listdir(cwd):
                if os.path.exists(os.path.join(directory, ".git")):
                    git_status(directory)
                    print('')
        else:
            print(helptext)
    else:
        print(helptext)

# --------------------------------------------------------------------------------------------------
# Execute only if run as a script.
if __name__ == "__main__":
    main(sys.argv)
