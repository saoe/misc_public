<#
    .Synopsis
    Read a CVS file with HR/employee data and evaluate its hierarchy.
 
    .Description
    This script is inspired by a task I had to do for my day job:
    
        "Analyze the data from a CSV file, provided by Human Resources (HR),
        which contains essential facts of each employee (employee number, name, business unit,
        department, supervisor, etc.).
        
        From that data, generate a hierarchy ('X is the boss of A and B', etc.)
        and also associate all the matching ActiveDirectory user accounts, if possible."
    
    Reason: We needed to extract the SamAccountNames of each subordinate of Manager X, so that
    those account IDs could be fed to another tool.
    
    Since some employees have multiple AD accounts in a domain, it would often be a 1:n relation.
    Luckily, for license tracking reasons, each AD user object was supposed to be tagged with the
    correct and unique employee number in the EmplyoeeID and/or EmployeeNumber attributes.
    
    The result can be displayed on the console (in a somewhat nice format, with indentation etc.);
    or can be returned as custom PS object, for further processing.
    
    ATTENTION: While I tried to convert the code from the very specific original script
               to a more generic and undetermined version, there a still a lot of rather
               specific and hard-wired requirements and settings left, which may need to be changed;
               this is not a ready solution! The code is mostly here to give you some ideas
               (and me some reminders) for similar tasks!
    
    This example script currently expects the following format for the input CSV file:
    Column names/header row, separated by semicolons (;):
    - EmployeeNumber
    - Surname
    - GivenName
    - BusinessUnit
    - Department
    - Description
    - ExecutiveType
    - SupervisorEmployeeNumber

    .Parameter BusinessUnit
    Only certain values are accepted; see code for details!
   
    .Parameter Identity
    To specify a particual person (identified by its surname or its employee number),
    for which its subordinates should be gathered.
    
    (If the surname is not unique in the business unit, a warning/hint will be displayed, to use
    the employee number instead).
   
    .Parameter Bosses
    Switch, to show all "bosses" of the specified business unit (BU).
    
    The definition of what is considerd to be a boss (head of sth. and/or team lead and/or ...)
    depends on what someone wishes it to be; in this example, it is currently whether the
    "ExecutiveType" is a 'A' or whether the "Description" contains the term 'PeopleLead' (Details: see code).
   
    Note: Using this switch in combination with '-AsPSObject' is possible,
          but the outcome will be rather illegible.

    .Parameter Server
    Optional parameter that specifies the domain, from which AD user accounts will be queried.
 
    .Parameter AsPSObject
    Switch to return the result as PSCustomObjects, for further processing
    (instead of a nicely formatted output for the console).
   
    .Parameter Recursive
    Switch to get the whole chain of subordinates; without it, only the direct reports will be
    listed
   
    .Example
    Example_Show-Employee-Hierarchy.ps1 -BusinessUnit YYB -Bosses
    Example_Show-Employee-Hierarchy.ps1 -BusinessUnit YYB -Bosses -Recursive
   
    List all "bosses" of that BU; the definition of what is considered a "boss" is very specific
    and defined in the code.
   
    .Example
    > Example_Show-Employee-Hierarchy.ps1 -BusinessUnit YYB  -Identity Miller
    > Example_Show-Employee-Hierarchy.ps1 -BusinessUnit YYB  -Identity 012345
    > Example_Show-Employee-Hierarchy.ps1 -BusinessUnit YYC -Identity 999999 -Recursive
   
    Get the subordinates of a particular person (which is identified by either its surname or
    its employee number); either just the direct reports, or the whole chain (by using -Recursive).
   
    .Example
    > Example_Show-Employee-Hierarchy.ps1 -BusinessUnit YYB -Identity xxx -Recursive -AsPSObject
   
    Instead of the pretty console output, "just the facts, ma'am" (i.e. PSCustomObjects).
   
    .Example
    > (Example_Show-Employee-Hierarchy.ps1 -BusinessUnit YYB -Identity xxx -Recursive -AsPSObject).SamAccountNames
   
    Get only the SamAccountNames of the employees.
 
    .Notes
    Author: Sascha Offe (so)
           
    History
    ----------------
    2022-09-23 (so): Finally works as expected (at my third approach in several days; and with the most simple concept of those).
    2022-09-28 (so): Did some tweaking in the meantime, which also improved the performance (runtime decreased ca. 50%).
    2022-10-03 (so): Generalized and translated the original script.
#>
 
[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true)]
    [ValidateScript({ Test-Path -Path $_ -PathType Leaf })]
    $InputFile, 
    
    [Parameter(Mandatory=$true)]
    [ValidateSet("YYA", "YYB", "YYC")]
    [string] $BusinessUnit,
   
    [Parameter(ParameterSetName="SpecificEmployee")]
    [ValidateNotNullOrEmpty()]
    [string] $Identity,
   
    [Parameter(ParameterSetName="AllBosses")]
    [switch] $Bosses,
   
    [string] $Server,
   
    [switch] $AsPSObject,
    [switch] $Recursive
)
 
# --------------------------------------------------------------------------------------------------
# Just cosmetics:
 
$BusinessUnit = $BusinessUnit.ToUpper()
 
$BulletPoints = @('-', '*', '+', 'o', '~')
    # To prettify the presentation on the console a bit.
    # Please note: It's in the assumption that the indentation won't be more than 5 level deep.
 
# --------------------------------------------------------------------------------------------------
# Get the source data from the input file:
 
$EmployeeData = import-csv -path $InputFile -delimiter ';' -encoding default
 
# --------------------------------------------------------------------------------------------------
# Get just the employees of the specified business unit:
#
# The 'heads' are the bosses/supervisors/managers, accordings to some specifications listed below.
 
switch ($BusinessUnit)
{
    "YYA"
    {
        write-warning "Business Unit '$($BusinessUnit)' is not supported yet!"
        return
    }
   
    "YYB"
    {
        $Employees = $EmployeeData | ? { $_.BusinessUnit -eq "YYB" }
        $heads = $Employees | ? { ($_.ExecutiveType -eq 'A') -or (($_.Description -like "*People Lead*") -or ($_.Description -like "*PeopleLead*")) }
        break
    }
   
    "YYC"
    {
        $Employees = $EmployeeData | ? { $_.BusinessUnit -eq "YYC" }
        $heads = $Employees | ? {
            # There is currently no characteristic in the input data that marks 'bosses' the proper way for this business unit.
            # Therefore, as a workaround, we list the known employee numbers explicitly:
            ($_.EmployeeNumber -eq "012345") -or
            ($_.EmployeeNumber -eq "543210")
        }
        break
    }
   
    default # Just in case (should never be hit, due to the 'Param ValidateSet' above...).
    {
        write-warning "Business Unit '$($BusinessUnit)' is unknown!"
        return
    }
}
 
# --------------------------------------------------------------------------------------------------
# Plausibility check of the input arguments:
#
# 1. Is 'Identity' a number (such as an employee number) or a string of text (such as a Surname)? Adjust accordingly.
# 2. Does such an entry exist at all? And if so: Only once?
#
# If not, we can bail out here already: The input data is not valid.
 
if (-not $Bosses)
{
    try
    {
        # If it's a pure number (such as a employee number), then this cast should work without fault...
        $IdentityAsInteger = [int] $Identity
       
        $number_matches = $Employees | ? { $_.EmployeeNumber -eq $Identity }
       
        if (@($number_matches).count -ne 1)
        {
            write-warning "No [unique] match for employee number '$($Identity)' in $($BusinessUnit)!"
            return
        }
    }
    catch
    {
        if ($_.FullyQualifiedErrorId -eq 'InvalidCastFromStringToInteger')
        {
            # ... else it doesn't seem to be a number, so let's treat it like a surname and try it again:
            # If such an employee exists (uniquely) in the input data, then extract the employee number from that for further processing.
 
            $name_matches = $Employees | ? { $_.Surname -eq $Identity }
 
            if (@($name_matches).count -eq 1)
            {
                $Identity = $name_matches.EmployeeNumber
            }
            elseif (@($name_matches).count -eq 0)
            {
                write-warning "No match for surname '$($Identity)' in $($BusinessUnit)!"
                return
            }
            elseif (@($name_matches).count -gt 1)
            {
                write-warning "The surname '$($Identity)' exists $(@($name_matches).count) times in $($BusinessUnit).`nPlease use the correct employee number instead as the argument:"
                $name_matches | select EmployeeNumber, GivenName, Surname
                return
            }
        }
    }
}
 
# --------------------------------------------------------------------------------------------------
# Get data from the AD accounts:
#
# For performance reason, we gather the data once, as a batch job and store the result in a look-up table,
# instead of querying the AD in a later loop individually for each user .
# (That change reduced the runtime of the whole script from circa 36 seconds down to 14 seconds!).
if ($Server)
{ 
    Write-Host "[1/4] Load AD user accounts..." -ForegroundColor DarkCyan
    $ADUsers = get-aduser -server $Server -filter { (EmployeeID -like "*") -and (-not (EmployeeID -like "*Support*")) -and (-not (EmployeeID -like "*Test*")) -and (-not (EmployeeID -like "*Funktion*")) } -properties EmployeeID
}
else
{
    $ADUsers = @()
    write-host "[1/4] is skipped: Nothing to load from an AD, since there was no argument for `$Server provided." -ForegroundColor DarkCyan
    write-host "[2/4] is skipped: No AD user accounts available." -ForegroundColor DarkCyan
}
 
$ADUsers_EID_Sam = @{}
    # The key of the hashtable is the employee number (i.e. just the digits from EmployeeID);
    # the value of such a key will then be an array that contains all SamAccountNames associated with that employee (number).
 
$counter = 1
ForEach ($ADUser in $ADUsers)
{
    $percent = [math]::Round($counter/@($ADUsers).Count*100)
    Write-Host "`r[2/4] Evaluate AD user accounts: $percent%" -NoNewline -ForegroundColor DarkCyan
    # -----------------
 
    # Special case handling, from the original task; you may need to adjust it to your needs:
    # There were external users, which had the same employee number as internal employees,
    # but with another prefix (e.g. X012345 vs. ABC012345).
    # We were only interested in our own/internal staff (i.e. 'ABC*').
    # (Note: There is also the attribute EmployeeNumber (for just the numeric part), but in our orginal case,
    # that attribute was very often not maintained-- that's why we settled for EmployeeID and extracted the number from it.)
    $Employee_Number = [regex]::Match($ADUser.EmployeeID, "([Aa][Bb][Cc])([0-9]{6})$").Groups[2].Value
   
    # If the key already exists, then add the current SamAccountName to the list;
    # else create a new key and add the current SamAccountName as its first array value.
    if ($ADUsers_EID_Sam.ContainsKey($Employee_Number)) { $ADUsers_EID_Sam[$Employee_Number] += $ADUser.samaccountname }
    else                                                { $ADUsers_EID_Sam.Add($Employee_Number, @($ADUser.samaccountname)) }
   
    # -----------------
    $counter++
}
write-host ""
 
# --------------------------------------------------------------------------------------------------
# Create the hierarchy:
 
$AllRecords = @{}
 
$counter = 1
ForEach ($Employee in $Employees)
{
    $percent = [math]::Round($counter/@($Employees).Count*100)
    Write-Host "`r[3/4] Read ZZZZ data and assign ActiveDirectory accounts: $percent%" -NoNewline -ForegroundColor DarkCyan
    # -----------------
   
    $i = @{
        # The following should match the columns of the input CSV file:
        EmployeeNumber           = $Employee.EmployeeNumber
        Surname                  = $Employee.Surname
        GivenName                = $Employee.GivenName
        BusinessUnit             = $Employee.BusinessUnit
        Department               = $Employee.Department
        Description              = $Employee.Description
        ExecutiveType            = $Employee.ExecutiveType
        SupervisorEmployeeNumber = $Employee.SupervisorEmployeeNumber

        # Additional infos, not provided by the CSV file:
        # 1. For collecting the subordinate employees. 
        children = [System.Collections.Generic.List[Object]] @()
       
        # 2. A single person (= unique employee ID) can have multiple accounts (which should be tagged with the same EmployeeID, though),
        # so those will be collected here. For performance reason, this is here just a simple 'lookup & copy' task.
        SamAccountNames = $ADUsers_EID_Sam[$Employee.EmployeeNumber]
    }
    
    $AllRecords.Add($i.EmployeeNumber, $i)
    
    # -----------------
    $counter++
}
write-host ""
 
# -----------------------------------------------------------------------------
# Map the subordinates to the right person/manager:
 
$counter = 1
ForEach ($record in $AllRecords.Values)
{
    $percent = [math]::Round($counter/@($Employees).Count*100)
    Write-Host "`r[4/4] Map the subordinates: $percent%" -NoNewline -ForegroundColor DarkCyan
    # ----------------------------------------------------
   
    # Pipeline-Version: TotalSeconds: 24,2978995 -> $AllRecords.Values | ? { $_.SupervisorEmployeeNumber -eq $record.EmployeeNumber } | % { $record.children.Add($_.EmployeeNumber) }
    # For-Each-Loop:    TotalSeconds: 12,1253947
    ForEach ($value in $AllRecords.Values)
    {
        if ($value.SupervisorEmployeeNumber -eq $record.EmployeeNumber) { $record.children.Add($value.EmployeeNumber) }
    }
    
    # ----------------------------------------------------
    $counter++
}
write-host ""
 
# -----------------------------------------------------------------------------
# Helper function, to determine/collect (recursively) the subordinates of X (UserID):
 
$ListOfChildren = [System.Collections.Generic.List[object]] @()
 
function getChildren
{
    Param ($UserID, $Level = 0, [switch] $Recursive)
   
    ForEach ($child in $AllRecords[$UserID].children)
    {
        if ($AsPSObject) { $ListOfChildren.Add($AllRecords[$child]) }
        else             { write-host "$(" " * $level)$(" " * $level)$($BulletPoints[$level]) $($AllRecords[$child].EmployeeNumber) $($AllRecords[$child].GivenName) $($AllRecords[$child].Surname)" }
       
        if ($Recursive) { getChildren -UserID $child -Level ($Level+1) -Recursive:$Recursive }
    }
}
 
# --------------------------------------------------------------------------------------------------
# Output
 
write-host ""
 
if ($Bosses)
{
    if ($AsPSObject)
    {
        $heads | % {
            write-host "$($_.EmployeeNumber) $($_.GivenName) $($_.Surname)"
            if ($Recursive) { getChildren -UserID $_.EmployeeNumber -Recursive:$Recursive }
           
            $ListOfChildren | % {
                # Convert to a PSCustomObject, because otherwise, the return format is not what I had in mind.
                $object = [PSCustomObject] @{
                    EmployeeNumber  = $_."EmployeeNumber"
                    GivenName       = $_."GivenName"
                    Surname         = $_."Surname"
                    SamAccountNames = @($_."SamAccountNames")
                }
               
                $object
            }
        }
    }
    else
    {
        $heads | % {
            write-host "$($_.EmployeeNumber) $($_.GivenName) $($_.Surname)"
            if ($Recursive) { getChildren -UserID $_.EmployeeNumber -Recursive:$Recursive }
        }
    }
}
else
{
    if ($AsPSObject)
    {
        getChildren -UserID $Identity -Recursive:$Recursive
        
        $ListOfChildren | % {
                # Convert to a PSCustomObject, because otherwise, the return format is not what I had in mind.
                $object = [PSCustomObject] @{
                    EmployeeNumber  = $_."EmployeeNumber"
                    GivenName       = $_."GivenName"
                    Surname         = $_."Surname"
                    SamAccountNames = @($_."SamAccountNames")
                }
               
                $object
        }    
    }
    else
    {
        
        write-host "$($AllRecords[$Identity].EmployeeNumber) $($AllRecords[$Identity].GivenName) $($AllRecords[$Identity].Surname)"
        getChildren -UserID $Identity -Recursive:$Recursive
    }
}
