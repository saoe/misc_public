<#
    Note: This is example source code, based/inspired on a real world counterpart,
          but it is not ready for use by means of copy and paste!
    
    Create a new folder, as well as associcated groups and permissisons for that resource and then
    give some users access to it.
   
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
#>

$Domain = "example.net"
$BaseFolder = "\\example.net\data\zzz"

# 1. Create new folder -----------------------------------------------------------------------------
 
$NewFolderName = "Name of the new folder"
New-Item -ItemType Directory -Path (Join-Path -Path $BaseFolder -ChildPath $NewFolderName)

# 2. Create new AD groups --------------------------------------------------------------------------
# DLG = Domain Local Group
# GG = Global Group
# DN = DistinguishedName
# OU = OrganizationalUnit
$GroupDescription = "Access permission for ..."

# Create new domain local groups to control the access to the resource (the folder).
$DN_OU_DLG = "OU=Resources,OU=Customers,DC=example,DC=net"
New-ADGroup -server $Domain -Name "DLG-FullControl-New-Folder" -sAMAccountName "DLG-FullControl-New-Folder" -Description $GroupDescription -Path $DN_OU_DLG -GroupCategory Security -GroupScope DomainLocal 
# ... and so on for DLG-Read-..., DLG-Write-... etc. permission groups.

# Create new global AD groups and make them member of the matching domain local AD group.
# Users are only ever added to the global group, never directly to the DLG.
# (Best practice, that has something to do with the functionality in an environment with multiple
# domains in a trust relationship...)
$DN_OU_GG = "OU=Users,OU=Customers,DC=example,DC=net"
New-ADGroup -server $Domain -Name "Global-New-Folder-ReadOnly" -sAMAccountName "Global-New-Folder-ReadOnly" -Description $GroupDescription -Path $DN_OU_GG -GroupCategory Security -GroupScope Global 
# ... and so on for Gobal-New-Folder-... permission groups.
 
Add-ADGroupMember -Identity "DLG-ReadOnly-New-Folder" -Members "Global-New-Folder-ReadOnly" -Server $Domain
# ... and so on for the other groups.

# 3. Prepare permissions and set ACLs --------------------------------------------------------------
# First, clean up: Kick out all inherited (or otherwise currently active) permissions from this resource.
$ACL = Get-Acl (Join-Path -Path $BaseFolder -ChildPath $NewFolderName)
$ACL.SetAccessRuleProtection($true, $false)
$ACL.Access | ForEach {[Void]$ACL.RemoveAccessRule($_) }
# Set-Acl -Path (Join-Path -Path $BaseFolder -ChildPath $NewFolderName) -AclObject $ACL
    # Note: Although it worked last time for me, I was actually sursprised it did.
    # I had expected to lock myself out, since I removed any permissions (Administrator group etc.)
    # and my user was also not listed as the owner of this object (also surprising...).
    # So, better save this action for later, just to be on the safe side.
 
# Define ACEs
$Rights_F = [System.Security.AccessControl.FileSystemRights] "FullControl"
$Rights_R = [System.Security.AccessControl.FileSystemRights] "ReadAndExecute, Synchronize"
$Rights_W = [System.Security.AccessControl.FileSystemRights] "Modify, Synchronize"
$Access = [System.Security.AccessControl.AccessControlType]::Allow
# Stop inheritation.
$Inherit = [System.Security.AccessControl.InheritanceFlags]::ContainerInherit -bor [System.Security.AccessControl.InheritanceFlags]::ObjectInherit
# Should apply to the folder itself.
$Prop = [System.Security.AccessControl.PropagationFlags]::None
 
# Bind the ACEs to rules, then add those rules to the ACL of the AD groups.
# You need to use the group's SID for cross-domain stuff.
$AccessRule_DLG_FullControl_New_Folder = new-object System.Security.AccessControl.FileSystemAccessRule ((Get-ADGroup "DLG-FullControl-New-Folder" -server $Domain).SID, $Rights_F, $Inherit, $Prop, $Access)
$AccessRule_Administrators = new-object System.Security.AccessControl.FileSystemAccessRule ("BUILTIN\Administrators", $Rights_F, $Inherit, $Prop, $Access)
$AccessRule_System = new-object System.Security.AccessControl.FileSystemAccessRule ("NT AUTHORITY\SYSTEM", $Rights_F, $Inherit, $Prop, $Access)
# ...
 
$ACL.AddAccessRule($AccessRule_DLG_FullControl_New_Folder)
$ACL.AddAccessRule($AccessRule_Administrators)
$ACL.AddAccessRule($AccessRule_System)
# ...
 
Set-Acl -Path (Join-Path -Path $BaseFolder -ChildPath $NewFolderName) -AclObject $ACL

# 4. Give access -----------------------------------------------------------------------------------
$team_x_members = Get-ADGroupMember -server $Domain -Identity "Team X"
Add-ADGroupMember -Identity "Global-New-Folder-ReadOnly" -Members $team_x_members -Server $Domain
    # Tip: Use Get-ADGroupMember with the -recursive parameter if you want to add only users, and not (nested) groups.
    # Also, this is required to do if the source AD group is an 'Universal Distribution Group':
    # Such a group cannot be a member of a 'Global Security Group' (would throw an error/exception).
