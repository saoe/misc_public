# Powershell section

* [Module _saoe_](Modules/saoe/ReadMe.md)  
  My Powershell toolkit; a collection of helper functions for diverse areas.

* [Setup-Module.ps1](Modules/Setup-Module.ps1)  
  A helper script that combines multiple script files (*.ps1) into a single script module (*.psm1),
  creates a module manifest (*.ps1) for it and finally installs those files to a specified directory.

* [Build-ReadMe.ps1](Modules/Build-ReadMe.ps1)  
  A helper script that extracts the Synopsis of the comment-based-help in the functions of my module _saoe_
  and generates the file that you can see here: [Module saoe](Modules/saoe/ReadMe.md).  

* ~~code-snippets.md~~  
  Moved to my blog: [Powershell Code Snippets](http://www.saoe.net/blog/powershell-code-snippets/)  

* ~~execute-powershell-commands-on-a-remote-client.ps1~~  
  Moved & updated as a blog post on my site: [Powershell: Remote Session](http://www.saoe.net/blog/powershell-remote-session/)  

* [get-windowsupdatelog-from-remote-client.ps1](get-windowsupdatelog-from-remote-client.ps1)  
  Creates a log file of the Windows Updates on a remote client and copies that file back onto the caller's machine.  

* [change-aduser-passwords.ps1](change-aduser-passwords.ps1)  
  Change password(s) of Active Directory (AD) user(s) in one or more domains.

* [Example_Create-Folder-and-Associated-Permissions-and-Groups.ps1](Example_Create-Folder-and-Associated-Permissions-and-Groups.ps1)  
  _Example_ - Create a new folder, as well as associcated groups and permissisons for that resource and then give some users access to it.

* [Example_Rename-Folder-and-Associated-Groups.ps1](Example_Rename-Folder-and-Associated-Groups.ps1)  
  _Example_ - Rename a folder and associcated AD groups and give (additonal) users access to the folder.
  
* [Example_WindowsForms-GUI.ps1](Example_WindowsForms-GUI.ps1)  
  _Example_ - PowerShell script that uses .NET Windows Forms for a simple GUI.  

* [Example_WPF-XAML-GUI.ps1](Example_WPF-XAML-GUI.ps1)  
  _Example_ - A simple demo of a PowerShell script that offers a GUI by using WPF and XAML.  

* [Example_Show-Employee-Hierarchy.ps1](Example_Show-Employee-Hierarchy.ps1)  
  _Example_ - A script to read a CVS file with HR/employee data and evaluate its hierarchy.  