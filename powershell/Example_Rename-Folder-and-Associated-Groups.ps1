<#
    Note: This is example source code, based/inspired on a real world counterpart,
          but it is not ready for use by means of copy and paste!
    
    Rename a folder and associcated AD groups and give (additonal) users access to the folder.
    
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
#>

$Domain = "example.net"
$BaseFolder = "\\example.net\data\zzz"

# 1. Rename folder ---------------------------------------------------------------------------------
$CurrentFolderName = "Current Folder Name"
$NewFolderName     = "New Folder Name"
Rename-Item -Path (Join-Path -Path $BaseFolder -ChildPath $CurrentFolderName) -NewName (Join-Path -Path $BaseFolder -ChildPath $NewFolderName)

# 2. Rename associcated AD groups ------------------------------------------------------------------
$NewGroupDescription = "Access permission for Path to New Folder Name"

# Rename Domain Local Groups
$CurrentDLG_FullControl = Get-ADGroup -server $Domain -Identity "DLG-FullControl-Current-Folder-Name"
$NewDLG_FullControl = "DLG-FullControl-New-Folder-Name"
 
Rename-ADObject -Identity $CurrentDLG_FullControl -NewName $NewDLG_FullControl -Server $Domain
Set-ADGroup -Identity $CurrentDLG_FullControl.Name -SamAccountName $NewDLG_FullControl -Description $NewGroupDescription -Server $Domain
    # Important: The samAccountName must be changed explicitly, Rename-ADObject will not do that for you!
    # (It's even written down in the cmdlet documentation at microsoft.com.)
 
# ...
 
# Rename Global Groups
$CurrentGG_ReadOnly = Get-ADGroup -server $Domain -Identity "Global-OldName-ReadOnly"
$NewGG_ReadOnly = "Global-NewName-ReadOnly"
 
Rename-ADObject -Identity $CurrentGG_ReadOnly -NewName $NewGG_ReadOnly -Server $Domain
Set-ADGroup -Identity $CurrentGG_ReadOnly.Name -SamAccountName $NewGG_ReadOnly -Description $NewGroupDescription -Server $Domain

# ...

# Note: While Powershell Get-ACL gets the new names, and also the check for the SID is OK,
#       the GUI (Explorer) at times still shows the old name in the Security tab:
# 
#       When removing and adding the group, it will find and show it under the new name,
#       but when you click on OK/Apply, suddenly the old name shows up again in the GUI.
#       Might be an issue with the storage behind it (NetApp), or the AD replication.
#       So, the permissions/renaming is correct, but the GUI view of it is confusing/out-of-sync.
#       
#       Didn't found nothing really helpful on the internet that would explain it.
#       Best match: https://www.reddit.com/r/sysadmin/comments/1h781v/rename_active_directory_security_group/
#       There, the old name dissappeared after a while...
# 
#       Update: Same behavior in my real-life case: I checked the Security tab in the Explorer after
#       a few weeks again, and now the new names were displayed there as well.

# 3. Give access -----------------------------------------------------------------------------------
$team_x_members = Get-ADGroupMember -server $Domain -Identity "Team X"
Add-ADGroupMember -Identity "Global-NewName-ReadOnly" -Members $team_x_members -Server $Domain
    # Tip: Use Get-ADGroupMember with the -recursive parameter if you want to add only users, and not (nested) groups.
    # Also, this is required to do if the source AD group is an 'Universal Distribution Group':
    # Such a group cannot be a member of a 'Global Security Group' (would throw an error/exception).