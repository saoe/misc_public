<#
    .Synopsis
    A simple demo of a PowerShell script that offers a GUI by using WPF and XAML.
 
    .Description
    A simple demo of a PowerShell script that offers a GUI by using WPF and XAML:
    - WPF = "Windows Presentation Foundation" (.NET Framework)
    - XAML = "Extensible Application Markup Language"
 
    .Notes
    Copyright © 2022 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    
    DockPanel:
    | Also interesting, but not used in this example (some conflict with StackPanel?
    | (But I could put a DockPanel inside of a StackPanel...):
    |   <DockPanel>
	|     <Button DockPanel.Dock="Top" Height="50">Top</Button>
	|     <Button DockPanel.Dock="Bottom" Height="50">Bottom</Button>
	|     <Button DockPanel.Dock="Left" Width="50">Left</Button>
	|     <Button DockPanel.Dock="Right" Width="50">Right</Button>	
	|     <Button>Center</Button>
	|   </DockPanel>
    | See also:
    | - <https://docs.microsoft.com/en-us/dotnet/desktop/wpf/controls/how-to-choose-between-stackpanel-and-dockpanel>
    | - <https://wpf-tutorial.com/panels/dockpanel/>
    
    [TODO]
    Binding? I've used it with C# in the past, but is it also possible with Powershell and XAML?
    Would make some things more elegant...
           
    2022-03-23 (so): Created.
#>
 
[CmdletBinding()] Param ()
 
Add-Type -AssemblyName PresentationFramework
Add-Type -AssemblyName PresentationCore
Add-Type -AssemblyName WindowsBase
Add-Type -AssemblyName System.Drawing
 
$ComboBox_ID_001_InitialText = "(Please select a value!)"
$TextBox_ID_002_InitialText = "(Please enter a value!)"

$NOP = "A NiftyOddity Production!"
 
$ComboBox_ID_001_ValueCollection = @(
    $ComboBox_ID_001_InitialText, # Muss der erste Eintrag sein!
    "Value 1",
    "Value 2",
    "Value 3",
    "Value 4"
)
 
$CheckBox_ValueCollection = @(
    "Check Value A",
    "Check Value B",
    "Check Value C",
    "Check Value D"
)
 
# ==================================================================================================
# The XAML string for the GUI layout and style.
 
[xml] $MainWindowXaml = @"
    <Window
    xmlns                 = "http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x               = "http://schemas.microsoft.com/winfx/2006/xaml"
    Title                 = "GUI Demo (Powershell + WPF/XAML)"
    x:Name                = "MainWindow"
    WindowStartupLocation = "CenterScreen"
    SizeToContent         = "WidthAndHeight"
    ResizeMode            = "CanMinimize"
    > 

    <Window.Background>
        <LinearGradientBrush StartPoint="0, 0" EndPoint="1, 1">
            <GradientStop Color="#aaffaa" Offset="0" />
            <GradientStop Color="#eaea00" Offset="0.4" />
            <GradientStop Color="#ffcc00" Offset="0.8" />
        </LinearGradientBrush>
    </Window.Background>

    <StackPanel Orientation="Vertical" x:Name="StackPanel1">
   
    <Menu IsMainMenu="True" Background="#44888888" Padding="2">
        <MenuItem Header="Menu 1">
            <MenuItem x:Name="Menu1_Item1" Header="Item 1" HorizontalAlignment="Left"/>
            <MenuItem x:Name="Menu1_Item2" Header="Item 2" HorizontalAlignment="Left"/>
            <Separator HorizontalAlignment="Left"/>
            <MenuItem x:Name="Menu1_Item3" Header="Item 3" HorizontalAlignment="Left"/>
        </MenuItem>
 
        <MenuItem Header="Menu 2">
            <MenuItem x:Name="Menu2_Item1" Header="Item 1" HorizontalAlignment="Left"/>
            <MenuItem x:Name="Menu2_Item2" Header="Item 2" HorizontalAlignment="Left"/>
            <Separator HorizontalAlignment="Left"/>
            <MenuItem x:Name="Menu2_Item3" Header="Item 3" HorizontalAlignment="Left"/>
        </MenuItem>
    </Menu>
   
    <Grid Margin="10">
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="auto" />
            <ColumnDefinition MinWidth="300" />
        </Grid.ColumnDefinitions>
        <Grid.RowDefinitions>
            <RowDefinition Height="auto" />
            <RowDefinition Height="auto" />
            <RowDefinition Height="auto" />
            <RowDefinition Height="auto" />
        </Grid.RowDefinitions>
 
        <Label Grid.Row="0" Grid.Column="0" x:Name="Label_ID_001">#001: </Label>
        <ComboBox Grid.Row="0" Grid.Column="1" x:Name="ComboBox_ID_001"/> <!-- Will be filled with items later, dynamically -->
 
        <Label Grid.Row="1" Grid.Column="0" x:Name="Label_ID_002">#002: </Label>
        <TextBox Grid.Row="1" Grid.Column="1" x:Name="TextBox_ID_002" VerticalAlignment="Center" Text="$TextBox_ID_002_InitialText" Foreground="Gray"/>
       
        <Label Grid.Row="2" Grid.Column="0" x:Name="Label_ID_003">#003: </Label>
        <DatePicker Grid.Row="2" Grid.Column="1" x:Name="DatePicker_ID_003"/>
       
        <Label Grid.Row="3" Grid.Column="0" x:Name="Label_ID_004">#004: </Label>
        <TextBox Grid.Row="3" Grid.Column="1" x:Name="TextBox_ID_004" VerticalAlignment="Center"/>
    </Grid>
   
    <StackPanel Orientation="Horizontal">
        <StackPanel Orientation="Vertical" Margin="10, 10, 10, 10">
            <CheckBox
                Name    = "CheckBox_All"
                Content = "Check all"
                Margin  = "0, 2.5"
            />
 
            <!-- This panel will contain the later auto-generated checkboxes -->
            <StackPanel x:Name="CheckBoxPanel" Orientation = "Vertical"/>
        </StackPanel>
       
        <StackPanel x:Name="StatusRadioButtons" Orientation="Vertical" Margin="10, 10, 10, 10">
            <RadioButton GroupName="Status" x:Name="RadioButton_X" Content="Status X" HorizontalAlignment="Left" VerticalAlignment="Top"/>
            <RadioButton GroupName="Status" x:Name="RadioButton_Y" Content="Status Y" HorizontalAlignment="Left" VerticalAlignment="Top"/>
        </StackPanel>
    </StackPanel>
   
    <Slider x:Name="Slider"
       Minimum="0" Maximum="100"
       TickFrequency="10" IsSnapToTickEnabled="True" TickPlacement="BottomRight"
       Margin="10"
       Background="#11000000"
       Foreground="#000000">
       </Slider>
   
    <ToggleButton x:Name="ToggleButton" Content="This is a ToggleButton (with a tooltip!)" HorizontalContentAlignment="Center" Padding="4" Margin="10">
        <ToggleButton.ToolTip>
            <ToolTip Background="#D5F0F0FF"> <!-- First byte sets the transparency! -->
                <StackPanel>
                    <TextBlock Text="A partially transparent tooltip" FontWeight="Bold"/>
                    <TextBlock>More text<LineBreak/>Even more text<LineBreak/>And more...</TextBlock>
                </StackPanel>
            </ToolTip>
        </ToggleButton.ToolTip>
    </ToggleButton>
  
    <Grid>
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="*"></ColumnDefinition>
            <ColumnDefinition Width="*"></ColumnDefinition>
        </Grid.ColumnDefinitions>
 
        <Button
            Grid.Column    = "0"
            x:Name         = "Button_OK"
            Content        = "Make it so!"
            IsEnabled      = "False"
            Margin         = "10, 10, 10, 10"
            Padding        = "10, 10, 10, 10"
        />
 
        <Button
            Grid.Column    = "1"
            x:Name         = "Button_Cancel"
            Content        = "Abort"
            IsCancel       = "True"
            IsDefault      = "True"
            Margin         = "10, 10, 10, 10"
            Padding        = "10, 10, 10, 10"
        />
    </Grid>
   
    <!-- There is also 'MinLines'/'MaxLines', but those only apply after the first user input (known issue) -->
    <TextBox x:Name = "Log"
    IsReadOnly = "True"
    MinHeight = "150"
    MaxHeight = "150"
    Margin = "10, 10, 10, 10"
    Padding = "0, 0, 4, 0"
    VerticalScrollBarVisibility = "Auto"
    HorizontalScrollBarVisibility = "Auto"
    Background="#eaeaea"
    />
    
    <StatusBar Background="#88aaaaaa" Padding="2">
        <StatusBar.ItemsPanel>
            <ItemsPanelTemplate>
                <Grid>
                    <Grid.ColumnDefinitions>
                        <ColumnDefinition Width="*"/>
                        <ColumnDefinition Width="auto"/>
                        <ColumnDefinition Width="auto"/>
                        <ColumnDefinition Width="auto"/>
                        <ColumnDefinition Width="auto"/>
                    </Grid.ColumnDefinitions>
                </Grid>
            </ItemsPanelTemplate>
        </StatusBar.ItemsPanel>
        <StatusBarItem Grid.Column="0">
            <TextBlock x:Name="StatusBar_Column1">$NOP</TextBlock>
        </StatusBarItem>
        <Separator Grid.Column="1"/>
        <StatusBarItem Grid.Column="2">
            <TextBlock x:Name="StatusBar_Column2" Text="Some text" />
        </StatusBarItem>
        <Separator Grid.Column="3"/>
        <StatusBarItem Grid.Column="4">
            <ProgressBar x:Name="StatusBar_ProgressBar" Value="0" Width="100" Height="16"/>
        </StatusBarItem>
    </StatusBar>    
    
    </StackPanel>
    
</Window>
"@
 
try
{
 
# ==================================================================================================
# Prepare the main window by processing the XAML and finding all controls in it and store them in PowerShell variables.
 
$MainWindowReader = (New-Object System.Xml.XmlNodeReader $MainWindowXaml)
$MainWin = [Windows.Markup.XamlReader]::Load($MainWindowReader)
 
$MainWindowXaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
            Set-Variable -Name $_.Name -Value $MainWin.FindName($_.Name) -Force -Scope Script
}
 
$font               = New-Object System.Drawing.Font("Calibri", 14)
$mono_font          = New-Object System.Drawing.Font("Consolas", $font.Size)
$MainWin.FontFamily = $font.Name
$MainWin.FontSize   = $font.Size
 
# ==================================================================================================
# Helper function for the "Log" textbox
 
# Usage: log -msg "foo"
function log ([string] $msg)
{
    $timestamp = get-date -UFormat "%Y-%m-%d %H:%M:%S"
    $Log.AppendText($timestamp + ": " + $msg + "`n")
   
    # Auto-scroll the log widget to the end: You have to focus the text box, update the caret position and then scroll to end:
    # https://stackoverflow.com/a/8939013
    $Log.Focus(); # [FIXME] Not nice: Steals focus e.g. from text box under certain circumstances.
    $Log.CaretIndex = $Log.Text.Length;
    $Log.ScrollToEnd();
}
 
# ==================================================================================================
# Menubar
 
$Menu1_Item1.add_Click({ log -msg "User clicked menu-item 1.1" })
$Menu1_Item2.add_Click({ log -msg "User clicked menu-item 1.2" })
$Menu1_Item3.add_Click({ log -msg "User clicked menu-item 1.3" })
 
$Menu2_Item1.add_Click({ log -msg "User clicked menu-item 2.1" })
$Menu2_Item2.add_Click({ log -msg "User clicked menu-item 2.2" })
$Menu2_Item3.add_Click({ log -msg "User clicked menu-item 2.3" })
 
# ==================================================================================================
# ComboBox (DropDownList)
 
function generate_ComboBox_ID_001
{
    $ComboBox_ID_001.Items.Clear()
    # $ComboBox_ID_001.Items.Add($null) # In case the first entry should be nothing (not even a hint text).
   
    foreach ($i in $ComboBox_ID_001_ValueCollection)
    {
        $ComboBox_ID_001.Items.Add($i)
    }
   
    $ComboBox_ID_001.SelectedIndex = 0
}
 
$ComboBox_ID_001.add_SelectionChanged({
    try
    {
        $var = $ComboBox_ID_001.SelectedValue.ToString()
        log -msg "User has selected: $var"
    }
    catch [System.Management.Automation.PropertyNotFoundException]
    {
        log -msg "Error: $($ComboBox_ID_001.SelectedValue.ToString()) is unknown!"
    }
   
})
 
# Intial construction
generate_ComboBox_ID_001 | out-null
 
# ==================================================================================================
# CheckBoxes
#
# This function automatically generates the checkboxes, based on the entries in $....
# Returns an array of those checkboxes, for further processing.
#
# TODO: No handling of the individual checkboxes yet..
 
$is_any_checkbox_checked = $false
 
# Only if at least one checkbox is selected and ... is not empty,
# then is makes sense to enable the OK button to start a search.
#
# This function is used by the event handlers of the checkboxes and textbox.
 
# [TODO] Parameters possible?
function checkState
{
            ForEach ($c in $checkbox_collection)
            {
                        if ($c.IsChecked)
                        {
                                    $script:is_any_checkbox_checked = $true
                                    break
                        }
                        else
                        {
                                    $script:is_any_checkbox_checked = $false
                        }
            }
 
    if ($is_any_checkbox_checked <# ... #>)
    {
       $Button_OK.IsEnabled = $true
    }
    else
    {
        $Button_OK.IsEnabled = $false
    }
}
 
# [TODO] Parameters possible?
function generateCheckboxes
{
    $CheckBoxCounter = 1
    $CheckBoxes = foreach ($entry in $script:CheckBox_ValueCollection)
    {
                        $NewCheckBox = New-Object System.Windows.Controls.CheckBox
                        $NewCheckBox.Name = "CheckBox$CheckBoxCounter" # Give it a unique name.
                        $NewCheckBox.Content = $entry
                        $NewCheckBox.Margin  = "20, 2.5"
                       
        $NewCheckBox.add_Checked( { checkState } )
                        $NewCheckBox.add_UnChecked( { checkState } )
 
                $CheckBoxPanel.AddChild($NewCheckBox)
        $CheckBoxCounter++           
                
        $NewCheckBox # return object ref to array
    }
 
    $CheckBoxes
}
 
# Setup the checkboxes and save the returned array of checkboxes for later.
$checkbox_collection = generateCheckboxes
 
# [TODO] Parameters possible?
$CheckBox_All.add_Checked({
    ForEach ($c in $checkbox_collection)
    {
        $c.IsChecked = $true
        $c.IsEnabled = $false
    }
})
 
# [TODO] Parameters possible?
$CheckBox_All.add_UnChecked({
    ForEach ($c in $checkbox_collection)
    {
        $c.IsChecked = $false
        $c.IsEnabled = $true
    }
})
 
# ==================================================================================================
# Radio Buttons
 
# Bubble up event handler
[System.Windows.RoutedEventHandler] $Script:CheckedEventHandler = {
    log -msg "Group Info: $($_.source.name)"
}
 
# For the whole group (or rather the panel that contains it):
$StatusRadioButtons.AddHandler([System.Windows.Controls.RadioButton]::CheckedEvent, $CheckedEventHandler)
 
# Specific control...
$RadioButton_X.add_Checked({
    log -msg "$($_.source.name) was checked"
})
 
$RadioButton_X.add_Unchecked({
    log -msg "$($_.source.name) was unchecked"
})
 
$RadioButton_Y.add_Checked({
    log -msg "$($_.source.name) was checked"
})
 
$RadioButton_Y.add_Unchecked({
    log -msg "$($_.source.name) was unchecked"
})
 
# ==================================================================================================
# TextBoxes
 
# On GotFocus/LostFocus:
# If the textbox is empty (e.g. on start-up), a help text will be shown, until the user focuses on that text box.
# If the user enters a value, that will stick, but if the user leaves it empty, the help text will be shown again.
#
$TextBox_ID_002.add_GotFocus({
    if ($TextBox_ID_002.Text -eq $TextBox_ID_002_InitialText)
    {
        $TextBox_ID_002.Foreground = 'Black'
        $TextBox_ID_002.Text = ''
    }
})
 
$TextBox_ID_002.add_LostFocus({
    if ($TextBox_ID_002.Text -eq '')
    {
        $TextBox_ID_002.Foreground = 'Gray'
        $TextBox_ID_002.Text = $TextBox_ID_002_InitialText
    }
    elseif (-not ($TextBox_ID_002.Text -eq ''))
    {
        $TextBox_ID_002.Foreground = 'Black'
    }
})
 
$TextBox_ID_002.add_TextChanged({
    $var = $TextBox_ID_002.Text
   
    if (-not ([string]::IsNullOrEmpty($this.Text)) -and ($this.Text -ne $TextBox_ID_002_InitialText))
    {
        log -msg "Text changed to: $($this.Text)"
        $TextBox_ID_002.Focus() # Log steals focus (for scrolling); get it back.
    }
})
 
# --------------------------------------------------------------------------------------------------
 
$ID_004_var = $null
 
$TextBox_ID_004.add_TextChanged({
    $script:ID_004_var = $TextBox_ID_004.Text
})
 
$TextBox_ID_004.add_LostFocus({
    if (-not ([string]::IsNullOrEmpty($script:ID_004_var)))
    {
        log -msg "User has entered: $script:ID_004_var"
    }
})
 
 
# ==================================================================================================
# DatePicker
 
$DatePicker_ID_003.Add_SelectedDateChanged({
    $var_str = $DatePicker_ID_003.SelectedDate.ToShortDateString()
   
    [datetime] $var_dt = get-date -date $var_str
    [string] $var_ts_str = get-date -date $var_dt -format s
   
    log -msg "User has selected: $var_str (= $var_ts_str)"
})
 
# ==================================================================================================
# Slider
 
$Slider.Add_ValueChanged({
    log -msg "Slider value: $($this.Value)"
    updateStatusBarProgressBar -value $this.Value
})
 
# ==================================================================================================
# ToggleButton
 
$ToggleButton.add_Checked({
    $ToggleButton.Content = "It's ON $([char]0x2714)"
    log -msg "$($_.source.name) was checked"
})
 
$ToggleButton.add_Unchecked({
    $ToggleButton.Content = "It's OFF $([char]0x2716)"
    log -msg "$($_.source.name) was unchecked"
})
 
# ==================================================================================================
# Buttons
#
# Some behavior is pre-defined in the XAML code (Cance, is default, etc.)
 
$Button_OK.add_Click({
    $Log.Clear()
})

# ==================================================================================================
# StatusBar & ProgressBar

function updateStatusBarProgressBar ($value)
{
    $StatusBar_ProgressBar.Value = $value
}
 
# ==================================================================================================
# Main/Start
 
$null = $MainWin.ShowDialog()
 
}
catch
{
    $_
}
