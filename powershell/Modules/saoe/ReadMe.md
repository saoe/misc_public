# Powershell module [_saoe_](https://bitbucket.org/saoe/misc_public/src/master/powershell/Modules/saoe/)

My personal toolkit; a collection of helper functions for diverse areas.

--------------------------------------------------------------------------------

# Table of Contents

~ Generated on 2024-03-10 18:14:17 (+01:00) by Add-saoeModuleTableOfContentsToReadMe ~

## Public Functions

Name         | Synopsis
:----------- | :-----------
[`Add-saoeADObjectToGroup`](./public/ActiveDirectory/Add-saoeADObjectToGroup.ps1) | Add members to an Active Directory group; reads the users/computers from a file.
[`Add-saoeADUserToLocalAdministrators`](./public/ComputerManagement/Add-saoeADUserToLocalAdministrators.ps1) | Add an AD user to the local administrator group on a remote computer.
[`Add-saoeModuleTableOfContentsToReadMe`](./public/SoftwareDevelopment/Add-saoeModuleTableOfContentsToReadMe.ps1) | Adds a Table of Contents of a module's functions to a ReadMe.md file.
[`Compare-saoeADGroupsOfUsers`](./public/ActiveDirectory/Compare-saoeADGroupsOfUsers.ps1) | Compare the group memberships of two AD user accounts.
[`Convert-saoeClockTimeMetricTime`](./public/Miscellaneous/Convert-saoeClockTimeMetricTime.ps1) | Convert from "Clock Time" (e.g. 08:10) to "Metric Time" (e.g. 8,17); or the other way around.
[`ConvertFrom-saoeEMailAddress`](./public/Miscellaneous/ConvertFrom-saoeEMailAddress.ps1) | Convert the user name from an e-mail address/user-principal-name to a display name text.
[`Enable-saoeRSAT`](./public/ComputerManagement/Enable-saoeRSAT.ps1) | Enables RSAT module(s)/optional feature(s) on Windows 10 version post-1809.
[`Export-saoeIniFile`](./public/Miscellaneous/Export-saoeIniFile.ps1) | Export a data structure to a text file in the INI format.
[`Format-saoeTextToCenter`](./public/Miscellaneous/Format-saoeTextToCenter.ps1) | Center text on a line of specific width.
[`Get-saoeADComputers`](./public/ActiveDirectory/Get-saoeADComputers.ps1) | List all AD computer objects in a given domain.
[`Get-saoeADComputersWithSpecificOS`](./public/ActiveDirectory/Get-saoeADComputersWithSpecificOS.ps1) | Get all computers in a given domain that matches the specified OS.
[`Get-saoeADComputersWithSpecificOSandActivity`](./public/ActiveDirectory/Get-saoeADComputersWithSpecificOSandActivity.ps1) | Show all computer objects in domain X, running operating system Y (version Z), that where active in the last N days.
[`Get-saoeADForestTrustRelationship`](./public/ActiveDirectory/Get-saoeADForestTrustRelationship.ps1) | Get the AD domains for the forest and with which the domain tree has a trust relationship.
[`Get-saoeADGroupMembers`](./public/ActiveDirectory/Get-saoeADGroupMembers.ps1) | Get (nested) members of AD group(s).
[`Get-saoeADLAPSPassword`](./public/ActiveDirectory/Get-saoeADLAPSPassword.ps1) | Get the password of the local administrator on a LAPS-managed client.
[`Get-saoeADLastLogon`](./public/ActiveDirectory/Get-saoeADLastLogon.ps1) | Return the most recent time of when a user has been authenticated in a given domain.
[`Get-saoeADUserDirectGroups`](./public/ActiveDirectory/Get-saoeADUserDirectGroups.ps1) | Show (just) the names of all groups of which the user is a direct member.
[`Get-saoeADUserInDomains`](./public/ActiveDirectory/Get-saoeADUserInDomains.ps1) | Find $user (as account name, first/given name or surname) in one single domain, or search accross all domains.
[`Get-saoeComputerBootTime`](./public/ComputerManagement/Get-saoeComputerBootTime.ps1) | Get the last boot time of this computer.
[`Get-saoeComputerInstallationDate`](./public/ComputerManagement/Get-saoeComputerInstallationDate.ps1) | Get the installation date (as reported by the OS) from the local or a remote computer.
[`Get-saoeDFSPath`](./public/Filesystem/Get-saoeDFSPath.ps1) | Get the file server(s) behind DFS share(s)/directorie(s).
[`Get-saoeFileTimestamps`](./public/Filesystem/Get-saoeFileTimestamps.ps1) | Get some time information (created, last modfied) on a directory's content.
[`Get-saoeLocalAdministrators`](./public/ComputerManagement/Get-saoeLocalAdministrators.ps1) | Get the members of the built-in administrators group on a computer client.
[`Get-saoeMSIFileInformation`](./public/InstallationManagement/Get-saoeMSIFileInformation.ps1) | Get file information from a MSI file.
[`Get-saoeTagFromADGroup`](./public/ActiveDirectory/Get-saoeTagFromADGroup.ps1) | Returns information about a specific tag in the description or info attribute of an AD group.
[`Get-saoeUninstallStringForInstalledProduct`](./public/InstallationManagement/Get-saoeUninstallStringForInstalledProduct.ps1) | Display the uninstall string, read from the registry (32-bit and 64-bit keys).
[`Import-saoeIniFile`](./public/Miscellaneous/Import-saoeIniFile.ps1) | Get the content (sections, keys/names and values) of an *.INI file.
[`Install-saoeModule`](./public/InstallationManagement/Install-saoeModule.ps1) | Combine multiple scripts into a module, create a manifest for it and install it.
[`New-saoeHTMLHelp`](./public/Miscellaneous/New-saoeHTMLHelp.ps1) | Creates one (big) HTML overview file for all the functions of one or more Powershell module
[`New-saoeIncludeGuard`](./public/SoftwareDevelopment/New-saoeIncludeGuard.ps1) | Generate an 'include guard' text block for a C or C++ header file.
[`New-saoePassword`](./public/Miscellaneous/New-saoePassword.ps1) | Generates a random password.
[`New-saoeProject`](./public/SoftwareDevelopment/New-saoeProject.ps1) | Create a new directory hierarchy and basic files for a new project.
[`Read-saoeINIFile`](./public/Filesystem/Read-saoeINIFile.ps1) | Read an INI file and return the data as a hash table.
[`Remove-saoeADUserFromLocalAdministrators`](./public/ComputerManagement/Remove-saoeADUserFromLocalAdministrators.ps1) | Remove an AD user to the local administrator group on a remote computer.
[`Remove-saoeItem`](./public/Filesystem/Remove-saoeItem.ps1) | Deletes problematic items (e.g. files with very long paths).
[`Rename-saoeADGroup`](./public/ActiveDirectory/Rename-saoeADGroup.ps1) | Rename an AD group (and all necessary attributes).
[`Rename-saoeFilmOrTVFile`](./public/Filesystem/Rename-saoeFilmOrTVFile.ps1) | Rename a file for a TV series or movie so that it fits my naming convention.
[`Repair-saoeACL`](./public/Filesystem/Repair-saoeACL.ps1) | Repair (or simply straighten) ACLs/permissions of a file or a directory [structure]
[`Repair-saoeDSMConfiguration`](./public/NetInstall/Repair-saoeDSMConfiguration.ps1) | Replace corrupt DSM configuration files (*.ncp) on a client.
[`Resolve-saoeADSIDInForestTrustRelationship`](./public/ActiveDirectory/Resolve-saoeADSIDInForestTrustRelationship.ps1) | Resolve the SID (Security Identifier) of a 'Foreign Security Principal' AD object within an ActiveDirectory forest.
[`Search-saoeTextInFiles`](./public/Filesystem/Search-saoeTextInFiles.ps1) | Searches for the specified term in (text) files.
[`Select-saoeChoice`](./public/Miscellaneous/Select-saoeChoice.ps1) | Prompt the user for a choice in a text menu of options
[`Set-saoeADGroupManager`](./public/ActiveDirectory/Set-saoeADGroupManager.ps1) | Set a manager for an AD group.
[`Set-saoeADPassword`](./public/ActiveDirectory/Set-saoeADPassword.ps1) | A function to reset an AD user password.
[`Show-saoeACLWithResolvedSID`](./public/ActiveDirectory/Show-saoeACLWithResolvedSID.ps1) | Show ACL of filesystem item (from different domain) with resolved account names (instead SIDs)
[`Show-saoeBanner`](./public/Miscellaneous/Show-saoeBanner.ps1) | Just a gimmick: Show some kind of a banner in a sort of poor man's ASCII art/FIGlet way.
[`Show-saoeBusySignal`](./public/Miscellaneous/Show-saoeBusySignal.ps1) | Shows a busy signal like a spinning cursor or a scrolling text.
[`Show-saoeCalendarMonth`](./public/Miscellaneous/Show-saoeCalendarMonth.ps1) | Show a compact view of a calendar month
[`Show-saoeConsoleColors`](./public/Miscellaneous/Show-saoeConsoleColors.ps1) | Displays the available console colore as background and foreground color variants
[`Split-saoeIntoChunks`](./public/Miscellaneous/Split-saoeIntoChunks.ps1) | Split an input object into smaller chunks.
[`Split-saoeModule`](./public/SoftwareDevelopment/Split-saoeModule.ps1) | Split the commands of a PowerShell module into separate files.
[`Start-saoeService`](./public/ComputerManagement/Start-saoeService.ps1) | Start a service on the local machine, or on a remote machine.
[`Stop-saoeService`](./public/ComputerManagement/Stop-saoeService.ps1) | Stop a service on the local machine, or on a remote machine.
[`Test-saoeADDirectoryPermissions`](./public/ActiveDirectory/Test-saoeADDirectoryPermissions.ps1) | Find directories in a directory hierachy, where a specific AD account is (not) present in the ACL.
[`Test-saoeADGroupNesting`](./public/ActiveDirectory/Test-saoeADGroupNesting.ps1) | Test, whether a certain AD group-nesting-combination is allowed.
[`Test-saoeADIsUserMemberOfGroup`](./public/ActiveDirectory/Test-saoeADIsUserMemberOfGroup.ps1) | Answer the question "Is $User a nested member of $Group?" with either $true or $false.
[`Test-saoeGUID`](./public/Miscellaneous/Test-saoeGUID.ps1) | Test whether a given value is a valid GUID.
[`Trace-saoeFunctionUsage`](./public/Miscellaneous/Trace-saoeFunctionUsage.ps1) | Utility function to trace, which other script (e.g. in an automated task) has called a certain function.
[`Uninstall-saoeModule`](./public/InstallationManagement/Uninstall-saoeModule.ps1) | Uninstall the 'saoe' module on the local machine.
[`Update-saoeADGroupMembership`](./public/ActiveDirectory/Update-saoeADGroupMembership.ps1) | Replace for all users in $OUSearchBase the group membership from $CurrentGroup to $NewGroup.
[`Wait-saoeTimedLoop`](./public/Miscellaneous/Wait-saoeTimedLoop.ps1) | Run a timed loop.

