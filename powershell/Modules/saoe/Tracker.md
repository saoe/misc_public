Ideas
====================================================================================================

# Active Directory

List all objects by name found in OU/Container 'Computers' in our domain.
    Get-ADComputer -Filter * -SearchBase "CN=Computers, DC=xx, DC=example, DC=net" |
        Select-Object Name

List all objects with name starting with DE* found in OU/Container 'Computers' in our domain
    Get-ADComputer -Filter {name -like "DE*" } -SearchBase "CN=Computers, DC=xx, DC=example, DC=net" |
        Select-Object Name

... write that list to a file.

    Get-ADComputer -Filter {name -like "DE*" } -SearchBase "CN=Computers, DC=xx, DC=example, DC=net" |
        Select-Object Name |
        Out-File "C:\temp\output.txt"

... with Tee-Object: saves command output in a file (or variable) and also sends it down the pipeline (displays it)
    Get-ADComputer -Filter {name -like "DE*" } -SearchBase "CN=Computers, DC=xx, DC=example, DC=net" |
        Select-Object Name |
        Tee-Object -FilePath "C:\temp\output.txt"
