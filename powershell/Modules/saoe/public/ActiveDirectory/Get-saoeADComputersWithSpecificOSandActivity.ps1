<#
    .Synopsis
    Show all computer objects in domain X, running operating system Y (version Z), that where active in the last N days.
    
    .Description

    -- Windows XP --
    OS Name: "Windows XP Professional"
    OS Version: "2600" -> 5.1

    -- Windows 10 --
    OS Name: "Windows 10 Pro"
    OS Version: "10240" -> 1507
    OS Version: "10586" -> 1511
    OS Version: "14393" -> 1607
    OS Version: "15063" -> 1703
    OS Version: "16299" -> 1709
    OS Version: "17134" -> 1803
    OS Version: "17763" -> 1809
    OS Version: "18362" -> 1903
    OS Version: "18363" -> 1909

    .Parameter Domain
    Mandatory. Full qualified domain name (e.g. xyz.example.net).
    
    .Parameter OSName
    Name of the client's Operating System; will be extended with a wildcard symbol.
    Use an empty value (space) to get all clients (" ").
    
    .Parameter OSVersion
    Use an empty value (space) to get all clients (" ").
    
    .Parameter Days
    Active within the last N days. (Default value is 180.)

    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true)] [string] $Domain,
    [Parameter(Mandatory=$true)] [string] $OSName = " ",
                                 [string] $OSVersion = " ",
                                 [int]    $Days = 180
)

try   { Import-Module ActiveDirectory -ErrorAction Stop }
catch { Write-Error -Message "ActiveDirectory module could not be imported!" -Category NotInstalled -ErrorAction Stop }

$result = Get-ADComputer -Server $domain -Filter "(OperatingSystem -like '*$OSName*') -and (OperatingSystemVersion -like '*$OSVersion*')" `
          -Properties DNSHostName, OperatingSystem, OperatingSystemVersion, OperatingSystemServicePack, LastLogonDate, PasswordLastSet |
          where { $_.PasswordLastSet -ge (Get-date).AddDays(-$Days) } |
          select DNSHostName, OperatingSystem, OperatingSystemVersion, OperatingSystemServicePack, LastLogonDate, PasswordLastSet

$result| Format-Table

Write-Host "Number of active computers in the last $Days days:" $result.Count
