<#
    .Synopsis
    Return the most recent time of when a user has been authenticated in a given domain.
 
    .Description
    Note that Windows/the Active Directory handles (or at least records) those "logons" 
    (or more general: "domain authentications") that happen against the available domain controllers
    different than the ones that happen 'otherwise' via a network connection (e.g. Kerberos)!
    
    An AD user account hast multiple 'Logon'-related attributes, wich may differ very strongly:
    
    - LastLogon         : Local value of a logon/authentication to a DC, i.e. it's usually different
                          on each DC in the domain (not replicated).
    - LastLogonTimestamp: The aforementioned "other" logon/authentication time; it's replicated,
                          but can lag behind the actual last logon time up to 14 days!
    - LastLogonDate     : Just a converted version of 'LastLogontimestamp'.
    
    By default, this functions analyzes LastLogon and LastLogonTimestamp and returns the newest/most
    recent of them.
    
    Prime example where these differences mattered (speaking from experience):

    | Your mission, should you choose to accept it: Deactivate all accounts of exteral support
    | users that haven't logged on to the domain in the last three months.
    | 
    | The old-style approach: Check the logon dates on every DC in the domain and return the
    | most recent value as the last logon date, and then compare it against the given deadline date.
    | 
    | The problem with this, as we found out: Those external users only ever log on to that
    | particular system (which they support), by some kind of 'VPN/web-gateway/whatever', which
    | doesn't seem to count as a normal DC logon.
    | 
    | Because the logon date on the DCs stayed for example either very old (on one DC almost a year)
    | or (on other DCs) represented the "has never logged on here" time: '0' or '1601-01-01'. 
    | And that although the supporter was definitely working only a few days ago!
    | Luckily the (converted) 'LastLogonTimestamp' showed a much younger time (that also matched the
    | date that we were given as the time when the user was reportedly active the last time).

    .Parameter Identity
    The user that should be examined.
   
    .Parameter Server
    The server (domain) from which the data should be gathered.
    
    .Parameter OnlyDC
    Optional switch: Only examine the local logon times of each DC (LastLogon) and return the
    youngest of just those.
    
    .Parameter NotDC
    Optional switch: Only examine the replicated "other" timestamp (LastLogonTimestamp), and ignore
    the local DC times.
    
    Note that this timestamp may lag up to 14 days behind the actual last authentication, since
    the value is not immediately replicated in the domain forest, due to performance reasons.
    
    .Parameter BadPassword
    Collecting the time (from each Domain Controller) when a logon attempt was done with a wrong password.
    
    .Parameter Verbose
    Output a few additional diagnostic messages, e.g. when a certain DC doesn't respond.
    
    .Example
    Get-saoeADLastLogon -Server example.net -Identity User0123
    
    .Example
    Get-saoeADLastLogon -Server example.net -Identity User0123 -OnlyDC
   
    .Example
    Get-saoeADLastLogon -Server example.net -Identity User0123 -BadPassword
    
    .Example
    Get-saoeADLastLogon -Server example.net -Identity User0123 -NotDC

    .Notes
    Copyright © 2021, 2023-2024 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    2021-08-17 (so): Created.
    2023-07-06 (so): General revision. But most notably, it now also takes 'LogonTimestamp' into account,
                     because the kind of domain authentications that are only registered in that attribute
                     have increased enormously in recent years.                    
    2023-08-14 (so): Removed the 'ValidateScript' clause from the parameter $Identity, since it was
                     dependant on the order of the parameter '-Server': If the user specified $Server
                     after $Identity on the command line, ValidateScript failed (because it wasn't yet set)!
    2024-02-28 (so): * Added parameter -BadPassword to show logon attempts that failed due to bad/wrong passwords.
                     * Removed parameter -ShowDC; one can get the same info with the -Verbose parameter.
    
    ================================================================================================
    More reading material/background info:
    
    https://learn.microsoft.com/en-us/windows/win32/adschema/a-lastlogon
    The last time the user logged on.
    Remarks: This attribute is not replicated and is maintained separately on each domain controller in the domain. 

    https://learn.microsoft.com/en-us/windows/win32/adschema/a-lastlogontimestamp
    This is the time that the user last logged into the domain. 

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    http://www.selfadsi.de/ads-attributes/user-lastLogonTimestamp.htm
    Das Active Directory Attribut lastLogonTimestamp zeigt den exakten Zeitpunkt, an dem sich der Benutzer das letzte Mal erfolgreich
    in der Domäne authentisiert hat. Im Unterschied zum Attribut lastLogon wird lastLogonTimestamp zwischen Domänencontrollern
    repliziert - allerdings nur, wenn es älter ist als zwei Wochen (minus einem zufälligen Wert von 0-5 Tagen).
    Diese Einschränkung soll die die Netzwerkbandbreite, die durch die AD-Replikation belegt wird, so gering wie möglich halten.
    
    Der Wert lastLogonTimestamp ist es also eher dazu geeignet, inaktive Konten ausfindig zu machen, die sich schon lange Zeit nicht
    mehr in der Domäne angemeldet haben.

    Es spielt dabei keine Rolle, auf welchem Weg die Anmeldung geschah - ob interaktiv, übers Netzwerk oder weitergeleitet von
    einem Radius-Server oder einem anderen Kerberos-Realm. Falls sich der Benutzer noch nie am DC angemeldet hat, ist der Wert
    von lastLogonTimestamp gleich Null.
    
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    https://social.technet.microsoft.com/Forums/office/de-DE/1ae08081-dcfe-44cd-bc3b-f5ac26d53f76/difference-between-lastlogon-and-lastlogontimestamp?forum=winserverDS
    
    - LastLogon
      When a user logs on, this attribute is updated on the Domain Controller that provided the authentication ONLY.
      Because it is only updated on one DC, that means this attribute is not replicated. [...]
      
      This value is stored as a large integer that represents the number of 100 nanosecond intervals since January 1, 1601 (UTC).
      A value of zero means that the last logon time is unknown.
    
    - LastLogonTimeStamp
      To summarize this attribute, this is the replicated version of the LastLogon attribute.
      Although, because it just can’t be that easy, there are a few things to understand before we start to use this attribute.                 
      * LastLogonTimeStamp only updates when the mood is right ["ms-DS-Logon-Time-Sync-Interval", 14 days by defauly].
      * [Comment section] There are other ways that lastLogonTimestamp can be updated without the user authenticating. 
    
    - LastLogonDate
      LastLogonDate is a converted version of LastLogontimestamp.
      [...] It's not a replicated attribute. [...]
    
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    https://liferay.dev/ask/questions/development/lr7-last-logon-date-not-updated-in-ldap-ms-ad-after-login-in-liferay-1

    [...] When a user signs in on Liferay Portal, the user is not really signing into AD.
    The Portal makes uses of the AD's LDAP feature to check the user's password.
    So most likely, the user has never logon according to AD. [...]

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        
    https://social.technet.microsoft.com/Forums/office/en-US/1ae08081-dcfe-44cd-bc3b-f5ac26d53f76/difference-between-lastlogon-and-lastlogontimestamp?forum=winserverDS

    LastLogon reflects the last interactive logon that was authenticated by a specific domain controller.
    The value is not replicated to other domain controllers.

    LastLogonTimestamp reflects many other types of logons:    
    - Interactive, Network and Service logons
    - Simple LDAP Bind operations
    - NTLM Network-based logons
    - Launching LDAP Queries
    - Accessing Email Servers
    - Certain types of security/group and effective permissions enumerations
    - IIS Logons
    - A Kerberos Operation known as Service-for-User-to-Self or “S4u2Self”.

    LastLogonTimestamp is replicated to all domain controllers.

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   
    * https://social.technet.microsoft.com/wiki/contents/articles/22461.understanding-the-ad-account-attributes-lastlogon-lastlogontimestamp-and-lastlogondate.aspx>
    * https://social.technet.microsoft.com/Forums/office/de-DE/1ae08081-dcfe-44cd-bc3b-f5ac26d53f76/difference-between-lastlogon-and-lastlogontimestamp?forum=winserverDS>
    * https://www.active-directory-faq.de/2021/01/lastlogon-vs-lastlogontimestamp/ (german)

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    > LastLogon
    > When a user logs on, this attribute is updated on the Domain Controller that provided the authentication ONLY.
    > Because it is only updated on one DC, that means this attribute is not replicated. [...]
    > This value is stored as a large integer that represents the number of 100 nanosecond intervals since January 1, 1601 (UTC).
    > A value of zero means that the last logon time is unknown.
    >
    > LastLogonTimeStamp
    > To summarize this attribute, this is the replicated version of the LastLogon attribute.
    > Although, because it just can’t be that easy, there are a few things to understand before we start to use this attribute.                
    > - LastLogonTimeStamp only updates when the mood is right ["ms-DS-Logon-Time-Sync-Interval", 14 days by default].
    > - [in the comments] There are other ways that lastLogonTimestamp can be updated without the user authenticating.
    >
    > LastLogonDate
    > LastLogonDate is a converted version of LastLogontimestamp.
    > [...] It’s not a replicated attribute. [...]
   
    ================================================================================================
 #>

[CmdletBinding(DefaultParameterSetName = "Default")]
param
(
    [Parameter(ParameterSetName="Default", Mandatory=$true)]
    [Parameter(ParameterSetName="OnlyDC", Mandatory=$true)]
    [Parameter(ParameterSetName="NotDC", Mandatory=$true)]
    [Parameter(ParameterSetName="BadPassword", Mandatory=$true)]
    $Server,
    
    [Parameter(ParameterSetName="Default",Mandatory=$true)]
    [Parameter(ParameterSetName="OnlyDC", Mandatory=$true)]
    [Parameter(ParameterSetName="NotDC", Mandatory=$true)]
    [Parameter(ParameterSetName="BadPassword", Mandatory=$true)]
    [ValidateScript({ get-aduser -server $Server -identity $_ })]
    $Identity,
    
    [Parameter(ParameterSetName="OnlyDC")]
    [switch] $OnlyDC,
    
    [Parameter(ParameterSetName="OnlyDC")]
    [switch] $ShowDC,
    
    [Parameter(ParameterSetName="NotDC")]
    [switch] $NotDC,
    
    [Parameter(ParameterSetName="BadPassword")]
    [switch] $BadPassword
)

try   { Import-Module ActiveDirectory -ErrorAction Stop }
catch { Write-Error -Message "ActiveDirectory module could not be imported!" -Category NotInstalled -ErrorAction Stop }
 
$DCs = Get-ADDomainController -Server $Server -Filter * | select -expand HostName | sort

$LastLogons = @()
$LastLogonTimestamps = @()

$BadPasswords = @() # Collection from all DCs, since that data is also not replicated between DCs.
$badpwdcount = 0
$badpwdtime = New-Object System.DateTime # Initializes it with a value that represents 0001-01-01 00:00:00.

ForEach ($DC in $DCs)
{
    try
    {
        # Collect the not-replicated, local LastLogon from the domain controllers.
        $user = get-aduser -server $DC -identity $Identity -ErrorAction Stop -properties LastLogon, LastLogonTimestamp
        
        # -- LastLogon & LastLogonTimestamp --
        [int64] $x = $user | select -expand LastLogon
        $ll = [datetime]::FromFileTime($x)
        $LastLogons += $ll
        
        # LastLogonTimestamp should be identical on all DCs (replicated).
        [int64] $y = $user | select -expand LastLogonTimestamp
        $llt = [datetime]::FromFileTime($y)
        $LastLogonTimestamps += $llt
        
        # -- BadPwdCount & BadPasswordTime --
        $badpwdcount += $user.BadPwdCount # Totale Summe (DC-übergreifend) erhöhen.
        $BadPwdTime_onDC = [datetime]::FromFileTime($user.BadPasswordTime)

        if ($badpwdtime -lt $BadPwdTime_onDC) { $badpwdtime = $BadPwdTime_onDC } # Auf den jeweils neuesten/jüngsten Wert setzten.
    
        $BadPasswords += $BadPwdTime_onDC
        
        # -- Detailed (verbose) output --
        if ($BadPassword) { write-verbose "$($DC): BadPassword: $($BadPwdTime_onDC | get-date -f 'yyyy-MM-dd HH:mm:ss z')" }
        else              { write-verbose "$($DC): LastLogon: $($ll | get-date -f 'yyyy-MM-dd HH:mm:ss z')" }
    }
    catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]
    {
        write-warning "$($_.Exception.Message)"
        $ll = $null
        $llt = $null
    }
    catch [Microsoft.ActiveDirectory.Management.ADServerDownException]
    {
        write-verbose "$($DC).$($Server): $($_.Exception.Message)"
        $ll = $null #Get-Date("1601-01-01 00:00:00") # Year 1601 = Windows convention for "has never logged on (to this DC)".
        $llt = $null #Get-Date("1601-01-01 00:00:00")
    }
    catch
    {
        write-warning $_.Exception.Message
        $ll = $null #Get-Date("1601-01-01 00:00:00")
        $llt = $null #Get-Date("1601-01-01 00:00:00")
    }
}

$NewestLL = Get-date (($LastLogons | sort-object)[-1])
$NewestLLT = Get-date (($LastLogonTimestamps | sort-object)[-1])
$NewestBadPassword = Get-date (($BadPasswords | sort-object)[-1])

if     ($BadPassword) { $NewestBadPassword }
elseif ($OnlyDC)      { $NewestLL }
elseif ($NotDC)       { $NewestLLT }
else
{
    if     ($NewestLL -gt $NewestLLT) { $NewestLL  }
    elseif ($NewestLLT -gt $NewestLL) { $NewestLLT }
    elseif ($NewestLLT -eq $NewestLL) { $NewestLLT } # When in doubt (and both equal), simply return LastLogonTimestamp.
    else                              { $null }      # Should never occur, but who knows.
}