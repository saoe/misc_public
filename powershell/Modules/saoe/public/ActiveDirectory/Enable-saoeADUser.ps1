<#
    .Synopsis
    Enable AD user account

    .Description
    Also possible to enable multiple accounts in one call:
    Either by providing IDs via pipeline or as an array/list to the parameter '-Identity'.
    --> See examples

    .Parameter Server
    The server/domain on which the user account can be found.
    (Tip: Only necessary when the value of the parameter '-Identity' is not already an ADUser object.)
    
    .Parameter Identity
    The ID or ADUser object of the account.
    (Tip: If it's an ADUser object, one can omit the parameter '-Server'.)
    
    .Example
    Enable-saoeADUser -Server example.net -Identity user12345
    
    .Example
    user12345, user54321 | Enable-saoeADUser -Server example.net
    
    .Example
    Enable-saoeADUser -Server example.net -Identity user12345, user54321
    
    .Example
    $result = get-aduser -server example.net -filter { ... something ... }
    $result | Enable-saoeADUser
    
    .Example
    "user12345", "user54321" | Enable-saoeADUser -Server example.net

    .Notes
    Copyright � 2023-2024 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2023-05-25 (so): Created.
    2024-02-01 (so): The server can now be determined by analyzing an ADUser object.
    2024-05-04 (so): Added to my personal code repository.
#>

[CmdletBinding()]
Param
(
    $Server,
    
    [Parameter(Mandatory = $true, ValueFromPipeline)]
    $Identity
)

Process
{
    ForEach ($id in $Identity)
    {
        if (($id.GetType().Name -eq "ADUser") -or ($id.GetType().Name -eq "PSCustomObject"))
        {
            $Server = ((($id | select -expand DistinguishedName) -split ',' | ? {$_ -like "DC=*"}) -replace 'DC=', '') -join '.'
            $id = $id | select -expand SamAccountName
                # Otherwise Get-ADUser will choke on the variable, if it's a PSCustomObject.
        }
        elseif ($Server)
        {
            if (-not $Server)
            {
                write-host "'$($Server)' cannot be resolved or reached!" -ForegroundColor Red
                continue
            }
        }
        else
        {
            write-host "Server/Domain could not be determined!" -ForegroundColor Red
            break
        }
        
        try
        {
            $user = Get-ADUser -Server $Server -Identity $id -properties 'msDS-PrincipalName'
            
            if ($user.Enabled -eq $true)
            {
                write-host "$($user.'msDS-PrincipalName') ($($user.Name)) was already enabled."
            }
            else
            {
                Set-ADUser -Server $Server -Identity $user -Enabled $true
                
                $user_fresh = Get-ADUser -Server $Server -Identity $user -properties 'msDS-PrincipalName'
                if     ($user_fresh.Enabled -eq $true)  { write-host    "$($user_fresh.'msDS-PrincipalName') ($($user_fresh.Name)) is now enabled." }
                elseif ($user_fresh.Enabled -eq $false) { write-warning "$($user_fresh.'msDS-PrincipalName') ($($user_fresh.Name)) could not be enabled!" }
            }
        }
        catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]
        {
            write-host "$($id): $($_.Exception.Message)" -ForegroundColor Red
        }
        catch
        {
            write-host "$($_)" -ForegroundColor Red
        }
    }
}
