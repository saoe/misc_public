<#
    .Synopsis
    Show (just) the names of all groups of which the user is a direct member.

    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
#>

[CmdletBinding()]
param
(
    [Parameter(Mandatory=$true)] [String] $User,
    [Parameter(Mandatory=$true)] [String] $Domain
)

try   { Import-Module ActiveDirectory -ErrorAction Stop }
catch { Write-Error -Message "ActiveDirectory module could not be imported!" -Category NotInstalled -ErrorAction Stop }

Get-ADPrincipalGroupMembership -Identity $User -Server $Domain | Select-Object Name
