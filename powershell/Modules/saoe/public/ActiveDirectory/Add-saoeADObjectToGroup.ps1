<#
    .Synopsis
    Add members to an Active Directory group; reads the users/computers from a file.
    
    .Parameter Group
    Mandatory. Name of the AD group to which the users should be added.
    
    .Parameter InputFile
    Mandatory. Filename/Path for input file with list of users to be imported.
    
    .Parameter Domain
    Mandatory. Full qualified domain name (e.g. xyz.example.net).
    
    .Notes
    Copyright © 2014-2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2014-11-06 (so): Initial release.
    2019-11-06 (so): Using Get-Content instead of Import-CSV for simple(r) input files.
                     User objects and computer objects can be added to a group.
    2019-11-22 (so): Works in given domain (-Domain).
                     Checks (and may skip) if the object is already a member of the group.
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true)] [string] $Group,
    [Parameter(Mandatory=$true)] $InputFile,
    [Parameter(Mandatory=$true)] [string] $Domain
)

try   { Import-Module ActiveDirectory -ErrorAction Stop }
catch { Write-Error -Message "ActiveDirectory module could not be imported!" -Category NotInstalled -ErrorAction Stop }
 
if ($Domain) { }
else         { $Domain = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain().Name }
 
$credential = Get-Credential -Message "Please enter an account that can modify the Active Directory."
 
$List = Get-Content $InputFile
 
ForEach ($Item in $List)
{
    $members = Get-ADGroupMember -Identity $Group -Server $Domain | Select -ExpandProperty Name

    If ($members -contains $Item)
    {
        Write-Output "$Item is already a member of `"$Group`"."
        Continue
    }
    
    try    # Try to add it as an user object.
    {
        $obj = Get-ADUser -Identity $Item -Server $Domain
        Add-ADGroupMember -Credential $credential -Server $Domain -Identity $Group -Members $Item
        Write-Output "Added user object $Item to `"$Group`"."
        Continue    # User object found, no need to check whether it might be a computer object.
    }
    catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]
    {
        # Ignore if the ADUser is not found.
    }

    try    # Try to add it as a computer object.
    {
        $obj = Get-ADComputer -Identity $Item -Server $Domain
        Add-ADGroupMember -Credential $credential -Server $Domain -Identity $Group -Members $obj
        Write-Output "Added computer object $Item to `"$Group`"."
        Continue
    }
    catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]
    {
        # Ignore if the ADComputer is not found.
    }
}
