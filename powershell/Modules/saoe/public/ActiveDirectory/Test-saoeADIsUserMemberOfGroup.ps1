<#
    .Synopsis
    Answer the question "Is $User a nested member of $Group?" with either $true or $false.
    
    .Description
    Traverses the group nesting up to check if $User is somehow a member in $Group.
    https://devblogs.microsoft.com/scripting/active-directory-week-explore-group-membership-with-powershell/
    
    .Parameter User
    Mandatory. The user that will be checked.
    
    .Parameter UserDomain
    Optional. In case the user that will be checked is in a different domain.
    
    .Parameter Group
    Mandatory. The group that will be checked
    
    .Parameter GroupDomain
    Optional. In case the group that will be checked is in a different domain.

    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true)] [string] $User,
                                 [string] $UserDomain =  $env:USERDOMAIN,
    [Parameter(Mandatory=$true)] [string] $Group,
                                 [string] $GroupDomain = $env:USERDOMAIN
)

try   { Import-Module ActiveDirectory -ErrorAction Stop }
catch { Write-Error -Message "ActiveDirectory module could not be imported!" -Category NotInstalled -ErrorAction Stop }

if (Get-ADUser -Filter "memberOf -RecursiveMatch '$((Get-ADGroup $Group -Server $GroupDomain).DistinguishedName)'" `
               -Server $UserDomain `
               -SearchBase $(Get-ADUser $User -Server $UserDomain).DistinguishedName)
{
    $true
}
else
{
    $false
}
