<#
    .Synopsis
    A function to reset an AD user password.

    .Description
    This function (re)sets the password of AD user accounts; it also unlocks the account (if
    needed) and requires that the user must change the password at the next logon.

    .Parameter User
    String(s) representing the user ID(s).
    
    .Parameter Domain
    The name of the domain of the user account(s).
    
    .Notes
    Copyright © 2021 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    Uses my own function New-saoePassword (from this module) to generate a random new password.
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true, ValueFromPipeline)]
    [string[]] $User,
   
    [Parameter(Mandatory=$true)]
    [string] $Domain
)

try   { Import-Module ActiveDirectory -ErrorAction Stop }
catch { Write-Error -Message "ActiveDirectory module could not be imported!" -Category NotInstalled -ErrorAction Stop }
 
Process
{
    ForEach ($u in $User)
    { 
        [string] $new_pw = $null
        $new_pw = New-saoePassword # Another function of mine, from this module.
        $sec_new_pw = ConvertTo-SecureString -String $new_pw -AsPlainText -Force

        $account = Get-ADUser -Identity $u -server $domain -properties LockedOut
        if ($account.LockedOut -eq $True) { Unlock-ADAccount -Identity $u -Server $Domain }
        Set-ADAccountPassword -Identity $u -NewPassword $sec_new_pw -Reset -PassThru -Server $Domain | Set-ADUser -ChangePasswordAtLogon $True

        write-host "Password reset to: $($new_pw) for user '$($u)' in domain $($Domain)."
    }
}
