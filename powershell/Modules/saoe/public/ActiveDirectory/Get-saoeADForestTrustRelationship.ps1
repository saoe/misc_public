<#
    .Synopsis
    Get the AD domains for the forest and with which the domain tree has a trust relationship.

    .Description
    Starting with the name of the (forest root) domain (as the parameter "Domain"), this function
    will pan out and collect all other domains of the tree and the ones with which it has a fitting
    trust relationship.
    
    .Parameter Domain
    The name of the (forest root) domain, whose (forest and trusted) domains will be gathered.
    
    .Outputs
    Returns a hash table object with the requested data.
    
    .Example
    > $x = Get-saoeADForestTrustRelationship -Domain "example.net"
    > $x["TrustedDomains"]
    
    .Notes
    Copyright © 2021 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2021-01-18 (so): Created.
#>

param
(
    [Parameter(Mandatory=$True)] [string] $Domain
)

try   { Import-Module ActiveDirectory -ErrorAction Stop }
catch { Write-Error -Message "ActiveDirectory module could not be imported!" -Category NotInstalled -ErrorAction Stop }

$context = [System.DirectoryServices.ActiveDirectory.DirectoryContextType]::Forest
    # Hints for other occasions, maybe: $context = [...]::Domain
    #                                   $context = [...]::DirectoryServer
$ad_context = New-Object System.DirectoryServices.ActiveDirectory.DirectoryContext $context, $Domain
$Forest = [System.DirectoryServices.ActiveDirectory.Forest]::GetForest($ad_context)
$Trusts = $Forest.GetAllTrustRelationships()

$result = @{
            "ForestDomains" = @()
            "TrustedDomains" = @()
}
       
ForEach ($fd in $Forest.Domains)
{      
    $result["ForestDomains"] += $fd
}

ForEach ($trust in $Trusts)
{
    ForEach ($dom_info in $trust.TrustedDomainInformation)
    {
        If (-not ($dom_info.dnsname.Contains("custom")) -and ($trust.TrustDirection -eq "Inbound" -or $trust.TrustDirection -eq "Bidirectional"))
        {
            $result["TrustedDomains"] += $dom_info
        }
    }
}

$result
