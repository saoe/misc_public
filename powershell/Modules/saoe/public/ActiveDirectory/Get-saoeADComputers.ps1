<#
    .Synopsis
    List all AD computer objects in a given domain.
    
    .Parameter Domain
    The full qualified domain name (FQDN).
    
    .Parameter OutputFolder
    The folder in which the file with the results (a CSV file with the name of the group) should be saved.
    Folder must already exist!

    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
#>

[CmdletBinding()]
param
(
    [Parameter(Mandatory=$true)] [String] $Domain,
                                 [String] $OutputFolder
)

try   { Import-Module ActiveDirectory -ErrorAction Stop }
catch { Write-Error -Message "ActiveDirectory module could not be imported!" -Category NotInstalled -ErrorAction Stop }

$computers = Get-ADComputer -Server $Domain -Filter * -Properties DNSHostName | Select-Object DNSHostName

if ($OutputFolder)
{
    $OutputFile = "$($Domain)-computers.csv"
     
    if (Test-Path $OutputFolder) { $OutputFile = Join-Path -Path $OutputFolder -ChildPath $OutputFile }
    else                         { Write-Host "`nWarning: Folder $out does not exist! Using default path!" }
    
    $computers | Export-CSV -Path $OutputFile -NoTypeInformation -Encoding UTF8
    Write-Output "Output written to $((Get-Item $OutputFile).FullName)"
}
else
{
    Write-Host "All AD computers in domain `"$Domain`""
    Write-Host "---------------------------------------------"
    $computers | Format-Table | Write-Output
}
