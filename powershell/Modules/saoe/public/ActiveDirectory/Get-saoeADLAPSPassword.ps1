<#
    .Synopsis
    Get the password of the local administrator on a LAPS-managed client.

    .Description
    The "Local Administrator Password Solution" (LAPS) manages the passwords for the built-in
    local administrator accounts of domain joined computers: 
    The passwords are stored in the Active Directory and protected by ACLs, so that only eligible
    users (e.g. helpdesk administrators) can access it.

    This provides a solution to the issue of using an identical password on every computer in a
    domain for the account mentioned above:        
    LAPS set a different, random password for the common local administrator account on every
    computer in the domain and changes it automatically in specified intervals.
    
    The data is stored in the following AD computer object attributes:
    - "ms-Mcs-AdmPwd"
    - "ms-Mcs-AdmPwdExpirationTime"
    (This script uses the LAPS-standard module 'AdmPwd.PS' for easier handling.)
    
    LAPS needs to be explicitly installed and set-up in an Active Directory domain.
    
    .Parameter ComputerName
    Specifies from which computer object we want the password.
    
    .Parameter Domain
    Specifies in which AD domain the computer object will be searched.        
    (By default, the current domain will be used.)
    
    .Outputs
    Returns a hash table object with the requested data.

    .Link
    https://www.microsoft.com/en-us/download/details.aspx?id=46899

    .Link
    https://support.microsoft.com/en-us/help/3062591/microsoft-security-advisory-local-administrator-password-solution-laps
    
    .Example
    > $x = Get-saoeADLAPSPassword -ComputerName "SomeClient123" -Domain example.net
    > $x.Password
    
    .Notes
    Copyright © 2020-2021 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2020-11-18 (so): Created (as a standalone script).
    2021-01-15 (so): Upgraded to a function and updated it a bit.
#>

param
(
     [Parameter(Mandatory=$True)] [String] $ComputerName,
                                  [String] $Domain = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain().Name
)

try   { Import-Module ActiveDirectory -ErrorAction Stop }
catch { Write-Error -Message "ActiveDirectory module could not be imported!" -Category NotInstalled -ErrorAction Stop }

try   { Import-Module AdmPwd.PS -ErrorAction Stop }
catch { Write-Error -Message "AdmPwd.PS module could not be imported!" -Category NotInstalled }

$comp = Get-ADComputer -Identity $ComputerName -Server $Domain

if ($comp)
{
    $admpwd = Get-AdmPwdPassword -ComputerName $comp
    
    if ($admpwd.Password)
    {
        #Write-Host "Local administrator password for $($admpwd.ComputerName)"   
        @{
            "Computer"       = $($admpwd.ComputerName)
            "Domain"         = $Domain
            "Password"       = $($admpwd.Password)
            "ExpirationTime" = $(get-date $($admpwd.ExpirationTimestamp))
        }  | % { $_.GetEnumerator() | sort Name } # Sorting a hash table output requires a bit more effort.
    }
    else
    {
        Write-Warning "No password found for the local administrator account of computer '$($admpwd.ComputerName)' in the AD/LAPS of '$Domain'! It's maybe either not set (yet) or you don't have read permissions for it."
    }
}
