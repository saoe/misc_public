<#
    .Synopsis
    Get all computers in a given domain that matches the specified OS.
    
    .Parameter Domain
    Mandatory. Full qualified domain name (e.g. xyz.example.net).
    
    .Parameter OSName
    Name of the client's Operating System; will be extended with a wildcard symbol.
    
    .Parameter OutputFolder
    The folder in which the file with the results (a CSV file with the name of the group) should be saved.
    Folder must already exist!
    
    .Example
    -Domain sub.example.net -OSName "Windows 10"       

    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true)] [string] $Domain,
    [Parameter(Mandatory=$true)] [string] $OSName,
                                 [string] $OutputFolder
)

try   { Import-Module ActiveDirectory -ErrorAction Stop }
catch { Write-Error -Message "ActiveDirectory module could not be imported!" -Category NotInstalled -ErrorAction Stop }

$computers = Get-ADComputer -server $Domain -Filter "operatingsystem -like '$OSName*'" -Properties Name, OperatingSystemVersion | Select-Object -Property Name, OperatingSystemVersion

if ($OutputFolder)
{
    $OutputFile = "$($Domain)-$($OSName)-computers.csv"
    
    if (Test-Path $OutputFolder) { $OutputFile = Join-Path -Path $OutputFolder -ChildPath $OutputFile }
    else                         { Write-Host "`nWarning: Folder $out does not exist! Using default path!" }
    
    $computers | Export-CSV -Path $OutputFile -NoTypeInformation -Encoding UTF8
    Write-Output "Result written to file $OutputFile"
}
else
{
    Write-Host "List of computers in $Domain with $($OSName):"
    $computers | Sort-Object -Property Name | Format-Table | Write-Output
}
