<#
    .Synopsis
    Get (nested) members of AD group(s).
    
    .Description
    Can handle big AD groups: No size limit for a high AD groups membership.
    Get the (optionally: nested/recursive) members of given AD group(s) from the specified domain(s).
    Can also handle members that are 'Foreign Security Principals', i.e. from different domains.
    Can filter for some constraints ('only users', 'only groups', etc.).
    Can save the result to a CSV file.
   
    .Parameter Groups
    The name of the group(s) for which the members should be collected.
    Can also be the ObjectGUID instead, in case the name is problematic or ambiguous.
   
    .Parameter Domains
    The domain(s) for which the operation will be executed.
    (If no value is specified, the current domain will be used by default.)
    
    .Parameter DomainsOfFSP
    Specifies the domain(s) that should be examined in case
    "Foreign Security Principal" group members should be resolved.
    
    .Parameter Credential
    To provide a credential object for the access to the domain controllers;
    will be used for all domains.
   
    .Parameter Recursive
    All nested members should be resolved.
    
    So, if other groups are members of the given group, then the users of such groups will
    be listed, instead of simply the group names.
    
    .Parameter OnlyGroups
    Limit the hits the class 'Group'.
    
    .Parameter OnlyUsers
    Limit the hits the class 'User' (category 'Person', not 'computer'!).
    
    .Parameter OnlyFSP
    Limit the hits to class 'Foreign Security Principals'.
   
    .Parameter SaveToFolder
    Save the result as a CSV file in the specified directory.
    
    A path to an existing directory must be provived, but you can simply use "." (dot) for the
    current working directory.
    
    The name of the generated CSV file is predefined in this script.
   
    .Example
    Get-saoeADGroupMembers -Groups "GPO-FOO" -Domains "sub.example1.net", "example2.com" -OnlyGroups -Recursive -Save "C:\temp"
    
    .Example
    Get-saoeADGroupMembers -Groups "GPO-FOO", "SW-BAR" -Domains "sub.example.net" -Save .
   
    .Notes
    Copyright © 2013, 2020, 2021, 2023 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2013-10-20 (so): Added first version to the repository (as "ListActiveDirectoryGroupMembers.ps1").
    2020-12-18 (so): General overhaul. Biggest change: It can now handle big AD groups.
                     Before, Get-ADGroupMember failed with the error "The size limit for this request
                     was exceeded" on AD groups with many members. Now the function was changed
                     to use Get-ADObject instead, which fixes the issue.
    2021-01-14 (so): Now capable of handling multiple domains and also limiting it to only groups or users.
    2021-01-15 (so): Fixed a missing flow for a parameter combination; restructed/simplied function;
                     added parameter to filter for FSPs; updated comments; removed output as a
                     preformatted table, now just returning the objects.
    2021-04-08 (so): Added optional parameter to provide credentials;
                     removed 'select-object $properties' from the pipeline when returning the member or
                     when writing the data to a file: Unnecessary and on big groups veeery slow.
    2023-01-05 (so): In the meantime, the productive and slightly customized version of this script
                     that I use at my current job, evolved; I now added those changes to this version too:
                     - Added the feature to get the FSP members (parameter -DomainsOfFSP); that was already available
                       in the other script, but then I disabled it there again for some reason, although it works fine, it seems.
                     - Learned the hard way that one cannot rely on the name of a group being a good identifier, since
                       Name, SamAccountName, etc. may differ. So one can now also supply the ObjectGUID of the group instead.
                     - Some other, minor chnges: Querying a few additional group properties; using a list
                       instead of an array for collecting the members.
    
    [TODO #1]
    For collecting the members (see "Get-ADObject ... | % { $members.Add($_) }"):
    Maybe worth using SearchBase and Scope to limit it further (via additional parameters)?
    That may help/speed things up...
#>

[CmdletBinding(DefaultParametersetname="Default")]
param
(
    [Parameter(ParameterSetName="Default")]
    [Parameter(ParameterSetName="OnlyGroups")]
    [Parameter(ParameterSetName="OnlyUsers")]
    [Parameter(ParameterSetName="OnlyFSP")]
    [Parameter(Mandatory=$true)]
    [string[]] $Groups,
           
    [Parameter(ParameterSetName="Default")]
    [Parameter(ParameterSetName="OnlyGroups")]
    [Parameter(ParameterSetName="OnlyUsers")]
    [Parameter(ParameterSetName="OnlyFSP")]
    [string[]] $Domains = [System.DirectoryServices.ActiveDirectory.Domain]::GetCurrentDomain().Name,
    
    [Parameter(ParameterSetName="Default")]
	[Parameter(ParameterSetName="OnlyGroups")]
	[Parameter(ParameterSetName="OnlyUsers")]
	[Parameter(ParameterSetName="OnlyFSP")]
	[string[]] $DomainsOfFSP,
    
    [Parameter(ParameterSetName="Default")]
    [Parameter(ParameterSetName="OnlyGroups")]
    [Parameter(ParameterSetName="OnlyUsers")]
    [Parameter(ParameterSetName="OnlyFSP")]
    [System.Management.Automation.PSCredential] $Credential,
           
    [Parameter(ParameterSetName="Default")]
    [Parameter(ParameterSetName="OnlyGroups")]
    [Parameter(ParameterSetName="OnlyUsers")]
    [Parameter(ParameterSetName="OnlyFSP")]
    [switch] $Recursive,
           
    [Parameter(ParameterSetName="OnlyGroups")]
    [switch] $OnlyGroups,
           
    [Parameter(ParameterSetName="OnlyUsers")]
    [switch] $OnlyUsers,
           
    [Parameter(ParameterSetName="OnlyFSP")]
    [switch] $OnlyFSP,
           
    [Parameter(ParameterSetName="Default")]
    [Parameter(ParameterSetName="OnlyGroups")]
    [Parameter(ParameterSetName="OnlyUsers")]
    [Parameter(ParameterSetName="OnlyFSP")]
    [ValidateNotNullOrEmpty()]
    [ValidateScript({Test-Path $_ -PathType Container})] # Argument must be an existing folder, not a file.
    [string] $SaveToFolder
)

try   { Import-Module ActiveDirectory -ErrorAction Stop }
catch { Write-Error -Message "ActiveDirectory module could not be imported!" -Category NotInstalled -ErrorAction Stop }

# --- Internal helper function ---
# (More or less the same as 'Test-saoeGUID'...)
function isGUID
{
    [OutputType([bool])]
    Param
    (
        [Parameter(Mandatory = $true)] $Value,
        [switch] $IsValidButEmpty
    )

    $ObjectGuid = [System.Guid]::empty

    # Returns True if successfully parsed, otherwise returns False.
    $result = [System.Guid]::TryParse($Value, [System.Management.Automation.PSReference] $ObjectGuid)

    # Alternative: Trying to match with an elaborated Regular Expression
    # https://morgantechspace.com/2021/01/powershell-check-if-string-is-valid-guid-or-not.html
    # $Value -match("^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$")

    if ($IsValidButEmpty -and ($result -eq $true))
    {
        # Sometimes, placeholder/dummy values are used, like "00000000-0000-0000-0000-000000000000".
        $result = if (([System.Guid]::New($Value)) -eq [System.Guid]::empty) { $true } else { $false }
    }

    return $result
}
# ---x---
  
$properties = 'givenName', 'sn', 'samAccountName', 'userPrincipalName', 'DistinguishedName', 'objectClass', 'msDS-PrincipalName', 'memberof', 'member'
    # Note that the property names may differ; e.g.: Get-ADUser: "Surname" -> Get-ADObject: "sn".

if ($Credential -eq $null) { $HashCredential = @{ <# empty #> } }
else                       { $HashCredential = @{ Credential = $Credential } }
 
ForEach ($Domain in $Domains)
{
    ForEach ($group in $Groups)
    {
        if (isGUID -Value $group) { $groupObject = Get-ADGroup -Filter {ObjectGUID -eq $group} -server $Domain @HashCredential -ResultSetSize:1 -Properties $properties }
        else                      { $groupObject = Get-ADGroup -Filter {(SamAccountName -eq $group) -or (Name -eq $group)} -server $Domain @HashCredential -ResultSetSize:1 -Properties $properties }
        
        if ($groupObject)
        {
            $members = [System.Collections.Generic.List[object]] @()
           
            $groupDN = $groupObject | Select-Object -ExpandProperty 'DistinguishedName'
           
            # Also get the FSP (foreign security principal) members:
            # "You need to get the group object, and enumerate the member property." <https://stackoverflow.com/a/15796657>
            # My note: Without that step, it can work (under certain conditions) also with just Get-ADObject -LDAPFilter... -- but not always/reliable.
            foreach ($gm in $groupObject.Member)
            {
                foreach ($DomainOfFSP in $DomainsOfFSP)
                {
                    if ($gm -like "*ForeignSecurityPrincipals*")
                    {
                        try
                        {
                            $m = [ADSI]("LDAP://" + $gm)
                            $SID = New-Object System.Security.Principal.SecurityIdentifier ($m.objectSid[0], 0)
                            $ado = Get-ADObject -Server $DomainOfFSP -Filter "ObjectSID -eq '$SID'" @HashCredential -Properties $properties
                            $members.Add($ado)
                        }
                        catch
                        {
                            Write-Warning "'$gm': Nothing to be found here -- or did you maybe miss the credentials?"
                        }
                    }
                }
            }
           
            if     ($OnlyGroups) { $LDAPFilter1 = "(objectclass=group)" }
            elseif ($OnlyUsers)  { $LDAPFilter1 = "(objectclass=user)(objectcategory=person)" }
            elseif ($OnlyFSP)    { $LDAPFilter1 = "(objectclass=foreignSecurityPrincipal)" }
            else                 { $LDAPFilter1 = "(objectclass=*)(objectcategory=*)" }
          
            # "1.2.840.113556.1.4.1941 is the OID for LDAP_MATCHING_RULE_IN_CHAIN and LDAP_MATCHING_RULE_TRANSITIVE_EVAL"
            # -- https://ldapwiki.com/wiki/1.2.840.113556.1.4.1941
            if   ($Recursive) { $LDAPFilter2 = "(memberof:1.2.840.113556.1.4.1941:={0})" -f $groupDN }                       
            else              { $LDAPFilter2 = "(memberof:={0})" -f $groupDN }

            $LDAPFilter = "(&{0}{1})" -f $LDAPFilter1, $LDAPFilter2

            Get-ADObject -LDAPFilter $LDAPFilter -Server $Domain @HashCredential -Properties $properties -ResultSetSize $null -ResultPageSize 1000 |
                % { $members.Add($_) }

            if ($SaveToFolder)
            {
               $OutputFile = "$Domain - $group ($(Get-Date -format 'yyyy-MM-dd')).csv"
               $OutputFullPath = Join-Path $SaveToFolder -ChildPath $OutputFile
             
               $members | Export-CSV -Path $OutputFullPath -Encoding UTF8 -NoTypeInformation -Delimiter ';'
               Write-Host "`nList of members saved to '$OutputFullPath'"
            }
            else
            {
               $members
            }
        }
        else
        {
           Write-Warning "Group '$group' does not exist in domain '$Domain'!"
        }
    }
}
