<#
    .Synopsis
    Show ACL of filesystem item (from different domain) with resolved account names (instead SIDs)

    .Description
    Normally, even in trusted domains, a SID (Security-Identified) in an ACL (Access Control List)
    of an filesystem item from a foreign domain will not be translated/resolved; one would only get
    to see the rather cryptic SIDs.
    
    This function resolves/translates these SIDs into readable values instead.
    
    .Parameter Path
    The path to the directory or file whose ACL should be inspected.
    
    .Parameter Server
    One or multiple server(s)/domain(s) in which the path's permission and users should be looked up.
    
    .Parameter ResolveTo
    Resolve the ACL's IdentityReference-SID values to either one of these attributes:
    - 'msDS-PrincipalName' (= DOMAIN\UserID) -> Default
    - 'SamAccountName'
    - 'Name'

    .Example
    Comparison between the FormatTable output of 'Get-ACL' and this function (when called from
    [trusted] domain A for [trusted] domain B):
     
    > (get-acl "\\server\share\directory").Access | ft
     
               FileSystemRights AccessControlType IdentityReference                     [...]
               ---------------- ----------------- -----------------                     [...]
                 ReadAndExecute              Deny S-1-5-21-2234567689-876543210-1234... [...]
            Modify, Synchronize             Allow S-1-5-21-2234567689-876543210-1234... [...]
    ReadAndExecute, Synchronize             Allow S-1-5-21-2234567689-876543210-1234... [...]
                 ReadAndExecute              Deny S-1-5-21-2234567689-876543210-1234... [...]
                    FullControl             Allow NT AUTHORITY\SYSTEM                   [...]
                    FullControl             Allow S-1-5-21-2234567689-876543210-1234... [...]
    ReadAndExecute, Synchronize             Allow S-1-5-21-2234567689-876543210-1234... [...]
            Modify, Synchronize             Allow S-1-5-21-2234567689-876543210-1234... [...]
                    FullControl             Allow BUILTIN\Administrators                [...]

    > Show-ACLWithResolvedSID -Path "\\server\share\directory" -Server example.net | format-table

               FileSystemRights AccessControlType IdentityReference [...]
               ---------------- ----------------- ----------------- [...]
                 ReadAndExecute              Deny SomeUserGroup     [...]
            Modify, Synchronize             Allow A1_Z-rw           [...]
    ReadAndExecute, Synchronize             Allow A1_Zr             [...]
                 ReadAndExecute              Deny AnotherUser       [...]
                    FullControl             Allow NT AUTHORITY\S... [...]
                    FullControl             Allow ABC-F-Projects    [...]
    ReadAndExecute, Synchronize             Allow ABC-R-Projects    [...]
            Modify, Synchronize             Allow ABC-W-Projects    [...]
                    FullControl             Allow BUILTIN\Admini... [...]

    .Example
    > Show-saoeACLWithResolvedSID -Path \\storage.example.net\Data\A012 -Server 'example.net', 'elsewhere.com' | ft
    
    Since the ACL entries can be from different domains, one needs to look up the SIDs on multiple servers.

    .Notes
    Copyright © 2021, 2023-2024 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2021-03-03 (so): Created.
    2023-08-18 (so): Misc.:
        - Changed the return value: Now a PSCustomeObject, instead of a Format-Table (better for further processing).
        - Added parameter '-ResolveTo', for either getting the 'SamAccountName' or the 'Name' attribute of the accounts (before, it was hardwired to 'Name').
        - Renamed parameter '-Domain' to '-Server', for the sake of consistency with other functions and cmdlets.
    2024-05-04 (so): Backporting a change from 2024-01-25 (then applied in the 'original version'):
        - One can now examine several domains/server at once (sometimes accounts from multiple origins are in an ACL).
        - For easier differentiation, "msDS-PrincipalName" (= DOMAIN\UserID) is now the default display format, instead of the SamAccountName (still selectable).
#>

param
(
    [Parameter(Mandatory=$true)] [string] $Path,
    [Parameter(Mandatory=$true)] $Server,
    [ValidateSet("Name", "SamAccountName", "msDS-PrincipalName")] [string] $TranslateTo = "msDS-PrincipalName"
)

function translateSID
{
    param
    (
        [Parameter(Mandatory=$true)] [string] $ID,
        [Parameter(Mandatory=$true)] $Server
    )
   
    $result = $Server | % { Get-ADObject -Server $_ -Filter "ObjectSID -eq '$ID'" -Properties SamAccountName, Name, 'msDS-PrincipalName' }
    $result = $result | ? { ($_.ObjectClass -eq 'user') -or ($_.ObjectClass -eq 'group') }
 
    # Simply pass the ID value through, as-is, if the ID wasn't found in any of the domains as an AD user or group:
    if ([string]::IsNullOrEmpty($result)) { $ID }
    else                                  { $result | select -ExpandProperty $TranslateTo }
}

(Get-ACL -Path $Path).Access |
    Select-Object -Property FileSystemRights, AccessControlType, @{Name="IdentityReference"; Expression={translateSID -ID $_.IdentityReference -Server $Server}}, IsInherited, InheritanceFlags, PropagationFlags
