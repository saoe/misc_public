<#
    .Synopsis
    Returns information about a specific tag in the description or info attribute of an AD group.
   
    .Description
    Examines one or multiple given AD groups for potential tags in a defined set of object
    attributes (e.g. Info or Description).
   
    Returns a collection of custom objects, which have these elements:
    - GroupObject: The actual AD group, with some of its details.
    - HasTag     : A boolean value (i.e. $true or $false) to show whether the group has such a tag.
    - TagValue   : The actual value of the tag (if available).
   
    .Parameter Server
    The domain in which this cmdlet should run.
   
    .Parameter Group
    The AD group that should be analyzed.
   
    Must be a specific and resolvable groupname, i.e. a filter or wildcard expression
    is not allowed here.
   
    .Parameter Attribute
    The attribute of the AD group object which should be analyzed;
    can be "Info" (default) or "Description" at the moment.
   
    .Parameter Tag
    The keyword of the tag (only yet tested with a one single keword (no whitespace)).
   
    .Example
    > Get-saoeTagFromADGroup -server example.net -Group "SomeGroup", "AnotherGroup", "YetAnotherGroup" -Tag "LabelName"
   
    Handover multiple groups as an argument to the parameter set.
   
    .Example
    > "SomeGroup", "AnotherGroup", "YetAnotherGroup" | Get-saoeTagFromADGroup -server example.net -Tag "LabelName"
   
    Handover multiple groups via the pipeline.
  
    .Example
    > Get-saoeTagFromADGroup -server example.net -Group "SomeGroup" -Tag "LabelName" | select @{n="sam";e={($_.GroupObject).SamAccountName}}, TagValue
   
    How to access details of a AD group (here: The samaccountname).
   
    .Example
    > "SomeGroup", "AnotherGroup", "YetAnotherGroup" | Get-saoeTagFromADGroup -server example.net -Tag "LabelName" | ? {$_.HasTag -eq $true} | select @{n="sam";e={($_.GroupObject).SamAccountName}}, TagValue
   
    Only show the groups that have a tag.
 
    .Notes
    Copyright © 2021, 2023 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2021-09-06 (so): This is a more generic/flexible version of a function that I wrote some days ago for in-house use.
    2023-01-30 (so): Attribute texts/string are split explicitly into an array of separate lines (defined by an trailing '\n');
                     see below for details on why.
#>

Param
(
    [Parameter(Mandatory, ValueFromPipeline)]
    [String] $Server,
   
    [Parameter(Mandatory, ValueFromPipeline)]
    [ValidateScript({ Get-ADGroup -Server $Server -Identity $_ })]
    $Group,
   
    [ValidateSet("Info", "Description")]
    $Attribute = "Info",
   
    [Parameter(Mandatory)]
    [String] $Tag
)

try   { Import-Module ActiveDirectory -ErrorAction Stop }
catch { Write-Error -Message "ActiveDirectory module could not be imported!" -Category NotInstalled -ErrorAction Stop }
 
Begin
{
    [regex] $pattern = "($Tag)(\s*)([:\-=]*)(\s*)(\d*|.*)"
        # Group 1: The keyword for the tag
        # Group 2, 3, 4: The seperator: Any combination of optional whitespace, followed by an optional predefined separator symbol, followed by an optional whitespace again.
        # Group 5: The value of the tag, be it decimal or any other characters (= free text).
        # That should match expressions like for example:
        # TagID: 01234
        # TagID=01234
        # TagID - Free Flowing Text
        # TagID 01234
   
    $results = [System.Collections.Generic.List[object]] @()
}
 
Process
{
    ForEach ($g in $Group)
    {
        $details = [PSCustomObject] @{
            GroupObject = $null
            HasTag      = $false
            TagValue    = $null
        }
       
        try
        {
            $grp = Get-ADGroup -Server $Server -Identity $g -Property $Attribute
            $details.GroupObject = $grp
            
            # Split the text explicitly into an array of separate lines (defined by an trailing '\n'),
            # then investigate each line individually for the pattern.
            #    The reason why we do it that way, instead of feeding a whole text-block/string (like 'Info') to the RegEx:
            # When playing around with 'Info', the "One-RegEx-for-all" approach somehow didn't detect newlines/linefeed in some cases (and I experimented a lot on that day with RegEx...).
            $all_lines = $grp.$Attribute -split '\n'
            
            $all_lines | % { $_ | select-string -pattern $pattern |
                    % { $details.HasTag   = $true;
                        $details.TagValue = ($_.matches.groups[5].Value).trim() } }
           
            $results.Add($details)
        }
        catch
        {
            Write-Error "Something went wrong"
        }
    }
}
 
End
{
    $results
}
