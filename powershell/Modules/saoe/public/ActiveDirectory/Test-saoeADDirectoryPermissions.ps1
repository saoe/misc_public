<#
    .Synopsis
    Find directories in a directory hierachy, where a specific AD account is (not) present in the ACL.
 
    .Description
    Analyze the subdirectories of a given starting directory whether a specific AD account (an user or group) is present (or not)
    in the individual ACL of a directory, and if present, whether by inheritance or nor(or directly).
    
    The parameters control the search criteria.
    
    Some implementation notes:
    - Instead of the friendly names (like DOMAIN\User.Name), only the Security-IDs (SID) will be used
      for comparison. That is to prevent issues in multi-langual environments.
      For system/built-in accounts, these are well-known SIDs anayway.
      (https://docs.microsoft.com/en-us/windows/win32/secauthz/well-known-sids)
 
    - Internally (and visible in the output), the paths will be modified (adding some \\?...-prefix),
   
    .Parameter Server
    The server, where the StartDirectory and Account are located.
   
    .Parameter StartDirectory
    The starting point of the directory hierachy that should be analyzed; will be searched
    recursively, minimum one level.

    Take note: The search will begin with the subdirectories, not the starting directory itself!
   
    .Parameter Depth
    '0': Unlimited recursion, so all subdirectories will be analyzed [DEFAULT]
    'n': (n = integer number) Up to n (inclusive) subdirectory levels will be analyzed.
 
    .Parameter Account
    The AD object (user or group) that should be considered in the analysis.
    If the account can't be found, the function will exit with an error.
   
    Take not: Due to some technical limitations, a few built-in accounts need special treatment:
    - use "System" for 'NT AUTHORITY\SYSTEM' or local notations of it; it's the same SID.
    - use "Administrators" for 'BUILTIN\Administrators' or local notations of it; it's the same SID.
    - use "AuthUsers" for 'NT AUTHORITY\Authenticated Users' or local notations of it; it's the same SID.
    - use "Users" for 'BUILTIN\Users' or local notations of it; it's the same SID.
    - use "Everyone" for 'Everyone' or local notations of it; it's the same SID.
 
    .Parameter AccountServer
    ~ not well tested yet ~
    In case you know that the account is from another domain, or you know only the SID of the account,
    then (also) this server will be contacted.
 
    .Parameter AccountMustNotBePresentInACL
    [Switch] List only the directories where the account must not be present in the ACL.
 
    .Parameter IgnoreInheritedPermissions
    [Switch] List only the directories where the account is set explicitly/directly in the ACL of
    the respective directory; ignore inherited permissions for it.
 
    .Notes
    Copyright © 2022 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    2021-05-24 First, simple version for a specific project (domain move).
    2022-02-24 Release day of the big overhaul and extension work.
 
    TODO:
    - *** Anderer Name: Nicht Test-, eher Search-/Find- (Show- ist vlt. zu ausgabe-orientiert)
    - [?] Analyzing accounts in the ACL that stem from a different domain? Might be tricky...:
          * Had a test case at work: Other domain was a friendly trust, but Get-ADDomain didn't work, despite -Credential.
          * Generally: Then what? Comparing with what? Domain-SID with Domain-SID?
            But all the built-in accounts then need more special care, because those well-known SIDs don't
            represent/contain the domain SIDs...
    - [?] Consider 'CREATOR OWNER' too?
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true)]
    $Server,
   
    [Parameter(Mandatory=$true)]
    [ValidateScript({ Test-Path -Path $_ -PathType Container })]
    $StartDirectory,
   
    <# Optional #>
    [int] $Depth = 0,
 
    [Parameter(Mandatory=$true)]
    $Account,
   
    <# Optional #>
    $AccountServer,
   
    <# Optional #>
    [switch] $IgnoreInheritedPermissions,
   
    <# Optional #>
    [switch] $AccountMustNotBePresentInACL
)

# Not yet used, but maybe useful in the future.
$DomainSID = (get-addomain $Server).DomainSID
if ($AccountServer) { $AccountServerDomainSID = (get-addomain $AccountServer).DomainSID }
 
$StartDirectory = join-path -path $StartDirectory -childpath '\'
    # To work around a weird issue: If you provide just a drive letter (e.g. 'X:'), then the current working directory on that drive will be used!
 
# --------------------------------------------------------------------------------------------------
# Helper function to handle very long paths (in deeply nested directory hierarchies, for example).
 
function convertPath ($Path)
{
    if (([System.Uri] $Path).IsUNC)
    {
        $x = (Join-Path -Path \\?\UNC -ChildPath $Path).replace('\\?\UNC\\', '\\?\UNC\')
    }
    else
    {
        $x = Join-Path -Path "\\?" -ChildPath $Path
    }

    $x
}
 
# --------------------------------------------------------------------------------------------------
# 1. Check if the account exists.
 
# You won't get a normal user/group object for some of the built-in accounts, so let's work around it:
# (These are all 'well-known' SIDs: https://docs.microsoft.com/en-us/windows/win32/secauthz/well-known-sids)
$SpecialAccounts = @{
    'SYSTEM'         = 'S-1-5-18'
    'Administrators' = 'S-1-5-32-544' 
    'AuthUsers'      = 'S-1-5-11'
    'Users   '       = 'S-1-5-32-545'
    'Everyone'       = 'S-1-1-0' 
}

if ($SpecialAccountSID = $SpecialAccounts[$Account])
{
    $IsSpecialAccount = $true
    $ADObject = $null
    $ADObject_PrincipalName = (new-object System.Security.Principal.SecurityIdentifier($SpecialAccountSID)).Translate([System.Security.Principal.NTAccount]).Value
    $ADObject_SID = $SpecialAccountSID
}
else # And here a we handle the normal case:
{
    $IsSpecialAccount = $false
   
    # Is it a group object?
    try
    {
        $ADObject = get-adgroup -server $Server -identity $Account -properties 'msDS-PrincipalName'
        $ADObject_PrincipalName = $ADObject.'msDS-PrincipalName'
        $ADObject_SID = $ADObject.SID
        write-verbose "$Account is an AD group object"
    }
    catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]
    {
        $ADObject = $null
    }
 
    # ... or an user object?
    if (-not $ADObject)
    {
        try
        {
            $ADObject = get-aduser -server $Server -identity $Account -properties 'msDS-PrincipalName'
            $ADObject_PrincipalName = $ADObject.'msDS-PrincipalName'
            $ADObject_SID = $ADObject.SID
            write-verbose "$Account is an AD user object"
        }
        catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]
        {
            $ADObject = $null
        }
    }
 
    # Or maybe an object from another planet, err... domain and can't be resolved?
    if (-not $ADObject)
    {
        try
        {
            #$ADObject = Get-ADObject -Server $AccountServer -Filter { ObjectSID -eq $Account } -properties 'msDS-PrincipalName'
            $ADObject = Get-ADObject -Server $AccountServer -Filter { SamAccountName -eq $Account } -properties 'msDS-PrincipalName'
            $ADObject_PrincipalName = $ADObject.'msDS-PrincipalName'
            $ADObject_SID = $ADObject.ObjectSID
            write-verbose "$ADObject_PrincipalName is from domain $AccountServer"
        }
        catch [System.Management.Automation.ParameterBindingException]
        {
            $ADObject = $null
        }
        catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]
        {
            $ADObject = $null
        }
    }
}
 
if ((-not $ADObject) -and (-not ($IsSpecialAccount)))
{
    write-error -message "Could not find '$($Account)'" -category ObjectNotFound -Exception ([System.IO.FileNotFoundException]::new()) -ErrorAction Stop
}
else
{
    write-host "Looking for '$ADObject_PrincipalName' ($ADObject_SID)"
}
 
# --------------------------------------------------------------------------------------------------
# 2. Get the subdirectories and analyze its ACLs.
 
switch ([int] $Depth)
{
    0 { $RecursionDepthArg = @{ Recurse = $true } }
        # Unlimited: Get all subdirectories!
   
    default { $RecursionDepthArg = @{ Recurse = $true; Depth = ($Depth - 1) } }
        # From a user's perspective, depth 1 from a staring directory is the first leve of subdirectories
        # But since we need to use Get-ChildItem (because of -Recursive and -Depth), the first level is already
        # implicitly considered ("get the child items from starting point x"), so depth 1 would take us down 2 levels!
        # To keep expectations and reality in sync, we manually reduce the level by one.
}
 
$count_directories = 0
 
write-host "------------------------------------------------------------"
if ($Depth -eq 0) { $Dir = "all"} else { $Dir = $Depth }
write-host "Collecting recursivley $Dir levels of subdirectories beneath '$StartDirectory'..."
$TempPath1 = convertPath -Path $StartDirectory # Um lange Pfade verarbeiten zu koennen.
$subdirectories = get-childitem -LiteralPath $TempPath1 -Directory @RecursionDepthArg
    # Weird issue: Using -LiteralPath is important, otherwise -Depth will be ignored when using the \\?\ notation!
 
$results = [System.Collections.Generic.List[object]] @()
 
write-host "Analyzing subdirectories..."
foreach ($current_directory in $subdirectories)
{
    $percent = [math]::Round($count_directories/@($subdirectories).Count*100)
    Write-Progress `
    -Id 1               <# The ID Must be an integer value #> `
    -Activity "Working..." `
    -Status "Directory $count_directories of $(@($subdirectories).count) in the hierarchy of '$StartDirectory' (using a recursion level of $Depth)" `
    -CurrentOperation "$percent%" `
    -PercentComplete $percent
   
    $ConvertedDirectoryPath = $current_directory.PSPath | Convert-Path
 
    $dir_acls = (get-item $ConvertedDirectoryPath -force).getaccesscontrol('access')
        # Some directories may (unintentionally) be set to 'hidden'; Get-Item will then complain if one doesn't use -force.
   
    foreach ($dir_acl in $dir_acls)
    {
        $details = [PSCustomObject] @{
            Path        = $ConvertedDirectoryPath
            Account     = $ADObject_PrincipalName
            IsInACL     = $null # Is the account present in the acl? True|False.
            IsInherited = $null # Is the permission inherited? True|False.
        }
        
        $dir_acl_access = $dir_acl.Access
       
        if ($AccountMustNotBePresentInACL)
        {
            # If the account should NOT be present in the ACL.
            if ($dir_acl_access.IdentityReference.Translate([System.Security.Principal.SecurityIdentifier]).Value -NotContains $ADObject_SID)
            {
                write-verbose "'$($ADObject_PrincipalName)' is missing in the ACL of '$ConvertedDirectoryPath'"
                $details.IsInACL = $false
                $results.Add($details)
            }
        }
        else
        {
            # If the account should be present in the ACL.
            foreach ($i in $dir_acl_access)
            {
                if (($i.IdentityReference.Translate([System.Security.Principal.SecurityIdentifier]).Value -eq $ADObject_SID) -and (-not $AccountMustNotBePresentInACL))
                {
                    if ($IgnoreInheritedPermissions)
                    {
                        if ($i.IsInherited -eq $false)
                        {
                            # Set as a direct permission on this directory.
                            write-verbose "'$($ADObject_PrincipalName)' is set explicitly in the ACL of '$ConvertedDirectoryPath'"
                            $details.IsInherited = $false
                            $details.IsInACL = $true
                            $results.Add($details)
                        }
                    }
                    else
                    {
                        if ($i.IsInherited -eq $false)
                        {
                            # Set as a direct permission on this directory.
                            write-verbose "'$($ADObject_PrincipalName)' is set explicitly in the ACL of '$ConvertedDirectoryPath'"
                            $details.IsInherited = $false
                            $details.IsInACL = $true
                            $results.Add($details)
                        }
 
                        if ($i.IsInherited -eq $true)
                        {
                            # Permission to this directoy is inherited.
                            write-verbose "'$($ADObject_PrincipalName)' has earned the permissions to '$ConvertedDirectoryPath' by inheritance"
                            $details.IsInherited = $true
                            $details.IsInACL = $true
                            $results.Add($details)
                        }
                    }
                }
            }
        }
    }
   
    $count_directories++
}
 
write-host "------------------------------------------------------------"
write-host "$(@($subdirectories).count) subdirectories in total beneath $StartDirectory have been analyzed"
write-host ""
write-host "Found $(@($results).count) hits that match the search criteria:"
if ($IgnoreInheritedPermissions) { write-host "- Ignore inherited permissions" } else { write-host "- Respect inherited permissions"}
if ($AccountMustNotBePresentInACL) { write-host "- Account '$ADObject_PrincipalName' must NOT be present in the permissions"  } else { write-host "- Account '$ADObject_PrincipalName' must BE present in the permission" }
write-host ""
write-host "Hint: When the paths are too long, the other columns will be cut-off in the console output; "
write-host "      the save this function's output to a variable and format/filter it from there, as you wish."
 
$results
