<#
    .Synopsis
    Replace for all users in $OUSearchBase the group membership from $CurrentGroup to $NewGroup.
    
    .Parameter OUSearchBase
    Mandatory. The starting point OU, from where all users that should be updated, are taken.
      
    .Parameter CurrentGroup
    Mandatory. Name of the AD group, which membership should be removed from the users.
    
    .Parameter NewGroup
    Mandatory. Name of the AD group, which membership should be added to the users.
    
    .Example
    Update-saoeADGroupMembership -OUSearchBase "OU=Users,OU=Location,DC=de,DC=xxx,DC=net" -CurrentGroup "Group-A" -NewGroup "Group-B"
    
    .Notes
    Copyright © 2017 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2017-05-26 (so): Created.
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true)] [String] $OUSearchBase, 
    [Parameter(Mandatory=$true)] [String] $CurrentGroup, 
    [Parameter(Mandatory=$true)] [String] $NewGroup
)

try   { Import-Module ActiveDirectory -ErrorAction Stop }
catch { Write-Error -Message "ActiveDirectory module could not be imported!" -Category NotInstalled -ErrorAction Stop }

$cred = Get-Credential

Get-ADUser -SearchBase $OUSearchBase -Filter * | ForEach-Object {
    $ListOfGroups = Get-ADPrincipalGroupMembership $_ | select name
    if ($ListOfGroups -match "$CurrentGroup")
    {
        Add-ADGroupMember -Identity '$NewGroup' -Credential $cred -Members $_
        Remove-ADGroupMember -Identity '$CurrentGroup' -Credential $cred -Members $_
    }
}
