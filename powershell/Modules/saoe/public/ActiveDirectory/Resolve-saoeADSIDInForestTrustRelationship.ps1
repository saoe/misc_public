<#
    .Synopsis
    Resolve the SID (Security Identifier) of a 'Foreign Security Principal' AD object within an ActiveDirectory forest.

    .Description
    Starting with the name of the (forest root) domain (as the parameter "Domain"), this function
    will first pan out and collect all other domains which have a fitting trust relationship with
    it and will then check those trusted domains for the given "SID".
    
    Needs another function of my 'saoe' module to work: 'Get-saoeADForestTrustRelationship'.

    .Parameter Domain
    The name of the (forest root) domain, whose trusted domains will be scanned.
    
    .Parameter SID
    The Security Identifier for which the matching AD object should be found.

    .Outputs
    Returns an AD object, if a match is found.

    .Example
    > $o = Resolve-saoeADSIDInForestTrustRelationship -Domain "example.net" -SID "S-1-2-34-567890123-4567890123-456789012-3456"

    .Notes
    Copyright © 2021 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2021-01-18 (so): Created.
#>

param
(
    [Parameter(Mandatory=$True)] [string] $Domain,
    [Parameter(Mandatory=$True)] [string] $SID
)

try   { Import-Module ActiveDirectory -ErrorAction Stop }
catch { Write-Error -Message "ActiveDirectory module could not be imported!" -Category NotInstalled -ErrorAction Stop }

ForEach ($t in (Get-saoeADForestTrustRelationship -Domain $Domain)."TrustedDomains")
{
    if ($SID -like "$($t.DomainSid)*")
    {
        Get-ADObject -Filter "objectSid -eq '$SID'" -server $t.DnsName
    }
}
