<#
    .Synopsis
    Set a manager for an AD group.

    .Description
    This function can set/overwrite or remove (clear) an account that entry that shows up under the
    attribute "ManagedBy" (and the ACL) of an AD group.
    (In the MMC GUI, it will be visible on the tab 'Managed by' and the tab 'Security'.)

    If the entry is a normal account, the data here is strictly informative:
    Any attempt by that user to modify the membership list will fail (execept if the account had
    been granted those permissions otherwise, of course).

    On the other hand, if the option "Manager can update membership list" is set, even a normal user
    without any other admin rights can add and remove members to that group.

    .Parameter Server
    In which domain should the AD group and the manager account be looked up?

    .Parameter Group
    The AD group for which the manager should be set.

    .Parameter Manager
    The AD account that should be set as the new manager for the specified AD group.
    It will overwrite any existing value (but will also print the current value during that action).

    The account can point to an AD user object or an AD group object.

    .Parameter Clear
    Removes any existing manager entry, so that this attribute will be empty again.
    It takes also care of the corresponding entry in the ACL (which, by the way, doesn't get
    modified if you do the same via the MMC/GUI, as far as I could see during my tests!
    So the MMC/GUI way may clutter up the ACL over time. Not sure if the entry there alone grants
    enough permissions; probably not).

    .Parameter CanUpdateMembershipList
    A switch parameter: If provided, the specified manager may also add or remove members to this
    group, even if the account normally would have not enough permissions (e.g. a normal user)

    It's visible in the MMC as a checkbox on the tab 'Managed by' ("Manager can update membership list")
    and in the 'Security' tab (the ACL) as "Special Permissions"; but it's not directly visible in
    the attributes of a Powershell ADGroup object; one would have to dig deeper in the ACL of that group.

    .Example

    .Notes
    Copyright © 2022 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2022-04-29 (so): Started it on the 27th; now it's been tested, improved and is ready for usage.
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true)]
    [Parameter(ParameterSetName="Set")]
    [Parameter(ParameterSetName="Unset")]
    $Server,

    [Parameter(Mandatory=$true)]
    [Parameter(ParameterSetName="Set")]
    [Parameter(ParameterSetName="Unset")]
    $Group,

    [Parameter(ParameterSetName="Set")]
    $Manager,

    [Parameter(ParameterSetName="Unset")]
    [switch] $Clear,

    [Parameter(ParameterSetName="Set")]
    [switch] $CanUpdateMembershipList
)

# --------------------------------------------------------------------------------------------------
# Global variables etc. for this function's scope:

$group_obj = $null
$cur_manager_obj = $null
$new_manager_obj = $null

New-PSDrive -Name AD2 -PSProvider ActiveDirectory -Server $Server -root "//RootDSE/" | out-null
    # To get the ACL of an AD object, one must speak to its Active Directory (AD).
    # But since this function may be called upon a different domain/server than the one of the caller's
    # current environment (see parameter 'Server'), the implicit "AD:" prefix may point to the wrong AD.
    # For that reason, we explictly connect to the AD of the desired domain/server and address it with "AD2:".

# --------------------------------------------------------------------------------------------------
# Helper function: Clean the security/ACL from the previous/current mananger.
#
# Note: Interesting observation in regards to handling this via MMC/GUI:
#       If one removes the entry via MMC/GUI, a refresh process is often needed (save, close, refresh, open window again),
#       otherwise, the old ACE will remain visible in the security tab/ACL...

function removeCurrentManagerFromACL
{
    param ($GroupObject)

    $GroupDN = $GroupObject.DistinguishedName
    $GroupACL = Get-Acl "AD2:$GroupDN"

    foreach ($ACE in $GroupACL.Access)
    {
        $value = $ACE.IdentityReference.Value

        if (($value -eq $cur_manager_obj.'msDS-Principalname') -or ($value -eq $CurManagerSID))
        {
            $GroupACL.RemoveAccessRule($ACE) | out-null
            Set-Acl -Path AD2:$GroupDN -AclObject $GroupACL -ErrorAction Stop
        }
    }
}

# --------------------------------------------------------------------------------------------------
# Get the object of the target AD group (and also any existing 'ManagedBy' value):

try { $group_obj = get-adgroup -server $Server -identity $Group -properties ManagedBy }
catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException] { write-warning "Group $Group doesn't exist in $($Server)!"; break }
catch                                                                    { write-warning "$_"; break }

# --------------------------------------------------------------------------------------------------
# Determine whether this AD group already has a manager:

if ([string]::IsNullOrEmpty($group_obj.ManagedBy))
{
    write-host "Group '$($group_obj.samaccountname)': No current manager found."
}
else
{
    # First, check if the detected current manager is an AD user object, else try to find a AD group object); if not, abort.

    try   { $cur_manager_obj = get-aduser -server $Server -identity $group_obj.ManagedBy -properties 'msDS-Principalname' }
    catch { <# ignorieren #> }

    if (-not ($cur_manager_obj))
    {
        try { $cur_manager_obj = get-adgroup -server $Server -identity $group_obj.ManagedBy -properties 'msDS-Principalname' }
        catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException] { write-warning "An AD account '$($Manager)' doesn't exist in $($Server)!"; break }
        catch                                                                    { write-warning "$_"; break }
    }

    $CurManagerSID = $null

    if ($cur_manager_obj)
    {
        write-host "Group '$($group_obj.samaccountname)': The current manager is '$($cur_manager_obj.samaccountname)' ($($cur_manager_obj.name))."
        $CurManagerSID = $cur_manager_obj.SID
    }
}

# --------------------------------------------------------------------------------------------------

if ($Clear)
{
    # In case any existing current manager should be removed (empty/clear the value completely):

    if ($cur_manager_obj)
    {
        set-adgroup -server $Server -identity $group_obj -ManagedBy $null
        removeCurrentManagerFromACL -GroupObject $group_obj
        write-host "Group '$($group_obj.samaccountname)': The current manager was removed."
    }
    else { write-host "Group '$($group_obj.samaccountname)': No current manager found." }
}
else
{
    # In case the manager should be set to a new value (writes new or overwrites an existing value):
    #
    # First, check if the specified new manager is available as an AD object (be it an user or a
    # group object); if not, the function will abort.

    try { $new_manager_obj = get-aduser -server $Server -identity $Manager -properties 'msDS-Principalname' }
    catch { }

    if (-not ($new_manager_obj))
    {
        try { $new_manager_obj = get-adgroup -server $Server -identity $Manager -properties 'msDS-Principalname' }
        catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException] { write-warning "An AD account '$($Manager)' doesn't exist in $($Server)!"; break }
        catch                                                                    { write-warning "$_"; break }
    }

    try
    {
        # Set the new manager and also remove any previous one from the group's ACL:

        if ($cur_manager_obj)
        {
            removeCurrentManagerFromACL -GroupObject $group_obj
        }
        set-adgroup -server $Server -identity $group_obj -ManagedBy $new_manager_obj
        write-host "Group '$($group_obj.samaccountname)': Set new manager '$($new_manager_obj.samaccountname)' ($($new_manager_obj.name))."
    }
    catch { write-warning "$_"; break }

    # --------------------------------------------------------------------------------------------------

    if ($CanUpdateMembershipList)
    {
        # In case the specified manager should also be able to add or remove members, even as a
        # normal user without any other special adminstrative permissions in the AD.
        #
        # Unfortunately this is a bit more laborious than a simple one-line expression.
        # Therefore, many thanks to the following articles for showing me the main parts (even
        # though some of it required a bit of refactoring for my special demands).
        # Not listed here (since I forgot to write them down) are some other minor resources
        # and single forum posts that helped me with some detail work...:
        #
        # - https://witit.blog/distribution-groups-manager-can-update-membership-with-powershell/
        # - http://vcloud-lab.com/entries/active-directory/powershell-active-directory-adgroup-managedby-checkbox-manager-can-update-membership-list
        # - https://docs.microsoft.com/de-de/archive/blogs/blur-lines_-powershell_-author_shirleym/manager-can-update-membership-list-part-1

        $GroupDN = $group_obj.DistinguishedName
        $GroupACL = Get-Acl "AD2:$GroupDN"

        $NewManagerSID = $new_manager_obj.SID

        $ctrl =[System.Security.AccessControl.AccessControlType]::Allow
        $rights = [System.DirectoryServices.ActiveDirectoryRights]::WriteProperty -bor [System.DirectoryServices.ActiveDirectoryRights]::ExtendedRight

        $guid = [GUID] "bf9679c0-0de6-11d0-a285-00aa003049e2"
            # The AD schema has a System ID expressed as a GUID for each attribute;
            # we need this: <https://docs.microsoft.com/en-us/windows/win32/adschema/a-member>.

        $rule = New-Object System.DirectoryServices.ActiveDirectoryAccessRule($NewManagerSID, $rights, $ctrl, $guid)
        $GroupACL.AddAccessRule($rule)
        Set-Acl -path "AD2:$GroupDN" -acl $GroupACL

        $GroupACL2 = Get-Acl "AD2:$GroupDN"
        $access_modified = $GroupACL2.Access | Where-Object { ($_.IdentityReference -eq $new_manager_obj.'msDS-Principalname') -or ($_.IdentityReference -eq $NewManagerSID) }

        if ($access_modified -eq $null) { write-warning "Group '$($group_obj.samaccountname)': Couldn't set option 'Manager can update membership list'!" }
        else                            { write-Host "Group '$($group_obj.samaccountname)': Option 'Manager can update membership list' was set." }
    }
}
