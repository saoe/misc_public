<#
    .Synopsis
    Find $user (as account name, first/given name or surname) in one single domain, or search accross all domains.

    .Parameter User
    Mandatory. The user to be found.
      
    .Parameter Domain
    Optional. Singular domain to be searched.

    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true)] [String] $User,
    [String[]] $Domains = @("default1.example.net",
                            "default2.example.net",
                            "default3.example.net")
)

try   { Import-Module ActiveDirectory -ErrorAction Stop }
catch { Write-Error -Message "ActiveDirectory module could not be imported!" -Category NotInstalled -ErrorAction Stop }

foreach ($d in $Domains)
{
    $users | Foreach { Get-ADUser -Filter "Name -eq '$User'-or GivenName -eq '$User' -or Surname -eq '$User'" -Server $d }
}
