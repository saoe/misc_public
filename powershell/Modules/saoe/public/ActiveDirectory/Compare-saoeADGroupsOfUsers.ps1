<#
    .Synopsis
    Compare the group memberships of two AD user accounts.
 
    .Description
    Gets the direct group memberships of 'User A' and 'User B' and shows the differences between
    both accounts, either as a list or as a table.
   
    .Parameter UserA
    AD user account 'A', whose (direct) groups will be compared with those of AD user account 'B'.
    (Can be provided as a SamAccountName, a DistinguishedName or as a Powershell object.)
   
    .Parameter UserB
    AD user account 'B', whose (direct) groups will be compared with those of AD user account 'A'.
    (Can be provided as a SamAccountName, a DistinguishedName or as a Powershell object.)
 
    .Parameter Server
    The server (or domain) in which the users will be looked up.
   
    .Parameter Format
    The format in which the result of the comparison will be presented (the default is "Table"):
   
    "Table": Group    User A  User B
             -------  ------  ------
             Group 1          X
             Group 2   X      X
             Group 3   X
             ...
   
    "List":  A list with three sections:   
             
             * AD groups, in which both users are members.
             * AD groups, in which only user 'A' is a member.
             * AD groups, in which only user 'B' is a member.
   
    .Example
    Compare-saoeADGroupsOfUsers -UserA 'user001' -UserB 'user002' -Server x
   
    .Example
    Compare-saoeADGroupsOfUsers -UserA 'user001' -UserB 'user002' -Server x -Format Table
 
    .Notes
    Copyright © 2023 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2023-10-05 (so): Created (elsewhere).
    2023-10-18 (so): Added to my repository.
#>
 
[CmdletBinding()]
Param
(
    [Parameter(Mandatory = $true)] $UserA,
    [Parameter(Mandatory = $true)] $UserB,
    [ValidateSet("List", "Table")] $Format = "Table",
    [Parameter(Mandatory = $true)] $Server
)
 
$a = get-aduser -Server $Server -Identity $UserA -properties MemberOf
$b = get-aduser -Server $Server -Identity $UserB -properties MemberOf
 
$all = Compare-Object -ReferenceObject ($a.MemberOf | sort) -DifferenceObject ($b.MemberOf | sort) -IncludeEqual
 
switch ($Format)
{
    "List"
    {
        write-host "`r`nAD groups in which both users are members: $($a.SamAccountName) ($($a.Name)) and $($b.SamAccountName) ($($b.Name))" -BackgroundColor DarkGreen -ForegroundColor White
        $all | ? { $_.SideIndicator -eq '==' } | select -expand InputObject | % { get-adgroup -server $Server -identity $_ } | sort SamAccountName | select SamAccountName, DistinguishedName | Format-Table -AutoSize
 
        write-host "AD groups in which only one of the two users is a member: $($a.SamAccountName) ($($a.Name))" -BackgroundColor Blue -ForegroundColor Yellow
        $all | ? { $_.SideIndicator -eq '<=' } | select -expand InputObject | % { get-adgroup -server $Server -identity $_ } | sort SamAccountName | select SamAccountName, DistinguishedName | Format-Table -AutoSize
 
        write-host "AD groups in which only one of the two users is a member: $($b.SamAccountName) ($($b.Name)) is a member:" -BackgroundColor DarkRed -ForegroundColor White
        $all | ? { $_.SideIndicator -eq '=>' } | select -expand InputObject | % { get-adgroup -server $Server -identity $_ } | sort SamAccountName | select SamAccountName, DistinguishedName | Format-Table -AutoSize
    }
   
    "Table"
    {
        $all | sort InputObject | select @{n="Group";e={(get-adgroup -server $Server -identity $_.InputObject).SamAccountName}}, `
                                         @{n="$($a.SamAccountName) ($($a.Name))";e={switch ($_.SideIndicator) { '==' {"X"}; '<=' {"X"}; '=>' {" "} }}}, `
                                         @{n="$($b.SamAccountName) ($($b.Name))";e={switch ($_.SideIndicator) { '==' {"X"}; '<=' {" "}; '=>' {"X"} }}} |
                                    Format-Table -AutoSize
    }
 
    Default # Just in case (should normally never be encountered due to the param ValidateSet at the top).
    {
        write-warning "Unknown format parameter: $Format"
    }
}
 