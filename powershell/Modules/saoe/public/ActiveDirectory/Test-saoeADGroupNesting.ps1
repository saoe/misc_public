<#
    .Synopsis
    Test, whether a certain AD group-nesting-combination is allowed.
    
    .Description
    Some combinations of nestings are not allowed for AD groups (e.g. an universal AD group cannot
    be a member of a global AD group):
    
            Can be a member?:   | Machine Local | Domain Local      | Global            | Universal 
    ----------------------------+---------------+-------------------+-------------------+----------
    Parent group: Machine Local |    No         | Yes               | Yes               | Yes            
    Parent group: Domain Local  |    No         | Yes (same domain) | Yes               | Yes            
    Parent group: Global        |    No         | No                | Yes (same domain) | No          
    Parent group: Universal     |    No         | No                | Yes               | Yes    

                                                   -- Source: https://ss64.com/nt/syntax-groups.html
    
    With this function, one can check it beforehand, instead of waiting that Add-ADGroupMember
    throws an exception. -- NOT ALL COMBINATIONS ARE TESTED YET!
 
    It checks the combination returns 'true', if everything is OK;
    or returns 'false' if an error is encountered.
    
    Note: If multiple groups should be checked in one run, the function will abort with a 'false'
          as soon as it hits just one of those groups that cannot be nested!
   
    Important: This function ONLY CHECKS the possibility of group nesting; to make the group(s)
               really a member of the other group, one must do it by another way!

    .Parameter Server
    The server/domain in which the groups are located.
   
    .Parameter Parent
    The 'parent' AD group, to which the other groups may be added as members.
   
    .Parameter Members
    One or multiple groups that will be checked (if nesting them to the parent group is possible).
   
    .Example
    Test-saoeADGroupNesting -Server example.net -Parent 'Gruppe-X' -Members 'Gruppe-Y1', 'Gruppe-Y2'
 
    .Notes
    Copyright © 2023 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    2023-10-13 (so): Created (for my dayjob); not all combinations are tested!
#>

 
[CmdletBinding()]
Param
(
    [Parameter(Mandatory = $true)] $Server,
    [Parameter(Mandatory = $true)] [string] $Parent,
    [Parameter(Mandatory = $true)] [array]  $Members
)
 
try
{
    $ParentObject = get-adgroup -server $Server -identity $Parent -ErrorAction Stop
    $MemberObjects = $Members | % { get-adgroup -server $Server -identity $_ -ErrorAction Stop }
 
    ForEach ($MemberObject in $MemberObjects)
    {
        switch ($MemberObject.GroupScope)
        {
            # 'Machine Local' is not checked: Too uncommon.
           
            'DomainLocal'
            {
                if ($ParentObject.GroupScope -eq 'Global')
                {
                    write-host "A domain-local AD group ($($MemberObject.Name)) cannot become a member of a global AD group ($($ParentObject.Name)) werden!" -BackgroundColor DarkRed -ForegroundColor White
                    return $false
                }
                elseif ($ParentObject.GroupScope -eq 'Universal')
                {
                    write-host "A domain-local AD group ($($MemberObject.Name)) cannot become a member of an universal AD group ($($ParentObject.Name)) werden!" -BackgroundColor DarkRed -ForegroundColor White
                    return $false
                }
                else { return $true }
            }
           
            'Global' { return $true }
           
            'Universal'
            {
                if ($ParentObject.GroupScope -eq 'Global')
                {
                    write-host "A universal AD group ($($MemberObject.Name)) cannot become a member of a global AD group ($($ParentObject.Name)) werden!" -BackgroundColor DarkRed -ForegroundColor White
                    return $false
                }
                else { return $true }
            }
           
            Default { return $true }
        }
    }
}
catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]
{
    write-host "$($_.Exception.Message)" -BackgroundColor Black -ForegroundColor Red
    return $false
}
catch
{
    write-host "$($_.Exception.Message)" -BackgroundColor Black -ForegroundColor Red
    return $false
}
 