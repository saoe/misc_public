<#
    .Synopsis
    Rename an AD group (and all necessary attributes).

    .Description
    One pitfall of renaming an existing AD group is the fact that the usual suspects like
    'Rename-ADObject' don't change the samAccountName (a cmdlet 'Rename-ADGroup' does not
    exist as of now)!
    
    Even when using the GUI/MMC, one needs to pay attention to change the value in both text
    fields when renaming an AD group.
    
    Divergent attribute values may haunt you later in life, when you're e.g. searching/filtering
    for the samAccountName and another time for the Name -- and the matches won't be the same.
    Or you find a group via samAccountName, but another cmdlet requires the Distinguished Name;
    with divergent values, you'll either not find the group, of the second cmdlet may fail;
    or... -- so it's best to keep them in sync.
    
    .Parameter Identity
    The current Identity (e.g. samAccountName, DisplayName, Object) of the AD group that is
    to be renamed.
    
    .Parameter NewName
    The new name that should be set as the group's samAccountName, DisplayName, etc.
    
    .Parameter Server
    On which server/in which the domain the action should happen.
    
    .Parameter NewDescription
    [Optional] In case the group's description should also be changed.
    
    .Parameter Credential
    [Optional] In case one needs to authenticate one with different credentials with the server.

    .Example
    > Rename-saoeADGroup -Identity "Foo" -NewName "Bar" -Server "example.net"
    
    .Example
    > $g = Get-ADGroup -Identity "Old_GroupName" -Server example.net
    > Rename-saoeADGroup -Identity $g -Server example.net -NewName "New_GroupName"
    
    .Notes
    Copyright © 2021 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2021-04-23 (so) - Update: Renamed some parameters, added Credential parameter.
    2021-11-09 (so) - Also change the display name of the group.
#>

param
(
    [Parameter(Mandatory=$true)]                                             $Identity,
    [Parameter(Mandatory=$true)] [string]                                    $NewName,
                                 [string]                                    $NewDescription,
    [Parameter(Mandatory=$true)] [string]                                    $Server,
                                 [System.Management.Automation.PSCredential] $Credential
)

try   { Import-Module ActiveDirectory -ErrorAction Stop }
catch { Write-Error -Message "ActiveDirectory module could not be imported!" -Category NotInstalled -ErrorAction Stop }

if   ($Credential -eq $null) { $CredentialHash = @{ <# empty #> }              }
else                         { $CredentialHash = @{ Credential = $Credential } }

$CurrentGroupObjectGUID = Get-ADGroup -Identity $Identity -Server $Server @CredentialHash -Property ObjectGUID | select -ExpandProperty ObjectGUID
    # Using the constant GUID will prevent that we would have to take into consideration the
    # order of the following function calls, or the order of the its arguments:
    # 
    # - If we would use the samAccountName: Make sure to call Rename-ADObject first,
    #   otherwise the samAccountName will have changed under your feet by Set-ADGroup!
    # 
    # - Other possible, but also fragile, workaround:
    #   Search for 'NewName' instead of 'CurrentName' in the second step, after renaming the samAccountName;
    #   but that requires that you'll remember it, and that the renaming itself, from step 1, was successful...
    # 
    # So, instead, we simply use the GUID instead.

Rename-ADObject -Identity $CurrentGroupObjectGUID -NewName $NewName -Server $Server @CredentialHash

Set-ADGroup -Identity $CurrentGroupObjectGUID -SamAccountName $NewName -Server $Server @CredentialHash
Set-ADGroup -Identity $CurrentGroupObjectGUID -DisplayName $NewName -Server $Server @CredentialHash

if (-not [string]::IsNullOrEmpty($NewDescription))
{
    Set-ADGroup -Identity $CurrentGroupObjectGUID -Description $NewDescription -Server $Server @CredentialHash
}
