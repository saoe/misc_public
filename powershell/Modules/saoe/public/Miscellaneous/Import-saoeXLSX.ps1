<#
    .Synopsis
    Import datasets from a XLSX-/Excel file to a Powershell object.

    .Description
    Import datasets from a XLSX-/Excel file to a Powershell object.
    
    Notes:
    - Requires the module "ImportExcel" <https://github.com/dfinke/ImportExcel> to be installed on the machine.
    - The function will import that module into the PS session; thus the initial run may take a bit longer. 
    
    .Parameter InputFile
    The path to the file that should be read.
    
    .Parameter WorksheetName
    Optional. Name of a specific worksheet that should be read exclusively.
    
    .Parameter HeaderRow
    Optional. To define the names of the datasets, if the XLSX file doesn't have a header row
    for the column names; the order here must match the order in the XLSX file.
    
    .Example
    $x = Import-saoeXLSX -InputFile "U:\data.xlsx"
    
    .Example
    $x = Import-saoeXLSX -InputFile "U:\data.xlsx" -WorksheetName "Worksheet1"
    
    Read only the Excel data from "Worksheet1" into the powershell variable.
    
    .Example
    $x = Import-saoeXLSX -InputFile "U:\data.xlsx" -HeaderRow "SamAccountName", "Surname", "GivenName"
    
    The content of the Excel's column A will be put into the variables attribute "SamAccountName", and so on.

    .Notes
    Copyright © 2022, 2024 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    2022-06-01 (so): Created.
    2022-06-20 (so): Added help text.
    2022-07-29 (so): - Added parameter 'HeaderRow'.
                     - Added a couple of examples.
    2024-06-08 (so): The function will look in all PSModulePaths for a subdirectory "ImportExcel" with a module file "ImportExcel.psm1".
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory = $true)]
    [ValidateScript({ Test-Path -Path $_ -PathType Leaf })]
    [string] $InputFile,
    
    [string] $WorksheetName,
    $HeaderRow = @()
)

# --------------------------------------------------------------------------------------------------
# Ensure, that the "ImportExcel" module is available; if not: Try to load it now...

$IEModule = get-module -Name "ImportExcel"

if ($IEModule)
{
    try   { $IEModuleBasePath = (get-module -Name "ImportExcel").Path | split-path -Parent }
    catch { $_ }
}
else
{
    $IsIEModuleFound = $false
    $env:PSModulePath -split ';' | % {
        try
        {
            Import-Module (join-path (join-path $_ "ImportExcel\") "ImportExcel.psm1") -ErrorAction SilentlyContinue                
            $IEModuleBasePath = (get-module -Name "ImportExcel").Path | split-path -Parent
            $IsIEModuleFound = $true
        }
        catch {}
    }
    
    if (-not $IsIEModuleFound) { write-error -Message "Could not load module 'ImportExcel'!" -Category ResourceUnavailable -ErrorAction Stop }
}

# IMPORTANT: Always necessary to do this in the script, even if the module was already and automatically loaded!
Add-Type -Path (join-path $IEModuleBasePath "EPPlus.dll")

# --------------------------------------------------------------------------------------------------

$Arguments = @{
    Path = $InputFile
}

if ($WorksheetName) { $Arguments += @{ WorksheetName = $WorksheetName } }
if ($HeaderRow)     { $Arguments += @{ HeaderName = $HeaderRow } }

$data = Import-Excel @Arguments
$data
