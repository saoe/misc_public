<#
    .Synopsis
    Run a timed loop.

    .Description
    Sometimes, an action takes an unknown and varying delay until completion (or failure);
    especially with cloud-related things nowadays.
    
    To handle that in a flexible way, this function will evaluate a given expression in
    specific intervalls, until that expression either succeeds (= evaluates to 'true') or 
    ultimately fails (= evaluates to 'false') in a given timeframe (defined by the timeout).
    
    At success, the result of the expression will be returned (might be an object);
    at failure, '$false' will be returned.
    
    Note: Heavily inspired by <https://mjolinor.wordpress.com/2012/01/14/making-a-timed-loop-in-powershell/>.

    .Parameter Timeout
    A timespan that defines the maximal lifetime of the loop.
    When the timeout is elapsed, the loop will end, regardless of the expression's evaluation.
    
    .Parameter Intervall
    A timespan that defines the waiting time between the attempts to evaluate the expression.
    Must be shorter than the timeout timespan.

    .Parameter Expression
    A scriptblock that acts as a 'condition check'.   
    For example: -Expression { Get-ADUser -Identity User123 }
      
    If the expression evaluates to $true, the loop will end immediately
    and the result of the expression will be returned.
    
    If the expression evaluates to $false, the loop will wait for the amount of time
    specified by the parameter 'Intervall', and then check again.
    
    (This repeats until either the evaluation is $true, or the timeout is reached.)
    
    .Parameter Compact
    The intervall/attempt status text will be shown compressed to only one line.
    
    .Parameter Silent
    No status text will be printed to the console at all.
   
    .Parameter Verbose
    Additional infos will be printed to the shell, e.g. the message of a thrown exception.
    
    .Example
    Wait-saoeTimedLoop -Timeout (New-TimeSpan -Seconds 6) -Intervall (New-TimeSpan -Seconds 3) -Expression {$true}
    
    .Example
    Wait-saoeTimedLoop -Timeout (New-TimeSpan -Seconds 6) -Intervall (New-TimeSpan -Seconds 3) -Expression {$false}
    
    .Example
    Wait-saoeTimedLoop -Timeout (New-TimeSpan -Minutes 1) -Intervall (New-TimeSpan -Seconds 5) -Expression {Get-ADUser -Identity UserName}
    
    .Notes
    Copyright © 2023 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    2023-07-13 (so): Created.
    2023-07-15 (so): Improved output of the status to the console.
    2023-07-16 (so): Revised.
#>

[CmdletBinding(DefaultParameterSetName = "Default")]
Param
(
    [Parameter(ParameterSetName = "Default", Mandatory = $true)]
    [Parameter(ParameterSetName = "Compact")]
    [Parameter(ParameterSetName = "Silent")]
    [Timespan] $Timeout,
   
    [Parameter(ParameterSetName = "Default", Mandatory = $true)]
    [Parameter(ParameterSetName = "Compact")]
    [Parameter(ParameterSetName = "Silent")]
    [ValidateScript({$_ -lt $Timeout})]
    [Timespan] $Intervall,
   
    [Parameter(ParameterSetName = "Default", Mandatory = $true)]
    [Parameter(ParameterSetName = "Compact")]
    [Parameter(ParameterSetName = "Silent")]
    [Scriptblock] $Expression,
    
    [Parameter(ParameterSetName = "Compact")]
    [Switch] $Compact,
    
    [Parameter(ParameterSetName = "Silent")]
    [Switch] $Silent
)

$VerboseMode = $null
$PSBoundParameters.TryGetValue("Verbose", [ref] $VerboseMode) | out-null
 
$result = $null   
$attempt = 0
$stopwatch = [diagnostics.stopwatch]::StartNew()
 
while ($stopwatch.elapsed -lt $Timeout)
{
    $attempt++
    $message = "Attempt #{0:d2}: No success, will try again in {1:hh}h:{1:mm}m:{1:ss}s..." -f $attempt, $Intervall
    
    if ($Compact) { $sb = { write-host -NoNewline -Object ("`r"+$message) } }
    else          { $sb = { write-host -Object $message } }
    
    try
    {
        if ($result = $Expression.Invoke()) { return $result }
        else                                { if (-not $Silent) { & $sb } }
    }
    catch
    {
        if (-not $Silent)
        {
            write-host -NoNewline -Object $(if (-not $VerboseMode) { $message } else { ($message + [Environment]::Newline) })
            write-verbose "$($_.Exception.Message)"
        }
    }
    
    Start-Sleep -Milliseconds $Intervall.TotalMilliseconds
}

if (-not $Silent) { write-host ("`r`nNo success. Stop after $($attempt) attempt(s), resp. {0:hh}h:{0:mm}m:{0:ss}s timeout" -f $Timeout) -BackgroundColor Black -ForegroundColor Red }
return $false