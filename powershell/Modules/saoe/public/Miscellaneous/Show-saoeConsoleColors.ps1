<#
    .Synopsis
    Displays the available console colore as background and foreground color variants
    
    .Description
    Displays the available console colore as background and foreground color variants
   
    .Notes
    Copyright © 2022 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    Inspired by https://stackoverflow.com/a/20588680.
    
    2021-12-20 (so): Created (the code, not this file...).
#>

$colors = [Enum]::GetValues( [ConsoleColor] )
$max = ($colors | foreach { "$_ ".Length } | Measure-Object -Maximum).Maximum

$currentFGcolor = (get-host).ui.rawui.ForegroundColor
$currentBGcolor = (get-host).ui.rawui.BackgroundColor

ForEach ($color in $colors)
{
    Write-Host ("{0,2} {1,$max} " -f [int]$color,$color) -NoNewline
    Write-Host "$color" -ForegroundColor $color -NoNewline
    Write-Host " " -NoNewline
    Write-Host "$color" -BackgroundColor $color -ForegroundColor $currentFGcolor -NoNewline
    Write-Host " " -NoNewline
    Write-Host "$color" -BackgroundColor $color -ForegroundColor $currentBGcolor
}
