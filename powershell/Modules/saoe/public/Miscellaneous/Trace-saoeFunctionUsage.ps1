<#
    .Synopsis
    Utility function to trace, which other script (e.g. in an automated task) has called a certain function.

    .Description
    Sends a mail with the name of the function that is under surveillance, the caller of that script and a timestamp.

    .Parameter FunctionName
    The name of the function that triggered this function/mail.

    .Parameter CallStackFrame
    The full path of the script which called the function.
    If the caller's origin is not script, but the console prompt, this will be '<ScriptBlock>'.

    .Parameter SMTPServer
    The SMTP server that will handle the mail delivery.

    .Parameter ToFrom
    An e-mail adress, to/from which the mail should be sent.

    .Example
    Put the following line of code into the function that should be watched; you can copy it
    virtually verbatim -- except for the 'SMTPServer' and 'ToFrom' parameter arguments, of course...:

    Trace-amFunctionUsage -FunctionName ($MyInvocation.MyCommand.Name) -CallStackFrame $((Get-PSCallStack)[1]) -SMTPServer mail.example.net -ToFrom user.name@example.net

    .Notes
    Copyright © 2022 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2022-01-28 (so): Created.
#>

[CmdletBinding()]
Param
(
    [string] $FunctionName,
    $CallStackFrame,
    [string] $SMTPServer,
    [Parameter(Mandatory = $true)] [string] $ToFrom
)

$TimeStamp = get-date -UFormat "%Y-%m-%d %H:%M"
$Subject = $Body = "[TRACE] Function '$FunctionName' called from '$($CallStackFrame.ScriptName)' at $TimeStamp"
Send-MailMessage -to $ToFrom -from $To>From -smtpServer $SMTPServer -subject $Subject -body $Body
