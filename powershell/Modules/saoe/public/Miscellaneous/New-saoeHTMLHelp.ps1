<#
    .Synopsis
    Creates one (big) HTML overview file for all the functions of one or more Powershell module
       
    .Description
    Extracts info mostly from the 'comment based help', which means the source code should have
    the structure as described in on this page:
    <https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_comment_based_help>
    
    Else, it will will build a file with just only minimalistic content (TOC/overview, syntax).
    
    ATTENTION: The generated HTML file will overwrite any other existing file of the same name
               at the specified location without asking!
    
    The header contains information about when and from which origins it was created;
    followed by a table of contents/overview table of all the functions, its alias and synopis.
    
    From there, one can jump to the more detailed sections, with long description, info about
    the parameters and syntax and examples for each function.
    
    Please be aware that ".Notes" are not collected by this function: I consider that section as
    internal/implementation-specific information. One would have to look into the orginal source
    code files for that.
    
    .Parameter SaveAs
    The full path or filename under which the generated output file should be saved.
    
    The file will be created (and overwritten!), but the directory path must already exist!
    
    If not provided, it will be saved under "$Env:PUBLIC\Documents" ('C:\Users\Public\Documents\') with a default filename.
    
    .Parameter $ModuleNames
    One or more names of modules, for which the overview/help file should be generated.
    
    If not explicitly specified, this function will extract the information for/from its own source module.
    
    .Example
    > New-saoeHTMLHelp -ModuleNames "abc", "def"
    
    Generates one single file for the two modules 'abc' and 'def'.
    Will be saved under a default path/filename
    
    .Example
    > New-saoeHTMLHelp -ModuleNames "abc", "def" -SaveAs C:\temp\output\index5.html
    
    Generates one single file for the two modules 'abc' and 'def'.
    Will be saved under the specified path/name -- the directory path must exist!
 
    .Notes
    Copyright © 2021-2024 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2021-06-17 (so): Created.
    2021-06-18 (so): Added the alias as a column to the TOC; other minor adjustment.
    2021-08-20 (so): Slight adjustments for syntax discovery/generation and text output/formatting.
    2021-09-03 (so): Translated to english; added parameters (SaveAs, ModuleNames) for flexibility.
    2022-01-11 (so): Added search filter for overview table, implemented in JavaScript; based on <https://www.w3schools.com/howto/howto_js_filter_table.asp>.
    2023-08-11 (so): The parameter 'ModuleNames' is no longer mandatory: If not explicitly specified,
                     this function will extract the information for/from its own source module.
    2023-08-12 (so): The parameter 'SaveAs' was modified: If not provided, the output will be saved
                     under "$Env:PUBLIC\Documents" ('C:\Users\Public\Documents\') with a default filename.
    2024-03-07 (so): * Improved the generated 'Examples' section in regards to linebreaks.
                     * Will now also display parameter aliases.
    2024-10-17 (so): Since some functions with dynamic parameters may cause trouble with Get-Command,
                     its invocations in the 'Syntax' and 'Parameter' sections needed to be adapted.
#>

Param
(
    [array] $ModuleNames = ($MyInvocation.MyCommand | select -expand ModuleName),
    $SaveAs = (join-path (join-path $Env:PUBLIC "Documents") "overview-and-help-for-powershell-modules.html")
)

$string_text_not_available = "(not available)"

# ----------------------------------------------------------------------------------------------
# Setup HTML file.

$top_title = "Overview and Help for Powershell modules"

$output_content_intro = @"
<!DOCTYPE html>
    <html lang="en">
        <head>
            <title>$top_title</title>
           
            <style>
                table, th, td { border: 1px solid black; border-collapse: collapse; }
                th, td        { padding: 0.25rem; text-align: left; vertical-align: top;}
                th            { background-color:#eaeaea; font-weight:bold; }
                tt, pre       { font-family: "Consolas", monospace; font-size: 1em;}
                #SearchInput {
                    width: 60em;
                    padding: 6px;
                    background-color: #ffffdc;
                    color: #000000;
                    border: 1;
                    margin-bottom: 10px; /* Add some space below the input */
                }
            </style>
        </head>
        
        <script>
            function FilterFunction()
            {
                // Declare variables
                var input, filter, table, tr, td, i, txtValue;
                input = document.getElementById("SearchInput");
                filter = input.value.toUpperCase();
                table = document.getElementById("overview");
                tr = table.getElementsByTagName("tr");

                // Loop through all table rows, and hide those who don't match the search query
                for (i = 0; i < tr.length; i++)
                {
                    // Search each column for the search term
                    td0 = tr[i].getElementsByTagName("td")[0]; // Column "Function"
                    td1 = tr[i].getElementsByTagName("td")[1]; // Column "Alias"
                    td2 = tr[i].getElementsByTagName("td")[2]; // Column "Synopsis"
                    td3 = tr[i].getElementsByTagName("td")[3]; // Column "Module"

                    if (td0)
                    {
                        txtValue0 = td0.textContent || td0.innerText;
                        txtValue1 = td1.textContent || td1.innerText;
                        txtValue2 = td2.textContent || td2.innerText;
                        txtValue3 = td3.textContent || td3.innerText;

                        if (txtValue0.toUpperCase().indexOf(filter) > -1 || txtValue1.toUpperCase().indexOf(filter) > -1 || txtValue2.toUpperCase().indexOf(filter) > -1 || txtValue3.toUpperCase().indexOf(filter) > -1)
                        {
                            tr[i].style.display = "";
                        }
                        else
                        {
                            tr[i].style.display = "none";
                        }
                    }
                }
            }

        </script>
    <body>
   
    <h1 id="top">$top_title</h1>       
"@ # Ugly, but necessary for a Here-String: The closing "@ must be at the start of the line (no space allowed).
 
$output_content_outro = @"
    </body>
</html>
"@ # Ugly, but necessary for a Here-String: The closing "@ must be at the start of the line (no space allowed).
 
# ----------------------------------------------------------------------------------------------
# Extract the comment-based help text (and some other data) from the module(s) and make it ready
# for embedding it into HTML.
 
$table_of_contents = [System.Collections.Generic.List[string]] @()
$all_exported_functions = [System.Collections.Generic.List[string]] @()
$single_function_details = [System.Collections.Generic.List[string]] @()
$all_function_details = [System.Collections.Generic.List[string]] @()
$modules_timestamps = @{}
 
$module_counter = 1
$table_of_contents.Add("<table id=`"overview`">`n<tr><th>Command</th><th>Alias</th><th>Synopsis</th><th>Module</th></tr>")
foreach ($ModuleName in $ModuleNames)
{
    write-host "Analyzing module $module_counter of $(@($ModuleNames).count): '$($ModuleName)'..."
    ((Get-Module -Name $ModuleName).ExportedFunctions).GetEnumerator() | % { $all_exported_functions.Add($_.Value.Name) }
   
    $modules_timestamps.add($ModuleName, ((get-module -Name $ModuleName).Path | % { (Get-Item $_).LastWriteTime | get-date -format "yyyy-MM-dd HH:mm"}))
    $module_counter++
}
 
# Addendum for the page header: When and from what origins was this content generated?
$output_content_intro_modules_timestamps = @"
    <p>Generated on $(Get-Date -Format "yyyy-MM-dd HH:mm") by $($MyInvocation.MyCommand.Name).
    <br>Contains information from the following module(s):
    <ul>
    $($modules_timestamps.keys | foreach-object {"<li>" + $_ + " &mdash; " + $modules_timestamps[$_] + "; " + (Get-Module -Name $_).Path + "</li>" })
    </ul>
"@ # Ugly, but necessary for a Here-String: The closing "@ must be at the start of the line (no space allowed).

# Search Bar
$output_content_intro_search_bar = @"
    &#x1F50D; <input type=`"text`" id=`"SearchInput`" onkeyup=`"FilterFunction()`" placeholder=`"Search term...`">
"@ # As above...

$output_content_intro += $output_content_intro_modules_timestamps + $output_content_intro_search_bar
 
$all_exported_functions = $all_exported_functions | sort
 
$function_counter = 1
foreach ($func in $all_exported_functions)
{
    write-host ("`rProcessing function {0,3}/{1,3}: {2,-50}" -f $function_counter, @($all_exported_functions).count, $func) -NoNewLine
    $help = Get-Help -Name $func
    $single_function_details.Clear()
    $single_function_details.Add("<h2><a id=`"$($func)`">$($func)</a></h2>`n")
   
    # ---- Synopsis ----
    $synopsis_text = $null
    try
    {
        $synopsis_text = $help.synopsis
       
        # Character replacements for HTML.
        $synopsis_text = $synopsis_text.Replace('<', '&lt;')
        $synopsis_text = $synopsis_text.Replace('>', '&gt;')
    }
    catch
    {
        # If no value is available, Get-Help returns a weird result; that's why this odd check
        # is here (instead of simply using [string]::IsNullOrEmpty() or [string]::IsNullOrWhitespace() for it).
        # The 'weird result' is this:
        # After a linebreak, the next best/following text or (if no 'synopsis' is available): `r...Syntax..., which also screws things up a bit.
        if ($synopsis_text -like "`r*") { $synopsis_text = "<em>$string_text_not_available</em>" }
    }
   
    $single_function_details.Add("<p>$synopsis_text</p>`n")
   
    # ---- After getting the synopsis: Use it to create the entry for the TOC ----
    $func_alias = "-"
    try { $func_alias = ((get-alias -Definition $func -ErrorAction Stop).Name) } catch { }
 
    $toc_entry = "<tr><td><a href=`"#$($func)`">$($func)</a></td><td>$func_alias</td><td>$synopsis_text</td><td>$((Get-Help -Name $func).ModuleName)</td></tr>`n"
    $table_of_contents.Add($toc_entry)
   
    # ---- Syntax ----
    # Not every function has this comment-based-help section, so we don't want to rely on it.
    # (If not available, $help.Syntax will return a hashtable format, which is pretty unreadable on the HTML page.)
    # Instead, better use 'Get-Command ... -Syntax'.    
    # But: If a function has dynamic parameters (with a 'read-host' in it), then Get-Command must be outwit:
    if ($func -eq 'Some-Function') { [string] $syntax_text = get-command -name $func -Syntax -ArgumentList '-SomeParam:dummy' | Out-String }
    else                           { [string] $syntax_text = get-command -name $func -Syntax | Out-String }
   
    if ([string]::IsNullOrEmpty($syntax_text)) { $syntax_text = "<em>$string_text_not_available</em>" }
   
    # Character replacements for HTML.
    $syntax_text = $syntax_text.Replace('<', '&lt;')
    $syntax_text = $syntax_text.Replace('>', '&gt;')
    $syntax_text = $syntax_text.Replace("`r`n`r`n", "`r`n")
    $syntax_text = $syntax_text.Trim("`r`n")
   
    $single_function_details.Add("<h3>Syntax</h3>`n`n<pre>$syntax_text</pre>")
       
    # ---- Description ----
    $description_text = $null
    try
    {
        #$description_text = (Get-Help -Name $func).Description.Text
        $description_text = $help.Description.Text
       
        # Character replacements for HTML.
        $description_text = $description_text.Replace('<', '&lt;')
        $description_text = $description_text.Replace('>', '&gt;')
    }
    catch
    {
        if ([string]::IsNullOrEmpty($description_text)) { $description_text = "<em>$string_text_not_available</em>" }
    }
   
    $single_function_details.Add("<h3>Description</h3>`n`n<pre>$description_text</pre>")
   
    # ---- Parameters ----
    $parameter_text = $null
    try
    {
        $parameter_text = "<dl>" + ($help.Parameters.parameter | % { "<dt>$($_.name)</dt>" + "<dd>$($_.description.text)</dd>" }) + "</dl>"
        
        # Get-Help doesn't provide data about parameter aliases, thus one has to use (once more) Get-Command:
        # Helpful: https://wahlnetwork.com/2017/07/10/powershell-aliases/ ("The Snazzy Secret of PowerShell Parameter Aliases")
        # But: If a function has dynamic parameters (with a 'read-host' in it), then Get-Command must be outwit:
        if ($func -eq 'Some-Function') { $command_parameters = get-command -name $func -ArgumentList '-SomeParam:dummy' }
        else                           { $command_parameters = get-command -name $func }

        $parameter_text = "<dl>" + ($help.Parameters.parameter | % { "<dt>$($_.name)" + $(`
                if (-not [string]::IsNullOrEmpty($command.Parameters[$_.name].Aliases)) { " (Alias: $($command.Parameters[$_.name].Aliases -join ', '))" }`
                else                                                                    { "" }`
            ) + "</dt>" + "<dd>$($_.description.text)</dd>" }) + "</dl>"
       
        # Character replacements for HTML.
        $parameter_text = $parameter_text.Replace('<', '&lt;')
        $parameter_text = $parameter_text.Replace('>', '&gt;')
        $parameter_text = $parameter_text.Trim("`r`n")
       
        # Character replacements for HTML... back again, for those parts that are really HTML tags...
        # (yes, it's probably not the most elegant workflow, but please bear with me:
        # It was 33 degrees celcius when I wrote this, and my brain couldn't find a better solution at that point...)
        $parameter_text = $parameter_text.Replace('&lt;dl&gt;', '<dl>')
        $parameter_text = $parameter_text.Replace('&lt;/dl&gt;', '</dl>')
        $parameter_text = $parameter_text.Replace('&lt;dt&gt;', '<dt>')
        $parameter_text = $parameter_text.Replace('&lt;/dt&gt;', '</dt>')
        $parameter_text = $parameter_text.Replace('&lt;dd&gt;', '<dd>')
        $parameter_text = $parameter_text.Replace('&lt;/dd&gt;', '</dd>')
    }
    catch
    {
        if ([string]::IsNullOrEmpty($parameter_text)) { $parameter_text = "<em>$string_text_not_available</em>" }
    }
   
    $single_function_details.Add("<h3>Parameters</h3><pre>$parameter_text</pre>")
   
    # ---- Examples ----
    $examples_text = $null
    try
    {
        # It's a bit tricky, to get the examples as plain text.
        $examples_text = $help.Examples.example | % { "$($_.title)`r`n$($_.code)" + $(if (-not ([string]::IsNullOrEmpty(($_.remarks.Text | out-string).Trim()))) { ("`r`n`r`n" + $_.remarks.Text) }) + "`r`n`r`n" }
       
        # Character replacements for HTML.
        $examples_text = $examples_text.Replace('<', '&lt;')
        $examples_text = $examples_text.Replace('>', '&gt;')
    }
    catch
    {
        if ([string]::IsNullOrEmpty($examples_text)) { $examples_text = "<em>$string_text_not_available</em>" }
    }
   
    $single_function_details.Add("<h3>Examples</h3>`r`n<pre>$examples_text</pre>")
   
    # -------------------------------------------------------------
   
    $single_function_details.Add("<a href=`"#top`">Back to the top</a>`n`n<hr/>`n")
   
    $all_function_details.Add($single_function_details)
    $function_counter++
}
 
$table_of_contents.Add("</table>")
 
# --------------------------------------------------------------------------------------------------
# Generate the HTML file.
write-Host ""
[string] $output = $output_content_intro + $table_of_contents + "<hr/>" + $all_function_details + $output_content_outro
try
{
    Out-File -InputObject $output -Encoding Default -FilePath $SaveAs
    write-Host "HTML output file saved as $SaveAs"
}
catch [System.IO.DirectoryNotFoundException]
{
    write-warning "One or more directories in the specified save path for the file ($SaveAs) do not exist; please create them first!"
}
