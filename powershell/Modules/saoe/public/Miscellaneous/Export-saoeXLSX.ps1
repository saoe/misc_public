<#
    .Synopsis
    Export powershell objects to a XLSX-/Excel file.

    .Description
    Export powershell objects to a XLSX-/Excel file.
    
    Notes:
    - Requires the module "ImportExcel" <https://github.com/dfinke/ImportExcel>
      to be installed on the machine.
    - The function will import that module into the PS session;
      thus the initial run may take a bit longer. 

    .Parameter InputData
    The powershell object(s) that should be exported to the XLSX file;
    can also be provided via pipeline (see example).

    .Parameter OutputFile
    Optional. The path, under which the XLSX file should be saved.
    
    If the parameter is missing, a default name will be used and the file
    will be saved in the current working directory.
    
    .Parameter OutputHome
    Optional switch parameter. If set, then the file will be saved
    (with the default name) in the Home Directory of the current user.
    
    .Parameter Worksheet
    Optional. In name of the worksheet.
   
    .Parameter FreezePane
    To define, which rows and columns shoud be fixed ("freezed"); by default,
    only the top row will be fixed:
    
    -FreezePane <Row>,<Column>
    z.B. -FreezePane 2,3: The first row (row 1) will be fixed and first
    two columns (columns 1 and 2).
    
    .Parameter WorkaroundMaxProps
    Workaround for a known issue*, that is apparenly caused by Powershell's
    PSCustomObjects, not by 'ImportExcel' (hmm...?):
    
    The maximum amount of columns will be determined by the amount of the
    properties of the first(!) input object. Which means: If later objects
    have additional properties, they will be ignored, i.e. no columns for
    those extra properties will appear in the XLSX file.
    
    This workaround switch will prevent that, but it will also change the
    original order of the columns!
    
    *) https://stackoverflow.com/questions/44428189/not-all-properties-displayed

    .Example
    > Get-Process | Export-saoeXLSX -OutputFile ".\test.xlsx"

    .Notes
    Copyright © 2022-2024 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
            
    2022-06-01 (so): Created.
    2022-06-20 (so): Added parameter 'OutputHome', added help text, other minor changes.
    2023-02-24 (so): Added pipeline support: 'InputData' can now also be provided via pipeline.
    2023-04-03 (so): Added workaround, so that now all properties of objects will be exported as columns; see below for details.
    2023-05-19 (so): Added parameter 'Worksheet'.
    2024-04-16 (so): * Added parameter 'FreezePane'.
                     * Added parameter 'WorkaroundMaxProps'.
    2024-06-08 (so): The function will look in all PSModulePaths for a subdirectory "ImportExcel" with a module file "ImportExcel.psm1".
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory = $true, ValueFromPipeline)] $InputData,
                                             [string] $OutputFile,
                                             [switch] $OutputHome,
                                             [string] $Worksheet,
                                                      $FreezePane,
                                             [switch] $WorkaroundMaxProps
)

# --------------------------------------------------------------------------------------------------

Begin
{
    $Data = @() # For collecting the data from the pipeline.
    
    # Ensure, that the "ImportExcel" module is available; if not: Try to load it now...
    $IEModule = get-module -Name "ImportExcel"

    if ($IEModule)
    {
        try   { $IEModuleBasePath = (get-module -Name "ImportExcel").Path | split-path -Parent }
        catch { $_ }
    }
    else
    {
        $IsIEModuleFound = $false
        $env:PSModulePath -split ';' | % {
            try
            {
                Import-Module (join-path (join-path $_ "ImportExcel\") "ImportExcel.psm1") -ErrorAction SilentlyContinue                
                $IEModuleBasePath = (get-module -Name "ImportExcel").Path | split-path -Parent
                $IsIEModuleFound = $true
            }
            catch {}
        }
        
        if (-not $IsIEModuleFound) { write-error -Message "Could not load module 'ImportExcel'!" -Category ResourceUnavailable -ErrorAction Stop }
    }

    # IMPORTANT: Always necessary to do this in the script, even if the module was already and automatically loaded!
    Add-Type -Path (join-path $IEModuleBasePath "EPPlus.dll")
}

Process
{
    ForEach ($i in $InputData) { $Data += $i }
}

End
{
    if ($WorkaroundMaxProps)
    {
        # Issue: The maximum amount of columns will be determined by the amount of the properties of the first(!) input object.
        # Which means: If later objects have additional properties, they will be ignored, i.e. no columns for those
        # extra properties will appear in the XLSX file.
        #
        # Workaround for a known issue (see <https://stackoverflow.com/questions/44428189/not-all-properties-displayed>),
        # that is apparenly caused by Powershell's PSCustomObjects, not by 'ImportExcel' (hmm...?):
        # 
        # What happens here: The highest count of properties of all datasets will be detected, and then the expart will start with
        #                    that maximum value, so that ever property will be exported (means the order of columns will change).
        $OutputData = $Data |
            select *, @{n="PropertyCount";e={@($_.PSObject.Properties).count}} |
                sort -Property 'PropertyCount' -Descending |
                    select * -ExcludeProperty 'PropertyCount'
    }
    else
    {
        $OutputData = $Data
    }
    
    if ($FreezePane)
    {
        $Arguments = @{
            Path         = $OutputFile
            InputObject  = $OutputData
            AutoSize     = $true
            AutoFilter   = $true
            FreezePane   = $FreezePane
        }
    }
    else
    {
        $Arguments = @{
            Path         = $OutputFile
            InputObject  = $OutputData
            AutoSize     = $true
            AutoFilter   = $true
            BoldTopRow   = $true
            FreezeTopRow = $true
        }
    }
    
    # Write to a specific worksheet:
    if ($Worksheet) { $Arguments += @{ WorksheetName = $Worksheet } }
    
    if ($OutputFile)
    {
        Export-Excel @Arguments
        write-host "Saved as '$($OutputFile)' gespeichert."
    }
    else
    {
        if ($OutputHome)
        {
            $OutputPath = join-path (join-path $ENV:HOMEDRIVE $ENV:HOMEPATH) "output_$(get-date -UFormat '%Y-%m-%d_%H%M%S').xlsx"
        }
        else
        {
            $OutputPath = join-path (Get-Location) "output_$(get-date -UFormat '%Y-%m-%d_%H%M%S').xlsx"
        }
        
        Export-Excel @Arguments
        write-host "Saved as '$($OutputPath)'."
    }
}