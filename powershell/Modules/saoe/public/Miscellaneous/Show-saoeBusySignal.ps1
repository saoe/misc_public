<#
    .Synopsis
    Shows a busy signal like a spinning cursor or a scrolling text.
    
    .Description
    Not working correctly in the ISE Host.
    
    .Parameter Type
    Choice between a simple spinning cursor symbol (default) or a text that scrolls in a loop.
    
    .Parameter Rate
    How quick the signal progresses (in milliseconds; default is 100).

    .Parameter Scroll
    Controls if the text should scroll from left to right (default) or from right to left.

    .Parameter Text
    The (scrolling) text that should be shown; a default text is set.
    
    .Parameter BackgroundColor
    Note that the default values won't function with ISE, you must set an explicit value if you run it in the ISE host.
    
    .Parameter ForegroundColor
    Note that the default values won't function with ISE, you must set an explicit value if you run it in the ISE host.
    
    .Example
    $i = 0; while ($i -lt 10) { Show-saoeBusySignal -Type Text -Text "Some example text..." -Scroll RightToLeft -BackgroundColor Yellow -ForegroundColor Red; $i++ }

    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
#>

[CmdletBinding()]
Param
(
    [ValidateSet("Cursor", "Text")]             [string]              $Type            = "Cursor",
                                                [int]                 $Rate            = 100,
    [ValidateSet("LeftToRight", "RightToLeft")] [string]              $Scroll          = "LeftToRight",
                                                [string]              $Text            = "Still Busy >>>",
                                                [System.ConsoleColor] $BackgroundColor = $Host.UI.RawUI.BackgroundColor,
                                                [System.ConsoleColor] $ForegroundColor = $Host.UI.RawUI.ForegroundColor
)

switch ($Type)
{
    "Cursor"
    {
        $cursor_symbols = @("[|]", "[/]", "[-]", "[\]")
        $backspaces = "`b" * $cursor_symbols.Length

        $cursor_symbols | % {
            Write-Host "$backspaces" -NoNewline
            Write-Host "$_" -NoNewline -Background $BackgroundColor -Foreground $ForegroundColor
            Start-Sleep -Milliseconds $Rate
        }
    }
    
    "Text"
    {
        $backspaces = "`b" * ($Text.Length + 1)
        $i = 0

        while ($i -lt $Text.Length)
        {
            Write-Host $backspaces -NoNewline
            
            if ($Scroll -eq "LeftToRight")
            {
                Write-Host $Text.Substring($Text.Length-$i, $i) $Text.Substring(0, $Text.Length-$i) -NoNewline -Background $BackgroundColor -Foreground $ForegroundColor
            }
            elseif ($Scroll -eq "RightToLeft")
            {
                Write-Host $Text.Substring($i, $Text.Length-$i) $Text.Substring(0, $i) -NoNewline -Background $BackgroundColor -Foreground $ForegroundColor
            }

            ++$i
            Start-Sleep -Milliseconds $Rate
        }
    }
}
