<#
    .Synopsis
    Generates a random password.
    
    .Parameter Length
    The length of the generated password (default: 8 characters).
    
    .Parameter Uppercase
    The amount of how many uppercase characters should be included in the password (default: 1).
    
    .Parameter Digit
    The amount of how many digits should be included in the password (default: 1).
    
    .Parameter Special
    The amount of how many special characters (e.g. # or !) should be included in the password (default: 1).
    
    .Example
    New-saoePassword -Length 2 -Uppercase 0 -Special 0 -Digit 1
    -> Output: u8
    
    .Example
    New-saoePassword -Length 3 -Uppercase 3 -Special 0 -Digit 0
    -> Output: DSK
    
    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    Inspired by https://activedirectoryfaq.com/2017/08/creating-individual-random-passwords/

    2020-12-09 (so): Created.
#>

Param
(
    [int] $Length = 8,
    [int] $Uppercase = 1,
    [int] $Digit = 1,
    [int] $Special = 1
)

if (($Uppercase -gt $Length) -or
    ($Digit -gt $Length) -or
    ($Special -gt $Length) -or
    (($Uppercase + $Digit + $Special) -gt $Length))
{
    Write-Error "Calculated password length exceeds requested length, maybe due the amount of default characters.!`
    Tip: See example on how to get a 3-character uppercase-only value." -ErrorAction Stop

}

# The lowercase characters are used as the default values if one substracts all other requested character types,
# and are later also used to fill up the password, if the amount of requested other character types are not enough.
$lowercase_characters_count = ($Length - $Uppercase - $Digit - $Special)

function Get-RandomCharacters ($_length, $FromCharacters)
{
    if (($_length -gt $Length))
    {
        Write-Error "Requested length of particular character type ($($FromCharacters)) is longer ($($_length)) than the requested total password length ($($Length))!" -ErrorAction Stop
    }
    else
    {
        if ($_length -lt 1)
        {
            return [String]""
        }
        else
        {
            $random = 1..$_length | ForEach-Object { Get-Random -Maximum $FromCharacters.length }

            # '$OFS' (= Output Field Separator) is an Automatic Variable in Powershell.
            # It contains a value that is used when one is converting an array to a string.
            # By default, $OFS i " " (a space charactar), but that can be changed:
            $private:OFS=""

            return [String]$FromCharacters[$random]
        }
    }
}

$password = Get-RandomCharacters -_length $lowercase_characters_count -FromCharacters "abcdefghijklmnopqrstuvwxyz"
$password += Get-RandomCharacters -_length $Uppercase -FromCharacters "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
$password += Get-RandomCharacters -_length $Digit -FromCharacters "0123456789"
$password += Get-RandomCharacters -_length $Special -FromCharacters ".:_-+=*/()[]{}<>!?$%&@#"

if (($password.length -gt $Length))
{
    write-error "[Internal Error] Generated password is longer than requested length!" -ErrorAction Stop
}

# Scramble characters in generated password string.
$character_array = $password.ToCharArray()   
$password = -join ($character_array | Get-Random -Count $character_array.Length)

$password
