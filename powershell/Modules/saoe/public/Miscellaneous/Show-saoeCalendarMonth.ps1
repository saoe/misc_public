<#
    .Synopsis
    Show a compact view of a calendar month
    
    .Description
    Show a compact view of a calendar month.
    
    Please notice:
    - Uses per default unicode box drawing elements (e.g. "$([char]0x2550)") to make it prettier.
      One can use the parameter '-NoUnicode' to fall back to only ASCII characters.
    
    Example:
    
        Applying unicode (Default):      Applying ASCII (via -NoUnicode):
                                         
        2023-08 (AUGUST)                 2023-08 (AUGUST)
        ═══╤═════════════════════        =========================
        KW │ Mo Di Mi Do Fr Sa So        KW | Mo Di Mi Do Fr Sa So
        ───┼─────────────────────        ---+---------------------
        31 │     1  2  3  4  5  6        31 |     1  2  3  4  5  6
        32 │  7  8  9 10 11 12 13        32 |  7  8  9 10 11 12 13
        33 │ 14 15 16 17 18 19 20        33 | 14 15 16 17 18 19 20
        34 │ 21 22 23 24 25 26 27        34 | 21 22 23 24 25 26 27
        35 │ 28 29 30 31                 35 | 28 29 30 31

    .Parameter Month
    The number of the month (i.e. 1-12) for which the calendar view should be generated.
    If not provided, the current month will be used.
    
    .Parameter Year
    If not provided, the current year will be used.
    
    .Parameter NoUnicode
    Uses per default unicode box drawing elements (e.g. "$([char]0x2550)") to make it prettier.
    Use this switch parameter to fall back to only ASCII characters.
    
    .Parameter Language
    Determines the language for the names of the months, days and abbrevations.
    At the moment, only english (en-US) and german (de-DE) are supported.
    Default is english (EN-US).
    
    .Parameter WithCalendarWeeks
    Adds a column to the left that shows the calendar week (see example).
    
    .Parameter WithDaysOfYear
    Adds a column to the right that shows the days of the year (see example).
    
    .Example
    > Show-saoeCalendarMonth
    
    Default mode: Current month of the current year.
    
    .Example
    > Show-saoeCalendarMonth -Month 12
    
    Month 12 (December) of the current year.
    
    .Example
    > Show-saoeCalendarMonth -Month 10 -Year 2024
    
    Month 10 (October) of the year 2024.
    
    .Example
    > Show-saoeCalendarMonth -Month 10 -Year 2024 -Language DE
    
    Month 10 (October) of the year 2024, using german terms:
    "Oktober", "KW", "Mo Di Mi Do Fr Sa So"
    
    .Example
    > Show-saoeCalendarMonth -Year 2030 -NoUnicode
    
    Current month of the year 2030.
    Using ASCII characters for the box drawing elements.
    
    .Example
    > Show-saoeCalendarMonth -WithCalendarWeeks -WithDaysOfYear
   
    2023-08 (AUGUST)
    ═══╤══════════════════════════════
    CW │ Mo Tu We Th Fr Sa Su │ DoY
    ───┼──────────────────────┼─────────
    31 │     1  2  3  4  5  6 │ 213-218
    32 │  7  8  9 10 11 12 13 │ 219-225
    33 │ 14 15 16 17 18 19 20 │ 226-232
    34 │ 21 22 23 24 25 26 27 │ 233-239
    35 │ 28 29 30 31          │ 240-243
    
    .Notes
    Copyright © 2023 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    2023-08-16 (so): Published.
    2023-08-22 (so): Added parameter 'Language' and support of different locales (for localized month and day names, etc.).
    2023-08-24 (so): Some refactoring, plus added features to optionally show calendar weeks and days of the year.
#>

[CmdletBinding()]
Param
(
    [ValidateSet(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12)] [string] $Month = (get-date).Month,
    [ValidateScript({ Get-Date -Year $_ })]              [int]    $Year = (get-date).Year,   
    [switch]                                                      $NoUnicode,
    [switch]                                                      $WithCalendarWeeks,
    [switch]                                                      $WithDaysOfYear,
    [ValidateSet("EN", "DE")]                                     $Language = "EN"
)

switch ($Language)
{
    "DE"
    {
        $Locale = New-Object System.Globalization.CultureInfo("de-DE")
        $CW = "KW" # Abbreviation for 'calendar week'.
        $DOTY = "TdJ" # Abbreviation for 'day of the year'.
        [string] $DayAbbreviations = "$($Locale.DateTimeFormat.ShortestDayNames[1]) $($Locale.DateTimeFormat.ShortestDayNames[2]) $($Locale.DateTimeFormat.ShortestDayNames[3]) $($Locale.DateTimeFormat.ShortestDayNames[4]) $($Locale.DateTimeFormat.ShortestDayNames[5]) $($Locale.DateTimeFormat.ShortestDayNames[6]) $($Locale.DateTimeFormat.ShortestDayNames[0])"
    }
    
    Default
    {
        $Locale = New-Object System.Globalization.CultureInfo("en-US")
        $CW = "CW" # Abbreviation for 'calendar week'.
        $DOTY = "DoY" # Abbreviation for 'day of the year'.
        [string] $DayAbbreviations = "$($Locale.DateTimeFormat.ShortestDayNames[1]) $($Locale.DateTimeFormat.ShortestDayNames[2]) $($Locale.DateTimeFormat.ShortestDayNames[3]) $($Locale.DateTimeFormat.ShortestDayNames[4]) $($Locale.DateTimeFormat.ShortestDayNames[5]) $($Locale.DateTimeFormat.ShortestDayNames[6]) $($Locale.DateTimeFormat.ShortestDayNames[0])"
    }
}

if ($NoUnicode)
{
    [string] $VBar   = "|"
    [string] $dHLine = "="
    [string] $Tee    = "="
    [string] $sHLine = "-"
    [string] $Cross  = "+" 
}
else
{
    [string] $VBar   = [char] 0x2502
    [string] $dHLine = [char] 0x2550
    [string] $Tee    = [char] 0x2564
    [string] $sHLine = [char] 0x2500
    [string] $Cross  = [char] 0x253C
}

# Helper function to get the calendar week that goes together with the provided date:
# (Stolen from <https://stackoverflow.com/a/60195096>.)
function Get-WeekNumber ([DateTime] $DateTime = (Get-Date))
{
    $Locale.Calendar.GetWeekOfYear($DateTime, $Locale.DateTimeFormat.CalendarWeekRule, $Locale.DateTimeFormat.FirstDayOfWeek)
}

# --------------------------------------------------------------------------------------------------
# Hash-Table with empty containers for the months, in which the day-objects will be collected.

$Months = @{ '1' = @(); '2' = @(); '3' = @(); '4' = @(); '5' = @(); '6' = @(); '7' = @(); '8' = @(); '9' = @(); '10' = @(); '11' = @(); '12' = @() }

# --------------------------------------------------------------------------------------------------
# Iterate over all days of the (current) year (1.1.-31.12.) and put each day into the suitable container for its month.

$EndOfTheYear = (get-date -date (get-date -Day 31 -Month 12 -year $Year -Hour 0 -Minute 0 -Second 0))

$ctr = 0
do
{
    $d = (get-date -date ((get-date -Day 1 -Month 1 ).DayOfYear) -Year $Year).AddDays($ctr)
    
    switch ($d.Month)
    {
         1 { $Months['1']  += $d }
         2 { $Months['2']  += $d }
         3 { $Months['3']  += $d }
         4 { $Months['4']  += $d }
         5 { $Months['5']  += $d }
         6 { $Months['6']  += $d }
         7 { $Months['7']  += $d }
         8 { $Months['8']  += $d }
         9 { $Months['9']  += $d }
        10 { $Months['10'] += $d }
        11 { $Months['11'] += $d }
        12 { $Months['12'] += $d }
    }
    $ctr++
} while ($d -lt $EndOfTheYear.AddDays(-1))

# Just in case: Make sure that the values are ordered correctly:
$Months.GetEnumerator() | % { $_ = $_.Value | sort -Property Day }

# --------------------------------------------------------------------------------------------------
# Generate and return the output string:

# The 'header': <Month & Year> <Linebreak>
#               <Separator Line> <Linebreak>
#               <CalendarWeek & Days in the week> <Linebreak>
#               <Separator Line> <Linebreak>
$LocMName = (get-date -Year $Year -Month $Month).ToString("MMMM", $Locale)
[string] $out = "$((`"$Year-$(get-date -Year $Year -Month $Month -f "MM") ($LocMName)`").ToUpper())`r`n"

$header_lineA = "$($dHLine*20)"
$header_lineB = "$($DayAbbreviations)"
$header_lineC = "$($sHLine*20)"

$header_line1 = $header_lineA + "`r`n"
$header_line2 = $header_lineB + "`r`n"
$header_line3 = $header_lineC + "`r`n"

if ($WithCalendarWeeks -and (-not $WithDaysOfYear))
{
    $header_line1 = "$($dHLine*3)$($Tee)$($dHLine*21)`r`n"
    $header_line2 = "$($CW) $VBar " + $header_lineB + "`r`n"
    $header_line3 = "$($sHLine*3)$($Cross)$($sHLine*21)`r`n"
}

if ($WithDaysOfYear -and (-not $WithCalendarWeeks))
{
    $header_line1 = "$($dHLine*21)$($Tee)$($dHLine*8)`r`n"
    $header_line2 = $header_lineB + " $VBar $($DOTY)" + "`r`n"
    $header_line3 = "$($sHLine*21)$($Cross)$($sHLine*8)`r`n"
}

if ($WithCalendarWeeks -and $WithDaysOfYear)
{
    $header_line1 = "$($dHLine*3)$($Tee)$($dHLine*22)$($Tee)$($dHLine*8)`r`n"
    $header_line2 = "$($CW) $VBar " + $header_lineB + " $VBar $($DOTY)" + "`r`n"
    $header_line3 = "$($sHLine*3)$($Cross)$($sHLine*22)$($Cross)$($sHLine*8)`r`n"
}

$out += $header_line1 + $header_line2 + $header_line3

$pad = 3

[array] $DoYThisWeek = @()

ForEach ($i in $Months[$Month])
{
    $p = $null
    [string] $line = ""
    
    $DoYThisWeek += $i.DayOfYear
    $DoYThisWeek = $DoYThisWeek | sort -unique
    
    $cw = "{0,2}" -f (Get-WeekNumber -DateTime $i)
    
    # Calculate padding:
    switch ([int] $i.DayOfWeek)
    {
        1 { $p = $pad*0 } # Mo
        2 { $p = $pad*1 } # Tu
        3 { $p = $pad*2 } # We
        4 { $p = $pad*3 } # Th
        5 { $p = $pad*4 } # Fr
        6 { $p = $pad*5 } # Sa
        0 { $p = $pad*6 } # Su
        default {}
    }

    if ($i.Day -eq 1) # Handle the first day of the month.
    {
        if ($WithCalendarWeeks) { $line += "$cw $VBar " }
        else                    { $line += "" }

        $line += ("$((' '*$p)){0,2}" -f $i.Day)
    }
    else # Handle all the remaining days of the months (i.e. except the very first one).
    {
        switch ([int] $i.DayOfWeek)
        {
            1 # Monday
            {
                if   ($WithCalendarWeeks) { $line += ("$cw $VBar {0,2}" -f $i.Day) }
                else                      { $line += ("{0,2}" -f $i.Day) }
            }
            default { $line += (" {0,2}" -f $i.Day) }
        }
    }

    if ([int] $i.DayOfWeek -eq 0) # Linebreak at the end of the week (after Sunday).
    {  
        if ($WithDaysOfYear) { $out += $line + " $VBar $($DoYThisWeek[0])-$($DoYThisWeek[-1])`r`n"}
        else                 { $out += $line + "`r`n" }
        
        $DoYThisWeek = @()
    }
    elseif ($i -eq $Months[$Month][-1])
    {
        if ($WithDaysOfYear) { $out += $line + "$((' '*$p)) $VBar $($DoYThisWeek[0])-$($DoYThisWeek[-1])`r`n"}
        else                 { $out += $line + "`r`n" }
    }
    else { $out += $line }
}

return $out
