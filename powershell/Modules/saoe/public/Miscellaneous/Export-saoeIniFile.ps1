<#
    .Synopsis
    Export a data structure to a text file in the INI format.
    
    .Description
    Exports a data structure to a text file in a simple *.ini format <https://en.wikipedia.org/wiki/INI_file>.
    
    Notes:
    - The data structure must have a specific format (in short: a nested hashtable)
      and some components must follow a special naming convention (*_comment); see below for details.
    - The sections (and the keys within each section) will be written to the file
      in alphabetical sorting order.
    - A global section or key-value pairs without a section are currently not supported.
    - The generated text file is encoded as 'UTF-8 without BOM (Byte Order Mark)'.
    
    Example:
    
    Input (Powershell data structure):
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    $DataStructure = @{
        section1_comment = "Multi-line`r`ncomment block`r`nfor Section 1."
        section1 = @{
            keyA = "value1.A"
            keyB = "value1.B"
            keyB_comment = "Inline cömment for KeyB."
        }
        
        section2 = $null
        section2_comment = "This is an empty section."
        
        section3 = @{
            keyA = $null
            keyB = 100
        }
    }
    
    Output (= *.INI file):
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    
    ; Multi-line
    ; comment block
    ; for Section 1.
    [section1]
    keyA = value1.A
    keyB = value1.B ; Inline cömment for KeyB.

    ; This is an empty section.
    [section2]

    [section3]
    keyA = 
    keyB = 100
    
    .Parameter FilePath
    Path to the file that should be written.
    Any existing file will be overwritten!
    
    .Parameter Data
    The data structure that should be written to the file.
    (Will also be accepted as a pipeline argument.)
    
    .Parameter CommentDelimiter
    Specifies which character denotes the start of a comment line.
    By default, it's the semicolon (;).
    
    .Example
    Export-saoeIniFile -FilePath X:\out.ini -Data $data
    
    .Example
    $data | Export-saoeIniFile -FilePath X:\out.ini
    
    .Notes
    Copyright © 2024 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    2024-03-10 (so): Created.
#>

Param
(
    [Parameter(Mandatory = $true)] [string] $FilePath,
    [Parameter(ValueFromPipeline)] [hashtable] $Data,
    [string] $CommentDelimiter = ';'
)

Begin
{
             $Encoding     = [System.Text.UTF8Encoding]::new($false) # UTF-8 (No BOM)
    [string] $OutputString = ""
}

Process
{
    # Process each section in alphabetical order, but ignore global values (_*) and section comment blocks (*_comment) for now.
    ForEach ($i in ($Data.GetEnumerator() | ? { $_.Name -notlike "*_comment" } | sort -Property Name))
    {
        $SectionComment = $null
        $Section        = $null
        
        try
        {
            if ($Data["$($i.Name)_comment"]) # Comment block for this section detected; save for later!
            {
                # Handle linebreaks, in case it's a multi-line comment-block:
                ($Data["$($i.Name)_comment"]).split([System.Environment]::NewLine) |
                    % { $SectionComment += "{0} {1}{2}" -f $CommentDelimiter, $_, [System.Environment]::NewLine }
            }
            
            # Format for output:
            $Section += ("[{0}]{1}" -f $i.Name, [System.Environment]::NewLine)
        
            # Key detected!
            if ($i.Value)
            {
                # Process each key/value pair in alphabetical order, but ignore key's comments for now.
                ($i.Value.Keys | ? { $_ -notlike "*_comment" } | sort) |
                    % {
                        $InlineComment = $null
                        
                        # Inline comment line (for this key) detected; save for later!
                        if ($Data[$i.Key]["$($_)_comment"])
                        {
                            $InlineComment = " {0} {1}" -f $CommentDelimiter, $Data[$i.Key]["$($_)_comment"]
                        }
                        
                        # Format for output:
                        $Section += ("{0} = {1}{2}{3}" -f $_, $i.Value[$_], $InlineComment, [System.Environment]::NewLine)
                    }
            }
        }
        catch { write-error $_.Exception.Message }
        
        $script:OutputString += ($SectionComment + $Section + [System.Environment]::NewLine)
    }
}

End
{
    try
    {
        out-file -FilePath $FilePath -Encoding $Encoding -InputObject ($script:OutputString).Trim()
        write-host "Exported data structure to file `"$($FilePath)`"."
    }
    catch { write-error $_.Exception.Message }
}
