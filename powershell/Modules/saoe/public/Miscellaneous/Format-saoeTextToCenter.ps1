<#
    .Synopsis
    Center text on a line of specific width.

    .Description
    Centers a given text on a line of a defined width.
    
    This is meant for text in a monospaced/fixed-width font in plain text files (or on command line
    prompts), where no other means to center text are available.
    
    In comparison, HTML with CSS offers better suited methods, and the same applies for word
    processors and similar applications -- if your output will result in such formats, it's
    recommended to use their builtin features for text alignment instead.
    
    Note: If the text or the width are not equally divisible by 2 (without a fraction or remainder),
          then the alignment will lean slightly more to the left border, rather than to the
          (virtual) right edge.
          
          That means that the padding on the left side will be slightly less than the
          padding on the right in these cases.

    .Parameter Text
    The text that should be centered on the line.

    .Parameter Width
    The width (or lenghth) of the line on which the text should be centered.
    Specified as the decimal amount of characters.
    
    .Parameter Padding
    The single character that will be used to pad the left and right sides.
    By default the 'space' character is used.
    
    .Parameter WithMargin
    Optional switch: Can be useful if you use a specific padding character, but want for
    readability reasons a single space margin around the text; example:
    
        -Padding '*'             -> *****TEXT*****
        -Padding '*' -WithMargin -> **** TEXT ****

    .Example
    > Format-saoeTextToCenter -Text "The Headline" -Width 100
    
    .Example
    > Format-saoeTextToCenter -Text "The Headline" -Width 100 -Padding '-' -WithMargin

    .Notes
    Copyright © 2023 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0

    2023-08-26 (so): Ready for initial release.

    *** Unit Testing (use -Verbose for padding values): ***
    -text F   -width 100 -> Should result in: PaddingLeft: 49, PaddingRight: 50 (*)
    -text Fo  -width 100 -> Should result in: PaddingLeft: 49, PaddingRight: 49
    -text Foo -width 100 -> Should result in: PaddingLeft: 48, PaddingRight: 49 (*)
    -text F   -width 101 -> Should result in: PaddingLeft: 50, PaddingRight: 50
    -text Fo  -width 101 -> Should result in: PaddingLeft: 49, PaddingRight: 50 (*)
    -text Foo -width 101 -> Should result in: PaddingLeft: 49, PaddingRight: 49
                         *) Default policy is to lean more to the left border than to the right.
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true)] [string] $Text,
    [Parameter(Mandatory=$true)] [int]    $Width,
                                 [char]   $Padding = ' ',
                                 [switch] $WithMargin
)

if (($Width % 2) -ne 0) # Odd line width:
{
    $PaddingLeft = [math]::ceiling($Width / 2) - 1
    $PaddingRight = [math]::ceiling($Width / 2)
}
else                    # Even line width:
{
    $PaddingLeft = [math]::ceiling($Width / 2)
    $PaddingRight = [math]::ceiling($Width / 2)   
}

$TextHalfLeft = $TextHalfRight = $Text.Length / 2

if ((($Text.Length % 2) -ne 0) -and (($Width % 2) -ne 0)) # Odd text length & odd line width:
{
    [int] $PaddingLeft  -= [math]::floor($TextHalfLeft)
    [int] $PaddingRight -= [math]::ceiling($TextHalfRight)   
}
elseif (($Text.Length % 2) -ne 0)                         # Odd text length & [implicitly] even line width:
{
    [int] $PaddingLeft  -= [math]::ceiling($TextHalfLeft)
    [int] $PaddingRight -= [math]::ceiling($TextHalfRight - 1)   
}
else                                                      # Even text length:
{
    [int] $PaddingLeft  -= [math]::ceiling($TextHalfLeft)
    [int] $PaddingRight -= [math]::ceiling($TextHalfRight)
}

write-verbose $PaddingLeft
write-verbose $PaddingRight

if ($WithMargin) { return (([string]$Padding) * ($PaddingLeft - 1)) + ' ' + $Text + ' ' + (([string]$Padding) * ($PaddingRight - 1)) }
else             { return (([string]$Padding) * $PaddingLeft)             + $Text +       (([string]$Padding) * $PaddingRight) }
