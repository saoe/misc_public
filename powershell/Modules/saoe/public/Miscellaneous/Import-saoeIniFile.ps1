<#
    .Synopsis
    Import the content of an INI file and return the data as a hash table.
    
    .Description
    Reads a configuration file of Type 'INI' ("initialization file", <https://en.wikipedia.org/wiki/INI_file>)
    and returns the values found there as a structured hash table.
    
    In general, this is a very simple implementation: Most of the restrictions, special and edg
    cases mentioned for example in the Wikipedia entry are not taken into consideration here.

    Although the INI format is pretty old and was even popularized by Microsoft itself,
    surprisingly PowerShell does not have native support for it. So, like many, many developers
    before me, I reinvented the wheel here...
    
    Input example:
    
    ----------------------- data.ini ----
    ; The following key is in no section.
    Global Key = SomeValue

    [Section 1]
    ; Another comment
    Key 1 = "Value A" ; A comment, again.
    Key 2 = 100
    ...
    -------------------------------------
    
    See the examples below to learn how to access the values.
    
    .Parameter FilePath
    Path to the *.ini file that should be used.
    
    .Parameter CommentDelimiter
    Specifies which character denotes the start of a comment line.
    By default, it's the semicolon (;).
    
    .Example
    # Load INI content into a variable:
    $IniFile = Import-saoeIniFile .\data.ini
    
    # Keys which are not in a section:
    $ValueOne = $IniFile."Global Key"
    
    # You can use the dot-notation (as above) or use brackets to access the values:
    $ValueTwo = $IniFile["Section 1"]["Key 2"]
    
    .Notes
    Copyright © 2020, 2024 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    Based on code from:
    - https://stackoverflow.com/questions/43690336/powershell-to-read-single-value-from-simple-ini-file
    - https://devblogs.microsoft.com/scripting/use-powershell-to-work-with-any-ini-file/
    (I did remove the parts that load the comments though...)
    
    2020-12-11 (so): Created.
    2024-06-01 (so): Added a 'ValidateScript' for $FilePath.
#>

param
(
    [parameter(Mandatory = $true)]
    [ValidateScript({ Test-Path -Path $_ -PathType Leaf })]
    [string] $FilePath,
    
    [string] $CommentDelimiter = ';'
)

$section   = $null
$ini       = @{}

switch -RegEx -File $FilePath
{
    "^\[(.+)\]$"                # RegEx for "[Section Name]".
    {
        $section = $matches[1]
        $ini[$section] = @{}
    }

    "(.+?)\s*=\s*(.*)"          # RegEx for "Key = Value ; Comment" (Comment will be split off).
    {
        $key, $value = $matches[1..2]
        
        if (!($section))
        {
            $ini[$key]           = ($value -split $CommentDelimiter)[0]
        }
        else
        {
            $ini[$section][$key] = ($value -split $CommentDelimiter)[0]
        }
    }
}

return $ini
