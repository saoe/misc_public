<#
    .Synopsis
    Just a gimmick: Show some kind of a banner in a sort of poor man's ASCII art/FIGlet way.
    
    .Parameter Text1
    The parmeters $LineText1 to $LineTextN append the given string to the named line, to the
    right of the banner.

    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
#>

[CmdletBinding()]
Param
(
    [string] $LineText1, [string] $LineText2, [string] $LineText3, [string] $LineText4, [string] $LineText5, [string] $LineText6,
    [string] $LineText7, [string] $LineText8, [string] $LineText9, [string] $LineText10, [string] $LineText11, [string] $LineText12, [string] $LineText13 
)

Write-Host -BackgroundColor Black -ForegroundColor Red  '  ______  ' -NoNewline; Write-Host -BackgroundColor Black -ForegroundColor Green  ' ______  ' -NoNewline; Write-Host " $LineText1"
Write-Host -BackgroundColor Black -ForegroundColor Red  ' |  ____| ' -NoNewline; Write-Host -BackgroundColor Black -ForegroundColor Green  '|____  | ' -NoNewline; Write-Host " $LineText2"
Write-Host -BackgroundColor Black -ForegroundColor Red  ' | |____  ' -NoNewline; Write-Host -BackgroundColor Black -ForegroundColor Green  ' ____| | ' -NoNewline; Write-Host " $LineText3"
Write-Host -BackgroundColor Black -ForegroundColor Red  ' |____  | ' -NoNewline; Write-Host -BackgroundColor Black -ForegroundColor Green  '|  __  | ' -NoNewline; Write-Host " $LineText4"
Write-Host -BackgroundColor Black -ForegroundColor Red  '  ____| | ' -NoNewline; Write-Host -BackgroundColor Black -ForegroundColor Green  '| |__| | ' -NoNewline; Write-Host " $LineText5"
Write-Host -BackgroundColor Black -ForegroundColor Red  ' |______| ' -NoNewline; Write-Host -BackgroundColor Black -ForegroundColor Green  '|______| ' -NoNewline; Write-Host " $LineText6"
Write-Host -BackgroundColor Black -ForegroundColor Cyan '  ______  ' -NoNewline; Write-Host -BackgroundColor Black -ForegroundColor Yellow ' ______  ' -NoNewline; Write-Host " $LineText7"
Write-Host -BackgroundColor Black -ForegroundColor Cyan ' |  __  | ' -NoNewline; Write-Host -BackgroundColor Black -ForegroundColor Yellow '|  __  | ' -NoNewline; Write-Host " $LineText8"
Write-Host -BackgroundColor Black -ForegroundColor Cyan ' | |  | | ' -NoNewline; Write-Host -BackgroundColor Black -ForegroundColor Yellow '| |__| | ' -NoNewline; Write-Host " $LineText9"
Write-Host -BackgroundColor Black -ForegroundColor Cyan ' | |  | | ' -NoNewline; Write-Host -BackgroundColor Black -ForegroundColor Yellow '|  ____| ' -NoNewline; Write-Host " $LineText10"
Write-Host -BackgroundColor Black -ForegroundColor Cyan ' | |__| | ' -NoNewline; Write-Host -BackgroundColor Black -ForegroundColor Yellow '| |____  ' -NoNewline; Write-Host " $LineText11"
Write-Host -BackgroundColor Black -ForegroundColor Cyan ' |______| ' -NoNewline; Write-Host -BackgroundColor Black -ForegroundColor Yellow '|______| ' -NoNewline; Write-Host " $LineText12"
Write-Host -BackgroundColor Black                       '          ' -NoNewline; Write-Host -BackgroundColor Black                         '         ' -NoNewline; Write-Host " $LineText13"
