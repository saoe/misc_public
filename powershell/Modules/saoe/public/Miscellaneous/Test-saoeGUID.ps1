<#
    .Synopsis
    Test whether a given value is a valid GUID.

    .Description
    Test whether a given value is a valid GUID;
    returns a boolean value (true or false).

    .Parameter Value
    The input value that will be checked.
    
    .Parameter IsValidButEmpty
    To check for a possible dummy/placeholder GUID like "00000000-0000-0000-0000-000000000000".

    .Example
    > Test-GUID -Value 0c7a45c4-7d7a-4238-8a23-2d8a58c98864
    True

    > Test-GUID -Value 0c7a45c4-7d7a-4238-8a23-2d8a58c98864 -IsValidButEmpty
    False

    > Test-GUID -Value 010a020b-30c0-def4
    False

    > Test-GUID -Value 010a020b-30c0-def4 -IsValidButEmpty
    False

    > Test-GUID -Value 00000000-0000-0000-0000-000000000000
    True

    > Test-GUID -Value 00000000-0000-0000-0000-000000000000 -IsValidButEmpty
    True

    > Test-GUID -Value 00000000-0000-0000-0000-000000000xxx
    False

    > Test-GUID -Value 00000000-0000-0000-0000-000000000xxx -IsValidButEmpty
    False

    > Test-GUID -Value 00000000-0000-0000-0000-000000123abc
    True

    > Test-GUID -Value 00000000-0000-0000-0000-000000123abc -IsValidButEmpty
    False

    .Notes
    Copyright © 2022 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2022-09-13 (so): Created; heavily based on <https://morgantechspace.com/2021/01/powershell-check-if-string-is-valid-guid-or-not.html>.
#>

[OutputType([bool])]
Param
(
    [Parameter(Mandatory = $true)]
    $Value,

    [switch] $IsValidButEmpty
)

$ObjectGuid = [System.Guid]::empty

# Returns True if successfully parsed, otherwise returns False.
$result = [System.Guid]::TryParse($Value, [System.Management.Automation.PSReference] $ObjectGuid)

# Alternative: Trying to match with an elaborated Regular Expression
# https://morgantechspace.com/2021/01/powershell-check-if-string-is-valid-guid-or-not.html
# $Value -match("^(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}$")

if ($IsValidButEmpty -and ($result -eq $true))
{
    # Sometimes, placeholder/dummy values are used, like "00000000-0000-0000-0000-000000000000".
    $result = if (([System.Guid]::New($Value)) -eq [System.Guid]::empty) { $true } else { $false }
}

return $result
