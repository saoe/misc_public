<#
    .Synopsis
    Helper function to create a log entry and/or a log file.

    .Description
    Mainly of interest for internal use, i.e. by another function or in another script.
    
    Puts a timestamped log message text to the console, and/or to a text file,
    and/or to a variable, or simply returns it for further processing.
    
    .Parameter Message
    The actual text of the log entry. 
    
    .Parameter NoTimestamp
    Switch parameter to determine that no timestamp should be prepended to the message text.
    
    .Parameter NoLevel
    Switch parameter to determine that the log level will not be prepended to the message text.
    
    .Parameter CLI
    Switch parameter to determine that message text will also be written to
    the console (the command-line-interface/CLI).
    
    .Parameter Output
    Specifies a variable reference, so that the log entry can be added to that variable
    (take note: add -- the previous content of that variable will not be overwritten).
    
    Note: Must be a variable reference; e.g. like this:
    > ... -Output ([ref] $Variable)
    
    .Parameter FilePath
    Path to a text file to which the log entry should be appended.
    (If the file/the path doesn't exist yet, it will be created first.)
    
    .Parameter PassThru
    Switch parameter to determine that the function simply returns ('passes through')
    the log entry, for further processing.
    
    .Parameter Level
    Determines the Log-Level:
    - 'Verbose'
    - 'Information' (= Default)
    - 'Success'
    - 'Warning'
    - 'Error'
    
    .Example
    Out-amLog -Message "This is a text for logging" -CLI -FilePath "X:\Blah\Logs\Log-2024.txt"
    
    The message will be written to the CLI, as well as appended to the specified file. 
    
    .Example
    Out-amLog -Message "This is a text for logging"" -CLI -Output ([ref] $Var)
    
    The message will be written to the CLI, as well as added to the variable $Var,
    which is being referenced.
    
    .Notes
    Copyright © 2024 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    2024-04-04 (so): Created.
    2024-04-05 (so): * Renamed function from 'Out-saoeLog' to 'Write-saoeLog'; seems to be a better fit.
                     * Added parameter '-Level'.
                     * Added parameter '-PassThru'.
                     * Generally refactored it a bit more (e.g. '-CLI' comes now with colored text).
    2024-04-25 (so): * Added 'AllowEmptyString()' to the 'Message' parameter.
                     * Fixed forgotten translation: It's 'Error', not 'Fehler'
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory, ValueFromPipeline)] [AllowEmptyString()] [string] $Message,
    [switch] $NoTimestamp,
    [switch] $NoLevel,
    [switch] $CLI,
    [ref]    $Output,
             $FilePath,
    [switch] $PassThru,
    [ValidateSet('Verbose', 'Information', 'Success', 'Warning', 'Error')] $Level = 'Information'
)

Process
{
    ForEach ($msg in $Message)
    {
        if     ($NoTimestamp -and $NoLevel) { $str = $msg }
        elseif ($NoTimestamp)               { $str = ("{0, -11} | {1}" -f $Level, $msg) }
        elseif ($NoLevel)                   { $str = "{0} | {1}" -f (Get-Date -Format 'yyyy-MM-dd HH:mm:ss.fff K'), $msg }
        else                                { $str = "{0} | {1, -11} | {2}" -f (Get-Date -Format 'yyyy-MM-dd HH:mm:ss.fff K'), $Level, $msg }

        if ($CLI)
        {
            switch ($Level)
            {
                'Verbose'     { write-host $str -ForegroundColor Cyan }
                'Information' { write-host $str }
                'Success'     { write-host $str -BackgroundColor Black -ForegroundColor Green  }
                'Warning'     { write-host $str -BackgroundColor Black -ForegroundColor Yellow }
                'Error'       { write-host $str -BackgroundColor Black -ForegroundColor Red    }
                Default       { write-host $str }
            }
        }

        if ($FilePath)
        {
            if (-not (Test-Path -PathType Leaf -Path $FilePath))
            {
                # We may need to create the path first; so we have to simply assume (hope)
                # that the final component is a filename, and the stuff before are directories.

                $Directory = (split-path $FilePath -Parent)
                
                if (-not (Test-Path -PathType Container -Path $Directory))
                {
                    try   { New-Item -ItemType Directory -Path $Directory -ErrorAction Stop | Out-Null }
                    catch { write-warning $_.Exception.Message }
                }
                
                New-Item -ItemType File -Path $FilePath | Out-Null
            }
            
            $str | Out-File $FilePath -append -encoding default
        }

        iif ($Output) { $Output.value += $str + "$([Environment]::NewLine)" }
        
        if ($PSBoundParameters['PassThru']) { $str }
    }
}
