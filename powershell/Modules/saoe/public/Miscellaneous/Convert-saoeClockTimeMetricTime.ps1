<#
    .Synopsis
    Convert from "Clock Time" (e.g. 08:10) to "Metric Time" (e.g. 8,17); or the other way around.
    
    .Example
    Convert-saoeClockTimeMetricTime -Time 7:45
    
    .Example
    Convert-saoeClockTimeMetricTime -Time "8,30"
    
    Note that the argument needs to be enclosed in quotes if you're using a comma!
    Otherwise it will not be detected as a string.
    
    .Example
    Convert-saoeClockTimeMetricTime -Time 9.50 -MetricDelimiter .
    
    The default delimiter for a metric fraction is the comma, as it should be :-)
    But you can override it.
    
    .Notes
    Copyright � 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2020-12-14 (so): Created.
#>

param
(
    [Parameter(Mandatory=$true)]
    [string] $Time,
    
    [string] $MetricDelimiter = ','
)

if ($Time.contains(':'))
{
    $time_components = $Time -split ':'

    if (($time_components[0] -notmatch "^\d*$") -or ($time_components[1] -notmatch "^\d*$"))
    {
        Write-Error "Only digits are allowed!" -ErrorAction Stop
    }

    # Convert ClockTime hours and minutes to a MetricTime hour value.
    # Example: "1:35" (1 hour, 35 minutes) -> 35 / 60 -> 0.58333... -> 1.58 hours

    $hours_MetricTime = $time_components[0]
    $minutes_MetricTime = $time_components[1] / 60

    $value = [float]$hours_MetricTime + [float] $minutes_MetricTime
    $precision_points = 2

    Write-Host "$Time (Clock Time) is in Metric Time:"
    [math]::Round($value, $precision_points)
}
elseif ($Time.contains($MetricDelimiter))
{
    $time_components = $Time -split [regex]::escape($MetricDelimiter)
    
    if (($time_components[0] -notmatch "^\d*$") -or ($time_components[1] -notmatch "^\d*$"))
    {
        Write-Error "Only digits are allowed!" -ErrorAction Stop
    }
    elseif ($time_components.count -gt 2)
    {
        Write-Error "Found more than one comma in expression." -ErrorAction Stop
            # Count returns the count of split components (i.e. before and after the delimiter), not the count of the delimiter itself.
    }

    # Convert a MetricTime hour value to ClockTime hours and minutes.
    # Example: "1,45" (hours) -> 0,45 * 60 -> 27 -> 1:27 (1 hour, 27 minutes)

    $hours_ClockTime = $time_components[0]
    $minutes_ClockTime = $time_components[1] / ([math]::Pow(10, ($time_components[1].Length))) * 60
    
    Write-Host "$Time (Metric Time) is in Clock Time:"
    $hours_ClockTime + ":" + $minutes_ClockTime
}
else
{
    Write-Warning "Wrong delimiter: Use ':' for clock time; or $MetricDelimiter for metric time"
}
