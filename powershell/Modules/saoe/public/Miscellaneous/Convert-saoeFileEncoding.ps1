<#
    .Synopsis
    Convert the content of a text file to another character encoding.

    .Description
    Converts the content of a text file to another character encoding and
    writes the result to a (different) text file.

    .Parameter InFile
    The text file whose content should be read and converted.
 
    .Parameter To
    Determines to which character encoding the content should be converted:
    - 'UTF8NoBOM': UTF-8 without Byte Order Mark
    - 'ANSI'     : Windows Code Page 1252
    
    .Parameter OutFile
    [Optional] The name/path of the file to which the conversion result should be saved.
    
    If this parameter is omitted, a default name will be used, based on some rules:
    - The name of the input file will be kept, but extended with a supplement that indicates
      the new encoding; for example: 'Filename.txt' -> 'Filename_UTF8NoBOM.txt'.
    - The default save path is the current working directory.
    
    .Parameter Force
    Forces to (over)write the OutFile.
    Else, a few measures of precaution will kick in, in case InFile and OutFile point
    to the same file or if the OutFile already exists.
    
    .Example
    Convert-saoeFileEncoding -InFile C:\File.txt -To UTF8NoBOM
    
    The content of File.txt will be converted to an "UTF-8 without BOM" character encoding
    and then saved as File_UTF8NoBOM.txt in the current working directory.
    
    .Example
    Convert-saoeFileEncoding -InFile C:\File.txt -To ANSI -OutFile Z:\NewFile.txt
    
    The content of File.txt will be converted to an "ANSI" character encoding
    and then saved as NewFile.txt in Z:.
    
    .Example
    Convert-saoeFileEncoding -InFile C:\File.txt -To UTF8NoBOM -OutFile Z:\NewFile.txt
    
    The content of File.txt will be converted to an "UTF-8 without BOM" character encoding
    and should then be saved as NewFile.txt in Z: -- but since that file already exists,
    the function will abort with a warning.
    
    One must provide the switch parameter '-Force' to overwrite that file:
    > Convert-saoeFileEncoding -InFile C:\File.txt -To UTF8NoBOM -OutFile Z:\NewFile.txt -Force

    .Notes
    Copyright © 2022, 2024 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    Resources:
    - https://techblog.dorogin.com/changing-source-files-encoding-and-some-fun-with-powershell-df23bf8410ab
    
    What is [System.Text.Encoding]::Convert()?
    
    2022-03-31 (so): Initial draft.
    2024-02-27 (so): Ready:
                     * Can now handle values from the pipeline.
                     * Can now also convert to 'ANSI' (Windows Code Page 1252).               
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
    [ValidateScript({Test-Path -Path $_ -PathType Leaf})]
    $InFile,
    
    [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
    [ValidateSet('UTF8NoBOM', 'ANSI')]
    $To,
    
    $OutFile,
    [switch] $Force
)

Begin
{
    if ($PSVersionTable.PSVersion.Major -ge 6) { $isPS6orHigher = $true }
    else                                       { $isPS6orHigher = $false }
}

Process
{
    ForEach ($file in $InFile)
    {
        try
        {
            # From a previous attempt (from 2022): What difference?
            # ...
            #if ($isPS6orHigher) { $InputData = Get-Content $InputFile -AsByteStream  -ReadCount 0 }
            #else                { $InputData = Get-Content $InputFile -Encoding Byte -ReadCount 0 }
            
            $InputData = get-content -raw -path $file
            $InputItem = get-item -path $file
            
            switch ($To)
            {
                'UTF8NoBOM'
                {
                    # In older versions of Powershell (before 'Core'/Version 6.x), the standard encoding for UTF-8 was
                    # 'UTF-8 with BOM (Byte Order Mark)' and one had to do some extra work to get an 'UTF-8 without BOM' encoding (see below).
                     # ('Without BOM' is nowadays the most common and widely accepted version of UTF-8, thanks to the Web, Linux et. al.)
                    $Encoding = [System.Text.UTF8Encoding]::new($false)
                    $Suffix = "_UTF8NoBOM"
                    break
                }
                
                'ANSI'
                {
                    # "ANSI" usually means "Windows Code Page 1252"/"Western European (Windows)").
                    # GetEncoding(28591) would return "ISO-8859-1"/"Western European (ISO)".
                    # [System.Text.Encoding]::GetEncodings() lists the available encodings.
                    $Encoding = [System.Text.Encoding]::GetEncoding(1252)
                    $Suffix = "_ANSI"
                    break
                }
            }
        
            # --------------------------------------------------------------------------------------------------
            # Where to save the converted content (name/path):
            
            if (-not $OutFile) { $OutputFilePath = join-path $PSScriptRoot ($InputItem.BaseName + $Suffix + $InputItem.Extension) }
            else               { $OutputFilePath = $OutFile }
            
            if (Test-Path -Path $OutputFilePath -PathType Leaf)
            {
                if (-not $Force)
                {
                    write-warning "File '$($OutputFilePath)' already exists! Use parameter '-Force' to overwrite."
                    continue
                }
            }
            
            # --------------------------------------------------------------------------------------------------
            # Convert and save:
            
            # https://stackoverflow.com/questions/495618/how-to-normalize-a-path-in-powershell
            #   Especially: https://stackoverflow.com/a/495875
            #   > .NET's GetFullPath() works with non-existent paths as well.
            #   > Its downside is the need to sync .NET's working folder with Powershell first.
            [System.IO.Directory]::SetCurrentDirectory(((Get-Location -PSProvider FileSystem).ProviderPath))
            $OutputFullFilePath = [System.IO.Path]::GetFullPath($OutputFilePath)
            
            # Using the .NET function because Powershell's Set-Content and Out-File with '-Encoding UTF8' always adds a BOM.
            #[System.IO.File]::WriteAllLines($OutputFullFilePath, $InputData, $Encoding) # ... input: strings.
            [System.IO.File]::WriteAllText($OutputFullFilePath, $InputData, $Encoding)
            write-host "Saved file '$($OutputFullFilePath)'."
        }
        catch
        {
            Write-Error $_.Exception.Message
        }
    }
}
