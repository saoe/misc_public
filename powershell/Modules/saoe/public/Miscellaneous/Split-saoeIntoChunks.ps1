<#
    .Synopsis
    Split an input object into smaller chunks.
    
    .Description
    Given an input array, it will be split according to the parameters supplied and then
    returned as an array of arrays.
    
    .Parameter Items
    The maximum amount of items that each chunk can have; the number of chunks are calculated.
    
    .Parameter Chunks
    The amount of chunks that should be created from the input data; the number of items per
    chunk will be calculated.
   
    .Example
    $inArray = @('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
                 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z')
    
    $out1 = Split-saoeIntoChunks -InputObject $inArray -Items 2 -verbose
    ForEach ($o1 in $out1) { write-host "$o1`n---" }

    $out2 = Split-saoeIntoChunks -InputObject $inArray -Chunks 2 -Verbose
    ForEach ($o2 in $out2) { write-host "$o2`n~~~" }
   
    .Notes
    Copyright © 2021 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    2021-04-07 (so): Created.
#>

[CmdletBinding()]
Param
(
    [Parameter(ParameterSetName="Items")]
    [Parameter(ParameterSetName="Chunks")]
    [Parameter(Mandatory=$true, ValueFromPipeline)]
    $InputObject,
    
    [Parameter(ParameterSetName="Items")]
    $Items,
    
    [Parameter(ParameterSetName="Chunks")]
    $Chunks
)

Begin
{
    $OutputData = @()
    
    Switch ($PsCmdlet.ParameterSetName)
    {
        "Items"
        {
            $MaxItemCountPerChunk = $Items
            $LoopCount = [math]::Ceiling($InputObject.Length/$Items)
        }
        
        "Chunks"
        {
            $MaxItemCountPerChunk = [math]::Ceiling($InputObject.Length/$Chunks)
            $LoopCount = $Chunks
        }
    }
    
    write-verbose "Max. number of chunks: $($LoopCount)"
    write-verbose "Max. items per chunk.: $($MaxItemCountPerChunk)"
}

Process
{
    for ($i = 0; $i -lt $LoopCount; $i++)
    {
        $start_index = $i * $MaxItemCountPerChunk
        $end_index = ($start_index + $MaxItemCountPerChunk) - 1
        
        # To prevent an 'index out of range' error: If we're on the last run of the loop,
        # then the final/end index must also point to the final item of the input object.
        if ($i -eq ($LoopCount - 1))
        {
            $end_index = ($InputObject.Length - 1)
        }
        
        write-verbose "-----------------------------------"
        write-verbose "Chunk #$($i+1) at index $($i)"
        write-verbose "Index Start: $($start_index) (Value: $($InputObject[$start_index]))"
        write-verbose "Index End..: $($end_index) (Value: $($InputObject[$end_index]))"
        
        $OutputData += ,@($InputObject[$start_index..$end_index])
            # The comma is not a typo: It's an array construction operator <https://devblogs.microsoft.com/powershell/array-literals-in-powershell/>
        
        write-verbose "Chunk items: $($OutputData[$i])"
    }
}

End
{
    ,$OutputData
        # Again, the comma is critical here:
        # "PowerShell unrolls arrays returned from a function. Prepend the returned array with
        #  the comma operator (unary array construction operator) to wrap it in another array,
        #  which is unrolled on return, leaving the nested array intact."
        # -- <https://stackoverflow.com/a/40220900>
}
