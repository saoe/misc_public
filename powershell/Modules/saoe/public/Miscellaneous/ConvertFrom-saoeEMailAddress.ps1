<#
    .Synopsis    
    Convert the user name from an e-mail address/user-principal-name to a display name text.
    
    .Description
    Extracts the user-name part of an e-mail address (or an user-principal-name) and returns it as
    a text string that can be used as a display name: With spaces and upper/lower case letters.
    
    Works for any input that adheres to a format that is similar to an e-mail address or
    an user principal name; meaning:    
    {user name/logon name/UPN prefix} {separator: 'at'-symbol (@)} {domain name/UPN suffix}
    
    Plus some special considerations to detect a given name and a surname: These parts should be
    divided by either dots ('.') or underscores ('_') or a mix of both.
    
    Yet still, there will remain exceptions/issues; for example:
    "geordi.la.forge@example.net": This function will not recognize that the full surname is
    "la forge", and not just "forge"; but as a standard display name, it still is helpful, as it
    will return "Geordi La Forge": With spaces and proper case.
    
    .Parameter Data
    Usually an e-mail address or user-principal-name.
    
    .Parameter Layout
    Determines the layout of the output:
    
    - GivenName_Surname - "<GivenName> <Surname>"  (default)
    - Surname_GivenName - "<Surname>, <GivenName>" (note the comma!)
    - GivenName         - "<GivenName>"
    - Surname           - "<Surname>"
    
    .Example
    > ConvertFrom-saoeEMailAddress -Data 'HOSHI.SATO@example.net'
    
    Hoshi Sato
    
    .Example
    > ConvertFrom-saoeEMailAddress -Data james_tiberius.kirk@example.net
    
    James Tiberius Kirk
    
    .Example
    > ConvertFrom-saoeEMailAddress -Data jean-luc.picard@example.net
    
    Jean-Luc Picard
    
    .Example
    > 'HOSHI.SATO@example.net', 'james_tiberius.kirk@example.net', 'Geordi.La.Forge@example.net' | ConvertFrom-saoeEMailAddress -Layout B
    
    Sato, Hoshi
    Kirk, James Tiberius
    Forge, Geordi La
    
    .Example
    > $array = @('HOSHI.SATO@example.net', 'james_tiberius.kirk@example.net', 'Geordi.La.Forge@example.net')
    > $array | ConvertFrom-saoeEMailAddress -Layout B
    
    Sato, Hoshi
    Kirk, James Tiberius
    Forge, Geordi La
    
    .Example
    > ConvertFrom-saoeEMailAddress -Data $array
    
    Hoshi Sato
    James Tiberius Kirk
    Geordi La Forge
    
    .Example
    > ConvertFrom-saoeEMailAddress -Data kathryn-janeway@example.net
    
    Failure, because "-" (hyphen) will not be considered as a separator.
    
    .Example
    > ConvertFrom-saoeEMailAddress -Data KathrynJaneway@example.net
    
    Failure, because there is no separator symbol, just lower/upper case letters.
        
    .Notes
    Copyright © 2023 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    2023-07-11 (so): Created.
    2023-07-16 (so): Revised.
#>

[CmdletBinding()]
Param
(
    [Parameter(ValueFromPipeline, Mandatory=$true)] [string[]] $Data,
    [ValidateSet("GivenName_Surname", "Surname_GivenName", "GivenName", "Surname")] $Layout = "GivenName_Surname"
)

Process
{
    ForEach ($n in $Data)
    {
        $n0 = $n.split('@')

        if (@($n0).count -eq 2)
        {
            $n1 = $n0[0]
            
            if (($n1 -match '.') -or ($n1 -match '_'))
            {
                $n2 = ($n1.replace('_', ' ').replace('.', ' ')).split(' ')
               
                if (@($n2).count -ge 2)
                {
                    $GivenName = (Get-Culture).TextInfo.ToTitleCase(($n2[0..(@($n2).count-2)]).ToLower())
                        # From the first element [index 0] to the one before the last [length -2]
                        # (the last one [-1] is supposedly the surname, see below).
                        # Example:
                        # - The count of the elements is 3: [#1|james] [#2|tiberius] [#3|kirk]
                        # - But offset/index begins at 0  : [ 0|james] [ 1|tiberius] [ 2|kirk]
                   
                    $Surname = (Get-Culture).TextInfo.ToTitleCase(($n2[-1]).ToLower())
                     
                    switch ($Layout)
                    {
                        "GivenName"         { return $GivenName }
                        "Surname"           { return $Surname }
                        "GivenName_Surname" { return "$GivenName $Surname" }
                        "Surname_GivenName" { return "$Surname, $GivenName" }
                         default            { return "$GivenName $Surname" }
                    }
                }
                else { write-warning "$($n2): Can't identify given name/surname." }
            }
            else { write-warning "$($n1): Can't identify given name/surname." }
        }
        else { write-warning "$($n0): Doesn't seem to be a valid e-mail address or UserPrincipalName." }
    }
}