<#
    .Synopsis
    Prompt the user for a choice in a text menu of options
    
    .Description
    The menu is created on-the-fly by the function based on a multi-dimensional array:

    .Parameter Caption
    The caption or title text of the menu
    
    .Parameter Message
    The text at the prompt, when asking the user for input (after showing the caption and
    the menu of available choices).
    
    .Parameter Choices
    A multi-dimensional array to determine the choice menu.
    
    ~~~ Example: ~~~
    $Choices = @(
        ("Server&1", "Logon to server1.example.net"),
        ("Server&2", "Logon to server2.example.net"),
        ("Server&3", "Logon to server3.example.net"),
        ("&TestA"  , "Test A: OK: First letter is the shortcut"),
        ("&TestB"  , "Test B: Fatal error: Not unique (same letter as the shortcut)"),
        ("TestC"   , "Test C: Warning: Without shortcut marker"),
        ("TestD&"  , "Test D: Fatal error: Shortcut marker at invalid position"),
        ("&Quit"   , "Quit/Abort")
    )
    ~~~~~~~~~~~~~~~~
    
    - The first entry ("Server&1") is the value and determines the shortcut for the selection:
      The character following the ampersand: "Server&1" -> '1'.
    
      Note:
      * If no shortcut is given, the item will not appear in the menu!
      * If it is at an invalid position (e.g. no other character right to it),
        the function will exit with a fatal error!
      * If the shortcut character is not unique in the menu,
        the function will exit with a fatal error!
    
    - The other entry ("Logon to...") is a short help text for the user.
    
    .Parameter Default
    Optional value that determines a default value (selected when the user simply hits ENTER).
    It corresponds to the shortcut character! It is not an index into the menu list!
    
    For "Server&1", do    -Default '1'
                    don't -Default 1
    
    .Parameter Format
    Optional. The menu can be displayed in slightly different formats.
    Currently one can choose between V1 and V2 (default).
    
    .Example
    > $Result = Select-saoeChoice -Caption "Text One" -Message "Text Two" -Choices $Choices -Default '1'
    
    .Notes
    Copyright © 2023-2024 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2023-07-09 (so): Initial release.
    2024-02-23 (so): Using it for real (in my day job) showed me that the help comment needed some polish.
    2024-06-07 (so): Added support for parameter '-Format'.
#>

Param
(
    [Parameter(Mandatory=$true)] $Caption,
    [Parameter(Mandatory=$true)] $Message,
    [Parameter(Mandatory=$true)] $Choices,
    [ValidateSet("V1", "V2")]    $Format = "V2",
                                 $Default
)

$ShortcutMarker   = '&'
    # The letter following this symbol (i.e. right to it) will become the shortcut ID; for example:
    # "&Test" -> 'T' | "Test&2" -> '2' | "Test3&" -> Fatal error: No letter right to the symbol!

$ShortcutForegroundColor = 'Green'
$ShortcutBackgroundColor = $host.ui.RawUI.BackgroundColor.ToString()
$HelpForegroundColor     = 'DarkGray'

[PSCustomObject] $Menu = @()

# Build the menu, according to the provided choices:
ForEach ($Choice in $Choices)
{
    if (($Choice[0]).Contains($ShortcutMarker))
    {
        [int] $i = ($Choice[0]).IndexOf($ShortcutMarker)
        
        try
        {
            $Shortcut = $Choice[0][$i+1]
        }
        catch [System.IndexOutOfRangeException]
        {
            write-error "'$($Choice[0])': Shortcut marker is at invalid position!" -ErrorAction Stop
        }       
    
        $MenuItem = [PSCustomObject] @{
            Shortcut = $Shortcut
            Value    = $Choice[0]
            Help     = $Choice[1]
        }
        
        $Menu += $MenuItem
    }
    else
    {
        write-warning "'$($Choice[0])': No shortcut marker! This entry will be skipped/ignored!"
        continue
    }
}

$ShortcutIsNotUnique = $Menu | Group-Object -Property Shortcut | Where-Object { $_.Count -gt 1 }

if ($ShortcutIsNotUnique)
{
    $ProblematicShortcut = ($ShortcutIsNotUnique.Group | select -expand Shortcut)[0]
    write-error "The shortcut '$($ProblematicShortcut)' is not unique!" -ErrorAction Stop
}
else
{
    $DefaultShortcut = $Menu | ? { $_.Shortcut -eq $Default } | select -expand Shortcut    
    $ValidShortcuts = @(($Menu | select -expand Shortcut))
    
    # Display the menu:
    Write-Host $Caption
    
    ForEach ($M in $Menu)
    {
        # Highlight the default entry by inverting the colors of the shortcut:
        if ($M.Shortcut -eq $DefaultShortcut) { write-host "[$($M.Shortcut)]" -NoNewline -ForegroundColor $ShortcutBackgroundColor -BackgroundColor $ShortcutForegroundColor }
        else                                  { write-host "[$($M.Shortcut)]" -NoNewline -ForegroundColor $ShortcutForegroundColor }

        switch ($Format)
        {
            "V1"
            {
                write-host " $(($M.Value).replace($ShortcutMarker, '')) " -NoNewline
                write-host "($($M.Help))" -ForegroundColor $HelpForegroundColor
            }

            "V2"
            {
                write-host (" {0,-15}: {1}" -f ($M.Value).replace($ShortcutMarker, ''), $M.Help)
            }
        }
    }

    # Get the input from the user and check if it's acceptable:
    # - Just ENTER means "use the default shortcut".
    # - Otherwise, if it's a valid shortcut, use that shortcut.
    # - Otherwise: Abort, because it's not an acceptable input.

    $in = Read-Host -Prompt ($Message + " (Default is '$($DefaultShortcut)')")
    
    if ([string]::IsNullOrEmpty($in))
    {
        try
        {
            $Menu | ? { $_.Shortcut -eq $DefaultShortcut } | select -expand Shortcut
        }
        catch [System.IndexOutOfRangeException] { write-error "'$($Default)': $($_.Exception.Message)" -ErrorAction Stop }
    }
    elseif ($ValidShortcuts -match $in)
    {
        $Menu | ? { $_.Shortcut -eq $in } | select -expand Shortcut
    }
    else
    {
        write-warning "'$($in)' is not a valid input shortcut!"
    }
}
