<#
    .Synopsis
    Compare and count the content (items) of multiple arrays.

    .Description
    Compares and counts the content (items) of multiple arrays.
    That way, similarities and differences between them can be revealed.
    
    .Parameter Format
    "Table"         : Display output as a cross-table on the console [Default].
    "PSCustomObject": Return the pure PSCustomObject.
    
    .Parameter Arrays
    The arrays, whose content should be compared.

    .Example
    > $A = @("One", "Two", "Three")
    > $B = @("One", "Two", "Three", "Four")
    > $C = @(       "Two", "Three", "Four", "Four")
    > Compare-saoeArrays -Arrays $A, $B, $C -Format Table

    Count Item  Array #1 Array #2 Array #3
    ----- ----  -------- -------- --------
        3 Two   1x       1x       1x
        3 Three 1x       1x       1x
        3 Four           1x       2x
        2 One   1x       1x

    .Notes
    Copyright © 2024 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
            
    2024-05-31 (so): Created; based on a similar function in which I compared the AD group memberships of multiple AD users.
        This function is specialized in a different way (comparing the content of any arrays), but at the same time also slightly more generic.
    
        I originally also had planned to process pipeline input, but to my surprise, I couldn't bring it to work as I intended. 
        After trying for hours numerous alternative techniques, I was at last getting closer to the result that I imagined:
        Closer, but not yet done.
        Additionally, that latest approach had already bloated and complicated the code unnecessarily; that's why finally decided against it.
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory)]                   $Arrays,
    [ValidateSet("Table", "PSCustomObject")] $Format = "Table"
)

# --------------------------------------------------------------------------------------------------
# Abort if only one array is provided.
# Analyzing a single array works, technically. But the result is then in a different/illogical format.
# (And by the way: What good is a comparison function with just one dataset?!)
# By the way: It was way harder than expected to find a reliable method for checking whether one or multiple arrays are provided as a parameter!

$NotArrays = $Arrays | ? { ($_.GetType()).BaseType.ToString() -ne 'System.Array' }
if ($NotArrays) { write-warning "Input must be two (or more) arrays!"; break }

# --------------------------------------------------------------------------------------------------

$Template = [PSCustomObject] @{
    Count    = $null
    Item = $null
    # More to come...: Further down, each input array will be added as a a distinct key (= NoteProperty) to this data structure!
}

$ArrayContentMapping = @{}
$Data                = [System.Collections.Generic.List[object]] @()

# --------------------------------------------------------------------------------------------------
# Extend the PSCustomObject, so that for each input array a distinct key (= NoteProperty) will be available in the template.
# Unfortunately, Powershell can't get the actual name of a variable (neither from the metadata of the variable nor the parameter;
# it always resolves them and only returns the values of the variable).
# That's why a generic name is being used, with the addition of an increasing counter.
# The order of the input is the same as in the output, that's how one can still recognize what is which.

$Counter = 0

ForEach ($Array in $Arrays)
{
    $Counter++
    try   { $Template | Add-Member -MemberType NoteProperty -Name "Array #$($Counter)" -Value $null -EA Stop }
    catch { write-verbose $_.Exception.Message }
    
    try   { ForEach ($i in $Array) { $ArrayContentMapping[$i] += @($i) } }
    catch { write-warning $_.Exception.Message }
}

# --------------------------------------------------------------------------------------------------
# Combine and assign the data, format the results and also count how often each item appears (per array):

ForEach ($ACM in $ArrayContentMapping.GetEnumerator())
{
    $t = $Template.PSObject.Copy()
    
    $t.Item  = $ACM.Key
    $t.Count = @($ACM.Value).Count
            
    $Counter = 0
    ForEach ($Array in $Arrays)
    {
        $Counter++
        # Attention: Use '-contains', because when using '.contains()' or '-match' instead, partial expressions (= similar text!)
        #            would also be wrongly registered and counted!
        $ItemCount = @($Array | ? { $_ -contains $t.Item }).count
        #if ($Array -contains $t.Item ) { $t."Array #$($Counter)" += ($ItemCount.ToString() + "x " + ($t.Item).ToString()) }
        if ($Array -contains $t.Item ) { $t."Array #$($Counter)" += ($ItemCount.ToString() + "x") }
    }
    
    $Data.Add($t)
}

# --------------------------------------------------------------------------------------------------
# Output the result:

switch ($Format)
{
    "Table"          { $Data | select * | sort -property Count, Item -Descending | format-table -Autosize; break }
    "PSCustomObject" { $Data; break }
    Default          { $Data }
}
