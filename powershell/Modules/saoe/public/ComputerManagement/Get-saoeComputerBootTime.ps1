<#
    .Synopsis
    Get the last boot time of this computer.
    
    .Description
    Gets the timestamp of the last full reboot/restart (as reported by the OS) of this local machine.
    
    Note that recent Windows versions (since around version 10 and later) may internally normally
    not really do a full restart in common daily situations; instead they then may only put a system
    to a sleep state or energy-saving mode or wake a system up again from such a hibernation mode.
    
    This function should report back the time when the operating system did a real, full reboot
    (as it is often required after a Windows Update).
    
    .Notes
    Copyright © 2024 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    2024-02-18 (so): Created.
#>

[CmdletBinding()] Param ()

Get-CimInstance -ClassName Win32_OperatingSystem | select -expand LastBootUptime 
