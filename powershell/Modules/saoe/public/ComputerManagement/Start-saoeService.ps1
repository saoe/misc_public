<#
    .Synopsis
    Start a service on the local machine, or on a remote machine.

    .Parameter ServiceName
    The name of the service that should be started; default is "WinRM".
    
    .Parameter ComputerName
    The FQDN of a remote machine; the default is the local machine.
    
    .Parameter Credential
    User credentials, if needed.
    
    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    Using Get-WmiObject, Invoke-WmiMethod, etc. because without a running WinRM (Windows Remote
    Management) service on the remote machine, cmdlets like Get-Service, Start-Service, etc.
    will not work there. And sometimes WinRM is exactly the one service that you now want to
    start with this function...
#>

[CmdletBinding()]
Param
(
    [string] $ServiceName = "WinRM",
    [string] $ComputerName = "localhost",
    [PSCredential] $Credential
)

$service = Get-WmiObject Win32_Service -ComputerName $ComputerName -Credential $Credential | where {$_.Name -eq "$ServiceName" }

$service | Where-Object {
    if ($_.State -eq "Stopped")
    {
        Write-Host "*** Service $ServiceName is stopped, trying to start..."
        Invoke-WmiMethod -Path "Win32_Service.Name='$ServiceName'" -Name StartService -Computername $ComputerName -Credential $Credential | Out-Null
        Start-Sleep -Seconds 5
        Write-Host "Service $ServiceName is now: " $(Get-WmiObject Win32_Service -ComputerName $ComputerName -Credential $Credential | where {$_.Name -eq "$ServiceName" }).State
    }
    elseif ($_.State -eq "Running")
    {
        Write-Host "*** Service $ServiceName is running"
    }
    else
    {
        Write-Host "*** Service $ServiceName is in an unknown state"
    }
}
