<#
    .Synopsis
    Enables RSAT module(s)/optional feature(s) on Windows 10 version post-1809.

    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>    
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
#>

[CmdletBinding()]
Param
(
    [ValidateSet("all", "ActiveDirectory", "DNS", "FileServices", "GroupPolicy", "DHCP", "WSUS")]
    [Parameter(Mandatory=$true)]
    [string] $Component
)

switch ($Component)
{
    all             { $tool = "RSAT*" }
    ActiveDirectory { $tool = "Rsat.ActiveDirectory.DS-LDS.Tools~~~~0.0.1.0" }
    DNS             { $tool = "Rsat.Dns.Tools~~~~0.0.1.0" }
    FileServices    { $tool = "Rsat.FileServices.Tools~~~~0.0.1.0" }
    GroupPolicy     { $tool = "Rsat.GroupPolicy.Management.Tools~~~~0.0.1.0" }
    DHCP            { $tool = "Rsat.DHCP.Tools~~~~0.0.1.0" }
    WSUS            { $tool = "Rsat.WSUS.Tools~~~~0.0.1.0" }
    # { $tool = "Rsat.IPAM.Client.Tools~~~~0.0.1.0" }
    # { $tool = "Rsat.LLDP.Tools~~~~0.0.1.0" }
    # { $tool = "Rsat.NetworkController.Tools~~~~0.0.1.0" }
    # { $tool = "Rsat.NetworkLoadBalancing.Tools~~~~0.0.1.0" }
    # { $tool = "Rsat.BitLocker.Recovery.Tools~~~~0.0.1.0" }
    # { $tool = "Rsat.CertificateServices.Tools~~~~0.0.1.0" }
    # { $tool = "Rsat.FailoverCluster.Management.Tools~~~~0.0.1.0" }
    # { $tool = "Rsat.RemoteAccess.Management.Tools~~~~0.0.1.0" }
    # { $tool = "Rsat.RemoteDesktop.Services.Tools~~~~0.0.1.0" }
    # { $tool = "Rsat.ServerManager.Tools~~~~0.0.1.0" }
    # { $tool = "Rsat.Shielded.VM.Tools~~~~0.0.1.0" }
    # { $tool = "Rsat.StorageMigrationService.Management.Tools~~~~0.0.1.0" }
    # { $tool = "Rsat.StorageReplica.Tools~~~~0.0.1.0" }
    # { $tool = "Rsat.SystemInsights.Management.Tools~~~~0.0.1.0" }
    # { $tool = "Rsat.VolumeActivation.Tools~~~~0.0.1.0" }
}

Add-WindowsCapability -Online -Name $tool
