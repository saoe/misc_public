<#
    .Synopsis
    Add an AD user to the local administrator group on a remote computer.
    
    .Example
    Add-saoeADUserFromLocalAdministrators -User user.name -Domain some.dom.net -Computer ClientName123

    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
#>

[CmdletBinding()]
param
(
    [Parameter(Mandatory=$true)] [String] $User,
    [Parameter(Mandatory=$true)] [String] $Domain,
    [Parameter(Mandatory=$true)] [String] $Computer
)

$g = (New-Object System.Security.Principal.SecurityIdentifier('S-1-5-32-544')).Translate([System.Security.Principal.NTAccount]).Value
$x = $g.Split('\')

$AdminGroup = [ADSI]"WinNT://$Computer.$Domain/$($x[1]),group"
$UserName = [ADSI]"WinNT://$Domain/$User,user"
$AdminGroup.Add($UserName.Path)
