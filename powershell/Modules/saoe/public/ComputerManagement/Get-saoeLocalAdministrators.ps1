<#
    .Synopsis
    Get the members of the built-in administrators group on a computer client.

    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
#>

[CmdletBinding()]
param
(
    [Parameter(Mandatory=$true)] [String] $Domain,
    [Parameter(Mandatory=$true)] [String] $Computer
)

# Using "S-1-5-32-544", the "well-known SID (security identifier)" for the built-in administrators group.
# <https://docs.microsoft.com/en-us/troubleshoot/windows-server/identity/security-identifiers-in-windows>
# Prevents also language problems: EN group name: "Administrators"; DE group name: "Administratoren"; etc.
Get-LocalGroupMember -SID S-1-5-32-544
