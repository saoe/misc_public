<#
    .Synopsis
    Get the installation date (as reported by the OS) from the local or a remote computer.
    
    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
#>

[CmdletBinding()]
Param
(
    [string] $RemoteComputer
)

if (-not ($RemoteComputer))
{
    # Local computer
    Write-Host "This computer ($env:computername) was installed on: " -NoNewline
    Write-Output ([WMI]'').ConvertToDateTime((Get-WmiObject -Class Win32_OperatingSystem).InstallDate)
}
else
{
    # Remote computer
    $cred = Get-Credential # In case it's in a different domain.
    Write-Host "The remote computer $RemoteComputer was installed on: " -NoNewline
    Write-Output ([WMI]'').ConvertToDateTime((Get-WmiObject -Class Win32_OperatingSystem -ComputerName $RemoteComputer -Credential $cred).InstallDate)
}
