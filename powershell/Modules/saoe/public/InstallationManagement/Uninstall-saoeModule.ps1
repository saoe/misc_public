<#
    .Synopsis
    Uninstall the 'saoe' module on the local machine.
    
    .Description
    By default, the environment variable for the module paths ($Env:PSModulePath) contains:

    1) $PSHome\Modules                             (%Windir%\System32\WindowsPowerShell\v1.0\Modules)
       Do not install modules to this location! This location is reserved for modules that ship with Windows.

    2) $Home\Documents\WindowsPowerShell\Modules   (%UserProfile%\Documents\WindowsPowerShell\Modules)
       Use it for installing modules for a specific/the current user.

    3) $Env:ProgramFiles\WindowsPowerShell\Modules (%ProgramFiles%\WindowsPowerShell\Modules)
       Use it for installing modules for all users of a machine.
    
    .Parameter ForCurrentUser
    The module files will be deleted from the current user's PS modules folder (default value).
    (See number 2 in the description.)
    
    .Parameter ForCurrentUserFromThisLocation
    This script's original parent folder will be removed from the current user's environment variable 'PSModulePath'.
    No files will be deleted; instead, only the original path will be removed from the user's PS module path.
    
    .Parameter ForMachine
    [WIP, requires admin rights]. The module files will be deleted from the machine's PS modules folder.
    (See number 3 in the description.)

    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
#>

[CmdletBinding(DefaultParameterSetName="User")]
Param
(
    [Parameter(ParameterSetName = "User")]         [switch] $ForCurrentUser,
    [Parameter(ParameterSetName = "ThisLocation")] [switch] $ForCurrentUserFromThisLocation,
    [Parameter(ParameterSetName = "Machine")]      [switch] $ForMachine # [TODO] Requires admin rights.
)

try
{
    $module_folder_name = Split-Path $PSScriptRoot -Leaf
    
    if ($ForCurrentUser)
    {
        $to_be_removed = "$Home\Documents\WindowsPowerShell\Modules\$module_folder_name"
        Remove-Item -Path $to_be_removed -Recurse -Verbose -ErrorAction Stop
    }
    elseif ($ForMachine)
    {
        $to_be_removed = "$Env:ProgramFiles\WindowsPowerShell\Modules\$module_folder_name"
        Remove-Item -Path $to_be_removed -Recurse -Verbose -ErrorAction Stop
    }
    elseif ($ForCurrentUserFromThisLocation)
    {
        # A bit more code to handle the path delimiter (';') correctly.
        # (Don't want to leave a mess with leading or double/trailing delimiters.)
        
        $p = [Environment]::GetEnvironmentVariable("PSModulePath", "User") -split ';'

        if ($p -contains $PSScriptRoot)
        {
            $p_modified = $p | where { $_ -ne $PSScriptRoot }
            $p_modified_str = $p_modified -join ';'
            [Environment]::SetEnvironmentVariable("PSModulePath", $p_modified_str, "User")
            Write-Host "Done: Changed PSModulePath (User)."
        }
        else
        {
            Write-Host " Nothing to do: No match in user's PSModulePath."
        }
    }
}
catch
{
    $error[0].exception.message
}
