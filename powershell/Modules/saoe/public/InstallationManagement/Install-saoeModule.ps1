<#
    .Synopsis
    Combine multiple scripts into a module, create a manifest for it and install the module.
    
    .Description
    Combines *.ps1 script files from a project to a singular *.psm1 script module file and
    creates an associated manifest file (*.psd1).
    
    Although the script is a bit more generic now, it's still very much opinionated about some things:
    
    - It expects a certain directory/file structure:
      The *.ps1 files with the functions are assumed to be in (or beneath) sub-directories that are
      named "private" and/or "public".
    
    - It also assumes that there is direct relationship between file name and function name:
      One function per script, and the function name and the file base name must be identical.
      Example: The file 'Get-ThisOrThat.ps1' contains only one function, named "Get-ThisOrThat".
    
    - There should also be a XML file (named 'config.xml') at the module's root directory,
      with certain key-value pairs (see code for details).
    
    .Parameter SourceDirectory
    The path to the module's root directory (parent of the source code folders and files).
    
    .Parameter ConfigFile
    Path to an alternative configuration file; by default, 'config.xml' in the
    module's root directory will be used.
    
    .Parameter CustomInstallationDirectory
    The path to the destination directory for a installation of type 'Custom'.
    
    A subdirectory (named like the module) will automatically be created/used beneath the given InstallationDirectory.
    For that reason, please only specify the parent folder as the destination!
    
    The script only support out-of-source builds, so source and destination directory must not be identical.
    
    .Parameter InstallationType
    - Custom
      The module will be copied to a custom location, defined in the code (default value).
    
    - User
      The module will be copied to the current user's PS modules folder.
      [FIXME] Different paths for 'Windows Powershell' (PS5) and 'Powershell Core' (PS6+)!
    
    - Machine
      [WIP, requires admin rights]. The module will be copied to the machine's PS modules folder.
      [FIXME] Different paths for 'Windows Powershell' (PS5) and 'Powershell Core' (PS6+)!

    .Parameter BackupDirectory
    The directory to which the current module file will be saved as a backup, under a new name.
    By default, a subdirectory (named "Backup") of the current working directory.
    (Only comes into play when the switch -Backup is active.)
    
    .Parameter Import
    Switch. Import this generated module into the current Powershell session.
    
    Important: Doesn't seem to work 100% everytime --- if in doubt: Better close the current
               session and then start a new Powershell.
    
    .Parameter Backup
    Enables that a backup of the current module will be saved.
    
    .Parameter DEV
    Switch. Generates a specially named module file for development/work-in-progress/testing.
    
    .Example
    > (Path to script)\Install-saoeModule.ps1 (path to source directory "saoe")
    
    This works if one needs to install the module for the very first time, when the Git repository
    is available, but not yet the module "saoe" itself (which contains this script as a function).
    
    .Example
    > Install-saoeModule .\saoe
    
    The module is installed to "C:\Users\<YourUsername>\Documents\WindowsPowerShell\Modules\<ModuleName>", which
    is the resolved value for [Environment]::GetEnvironmentVariable('PSModulePath', 'User').
    
    .Notes
    Copyright © 2021-2024 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    
    2021-01-13: Created.
    2021-01-14: Installation directory is now optional, because it has a default value; plus
                     some more examples and code renaming/rephrasing/reformatting.
    2021-01-16: Gets some values from a module-specific INI file.
    2022-09-16: 1. The input files are now expected to be normal scripts; the code from each of those files
                   will be enclosed in its own function block (by this script).
                2. New switch parameter to (re)import the newly generated module into the current Powershell session.
                3. New switch parameter to generate a specially named module file for development/work-in-progress/testing.
                4. New parameters for making backups of the current file (module and manifest).
    2023-07-09: Added try-catch-clauses when attempting to remove and to re-import the module.
    2023-07-15: Bug: This is supposed to be a generic script, but the GUID of my 'saoe'-module was hardwired;
                     same goes for LicenseURL, author name and copyright notice.
                Fix: They all are read from the config.ini from now on.
                If the config file is read, there will be a reminder to check if the values are still up-to-date.
                If the config file it not found, the script will end now.
    2023-07-16: - Switched from *.ini to *.xml format for the configuration file, because reading the INI file was done with
                  a function from my 'saoe' module: Resulted in an error when building that module for the first time in a new environment,
                  since the function was not available at first.
                - Added optional parameter 'ConfigFile', in case a non-standard file(path) should be used.
                - Added a few tests and try-catch blocks to better deal with errors.
    2023-08-11: When trying to remove modules before re-import: Added checks to see if such a module is loaded at all;
                else there was an error due to the attempt to remove a not-loaded module (e.g. a '*_DEV' version).
    2023-08-28: Script was renamed and is now a function in the module 'saoe': Setup-Module.ps1 -> Install-saoeModule.
    2023-08-29: There was already an Install-saoeModule (since at least 2020)! Merged its content with this new function
                and added parameter -InstallationType.
    2023-09-03: Added warning-comment to parameter 'Import', because that's not working all the time.
    2024-09-30: Added an example for first-time-usage; fixed the script/function name in the examples.
    2024-11-18: - Renamed parameter $InstallationDirectory to $CustomInstallationDirectory.
                - For parameter $InstallationType: Replaced 'ThisLocation' with 'Custom' and reworked it a bit.
                - Reworked a bit...
#>

param
(
    [Parameter(Mandatory=$true)]
    [ValidateScript({ Test-Path -Path $_ -PathType Container })]
    [string] $SourceDirectory,
    
    [string] $ConfigFile, # No Test-Path-Validation here, because that would limit the name AND the path of any alternative file.
    
    [ValidateScript({ Test-Path -Path $_ -PathType Container })]
    [string] $CustomInstallationDirectory = "C:\dev\shared\PowershellModules",
    
    [ValidateSet("Custom", "User", "Machine")]
    $InstallationType = "Custom",
    
    [string] $BackupDirectory = ".\Backup",   
    [switch] $Import,
    [switch] $Backup,
    [switch] $DEV
)

if ($DEV) { $Appendix = "_DEV" } else { $Appendix = "" }

$ModuleBaseName = (Split-Path -Path $SourceDirectory -Leaf)
$ModuleName = $ModuleBaseName + $Appendix

$ModulePrivateDirectory = Join-Path -Path $SourceDirectory -ChildPath "private"
$ModulePublicDirectory = Join-Path -Path $SourceDirectory -ChildPath "public"

# --------------------------------------------------------------------------------------------------
# Get data from a project-specific configuration file:

try
{
    if ($ConfigFile) { $ConfigFilePath = get-item $ConfigFile -ErrorAction Stop }
    else             { $ConfigFilePath = get-item (Join-Path -Path $SourceDirectory -ChildPath "config.xml") -ErrorAction Stop }
    
    $config = (Select-Xml -Path $ConfigFilePath -XPath /).Node
    write-host ""
    write-host "Using these settings from $($ConfigFilePath.FullName):" -NoNewline
    $config.Settings
    write-host ""
}
catch
{
    Write-Host "$($_.Exception.Message)" -BackgroundColor Black -ForegroundColor Red
    break
}

# --------------------------------------------------------------------------------------------------
# Determine a few values:

switch ($InstallationType)
{
    "Custom"
    {
        $InstallationDirectory = $CustomInstallationDirectory
        
        #$p = [Environment]::GetEnvironmentVariable("PSModulePath", "User")
        #
        #if ($p -like "*$InstallationDirectory*")
        #{
        #    Write-Host "Nothing to do: Path is already set."
        #}
        #else
        #{
        #    # A bit more code to handle the path delimiter (';') correctly.
        #    # (Don't want to leave a mess with leading or double/trailing delimiters.)
        #
        #    if ($p -eq $null)
        #    {
        #        [Environment]::SetEnvironmentVariable("PSModulePath", $InstallationDirectory + [System.IO.Path]::PathSeparator, "User")                    
        #    }
        #    elseif ($p -ne $null)
        #    {
        #        $p_str = $p.ToString()
        #
        #        if ($p_str.EndsWith(';'))
        #        {
        #            [Environment]::SetEnvironmentVariable("PSModulePath", $p + $InstallationDirectory + [System.IO.Path]::PathSeparator, "User")
        #        }
        #        else
        #        {
        #            [Environment]::SetEnvironmentVariable("PSModulePath", $p + [System.IO.Path]::PathSeparator + $InstallationDirectory + [System.IO.Path]::PathSeparator, "User")
        #        }
        #    }
        #
        #    Write-Host "Done: Changed PSModulePath (User)."
        #}
    }
    
    "User"
    {
        $InstallationDirectory = "$Home\Documents\WindowsPowerShell\Modules"
        # [FIXME] For PS6+/Core, there is now also ...\PowerShell\Modules!
    }
    
    "Machine"
    {
        $InstallationDirectory = "$Env:ProgramFiles\WindowsPowerShell\Modules"
        # [FIXME] For PS6+/Core, there is now also ...\PowerShell\<Version>\Modules!
    }
}

# --------------------------------------------------------------------------------------------------
# Prepare script module file (*.psm1):

$ModuleFileContent = @()

# Q: Why the two-pass filtering for the *.ps1 filename extension?
# A: Get-ChildItem is a bit sloppy and would also get *.ps1xml and similar files.
# ?: Still the case nowadays (Powershell 5+)?

$PrivateFunctions = @()

Get-ChildItem -Path $ModulePrivateDirectory -Recurse -Filter "*.ps1" |
    Where-Object { $_.Extension -eq ".ps1" } |
    ForEach-Object {
        $PrivateFunctions += $_.BaseName
    }

$PublicFunctions = @()
$PublicFunctionsFullPathsToFiles = @()

Get-ChildItem -Path $ModulePublicDirectory -Recurse -Filter "*.ps1" |
    Where-Object { $_.Extension -eq ".ps1" } |
    ForEach-Object {
        $PublicFunctions += $_.BaseName
        $PublicFunctionsFullPathsToFiles += $_.FullName
    }

$PublicFunctions = $PublicFunctions | sort
    # Just for cosmetics: Looks nicer in the *.psd1 file that way...

ForEach ($f in $PublicFunctionsFullPathsToFiles)
{
    # Get the code from each file/script and enclose it in its own "function <Name> { [...] }" block.
    $ModuleFileContent += "function $((Get-Item $f).BaseName)$([System.Environment]::NewLine){$([System.Environment]::NewLine)" + `
                          (Get-Content -Path $f -Encoding UTF8 -Raw) + `
                          "}$([System.Environment]::NewLine)"
}

# --------------------------------------------------------------------------------------------------
# Prepare the module manifest file (*.psd1):

try {
# This Here-String is the template from which the module's manifest file (*.psd1) will be generated.
# Several variables will be expanded and some commands executed here, too.
$ModuleManifest = @"
# --------------------------------------------------------------------------------------------------
# This file was automatically generated by $($script:MyInvocation.MyCommand.Path).
# --------------------------------------------------------------------------------------------------
@{
RootModule = "$($ModuleName).psm1"
ModuleVersion = "$(Get-Date -format yyyy).$(Get-Date -format MM).$(Get-Date -format dd)"
# CompatiblePSEditions = @()
GUID = "$($config.Settings.GUID)"
Author = "$($config.Settings.Author)"
CompanyName = "Unknown"
Copyright = "$($config.Settings.CopyrightNotice)"
Description = "$($config.Settings.Description)"
# PowerShellVersion = ""
# PowerShellHostName = ""
# PowerShellHostVersion = ""
# DotNetFrameworkVersion = ""
# CLRVersion = ""
# ProcessorArchitecture = ""
# RequiredModules = @()
# RequiredAssemblies = @()
# ScriptsToProcess = @()
# TypesToProcess = @()
# FormatsToProcess = @()
# NestedModules = @()
# -- "When the value of any *ToExport key is an empty array, no objects of that type are exported, regardless of the value the Export-ModuleMember."
# -- https://www.sapien.com/blog/2016/03/24/exporting-from-modules-a-reminder/
FunctionsToExport = $('"' + ($PublicFunctions -join '",' + $([Environment]::Newline) + '"') + '"')
CmdletsToExport = @()
VariablesToExport = "*"
# AliasesToExport = @()
# DscResourcesToExport = @()
# ModuleList = @()
# FileList = @()

PrivateData = @{
    PSData = @{
        # Tags = @()
        LicenseUri = "$($config.Settings.LicenseURL)"
        ProjectUri = "$($config.Settings.URL)"
        # IconUri = ""
        # ReleaseNotes = ""
    }
}

# HelpInfoURI = ""
# DefaultCommandPrefix = ""
}
"@
}
catch
{
    write-error $_
    break
}

# --------------------------------------------------------------------------------------------------
# Make a backup of the current module & manifest first:

if ($Backup)
{
    $BackupModuleFilename = "$($ModuleName)_$(Get-Date -UFormat %Y-%m-%d_%H-%M-%S).psm1"
    $BackupManifestFilename = "$($ModuleName)_$(Get-Date -UFormat %Y-%m-%d_%H-%M-%S).psd1"

    try
    {
        $BackupDirectory = New-Item -Path $BackupDirectory -ItemType Directory -Force
            # Despite the name, '-Force' will not overwrite an existing folder, but will use it,
            # if it already exists (only the case for a directory -- a file would be overwritten!).
        
        $BackupModuleFullPath = Join-Path $BackupDirectory $BackupModuleFilename
        $BackupManifestFullPath = Join-Path $BackupDirectory $BackupManifestFilename
        
        # Renaming and moving must be done in two steps, since Rename-Item doesn't like a full path as a "NewName".
        
        # -- Module (*.psm1)
        Rename-Item -Path ((Join-Path $InstallationDirectory $ModuleName) | Join-Path -ChildPath "$ModuleName.psm1") -NewName $BackupModuleFilename
        Move-Item -Path ((Join-Path $InstallationDirectory $ModuleName) | Join-Path -ChildPath $BackupModuleFilename) -Destination $BackupModuleFullPath
        
        # -- Manifest (*.psd1)
        Rename-Item -Path ((Join-Path $InstallationDirectory $ModuleName) | Join-Path -ChildPath "$ModuleName.psd1") -NewName $BackupManifestFilename
        Move-Item -Path ((Join-Path $InstallationDirectory $ModuleName) | Join-Path -ChildPath $BackupManifestFilename) -Destination $BackupManifestFullPath
        
        Write-Host "The current module was saved to: $([System.Environment]::Newline)  - $BackupModuleFullPath $([System.Environment]::Newline)  - $BackupManifestFullPath"
    }
    catch
    {
        return "Error when trying to save the current module: $_"
    }
}


# --------------------------------------------------------------------------------------------------
# Install the module files:

if ((Join-Path $InstallationDirectory $ModuleName) -ne $SourceDirectory)
{
    if ((Split-Path -Path $InstallationDirectory -Leaf) -eq $ModuleName)
    {
        $text = "The name of the destination directory is the same as the module's name:" + [Environment]::Newline +
        "'$InstallationDirectory' = '$ModuleName'" + [Environment]::Newline +
        "Since the script will create/use a subdirectory with the name of the module, that is probably not what you want." + [Environment]::Newline +
        "Please use a different directory."
        
        Write-Warning $text
        break
    }
    else
    {
        $InstallFolder = New-Item -Path (Join-Path $InstallationDirectory $ModuleName) -ItemType Directory -Force
            # Despite the name, '-Force' will not overwrite an existing folder, but will use it,
            # if it already exists (only the case for a directory -- a file would be overwritten!).
        
        try
        {
            $ModuleFileContent | Out-File -FilePath (Join-Path $InstallFolder "$ModuleName.psm1") -Encoding UTF8 -ErrorAction Stop
            $ModuleManifest | Out-File -FilePath (Join-Path $InstallFolder "$ModuleName.psd1") -Encoding UTF8 -ErrorAction Stop
            Write-Output "Module installed to '$InstallFolder'"
        }
        catch
        {
            write-error $_
            break
        }
    }
}
else
{
    Write-Warning "Source and Destination are the same directory. This script only support out-of-source builds!"
    break
}

# --------------------------------------------------------------------------------------------------
# Import the newly generated module into the current Powershell session:

if ($Import)
{
    switch ((get-module).Name)
    {
        # To prevent name collisions, kick out all currently imported versions of the module,
        # be it the normal and/or the DEV one.
        
        $ModuleBaseName
        {
            if (get-module $ModuleBaseName)
            {
                try
                {
                    remove-module -Name $ModuleBaseName -Force -ErrorAction Stop
                    write-host "Module '$ModuleBaseName' was removed."
                }
                catch { write-warning $_.Exception.Message }
            }
        }
        
        $ModuleName
        {
            if (get-module $ModuleName)
            {
                try
                {
                    remove-module -Name $ModuleName -Force -ErrorAction Stop
                    write-host "Module '$ModuleName' was removed."
                }
                catch { write-warning $_.Exception.Message }
            }
        }
        default { <# nothing #> }
    }

    import-module -Name (Join-Path $InstallationDirectory $ModuleName | Join-Path -ChildPath "$ModuleName.psm1") -Force -DisableNameChecking -Function "*"

    if (get-module $ModuleName) { write-host "Module '$ModuleName' is imported." }
    else                        { write-warning "Module '$ModuleName' could not be imported!" }
}
