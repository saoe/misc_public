<#
    .Synopsis
    Display the uninstall string, read from the registry (32-bit and 64-bit keys).
      
    .Example
    Use wildcard symbol (*) if not sure about the excat notation; e.g. "*Microsoft*" or just "*".
    ThisFunction -Product "*Office*"

    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
#>

[CmdletBinding()]
Param
(
    [string] $Product = "*",
    [string] $Publisher = "*"
)

$paths = @("HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall",
           "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall")

ForEach ($path in $paths)
{
    $InstalledApps = Get-ChildItem -Path $path |
                                   ForEach { Get-ItemProperty $_.PSPath } |
                                   Where-Object { $_.Publisher -like $Publisher -and $_.DisplayName -like $Product} |
                                   Select-Object -Property DisplayName, DisplayVersion, Publisher, UninstallString

    ForEach ($app in $InstalledApps)
    {
        Write-Output "DisplayName    : $($app.DisplayName)"
        Write-Output "DisplayVersion : $($app.DisplayVersion)"
        Write-Output "Publisher      : $($app.Publisher)"
        Write-Output "UninstallString: $($app.UninstallString)"
        Write-Output ""
    }
}
