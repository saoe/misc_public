<#
    .Synopsis
    Deletes problematic items (e.g. files with very long paths).

    .Description
    Despite the "item" in the name: This is currently limited to only filesystem actions,
    handling special cases of file and directory removals.
    
    For example file items, whose full path is longer than 260 characters.
    
    But note: Not all edge cases can be handled successfully with this function!
    
    Can also be used in a Powershell pipeline.
    
    .Parameter Path
    The path to the existing file or directory that should be deleted.
    
    .Parameter Recurse
    Commands that also all items benath the current path should be deleted.
    
    .Parameter Force
    Forces the removal of some special case items, e.g. hidden files or folders.
    
    .Example
    > Remove-amItem -Path <.\Very\Long\Path>

    .Example
    > Remove-amItem -Path <HiddenFolder> -Force
    
    .Example
    > '\\example.net\users\id012345\Documents\test.txt' | Remove-amItem
    > '.\test1.txt', '.\test2.txt' | Remove-amItem
       
    .Notes
    Copyright © 2023 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
            
    2022-01-18 (so): Created
    2023-04-12 (so): Refactoring, after a new real-life test case.
    2023-04-13 (so): Now also possible to be used in a pipeline.

    NTFS long path limitations
    https://docs.microsoft.com/en-us/windows/win32/fileio/maximum-file-path-limitation
    > Because you cannot use the "\\?\" prefix with a relative path, relative paths are always limited to a total of MAX_PATH characters.
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory = $true, ValueFromPipeline)]
    [string] $Path,
    
    [switch] $Recurse,
    [switch] $Force
)

Process
{
    ForEach ($p in $Path)
    {
        try
        {
            $p2 = Convert-Path -Path $p
            
            if (([System.Uri] $p2).IsUNC) { $p3 = (Join-Path -Path "\\?\UNC" -ChildPath $p2).replace('\\?\UNC\\', '\\?\UNC\') }
            else                          { $p3 =  Join-Path -Path "\\?"     -ChildPath $p2 }

            # if (Test-Path -LiteralPath $p3) -- Hmm...
            
            $ConvertedPath = Resolve-Path -Path $p3

            if ($Recurse) { $RecurseArgument = @{"Recurse" = $true} } else { $RecurseArgument = @{"Recurse" = $false} } 
            if ($Force)   { $ForceArgument   = @{"Force"   = $true} } else { $ForceArgument   = @{"Force"   = $false} }

            try
            {
                if (Test-Path -LiteralPath $ConvertedPath -PathType Container) # = Verzeichnis
                {
                    Remove-Item -LiteralPath $ConvertedPath @RecurseArgument @ForceArgument -ErrorAction Stop
                    #Get-Item -LiteralPath $ConvertedPath -ErrorAction Stop
                }
                elseif (Test-Path -LiteralPath $ConvertedPath -PathType Leaf) # = Datei
                {
                    Remove-Item -LiteralPath $ConvertedPath @ForceArgument -ErrorAction Stop
                    #Get-Item -LiteralPath $ConvertedPath -ErrorAction Stop
                }
                else
                {
                    write-warning "Path '$p' could not be found."
                }
            }
            catch [System.IO.IOException] { Write-Host "$($_.Exception.Message)" -ForegroundColor Red }
            catch                         { $_ }
        }
        catch
        {
            Write-Error "Something went wrong with $($p)."
        }
    }
}