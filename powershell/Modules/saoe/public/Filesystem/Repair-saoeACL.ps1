<#
    .Synopsis
    Repair (or simply straighten) ACLs/permissions of a file or a directory [structure]
    
    .Description
    Changes the permissions (ACEs/ACLs) of one or more files and directories, as specified by the
    parameters (see below).
    
    -- APPLY CAREFULLY! --
   
    Important: Requries elevated privileges: Must be run from an administrator Powershell!"
    - Either right-click the Powershell icon -> 'Run as administrator'
    - Or right-click the Powershell tile in the start menu -> 'More' -> 'Run as administrator'
 
    .Parameter Path
    The (full) path to the file/directory that should be fixed.
   
    .Parameter ResetOwner
    Resets the owner of the file/directory to the group of built-in admins (e.g. 'BUILTIN\Administrators').
   
    .Parameter OnlyInheritedPermissions
    Allows only inherited permissions and throws out all explicitly set permissions from the ACL.
    
    Be careful: There may be historical reasons why certain explict permissions were set!
   
    .Parameter Recursive
    Executes the action recursively for all files and sub-directories in the specified
    starting path.
    
    ATTENTION: On big/deep directory hierarchies such an action can take a very long time!

    .Parameter DisableInheritance_Remove
    Disables inheritance and removes the inherited permissions
    (they will not be converted to explicit permissions).
    
    Only already set explicit permissions will remain in the ACL.
    
    [i] Not tested yet with recursive!
    [i] Only enabled/tested with directories, not files yet.
    
    .Parameter DisableInheritance_ConvertToExplicit
    Disables inheritance and converts inherited permissions to explicit permissions.
    
    Already set explicit permissions will remain, and additionally the converted,
    (formerly inherited) permissions will appear in the ACL.
    
    [i] Not tested yet with recursive!
    [i] Only enabled/tested with directories, not files yet.

    .Example
    > Repair-saoeACL -Path \\example.net\share\file.pdf -ResetOwner
    
    .Notes
    Copyright © 2023 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    2022-01-14 (so): Created (elsewhere).
    2022-03-02 (so): - The work steps can now be invoked individually, by the choice of parameters.
                     - Extended documentation.
    2023-08-16 (so): Published in the 'saoe' module.
    2023-08-17 (so): The parameter '-Recursive' now also works with the parameter '-AllowOnlyInheritedPermissions' (i.e. 'icacls').
    2023-09-15 (so): Implemented parameters '-DisableInheritance_Remove' and '-DisableInheritance_ConvertToExplicit' (with limitations).
                     Renamed parameter '-AllowOnlyInheritedPermissions' to '-OnlyInheritedPermissions'.
                     Some other minor refactoring (parameter sets, etc.).
#>
 
[CmdletBinding()]
Param
(
    [Parameter(ParameterSetName="ResetOwner", Mandatory = $true)]
    [Parameter(ParameterSetName="OnlyInheritedPermissions", Mandatory = $true)]
    [Parameter(ParameterSetName="DisableInheritance_Remove", Mandatory = $true)]
    [Parameter(ParameterSetName="DisableInheritance_ConvertToExplicit", Mandatory = $true)]
    [ValidateScript({ Test-Path -Path $_ })]
    [string] $Path,
   
    [Parameter(ParameterSetName="ResetOwner")]
    [switch] $ResetOwner,
    
    [Parameter(ParameterSetName="OnlyInheritedPermissions")]
    [switch] $OnlyInheritedPermissions,
    
    [Parameter(ParameterSetName="ResetOwner")]
    [Parameter(ParameterSetName="OnlyInheritedPermissions")]
    [switch] $Recursive,
    
    [Parameter(ParameterSetName="DisableInheritance_Remove")]
    [switch] $DisableInheritance_Remove,
    
    [Parameter(ParameterSetName="DisableInheritance_ConvertToExplicit")]
    [switch] $DisableInheritance_ConvertToExplicit
)
 
function Test-IsAdmin()
{
     # Determine if PowerShell launched with admin privileges.
     # Thanks to https://stackoverflow.com/questions/9999963/powershell-test-admin-rights-within-powershell-script#10000292
     try
     {
         $identity = [Security.Principal.WindowsIdentity]::GetCurrent()
         $principal = New-Object Security.Principal.WindowsPrincipal -ArgumentList $identity
         return $principal.IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator)
     }
     catch { throw "Could not determine whether the current account has elevated privileges. Error: '{0}'." -f $_ }
}
 
if (Test-IsAdmin)
{
    if ($DisableInheritance_Remove -or $DisableInheritance_ConvertToExplicit)
    {
        if (Test-Path -Path $Path -PathType Container)
        {
            $ACL = Get-ACL $Path
            
            # SetAccessRuleProtection(isProtected, preserveInheritance):
            # 
            # * isProtected:
            #   - true: Protect the access rules associated with this ObjectSecurity object from inheritance.
            #   - false: Allow inheritance.
            #
            # * preserveInheritance:
            #   - true: Preserve inherited access rules.
            #   - false: Remove inherited access rules. This parameter is ignored if isProtected is false.
            
            if ($DisableInheritance_Remove)
            {
                $ACL.SetAccessRuleProtection($true, $false)
                    # Break inheritance and remove all inherited permissions.

                Set-Acl -Path $Path -AclObject $ACL
                write-host "* Broke inheritance and removed all inherited permissions."
            }
            elseif ($DisableInheritance_ConvertToExplicit)
            {
                $ACL.SetAccessRuleProtection($true, $true)
                    # Break inheritance and convert all inherited permissions to explicit permissions.
                
                Set-Acl -Path $Path -AclObject $ACL
                write-host "* Broke inheritance and converted all inherited permissions to explicit permissions."
            }
            else { }
        }
        else
        {
            write-warning "Item either does not exist or is not a directory (currently files are not suppoorted)!"
        }
    }    
    else
    {
        if ($ResetOwner)
        {
            write-host "* Ownership of the item will be given to the group of built-in administrators..."

            # takeown -- https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/takeown
            #
            # /F = Specifies the file name or directory name pattern. You can use the wildcard character * when specifying the pattern.
            #      You can also use the syntax <sharename>\<filename>.
            # /A = Gives ownership to the Administrators group instead of the current user.
            #      If you don't specify this option, file ownership is given to the user who is currently logged on to the computer.
            # /R = Performs a recursive operation on all files in the specified directory and subdirectories.
           
            if ($Recursive) { takeown /F $Path /A /R }
            else            { takeown /F $Path /A }
        }
     
        if ($OnlyInheritedPermissions)
        {
            write-host "* NTFS filesystem permissions will be replaced by the inherited (standard) permissions in the ACLs..."
           
            # icacls -- https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/icacls
            #           (= "Integrity Control Access Control List" https://en.wikipedia.org/wiki/Cacls)
            #
            # [/Reset [/T] [/C] [/L] [/Q]] = Replaces ACLs with default inherited ACLs for all matching files.       
            # /T                           = Performs the operation on all specified files in the current directory and its subdirectories.
            # /C                           = Continues the operation despite any file errors. Error messages will still be displayed.

            if ($Recursive) { icacls $Path /Reset /C /T }
            else            { icacls $Path /Reset /C    }
        }
    }
}
else
{
    write-warning "Requries elevated privileges: Run Powershell as an administrator!"
    write-warning "  - Either right-click the Powershell icon -> 'Run as administrator'"
    write-warning "  - Or right-click the Powershell tile in the start menu -> 'More' -> 'Run as administrator'"
}
