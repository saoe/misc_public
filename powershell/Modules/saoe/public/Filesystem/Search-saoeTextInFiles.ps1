<#
    .Synopsis
    Searches for the specified term in (text) files.
    
    .Description
    Searches for the specified term in files (Texts, Scripts, Logs, etc.) and returns
    the file path, line number and line content of the results.
            
    .Parameter StartDirectory
    The starting point, from which all files (or rather: their content) will be searched
    for the term; happens automatically recursively for all subdirectories benath it.

    .Parameter FileType
    Optional parameter, in case you want to limit the inspected files to a certain type
    (e.g. *.txt, *.csv or *.log).

    .Parameter SearchTerm
    The term that must appear in the file.
    If it contains space characters, enclose the term in quotation marks.
    
    .Parameter CaseSensitive
    Optional switch to define that the search should be done case sensitive.
    
    .Parameter OnlyUniquePaths
    Optional switch to define that each path with a match will only appear once.
    Note that this will lack details: No line number or line content will be displayed.
    
    .Parameter OutputFormat
    By default, Powershell objects will be returned by this function;
    alternatively, it can be printed (and returned) as a table or in list form.

    .Example
    Search-saoeTextInFiles -StartDirectory "C:\Foo\Bar" -SearchTerm "baz and more"

    .Example
    Search-saoeTextInFiles -StartDirectory "C:\Foo\Bar" -FileType *.log -SearchTerm "Baz" 

    .Example
    Search-saoeTextInFiles -StartDirectory "C:\Foo\Bar" -FileType *.txt -SearchTerm "Baz" -CaseSensitive -OnlyUniquePaths -OutputFormat List

    .Notes
    Copyright © 2021-2022 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2021-12-21 (so): Created (the code, not this file...).
    2022-05-24 (so): Some minor modifications (name, output formats, displayed text, help/comments, etc.)
#>

Param
(
    [Parameter(Mandatory=$true)]
    [ValidateScript({ Test-Path -Path $_ -PathType Container })]
    $StartDirectory,

    $FileType,

    [Parameter(Mandatory=$true)]
    $SearchTerm,
    
    [ValidateSet("Object", "Table", "List")]
    $OutputFormat = "Object",
    
    [switch] $CaseSensitive,
    
    [switch] $OnlyUniquePaths
)

$Matches = @()

if ($CaseSensitive) { $Arguments = @{CaseSensitive = $true}  }
else                { $Arguments = @{CaseSensitive = $false} }

$Line_GetAll = "Get all" + $(if($FileType){" "+$FileType+" "}else{" "}) + "file items from '$(Convert-Path (Resolve-Path $StartDirectory).Path)' and its subdirectories."
write-host $Line_GetAll
$file_items = get-childitem -Path $StartDirectory -Recurse -File -Include $FileType

write-host "Searching the file contents for '$($SearchTerm)'..."
$file_items | select-string -pattern $SearchTerm -AllMatches @Arguments | % { $Matches += $_ }

if ($OnlyUniquePaths) { $Result = $Matches | sort -Unique -Property Path | select Path }
else                  { $Result = $Matches | select Path, LineNumber, Line }

write-host "Found the term '" -NoNewline; write-host "$($SearchTerm)" -NoNewline -ForegroundColor Yellow; write-host "' in $(@($Result).count) file items:"

if ($Result)
{
    switch ($OutputFormat)
    {
        'List'   { $Result | Format-List }
        'Table'  { $Result | Format-Table }
        'Object' { $Result }
        default  { $Result } # Same.
    }
}
else
{
    write-host "-- no result --"
}
