<#
    .Synopsis
    Rename a file for a TV series or movie so that it fits my naming convention.

    .Description
    Will not work in all cases: There are too many different naming schemes.
    For example, the season-episode marker must currently be "s01e01", not "101", to be detected.

    This should work:
        "The.Great.Show.s01e01-FooBar.mkv" -> "The Great Show - 1.01.mkv"
        
    This won't work (yet, see TODO):
        "The-Great-Show-FooBar-101-xyz.mkv" -> :-( (Unknown season/episode format)

    .Parameter FullName
    One or more filenames/full path strings.

    .Example
    PS C:\media\> Get-ChildItem | Rename-saoeFilmOrTVFile -WhatIf

    .Example
    PS C:\> Rename-saoeFilmOrTVFile -FullName C:\media\The.Great.Show.s01e01-FooBar.mkv

    .Example
    gci -File -Path "Z:\show-X\show-x-subdir" | .\Rename-saoeFilmOrTVFile

    .Example
    PS C:\> $x = @("C:\media\The.Great.Show.s01e01-FooBar.mkv",
    >>             "C:\media\The.Great.Show.s01e02-FooBar.mkv",
    >>             "C:\media\The.Great.Show.s01e03-FooBar.mkv")
    PS C:\> Rename-saoeFilmOrTVFile -FullName $x

    .Notes
    Copyright © 2020, 2021 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    2020-12-15 (so): Created.
    2020-12-25 (so): Support for pipeline; support for -WhatIf; reworked Regular Expressions.
    2021-07-07 (so): Fix naming/path issue for the input parameter.

    TODO: Can currently handles files with "S01E01" in the name, for season and episode;
          will also need a way to handle the "101" or "1x01" variants (missing test cases at the moment).
          Idea: RegEx for three digits or digit-x-digits -- should only occur one time in a filename...
#>

[CmdletBinding(SupportsShouldProcess)]
Param
(
    [Parameter(Mandatory, ValueFromPipelineByPropertyName)]
    [ValidateScript({ Test-Path -Path $_ -PathType Leaf })]
    $FullName
        # Parameter must be called FullName, to match the property of the objects that are
        # returned by Get-ChildItem (due ValueFromPipelineByPropertyName).
)

Process
{
    ForEach ($name_input in $FullName)
    {
        $NewName = ""
     
        try
        {
            $OriginalFileName = (Split-Path -Path $name_input -Leaf)
            
            # File base name: Everything before the last dot (excluding the dot!).
            $file_base_name_pattern = '(.*)\.'
            $file_base_name_match = [Regex]::Matches($OriginalFileName, $file_base_name_pattern).Groups[1].Value
            
            # File type extension: Everything after (and including!) the last dot.
            # Though multiple dots, like ".tar.gz" will currently not be matched;
            # but those should also not be common for video files (".mkv", ".mp4", etc.).
            #   \.    -> Matches a dot.
            #   [^.]+ -> Matches chars that are not a dot; one or more matches, matching as much as possible.
            #   $     -> End of string anchor.
            $file_type_extension_pattern = '\.[^.]+$'            
            $file_type_extension_match = [Regex]::Matches($OriginalFileName, $file_type_extension_pattern)
            
            # In File Base Name, replace every occurence of dot, underscore, or hyphen with a space.
            # (I tried to get rid of the temporary variables, but that somehow didn't work out.)
            $temp_new_name_1 = $file_base_name_match -replace '[\._-]', ' '
            
            # Replace the season/episode marker with my format; e.g. 's02e04' => '- 2.04'.
            $season_episode_pattern = "[Ss][0-9]{1,2}[Ee][0-9]{1,2}"
            $season_episode_match = [Regex]::Matches($temp_new_name_1, $season_episode_pattern)
            
            $season_episode_match2 = $season_episode_match.Value -replace '[Ss]', '- ' |
                % { $_ -replace '- [0]{1}', '- ' } | # Trim leading zero in the season number.
                    % { $_ -replace '[Ee]', '.' }    # Replace the episode seperator with a dot.                                    
            
            $temp_new_name_2 = $temp_new_name_1 -replace $season_episode_pattern, $season_episode_match2
                        
            # Find & replace (with nothing) a possible part between the season/episode marker and
            # the end of the file base name.
            $suffix_pattern = '([\d]{2})(.*)$' # Any text between episode number (two digits) and the end.
            $suffix_match = [Regex]::Matches($temp_new_name_2, $suffix_pattern).Groups[2].Value
            $NewName = $temp_new_name_2 -replace [regex]$suffix_match, ""
            
            # Capitalize the first letter of each word (and forces the text to lowercase before,
            # otherwise, only the first letter is affected).
            # ... and append the file extension to the new name.
            $NewName = (Get-Culture).TextInfo.ToTitleCase($NewName.ToLower()) + $file_type_extension_match
            
            if (-not ([string]::IsNullOrEmpty($NewName)))
            {
                If ($WhatIfPreference)
                {
                    Write-Host "Current file name: $OriginalFileName"
                    Write-Host "New file name    : $NewName"
                    Write-Host ""
                }
                else
                {
                    Rename-Item -Path $name_input -NewName $NewName -Verbose
                } 
            }
            else
            {
                Write-Warning "File $name_input was not renamed."
            }
        }
        catch
        {
            Write-Error "Something went wrong with file $($name_input)... :-("
        }
    }
}
