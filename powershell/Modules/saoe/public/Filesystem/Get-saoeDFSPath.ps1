<#
    .Synopsis
    Get the file server(s) behind DFS share(s)/directorie(s).

    .Description
    Not perfect yet (has some minor quirks), but works well enough...

    .Parameter Server
    Server/Domain, if you want to list all that is available for that environment.
    
    .Parameter Directory
    A specific directory path.
    Only UNC paths will be processed (e.g. \\Server\Share\Directory, but not C:\Share\Directory).
    
    Note: Not really that particuar directory may be investigated, but maybe one of its parents,
          because that's how DFS rolls (don't ask me).
          The path may be travelled (upwards!), until a TargetPath will be detected.
    
    .Parameter OnlineDFSServer
    Only include servers with the state 'online' [DFS-lingo, don't ask me].
    
    .Example
    Get-saoeDFSPath -Server example.net
    
    .Example
    Get-saoeDFSPath -Directory "\\example.net\data\"
    
    .Example
    Get-saoeDFSPath -Directory "\\example.net\data\More\"
    
    .Example
    "U:\dev\test", "O:", "\\example.net\", "\\example.net\data\",  "\\example.net\data\More\" | % { Get-saoeDFSPath -Directory $_ }

    .Notes
    Copyright © 2023 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
            
    2023-05-23 (so): Created (based on my notes on http://www.saoe.net/blog/powershell-snippets/#get-the-folder-target-of-a-dfs-link-folder).
    2023-07-06 (so): First version, even though it still has some quirks.
    2023-09-20 (so): Minor adjustments: Handling an error, making the output a bit more generic, ...
    2023-09-21 (so): Some refactoring and a futile attempt to get rid of a particular annoyance regading he output;
                     and added to the "saoe" module.

    ___________________________________________________________
    Useful?   
    * Get-DfsnAccess -- ???
    * Get-DfsnFolder -- Get-DfsnFolder -Path "\\example.net\data\More\"
    * Get-DfsnFolderTarget -- Get-DfsnFolderTarget -Path "\\example.net\data\More\"
    * Get-DfsnRoot -- Get-DfsnRoot -Domain example.net / Get-DfsnRoot -Path "\\example.net\data"
    * Get-DfsnRootTarget -- Get-DfsnRootTarget -Path "\\example.net\data" -> I don't think that these TargetPaths will be helpful...
    * Get-DfsnServerConfiguration -- Get-DfsnServerConfiguration -ComputerName SRV11AB0789.example.net
#>

[CmdletBinding()]
Param
(
    [Parameter(ParameterSetName="Domain")]
    $Server,
    
    [Parameter(ParameterSetName="OnlySpecificDirectory")]
    [string] $Directory,
    
    [Parameter(ParameterSetName="OnlineDFSServer")]
    [Parameter(ParameterSetName="OnlySpecificDirectory")]
    [switch] $OnlineDFSServer
)

"" # Empty line for readability.

if ($Server)
{
    # Note: The Trailing \* for the  Path parameter of Get-DfsnFolder is important!
    (Get-DfsnRoot -Domain $Server).Path |
        % { Get-DfsnFolder -Path (Join-Path -Path $_ -ChildPath "\*") | select -expand Path } |
            % { Get-DfsnFolderTarget -Path $_ | select Path, TargetPath } |
                sort Path
}
elseif ($Directory -and $OnlineDFSServer)
{
    try
    {
        $DFSServer_that_are_online = Get-DfsnRootTarget -Path $Directory -EA Stop | ? { $_.State -eq 'Online' } | select Path, TargetPath, State
        write-host "Online DFS servers behind $($Directory):"
        $DFSServer_that_are_online
    }
    catch { write-warning "$($_.Exception.Message)`n" }
}
elseif ($Directory)
{
    if     (-not ([bool]([System.Uri]$Directory).IsUnc))           { write-warning "The function can only process UNC paths (i.e. \\Server\Share\Directory, but not '$($Directory)')`n" }
    elseif (-not (Test-Path -Path $Directory -PathType Container)) { write-warning "Directory not found: $($Directory)`n" }
    else
    {
        $Parent = split-path -path $Directory -parent
        $Leaf   = split-path -path $Directory -leaf

        # Specific directory works only at a specific level(when it is not a share) [don't know enough about DFS].
        # If it's a share, all direct folders beneath will be listed.
        
        if ([string]::IsNullOrEmpty($Parent) -and [string]::IsNullOrEmpty($Leaf))
        {
            # \\example.net\Data -> Parent & Leaf are empty.
            write-warning "$($Directory) is not a directory, but a share with maybe multiple directories beneath it:"
            
            try
            {
                # Note: The Trailing \* for the  Path parameter of Get-DfsnFolder is important!
                (Get-DfsnRoot -Path $Directory -EA Stop).Path |
                    % { (Get-DfsnFolder -Path (Join-Path -Path $Directory -ChildPath "\*") -EA Stop).Path } |
                        % { Get-DfsnFolderTarget -Path $_ -EA Stop | select Path, TargetPath } |
                            sort Path
            }
            catch
            {
                write-warning "$($_.Exception.Message)"
            }
        }
        elseif ([string]::IsNullOrEmpty($Parent) -and (-not [string]::IsNullOrEmpty($Leaf)))
        {
            # \\example.net\ -> Parent is empty, but Leaf is 'example.net'.
            write-warning "'$($Directory)' is no valid Distributed File System Namespace (DFSN)!"
        }
        elseif (-not [string]::IsNullOrEmpty($Leaf))
        {
            # Do-While-Schleife is required to climb the path upward, if necessary:
            
            $OK = $false
            
            do
            {
                try
                {
                    Get-DfsnFolderTarget -Path $Directory -EA Stop | select Path, TargetPath
                    $OK = $true
                }
                catch
                {
                    write-warning "$($_.Exception.Message)Will try the superordinate directory as the entry point..."
                    $Directory = split-path $Directory -Parent
                }
            }
            while (-not $OK)
        }
        else
        {
            write-warning "Unexpected state."
        }
    
        "" # Empty line for readability.
    }
}