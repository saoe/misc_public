<#
    .Synopsis
    Get some time information (created, last modfied) on a directory's content.

    .Parameter Path
    This path's child items will be inspected.

    .Parameter TimeType
    To decide whether one is interested in the creation time of the item, or the time it was
    last modified (the default).

    .Parameter Descending
    A switch to enable a descending sort order (the default is ascending).

    .Example
    Get-saoeTimestamps -Path "C:\temp\X & Y" -TimeType "CreationTime" -Descending

    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    2020-12-10 (so): Created.
#>

param
(
    [Parameter(Mandatory=$true)]                            $Path,
    [ValidateSet("CreationTime", "LastWriteTime")] [string] $TimeType = "LastWriteTime",
                                                   [switch] $Descending
)

Get-ChildItem -Path $Path -Recurse |
    select-object -Property FullName, CreationTime, LastWriteTime |
        sort-object -Property $TimeType -Descending:$Descending
