<#
    .Synopsis
    Create a new directory hierarchy and basic files for a new project.
    
    .Description
    Run this one time, to create the initial directories and files.
    After that, you can check it into a Git repository and maintain and modify it as usual.

    .Parameter Path
    The location and name of the the project's directory.
    
    If ProjectName is provided, the path will be the combination of it: <Path>/<ProjectName>
    
    .Parameter ProjectName
    The name of the project, used in the ReadMe, CMake and other places.
    
    If no value is provided, the name of the project directory is automatically chosen:
    The name of the project's (leaf) directory, as provided with parameter 'Path'.
    
    .Parameter ProjectBy
    The name of the author and/or copyright holder of this project.
    
    .Parameter ProjectEMailAddress
    The e-mail address for contacting the project's author/copyright holder.
    
    .Parameter SourceBaseDirectory
    The location of the base directory for the boilerplate files.
    
    By default the root directory of my misc_public repo on my hard disk (according to my
    current convention)!
    
    Attention: Assumes certain directories and files to exist there under specific names!
    
    .Parameter Type
    Depending on the type of the new project, different files and actions will be chosen.
    It's a fixed set; use the TAB key to scroll through the possible types.
    
    .Example
    > New-saoeProject -Path "C:\devel\_test\NewProjectX" -Type CPP-CLI-Application
    
    .Notes
    Copyright © 2021-2022 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>

    2021-01-27/28/30 (so): Created.
    2021-05-02 (so): Type is mandatory; help text revised; changed several names and locations;
                     added more types (Qt*).
    2021-06-05 (so): File 'configure-and-build.bat' is replaced by 'CMakePresets.json'.
    2021-06-06 (so): - Added helper script 'print_all_targets.cmake'.
                     - Renamed the Type/Directory names.
    2022-01-30 (so): - Switch from "copy multiple generic files to folder LICENSES" to "copy just one specific and customized file (LICENSE.txt)".
                     - Added parameter 'ProjectBy', which will also be used in the placeholder replacement.
                     - Added parameter 'ProjectEMailAddress', which will also be used in the placeholder replacement.
    2022-02-17 (so): Made it more versatile and thus also applicable for Python and Powershell scripts.
    2022-02-25 (so): Small changes for path handling; additions to support Python packages.
    2022-02-27 (so): Added CPP-Win32-* project boilerplates.
    2022-04-02 (so): Additions to "CPP-Win32-DLL" section (wasn't ready yet).
    2022-05-13 (so): Added 'BSL-1.0' (Boost Software License 1.0) to the available licenses, and made it also the default value.
    2022-05-13 (so): Added '0BSD' (Zero Clause BSD License) to the available licenses; it's a public domain equivalent license that is slightly older and shorter than MIT-0.
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true)]
    [string] $Path,
    
    [string] $ProjectName,
    
    [string] $ProjectBy = "Sascha Offe",
    
    [string] $ProjectEMailAddress = "so@saoe.net",
    
    [ValidateScript({ Test-Path -Path $_ -PathType Container })]
    $SourceBaseDirectory = "C:\devel\git\misc_public",
    
    [Parameter(Mandatory=$true)]
    # Attention: The names are also used to find the external 'boilerplates' directories!
    [ValidateSet("CPP-CLI-Application", `
                 "CPP-Win32-DLL", `
                 "CPP-GUI-Qt-QWidgets-Application", `
                 "CPP-GUI-Qt-QML-Application", `
                 "CPP-GUI-Qt6-QWidgets-Application", `
                 "CPP-GUI-Qt6-QML-Application", `
                 "CPP-Win32-GUI-App", `
                 "CPP-Win32-CLI-and-GUI-App", `
                 "CPP-Win32-CLI-or-GUI-App", `
                 "Python-Script", `
                 "Python-Package", `
                 "Powershell-Script")]
    $Type,
    
    #[ValidateScript({ Test-Path -Path $_ -PathType Container })]
    $QtDirectory,
    
    [ValidateSet("MIT", "MIT-0", "BSL-1.0", "0BSD")]
    $License = "BSL-1.0"
)

if ($ProjectName) { $Path = Join-Path -Path $Path -ChildPath $ProjectName}
else              { $ProjectName = Split-Path -Path $Path -Leaf }

if (Test-Path -Path $Path -PathType Container)
{
    Write-Warning ("The directory '$Path' already exists."+[Environment]::Newline+"Please choose a different name.")
    break
}

# Additional check, because the parameter's ValidateScript only checks an incoming argument, not its default value.
if (-not (Test-Path -Path $SourceBaseDirectory -PathType Container))
{
    Write-Warning ("No local repository found for 'misc_public', won't continue!"  + [Environment]::Newline +
    "Rationale: This function needs multiple files from that source directory." + [Environment]::Newline +
    "-SourceBaseDirectory is currently set to: '$SourceBaseDirectory'")
    break
}


if ($Type -like "*Qt*")
{
    if     (-not $QtDirectory)                                       { Write-Error "No Qt directory provided!" -ErrorAction Stop }
    elseif (-not (Test-Path -Path $QtDirectory -PathType Container)) { Write-Error "Invalid Qt directory provided: '$QtDirectory'" -ErrorAction Stop }
}

$Utf8NoBomEncoding = New-Object System.Text.UTF8Encoding $False
    # Workaround for a character encodig issue with Powershell 5.x.:
    # Without this (and the use of WriteAllLines() later), "UTF-8" would mean "UTF-8 with BOM".

$NewProjectDestinationDirectory = New-Item -ItemType Directory -Path $Path

# -- ".gitignore", "ReadMe.md", "Notes.md" and "LICENSE.txt" --
Copy-Item -Path "$SourceBaseDirectory\boilerplates\gitignore-without-leading-dot" -Destination "$NewProjectDestinationDirectory\.gitignore"
Copy-Item -Path "$SourceBaseDirectory\boilerplates\ReadMe-boilerplate.md" -Destination "$NewProjectDestinationDirectory\ReadMe.md"
Copy-Item -Path "$SourceBaseDirectory\boilerplates\Notes.md" -Destination "$NewProjectDestinationDirectory\Notes.md"
Copy-Item -Path "$SourceBaseDirectory\boilerplates\Licenses\$($License).txt" -Destination "$NewProjectDestinationDirectory\LICENSE.txt"

if ($Type -like "*CPP*")
{
    # -- 'src' directory (and subdirectories) --
    New-Item -ItemType Directory -Path "$NewProjectDestinationDirectory\src\" | Out-Null
    New-Item -ItemType Directory -Path "$NewProjectDestinationDirectory\src\cmake\" | Out-Null
    New-Item -ItemType Directory -Path "$NewProjectDestinationDirectory\src\assets\" | Out-Null
    New-Item -ItemType Directory -Path "$NewProjectDestinationDirectory\src\resources\" | Out-Null
        # For single file copies, the destination folder must exist; thus we need to create it beforehand.
    
    # -- Misc. CMake files in 'src' directory --
    Copy-Item -Path $SourceBaseDirectory\boilerplates\CMake\CMakeLists.txt    -Destination $NewProjectDestinationDirectory\src\
    Copy-Item -Path $SourceBaseDirectory\boilerplates\CMake\version.cmake     -Destination $NewProjectDestinationDirectory\src\cmake\
    Copy-Item -Path $SourceBaseDirectory\boilerplates\CMake\config.cmake.in   -Destination $NewProjectDestinationDirectory\src\resources\
    Copy-Item -Path $SourceBaseDirectory\boilerplates\CMake\version.h.in      -Destination $NewProjectDestinationDirectory\src\resources\
    Copy-Item -Path $SourceBaseDirectory\boilerplates\CMake\winres.rc         -Destination $NewProjectDestinationDirectory\src\resources\
    
    Copy-Item -Path $SourceBaseDirectory\boilerplates\CMake\CMakePresets.json -Destination $NewProjectDestinationDirectory\src\
    Copy-Item -Path $SourceBaseDirectory\cmake\print_all_targets.cmake        -Destination $NewProjectDestinationDirectory\src\cmake\
    Copy-Item -Path $SourceBaseDirectory\boilerplates\CMake\doc               -Destination $NewProjectDestinationDirectory\src\doc\ -Recurse
    $CMakePresetsJson = "$NewProjectDestinationDirectory\src\CMakePresets.json"
}

# -- Other files, depending on the project type --
switch ($Type)
{
    "CPP-CLI-Application"
    {
        New-Item -ItemType Directory -Path "$NewProjectDestinationDirectory\src\Application\" | Out-Null
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\CMakeLists.txt" -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\main.cpp"       -Destination "$NewProjectDestinationDirectory\src\Application\"
        
        $rc = ((Get-Content $CMakePresetsJson -Raw) -replace "<QT_DIRECTORY_PLACEHOLDER>", "")
        Set-Content -Path $CMakePresetsJson -Value $rc -NoNewline
    }
    
    "CPP-Win32-DLL"
    {
        New-Item -ItemType Directory -Path "$NewProjectDestinationDirectory\src\Library\" | Out-Null
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\Library\CMakeLists.txt" -Destination "$NewProjectDestinationDirectory\src\Library\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\Library\ExampleDLL.cpp" -Destination "$NewProjectDestinationDirectory\src\Library\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\Library\ExampleDLL.hpp" -Destination "$NewProjectDestinationDirectory\src\Library\"
        
        New-Item -ItemType Directory -Path "$NewProjectDestinationDirectory\src\Application\" | Out-Null
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\Application\CMakeLists.txt" -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\Application\main.cpp"       -Destination "$NewProjectDestinationDirectory\src\Application\"
        
        $rc = ((Get-Content $CMakePresetsJson -Raw) -replace "<QT_DIRECTORY_PLACEHOLDER>", "")
        Set-Content -Path $CMakePresetsJson -Value $rc -NoNewline
    }
    
    "CPP-GUI-Qt-QWidgets-Application"
    {
        New-Item -ItemType Directory -Path "$NewProjectDestinationDirectory\src\Application\" | Out-Null
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\CMakeLists.txt" -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\main.cpp"       -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\MainWidget.cpp" -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\MainWidget.h"   -Destination "$NewProjectDestinationDirectory\src\Application\"
        
        $rc = ((Get-Content $CMakePresetsJson -Raw) -replace "<QT_DIRECTORY_PLACEHOLDER>", ($QtDirectory -replace "\\", "/"))
        Set-Content -Path $CMakePresetsJson -Value $rc -NoNewline
    }
    
    "CPP-GUI-Qt-QML-Application"
    {
        New-Item -ItemType Directory -Path "$NewProjectDestinationDirectory\src\Application\" | Out-Null
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\CMakeLists.txt" -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\main.cpp"       -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\main.qml"       -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\qml.qrc"        -Destination "$NewProjectDestinationDirectory\src\Application\"
        
        $rc = ((Get-Content $CMakePresetsJson -Raw) -replace "<QT_DIRECTORY_PLACEHOLDER>", ($QtDirectory -replace "\\", "/"))
        Set-Content -Path $CMakePresetsJson -Value $rc -NoNewline
    }
    
    "CPP-GUI-Qt6-QWidgets-Application"
    {
        New-Item -ItemType Directory -Path "$NewProjectDestinationDirectory\src\Application\" | Out-Null
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\CMakeLists.txt" -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\main.cpp"       -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\MainWidget.cpp" -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\MainWidget.h"   -Destination "$NewProjectDestinationDirectory\src\Application\"
        
        $rc = ((Get-Content $CMakePresetsJson -Raw) -replace "<QT_DIRECTORY_PLACEHOLDER>", ($QtDirectory -replace "\\", "/"))
        Set-Content -Path $CMakePresetsJson -Value $rc -NoNewline
    }
    
    "CPP-GUI-Qt6-QML-Application"
    {
        New-Item -ItemType Directory -Path "$NewProjectDestinationDirectory\src\Application\" | Out-Null
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\CMakeLists.txt" -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\main.cpp"       -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\main.qml"       -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\qml.qrc"        -Destination "$NewProjectDestinationDirectory\src\Application\"
        
        $rc = ((Get-Content $CMakePresetsJson -Raw) -replace "<QT_DIRECTORY_PLACEHOLDER>", ($QtDirectory -replace "\\", "/"))
        Set-Content -Path $CMakePresetsJson -Value $rc -NoNewline
    }
    
    "CPP-Win32-GUI-App"
    {
        New-Item -ItemType Directory -Path "$NewProjectDestinationDirectory\src\Application\" | Out-Null
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\CMakeLists.txt" -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\main.cpp"       -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\winres.rc"      -Destination "$NewProjectDestinationDirectory\src\Application\"
            # This project boilerplate has its own, custom resource file; thus we overwrite the one that has already been copied here.
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\res.h"          -Destination "$NewProjectDestinationDirectory\src\Application\"
        
        $rc = ((Get-Content $CMakePresetsJson -Raw) -replace "<QT_DIRECTORY_PLACEHOLDER>", ($QtDirectory -replace "\\", "/"))
        Set-Content -Path $CMakePresetsJson -Value $rc -NoNewline
    }
    
    "CPP-Win32-CLI-and-GUI-App"
    {
        New-Item -ItemType Directory -Path "$NewProjectDestinationDirectory\src\Application\" | Out-Null
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\CMakeLists.txt" -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\main.cpp"       -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\winres.rc"      -Destination "$NewProjectDestinationDirectory\src\Application\"
            # This project boilerplate has its own, custom resource file; thus we overwrite the one that has already been copied here.
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\res.h"          -Destination "$NewProjectDestinationDirectory\src\Application\"
        
        $rc = ((Get-Content $CMakePresetsJson -Raw) -replace "<QT_DIRECTORY_PLACEHOLDER>", ($QtDirectory -replace "\\", "/"))
        Set-Content -Path $CMakePresetsJson -Value $rc -NoNewline
    }
    
    "CPP-Win32-CLI-or-GUI-App"
    {
        New-Item -ItemType Directory -Path "$NewProjectDestinationDirectory\src\Application\" | Out-Null
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\CMakeLists.txt" -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\main.cpp"       -Destination "$NewProjectDestinationDirectory\src\Application\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\winres.rc"      -Destination "$NewProjectDestinationDirectory\src\Application\"
            # This project boilerplate has its own, custom resource file; thus we overwrite the one that has already been copied here.
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\res.h"          -Destination "$NewProjectDestinationDirectory\src\Application\"
        
        $rc = ((Get-Content $CMakePresetsJson -Raw) -replace "<QT_DIRECTORY_PLACEHOLDER>", ($QtDirectory -replace "\\", "/"))
        Set-Content -Path $CMakePresetsJson -Value $rc -NoNewline
    }
    
    "Python-Script"
    {
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\script.py" -Destination "$NewProjectDestinationDirectory\"
    }
    
    "Python-Package"
    {
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\MANIFEST.in"    -Destination "$NewProjectDestinationDirectory\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\pyproject.toml" -Destination "$NewProjectDestinationDirectory\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\pyproject.toml" -Destination "$NewProjectDestinationDirectory\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\setup.cfg"      -Destination "$NewProjectDestinationDirectory\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\setup.py"       -Destination "$NewProjectDestinationDirectory\"
        
        New-Item -ItemType Directory -Path "$NewProjectDestinationDirectory\$ProjectName" | Out-Null
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\__init__.py" -Destination "$NewProjectDestinationDirectory\$ProjectName\"
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\__main__.py" -Destination "$NewProjectDestinationDirectory\$ProjectName\"
    }
    
    "Powershell-Script"
    {
        Copy-Item -Path "$SourceBaseDirectory\boilerplates\$($Type)\script.ps1"       -Destination "$NewProjectDestinationDirectory\"
    }
}

# -- Replace common placeholders with valid values in each file --    
ForEach ($file in (Get-ChildItem -File -Path $NewProjectDestinationDirectory -Recurse))
{
    $rc = ((Get-Content ($file).FullName -Raw) -replace "<COPYRIGHT_YEAR_PLACEHOLDER>", (Get-Date -UFormat "%Y"))
    $rc =                                  $rc -replace "<LICENSE_PLACEHOLDER>", $License
    $rc =                                  $rc -replace "<PROJECT_NAME_PLACEHOLDER>", $ProjectName
    $rc =                                  $rc -replace "<PROJECT_BY_PLACEHOLDER>", $ProjectBy
    $rc =                                  $rc -replace "<PROJECT_EMAILADDRESS_PLACEHOLDER>", $ProjectEMailAddress
    Set-Content -Path ($file).FullName -Value $rc -NoNewline
        # -NoNewline: No trailing/additional Linebreak at the end; the input is a 'single line'
        # anyway (due Get-Content -RAW), this works fine.
}

write-host "Created a new project with the name '$($ProjectName)' and of type '$($Type)' under '$($NewProjectDestinationDirectory)'"
