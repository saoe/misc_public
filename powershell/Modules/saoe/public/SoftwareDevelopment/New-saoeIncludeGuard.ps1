<#
    .Synopsis
    Generate an 'include guard' text block for a C or C++ header file.
    
    .Description
    The guard text itself is combined from a standard text, an optional CustomText text and
    a sequence of random characters (created by a GUID/UUID function).
    
    The text will be normalized: All uppercase, problematic leading/trailing characters will be
    removed and all special symbols will be replaced by an underscore.
    
    By default, the text block will be printed to the shell and also put in the clipboard.
    
    .Parameter CustomText
    To provide another custom text or to override/ignore the default one (by specifying an empty string).
    
    .Parameter Order
    A predefined set of how the ID string in the text block should be formatted:
        - StandardText_CustomText_GUID
        - CustomText_StandardText_GUID
    (Optional; a default value is provided.)
    
    .Parameter NoClipboard
    Prevents that the text block will be put into the clipboard.
    
    .Example
    > New-saoeIncludeGuard -CustomText "ProjectZNotX" -NoClipboard
    
    Use a different custom text and don't copy the result in the clipboard, just put it in the shell.
    
    .Example
    > New-saoeIncludeGuard -CustomText ""
    
    Override the default custom text (if there is one defined in the function) with an empty one.
    
    .Notes
    Copyright © 2022 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    2022-02-09: Created.
#>

[CmdletBinding()]
Param
(
    [AllowEmptyString()]
    $CustomText = "NIFTYODDITY",
    
    [ValidateSet('StandardText_CustomText_GUID', 'CustomText_StandardText_GUID')]
    $Order = 'StandardText_CustomText_GUID',
    
    [switch] $NoClipboard
)

$StandardText = 'INCLUDE_GUARD'
$GUID         = (New-GUID).Guid
 
if ($CustomText)
{
    # Get rid of empty spaces at the beginning or end.
    $CustomText = $CustomText.trim()
    
    # Get rid of (any) special characters and numbers(!) at the beginning.
    [regex] $leading_pattern = "^[^a-zA-Z]*"
    if ((select-string -inputobject $CustomText -pattern $leading_pattern).matches.value)
    {
        $CustomText = $CustomText -replace $leading_pattern, ''
    }
   
    # Get rid of (any) special characters at the end (numbers at the end are OK, though).
    [regex] $trailing_pattern = "[^a-zA-Z0-9]*$"
    if ((select-string -inputobject $CustomText -pattern $trailing_pattern).matches.value)
    {
        $CustomText = $CustomText -replace $trailing_pattern, ''
    }
    
    switch ($Order)
    {
        'StandardText_CustomText_GUID' { $IDString = $StandardText + '_' + $CustomText + '_' + $GUID; break }
        'CustomText_StandardText_GUID' { $IDString = $CustomText + '_' + $StandardText + '_' + $GUID; break }
        default                        { "unknown"; break } # Just in case (should never be hit, due Parameter ValidateSet).
    }
}
else
{
    $IDString = $StandardText + '_' + $GUID
}

# Replace any remaining unwanted character with an underscore and make all characters uppercase.
[regex] $pattern = "[^0-9a-zA-Z_]"
if (select-string -inputobject $IDString -pattern $pattern)
{
    $IDString = ($IDString -replace $pattern, '_').ToUpper()
}

write-host ""

#    
# Don't intend the Here-String!
#
$textblock = @"
#ifndef $IDString
#define $IDString

#endif // include guard
"@

$textblock
 
if (-not $NoClipboard)
{
    Set-Clipboard -Value $textblock
    write-host "`nCopied to clipboard!" -foregroundcolor green
}
