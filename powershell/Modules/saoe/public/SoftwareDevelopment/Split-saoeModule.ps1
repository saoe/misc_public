<#
    .Synopsis
    Split the commands of a PowerShell module into separate files.
    
    .Description
    The definitions of a module's commands will be read and then exported to discrete script files
    (with an ASCII encoding); in short: Split one *.psm1 file into many *.ps1 files.
    
    The files will be saved in the directory specified by the 'Destination' parameter.
    
    .Parameter Module
    The name of the module.
    
    .Parameter Destination
    The directory that will store the new files.
    (By default, it will be a subdirectory in the current working directory.)
    
    .Example
    Split-saoeModule -Module saoe
    
    .Example
    Split-saoeModule -Module saoe -Destination C:\foo\saoe-export
    
    .Notes
    Copyright © 2023 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT-0
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    2023-09-24 (so): Created; based on <https://stackoverflow.com/a/40267174>.
    2023-09-26 (so): - Switched from using Import-Module to Get-Module.
                     - The function definition contains inline comments, but not any leading comment-blocks.
                       I can't get any standard comment that is outside of the function, but I now can get
                       the associated comment-based help section and add it to the exported file.
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true)] $Module,
                                 $Destination = (join-path $PWD "$Module (Extracted)")
)

# Helper function to get the Comment-based Help for a function:
function getHelp ($CommandName)
{
    # IMPORTANT: I've not invested more effort to also get some of the lessern known/used (by me) section, like: 
    # .Inputs
    # .Outputs
    # .Link
    
    $help = get-help $CommandName -Full
        # -Full is neccessary, to also get the '.NOTES'!
 
    [string] $Synopsis = ".Synopsis`r`n"
    try { $Synopsis += "$($help | select -expand Synopsis)`r`n" } catch {}
   
    [string] $Description = ".Description`r`n"
    try { $Description += "$(($help | select -expand Description).Text)`r`n" } catch {}
   
    [string] $Parameters = ""
    try { $Parameters += $help.Parameters | % { $_.parameter | % { ".Parameter $($_.name)`r`n$($_.description.text)`r`n`r`n" } } } catch {}
   
    [string] $Examples = ""
    try { $Examples += $help.Examples | % { $_.example | % { ".Example`r`n$($_.code)`r`n`r`n$($_.remarks.text)`r`n`r`n" } } } catch {}
 
    [string] $Notes = ".Notes`r`n"
    try { $Notes += "$($help.PSExtended.AlertSet | out-string)" } catch {}

    return ("<#`r`n" + $Synopsis + "`r`n" + $Description + "`r`n" + $Parameters + "`r`n" + $Examples + "`r`n" + $Notes + "`r`n#>`r`n")
}

if (-not (test-path $Destination -PathType Container)) { new-item -ItemType Directory -Path $Destination; "" }

$mod = Get-Module $Module
if (-not $mod) { return "Modul $Module not available!" }

ForEach ($Command in (Get-Command -Module $Module).Name)
{
   $File = join-path $Destination "$($Command).ps1"
   ((getHelp -CommandName $Command) + (get-command $Command | select -expand definition).trim()) | out-file $File -Encoding Default
   write-host "Extracted command $Command from module $Module to file $file"
}

write-warning "The definition and parts(!) of the comment-based-help were written -- please check!"