<#
    .SYNOPSIS
    Adds a Table of Contents of a module's functions to a ReadMe.md file.
    
    .DESCRIPTION  
    A section in an existing ReadMe.md that begins with "# Table of Contents" will be updated/replaced.
    
    The end of the section is defined as either a separator line ("----....") or the end-of-file.
    
    Uses the ".Synopsis" from the comment-based help text of a Powershell script/function.
    
    The generated file is formatted as a 'Markdown' text file <https://en.wikipedia.org/wiki/Markdown>.
    
    Although the code is a bit more generic now, it's still very much opinionated about some things:
    
    - The output is a file named 'ReadMe.md' in the module's root directory; an existing file with
      that name will be overwritten!
    
    - There should also be an XML file (named 'config.xml') at the module's root directory,
      with certain key-value pairs (see code for details).
    
    .PARAMETER SourceDirectory
    The top-directory where the module's Powershell source code files are located, e.g.
    '.\powershell\Modules\saoe\'.

    .PARAMETER Format
    Defines the output format of the generated Table of Contents:
    
    - Table: [default] A table (Markdown format).
    - List :           An unordered list (Markdown format).

    .PARAMETER Verbose
    Shows more details.
    
    .EXAMPLE
    Add-saoeModuleTableOfContentsToReadMe -SourceDirectory .\powershell\Modules\saoe\

    .NOTES
    Copyright © 2020-2021, 2023 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    
    2020-12-29 (so): Created.
    2021-01-15 (so): Now a standalone script, no longer a function (for an easier workflow).
    2021-01-16 (so): - Moved the script outside the original module; should now work as a generic
                       script for different modules.
                     - Gets now some values from a module-specific INI file.
    2023-08-29 (so): Some updates:
                     - The configuration files have changed from INI to XML format.
                     - Dot-sourcing a script for Get-Help is not needed (and also wouldn't work [anymore?],
                       because dot-sourcing also tries to execute the script).
    2023-08-31 (so): More changes:
                     - With the new parameter "-Format", one can choose the ToC-format.
                     - Support for parameter "-Verbose".
                     - Now a function again in the module, instead of a standalone script; can nevertheless
                       still be used for other directories/modules via parameters!
    2024-02-18 (so): Slight update/addition to the help comment.
#>

param
(
    [Parameter(Mandatory=$true)] [ValidateScript({ Test-Path -Path $_ -PathType Container })] [string] $SourceDirectory,
                                 [ValidateSet("List", "Table")]                                        $Format = "Table"
)

$Filename       = "ReadMe.md"
$ConfigFilename = "config.xml"

$Filepath       = Join-Path -Path $SourceDirectory -ChildPath $Filename
$ConfigFilepath = Join-Path -Path $SourceDirectory -ChildPath $ConfigFilename

if (-not (Test-Path -Path $Filepath -PathType Leaf))       { write-warning "Missing $Filename - Can't find $Filepath"; break }
if (-not (Test-Path -Path $ConfigFilepath -PathType Leaf)) { write-warning "Missing $ConfigFilename - Can't find $ConfigFilepath"; break }

$ModuleName             = Split-Path -Path $SourceDirectory -Leaf
$ModulePublicDirectory  = Join-Path -Path $SourceDirectory -ChildPath "public"
$ModulePrivateDirectory = Join-Path -Path $SourceDirectory -ChildPath "private"

# --------------------------------------------------------------------------------------------------
# Get the content of the current file:

$Data = Get-Content -Encoding UTF8 -Path $Filepath -Raw
    # Must be "raw", so that RegEx can later detect all lines as one data blob!
    # Note that with Powershell 5, 'UTF8"'means "UTF-8 with BOM", 'UTF8NoBOM' is only available in later versions.

# --------------------------------------------------------------------------------------------------
# Read project-specific XML configuration file.

$ConfigFileitem = get-item $ConfigFilepath -ErrorAction Stop
$config = (Select-Xml -Path $ConfigFileitem -XPath /).Node
write-verbose ("{0}{1}" -f "Using settings from $($ConfigFileitem.FullName):", ($config.Settings | out-string))

# --------------------------------------------------------------------------------------------------
# For creating the TOC: Get all public/exported functions:
#
# - By my convention(!), the file basename must be identical to the function name.
# - This script assumes a specific directory hierarchy (see comments at the top). 

$all_public_files = Get-ChildItem -Path $ModulePublicDirectory -Recurse -Filter "*.ps1" |
    Where-Object { $_.Extension -eq ".ps1" } |
        Select-Object -Property FullName, BaseName |
            Sort-Object -Property BaseName

# --------------------------------------------------------------------------------------------------
# Prepare the new TOC (will replace the old section in the file):

$NewTOC += "# Table of Contents" + [Environment]::NewLine + [Environment]::NewLine
$NewTOC += "~ Generated on $(Get-Date -Format "yyyy-MM-dd HH:mm:ss (zzz)") by $($MyInvocation.MyCommand.Name) ~" + [Environment]::NewLine + [Environment]::NewLine
$NewTOC += "## Public Functions" + [Environment]::NewLine + [Environment]::NewLine

switch ($Format)
{
    "List"
    {
        ForEach ($public_file in $all_public_files)
        {
            $relative_link = ($public_file.FullName -split "\\$ModuleName\\")[1] -replace '\\', '/'
                # Using a forward slash, since this is supposed to be viewed also on the web.
            
            $NewTOC += ("- [``"+$public_file.BaseName+"``](./$relative_link)  " + [Environment]::NewLine + "  $((Get-Help $public_file.FullName).Synopsis)" + [Environment]::NewLine + [Environment]::NewLine)
        }
    }
    
    "Table"
    {
        $NewTOC += "Name         | Synopsis"     + [Environment]::NewLine
        $NewTOC += ":----------- | :-----------" + [Environment]::NewLine
        ForEach ($public_file in $all_public_files)
        {
            $relative_link = ($public_file.FullName -split "\\$ModuleName\\")[1] -replace '\\', '/'
                # Using a forward slash, since this is supposed to be viewed also on the web.
            
            $NewTOC += "[``"+$public_file.BaseName+"``](./$relative_link) | $((Get-Help $public_file.FullName).Synopsis)" + [Environment]::NewLine
        }
    }
}

# --------------------------------------------------------------------------------------------------
# Detect the old TOC section in the file and replace that part:

[regex] $TOC_pattern = "(?smi)(^# Table of Contents)(.*)(^(\r?\n{1}[-]{3,}|\Z))"
    # \Z = Absolute end of string (i.e. end of file); do not add spaces for readability to the RegEx!

[string] $OldTOC = $Data | select-string -Pattern $TOC_pattern -AllMatches | % { $_.matches.groups[1].value + $_.matches.groups[2].value}

$Data = $Data.replace($OldTOC, $NewTOC)

# --------------------------------------------------------------------------------------------------
# (Over)Write the file with the updated content:

$Data | Set-Content -Encoding UTF8 -Path $Filepath
    # Note that with Powershell 5, 'UTF8"'means "UTF-8 with BOM", 'UTF8NoBOM' is only available in later versions.

write-host "Updated $Filepath"