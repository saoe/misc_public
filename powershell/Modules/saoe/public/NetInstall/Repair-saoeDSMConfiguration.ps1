<#
    .Synopsis
    Replace corrupt DSM configuration files (*.ncp) on a client.
    
    .Description
    A repair job for an Enteo/FrontRange/NetInstall/DSM environment (ca. 2013-2016 version).
    
    Sometimes, the configuration files on an client gets corrupted (the most common case I had:
    NiCfgSrv.ncp for some reason has a wrong 'created' (or 'modified'...?) timestamp, usually of
    the (still future) year 2038. Due to this, the DSM server and client won't communicate anymore.
    
    .Parameter InputFile
    A plain text file that contain one computer name (FQDN format) per line,
    example:
    HostName100.sub.example.net
    HostName101.sub.example.net
    HostName102.sub.example.net
    
    .Parameter DSMServer
    The name of the server from which the configuration files will be copied.
    Note that the path is currently hardcoded in the code!
    
    .Parameter FolderWithPsExec
    Path to a folder(!) that contains psexec.exe.
    SysInternal's PsExec.exe is required to invoke a special command on the client.
    See <https://docs.microsoft.com/en-us/sysinternals/downloads/psexec>.
    
    .Notes
    Copyright © 2020 Sascha Offe <so@saoe.net>
    SPDX-License-Identifier: MIT
    Part of the Powershell module "saoe" <https://bitbucket.org/saoe/misc_public/>
    
    Some values are hardcoded or language-specific ('Programme').
    May need to be adjusted/made for flexible if I ever need this in a different environment...
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true)] [string] $InputFile,
    [Parameter(Mandatory=$true)] [string] $DSMServer,
    [Parameter(Mandatory=$true)] [string] $FolderWithPsExec
)

$computers = Get-Content -Path $InputFile

foreach ($computer in $computers)
{
    if (Test-Connection -ComputerName $computer -Quiet)
    {
        # Copy up-to-date configuration files from the DSM server to the client.
        Write-Progress -Id 1 -Activity "$computer - Copy files to client" -Status "Working..." -PercentComplete 20
        Copy-Item -Force -Path "\\$DSMServer\C$\Program Files (x86)\netinst\NiCfgLcl.ncp" -Destination "\\$computer\c$\Programme\netinst"
        Copy-Item -Force -Path "\\$DSMServer\C$\Program Files (x86)\netinst\NiCfgSrv.ncp" -Destination "\\$computer\c$\Programme\netinst"

        # Stop DSM services on the client.
        Write-Progress -Id 1 -Activity "$computer - Stop NI services on client" -Status "Working..." -PercentComplete 40
        Stop-Service -force -inputobject $(Get-Service -ComputerName $computer -Name ersupext)
        Stop-Service -force -inputobject $(Get-Service -ComputerName $computer -Name esiCore)
        
        Start-Sleep -Seconds 5
        
        # Start DSM services on the client again; now the replaced configuration files will be read.
        Write-Progress -Id 1 -Activity "$computer - Start NI services on client" -Status "Working..." -PercentComplete 60
        Start-Service -inputobject $(Get-Service -ComputerName $computer -Name esiCore)
        Start-Service -inputobject $(Get-Service -ComputerName $computer -Name ersupext)

        # Trigger an activation command of the DSM agent on the client.
        Write-Progress -Id 1 -Activity "$computer - Execute NI changes on client" -Status "Working..." -PercentComplete 80
        $psexec = "$FolderWithPsExec\PsExec.exe"
        $arguments = "-accepteula -d \\$computer C:\Programme\netinst\niinst32.exe /ai"
        Start-Process -FilePath $psexec -ArgumentList $arguments -Wait -Passthru
        
        Write-Progress -Id 1 -Activity "$computer" -Status "Done" -PercentComplete 100
        Write-Output "$computer is done!"
    }
    else
    {
        Write-Warning "$computer is not online!"
    }
}
