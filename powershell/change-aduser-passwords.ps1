 <#
.SYNOPSIS
    Change password(s) of Active Directory (AD) user(s) in one or more domains.
 
.DESCRIPTION
    This script changes the password of a single user accross multiple domains,
    if the username is identical in all those domains.
    
    Alternatively, a file can be provided with multiple (and different!) usernames and domains.
    
    In both cases, the old/current password must the same for all accounts, and the new
    password will then also be set to the same password for all accounts in all named domains!
    
    At the moment, this script can only change the password of a user, not reset it!
    (For different accounts in different domains, multiple credentials may be needed for that...)
    
    Requires the ActiveDirectory module (install RSAT before, if not available).
 
.PARAMETER UserAccountName
    The username (without the domain) of the account for which you want to change the password.

.PARAMETER Domains
    Comma-sperated list of one or more FQDNs, in which the username should exist.

.PARAMETER InputFile
    A text file that contains one or more usernames, whose password will be changed.
    In UPN format, one entry per line.
   
.NOTES
    Copyright � 2020 Sascha Offe (so@saoe.net).
    SPDX-License-Identifier: MIT-0

.EXAMPLE
    C:\> ThisScript.ps1 -UserAccountName user.name -Domains domain1.tld,another.domain.tld,domain-X.tld

.EXAMPLE
    C:\> ThisScript.ps1 -InputFile "C:\Some Long Path\data.txt"
    
    The file "data.txt" must contain one user principal name (UPN) per line; example:
    user.name@domain1.tld
    user.ABC@another.domain.tld
    someone@domain-X.tld
#>
 
[CmdletBinding()]
Param
(
    [Parameter(Mandatory = $True, ParameterSetName = "CommandLine")]
    [string] $UserAccountName,
    
    [Parameter(Mandatory = $True, ParameterSetName = "CommandLine")]
    [string[]] $Domains,
    
    [Parameter(Mandatory = $True, ParameterSetName = "InputFile")]
    [ValidateScript({ Test-Path -Path $_ -PathType Leaf })]
    [string] $InputFile
)

Import-Module ActiveDirectory -ErrorAction Stop
    # Hint: Install/activate RSAT, if not available.

function setPassword
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory = $true)]
        [string] $username,
        
        [Parameter(Mandatory = $true)]
        [string] $domain
    )

    Try
    {
        $u = Get-ADUser -Filter "UserPrincipalName -eq '$username@$domain'" -Server $domain
        
        if ($u -eq $null)
        {
            Write-Output "User $username does not exist in domain $domain!"
        }
        else
        {
            Write-Output "OK, found $username in domain $domain; trying to set new password..."
            try
            {
                Set-ADAccountPassword -Server $domain -Identity $username -OldPassword $old_password -NewPassword $new_password -ErrorAction Stop
                Write-Output "... password set."
            }
            catch
            {
                if ($_.Exception.GetType().FullName -eq "Microsoft.ActiveDirectory.Management.ADPasswordComplexityException")
                {
                    Write-Host "The new password doesn't meet the domain requirements for complexity." -ForegroundColor Red
                }
                
                if ($_.Exception.GetType().FullName -eq "Microsoft.ActiveDirectory.Management.ADInvalidPasswordException")
                {
                    Write-Host "The provided old password is wrong." -ForegroundColor Red
                }
            }
        }
    }
    Catch
    {
        Write-Output "Error querying domain $domain."
        continue
    }
}

# --------------------------------------------------------------------------------------------------

$old_password         = Read-Host -AsSecureString -Prompt "Please provide the OLD password for $UserAccountName"
$new_password         = Read-Host -AsSecureString -Prompt "Please provide the NEW password for $UserAccountName"
$new_password_confirm = Read-Host -AsSecureString -Prompt "Please CONFIRM the NEW password for $UserAccountName"

if ([Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($new_password)) -cne # "case-sensitive not equal"
    [Runtime.InteropServices.Marshal]::PtrToStringAuto([Runtime.InteropServices.Marshal]::SecureStringToBSTR($new_password_confirm)))
    # The verification is not 100% secure, since the secure strings are being decrypted to be comparable.
    # And yes, a bit unreadable, but I don't want to use too many temporary variables that contain an unencrypted password.
{
    Write-Error -Message "Passwords don't match... Please try again."
    Exit
}

if ($InputFile)
{
    $input = Get-Content $InputFile

    ForEach ($line in $input)
    {
        $line_array = $line.Split('@')
        setPassword -username $line_array[0] -domain $line_array[1]
    }
}
elseif ($UserAccountName -and $Domains)
{
    ForEach ($domain in $Domains)
    {
        setPassword -username $UserAccountName -domain $domain
    }
}
