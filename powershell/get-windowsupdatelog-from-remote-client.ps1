﻿<# 
	Creates a log file of the Windows Updates on a remote client and copies that file then back onto
	the caller's machine, for further investigation.
	
	-- sascha.offe@saoe.net, 2020-01-24.
#>

$computer = "hostname.full.qualified.domainname"
    # FQDN of the remote machine.

$destination_folder = "C:\"
    # On your local machine.

$cred = Get-Credential
	# Get (and save for later use) the credentials of an local admin on the remote machine.

# Prerequisite: On the remote computer, the service "Windows Remote Management (WS-Management)" a/k/a "WinRM" must be running.
Get-Service -Name WinRM -ComputerName $computer | Set-Service -Status Running

# Tests whether the WinRM service is running on a local or remote computer. 
Test-WSMan -ComputerName $computer

# Create the Windows Update log file on the remote machine.

Invoke-Command -ComputerName $computer -ArgumentList $computer -Credential $cred -ScriptBlock {
        param($computer)
			# Use -ArgumentList and param(...) to handover local variables to the script block.
		
		Get-WindowsUpdateLog -LogPath C:\$computer-WindowsUpdate.log
			# Provide a full path, including filename, else you may get an Access Denied error.
    } 

# Copy the log file from the remote client to your own machine.
$copy_status = Copy-Item "\\$computer\c$\$($computer)-WindowsUpdate.log" -Destination $destination_folder -PassThru

if ($copy_status)
{
    Write-Host "Log file copied to folder $destination_folder on your machine."
}

# Stop WinRM on the remote client again.
Get-Service -Name WinRM -ComputerName $computer | Stop-Service -Force
