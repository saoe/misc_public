# ==============================================================================================
# Example PowerShell script that uses .NET Windows Forms for a simple GUI.
#
# Copyright © 2014-2016 Sascha Offe <so@saoe.net>.
# Distributed under the terms of the MIT license <https://opensource.org/licenses/MIT>.
#
# 2014-06-20: Started as a GUI for a command-line tool, but it wasn't really needed.
# 2016-04-29: Turned it into a more generic example (some names and parts are still hinting at the original cause).
# 2016-05-01: Minor tweaks (hiding the console, etc.)
#
# Helpful pages:
# <http://www.martinlehmann.de/wp/download/powershell-gui-programmierung-fur-dummies-step-by-step/> *german!*
# <https://blogs.technet.microsoft.com/stephap/2012/04/23/building-forms-with-powershell-part-1-the-form/>
# <https://sysadminemporium.wordpress.com/2012/11/26/powershell-gui-front-end-for-your-scripts-episode-1/>
# ==============================================================================================

# Load assemblies of the .NET framework.
# Using the recommended 'Add-Type' way instead of the deprecated LoadWithPartialName() method; but look out: http://www.madwithpowershell.com/2013/10/add-type-vs-reflectionassembly-in.html
Add-Type -AssemblyName System.Drawing
Add-Type -AssemblyName System.Windows.Forms
Add-Type -Name Window -Namespace Console -MemberDefinition '[DllImport("Kernel32.dll")] public static extern IntPtr GetConsoleWindow();
                                                            [DllImport("user32.dll")]   public static extern bool ShowWindow(IntPtr hWnd, Int32 nCmdShow);'

# Modules
# Import-Module ActiveDirectory

# ----------------------------------------------------------------------------------------------

Function Hide-Console
{
	$con = [Console.Window]::GetConsoleWindow()
	[Console.Window]::ShowWindow($con, 0)
}

Function Call-Action
{
	$TextBox.Clear()
	$TextBox.AppendText("Username: " + $InputUsername.Text + [System.Environment]::NewLine)
	$TextBox.AppendText("Password: " + $InputPassword.Text + [System.Environment]::NewLine)
	$TextBox.AppendText("Domain Controller: " + $InputDC.Text + [System.Environment]::NewLine)

	$TextBox.ScrollToCaret()
	$TextBox.Refresh()
}

# ----------------------------------------------------------------------------------------------
# Form (Window)

$Form = New-Object System.Windows.Forms.Form
$Form.StartPosition = "CenterScreen" # On multiple screens: Pops up on the screen where the mouse cursor currently is.
$Form.Text = "GUI Example (PowerShell using Windows Forms)"
$Form.Width = 500
$Form.Height = 250

$Form.FormBorderStyle = "FixedDialog"
$Form.MinimizeBox = $False
$Form.MaximizeBox = $False

$Form.Icon = [system.drawing.icon]::ExtractAssociatedIcon($PSHOME + "\powershell.exe")

# ----------------------------------------------------------------------------------------------
# Controls

# Input control for username
$LabelUsername = New-Object System.Windows.Forms.Label
$LabelUsername.Text = "Username:"
$LabelUsername.TextAlign = "MiddleLeft"

$InputUsername = New-Object System.Windows.Forms.TextBox
$InputUsername.TextAlign = "Left"
$InputUsername.Width = $Form.Width - $LabelUsername.Width

# Input control for password
$LabelPassword = New-Object System.Windows.Forms.Label
$LabelPassword.Text = "Password:"
$LabelPassword.TextAlign = "MiddleLeft"

$InputPassword = New-Object System.Windows.Forms.MaskedTextBox
$InputPassword.TextAlign = "Left"
$InputPassword.Width = $Form.Width - $LabelPassword.Width
$InputPassword.PasswordChar = "*"

# Input control for domain controller
$LabelDC = New-Object System.Windows.Forms.Label
$LabelDC.Text = "Domain Controller:"
$LabelDC.TextAlign = "MiddleLeft"

$InputDC = New-Object System.Windows.Forms.TextBox
$InputDC.TextAlign = "Left"
$InputDC.Width = $Form.Width - $LabelDC.Width
# $CurrentDC = Get-ADDomainController # Gets the domain controller in the user's current session. 
# $InputDC.Text += $CurrentDC.Name


# ----------------------------------------------------------------------------------------------
# The table layout for the controls.

$Area = New-Object System.Windows.Forms.TableLayoutPanel
$Area.Dock = "Fill" 
$Area.AutoSize = $True
$Area.Dock = [System.Windows.Forms.DockStyle]::Top
$Area.CellBorderStyle = "None" # "Single"
$Area.RowCount = 3
$Area.ColumnCount = 2

# Pipe the return value to Null, don't write it to the console.
$Area.ColumnStyles.Add((new-object System.Windows.Forms.ColumnStyle([System.Windows.Forms.SizeType]::Percent, 30))) | Out-Null
$Area.ColumnStyles.Add((new-object System.Windows.Forms.ColumnStyle([System.Windows.Forms.SizeType]::Percent, 70))) | Out-Null

# Adding the controls. Order of arguments: control, column, row.
$Area.Controls.Add($LabelUsername, 0, 0)
$Area.Controls.Add($InputUsername, 1, 0)

$Area.Controls.Add($LabelPassword, 0, 1)
$Area.Controls.Add($InputPassword, 1, 1)

$Area.Controls.Add($LabelDC, 0, 2)
$Area.Controls.Add($InputDC, 1, 2)


# ----------------------------------------------------------------------------------------------
# Buttons

$ActionButton = New-Object System.Windows.Forms.Button
$ActionButton.Location = New-Object System.Drawing.Point((($Form.Width / 2) - 80), (($Form.Height / 2) - 40))

$ActionButton.Text = "Action"
$ActionButton.AutoSize = $True

$ActionButton.Add_Click(
{
	$action_result = Call-Action
})

$CancelButton = New-Object System.Windows.Forms.Button
$CancelButton.Location = New-Object System.Drawing.Point((($Form.Width / 2) + 20), (($Form.Height / 2) - 40))

$CancelButton.Text = "Cancel"
$CancelButton.AutoSize = $True

$CancelButton.Add_Click(
{
	$Form.Close()
})


# ----------------------------------------------------------------------------------------------
# The Text Box

$TextBox = New-Object System.Windows.Forms.TextBox
$TextBox.Location = New-Object System.Drawing.Point(0, 100)                   # overridden by DockStyle::Bottom.
$TextBox.Size = New-Object System.Drawing.Size($Form.Width, ($Form.Height/3)) # width: overridden by DockStyle::Bottom.
$TextBox.Dock = [System.Windows.Forms.DockStyle]::Bottom
$TextBox.ScrollBars = [System.Windows.Forms.ScrollBars]::Vertical
#$TextBox.Font = New-Object System.Drawing.Font("Courier New", "10")
$TextBox.MultiLine = $True
$TextBox.Wordwrap = $True
$TextBox.ReadOnly = $True
$TextBox.Text = "Click 'Action' to execute the command, or click 'Cancel' to exit this script."

# Text should not be automatically selected/highlighted.
$TextBox.SelectionStart = 0
$TextBox.SelectionLength = 0


# ----------------------------------------------------------------------------------------------
# Add controls to form.

$Form.Controls.Add($Area)
$Form.Controls.Add($ActionButton)
$Form.Controls.Add($CancelButton)
$Form.Controls.Add($TextBox)


# ----------------------------------------------------------------------------------------------

Hide-Console
[void] $Form.ShowDialog()
