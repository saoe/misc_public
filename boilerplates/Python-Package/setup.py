# Configuring package metadata
#
# There are two types of metadata:
#
# 1. Static metadata (setup.cfg):
#    Guaranteed to be the same every time. This is simpler, easier to read, and avoids many common errors, like encoding errors.
# 2. Dynamic metadata (setup.py): possibly non-deterministic.
#    Any items that are dynamic or determined at install-time, as well as extension modules or extensions to setuptools, need to go into setup.py.
#
# Static metadata (setup.cfg) should be preferred.
# Dynamic metadata (setup.py) should be used only as an escape hatch when absolutely necessary.
# setup.py used to be required, but can be omitted with newer versions of setuptools and pip.

import pathlib
from setuptools import setup

# The directory containing this file
THIS_DIR = pathlib.Path(__file__).parent

# The text of the README file
README_FILE = (THIS_DIR/"ReadMe.md").read_text()

# https://setuptools.pypa.io/en/latest/setuptools.html
# https://setuptools.pypa.io/en/latest/references/keywords.html
#
setup(name='<PROJECT_NAME_PLACEHOLDER>',
      version='0.1',
      description='Short description',
      long_description=README_FILE,
      long_description_content_type="text/markdown",
      url='https://www.saoe.net/',
      author='<PROJECT_BY_PLACEHOLDER>',
      author_email='<PROJECT_EMAILADDRESS_PLACEHOLDER>',
      license='<LICENSE_PLACEHOLDER>',
      classifiers=[
            "License :: OSI Approved :: <LICENSE_PLACEHOLDER> License",
            "Programming Language :: Python :: 3",
        ],
      packages=[]#find_packages(),
      include_package_data=True,         # The include_package_data argument controls whether non-code files (see MANIFEST.in) are copied when your package is installed.
      install_requires=[],               # install_requires is not a duplication of requirements.txt; requirements.txt is supposed to be just a complement.
      entry_points={
        "console_scripts": [
            "<PROJECT_NAME_PLACEHOLDER>=<PROJECT_NAME_PLACEHOLDER>.__main__:main",
        ]
      })