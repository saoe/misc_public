# ╭────────────────────────────────────────────────────────────────────────────────────────────────╮
# │                                   <PROJECT_NAME_PLACEHOLDER>                                   │
# ╰────────────────────────────────────────────────────────────────────────────────────────────────╯
# Copyright © <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> <<PROJECT_EMAILADDRESS_PLACEHOLDER>>
# SPDX-License-Identifier: <LICENSE_PLACEHOLDER>

add_executable (${PROJECT_NAME} WIN32) # ... or add_library() or add_custom_target().

target_sources (${PROJECT_NAME}
    PRIVATE
        main.cpp
        ${CMAKE_BINARY_DIR}/version.h
        ${CMAKE_SOURCE_DIR}/resources/winres.rc
)

## Add compile (pre-processor) definitions to a target. (Any leading -D on an item will be removed.)
#target_compile_definitions (${PROJECT_NAME}
#    PRIVATE
#        # ...
#)

# Also of interest:
#   - target_compile_options ()
#   - target_compile_features ()

target_include_directories (${PROJECT_NAME}
	PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
		$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
	PRIVATE
		${CMAKE_PREFIX_PATH}
        $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}>
			# Because some auto-generated header files (i.e. version.h) are put in the binary output folder.
)

target_link_libraries (${PROJECT_NAME}
    PRIVATE
        Comctl32.lib
)

# Rename the output name of the executable file:
set_target_properties (${PROJECT_NAME} PROPERTIES OUTPUT_NAME "${CMAKE_PROJECT_NAME}-${CPU_ARCHITECTURE}")

## Even for pure console programs, CMake adds a definition for _WINDOWS (built-in feature/bug).
## May become problematic under certain circumstances, better remove it.
#string (REPLACE "/D_WINDOWS" "" CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS})

# Also handy:
#   - add_dependencies ()
#   - add_custom_command ()

# --------------------------------------------------------------------------------------------------
#                                          Generate version.h
# 
# Because there is no target to be build on this level, we must use a custom dummy target:
# Using just add_custom_command with a dependency on the generated output file doesn't work and seems
# to be a known issue: <https://discourse.cmake.org/t/define-a-pre-build-command-without-creating-a-new-target/1623>
#
# If/when BUILDTIME is being used again (currently commented out), the workflow logic needs to be rethought,
# because it would be done on each build now and prolong the the build time ('cause it's an included header...).

add_custom_target (VersionHeader
    ALL
    BYPRODUCTS ${CMAKE_BINARY_DIR}/version.h
    COMMAND ${CMAKE_COMMAND}                                  # CMake unfortunately doesn't make it easy to hand over/access variables, so we have to define them here for the *.cmake file.
            -D IN=${CMAKE_SOURCE_DIR}/resources/version.h.in
            -D OUT=${CMAKE_BINARY_DIR}/version.h              # Put in the build directory!
            -D TOPLEVELDIR=${${CMAKE_PROJECT_NAME}_REPO_ROOT_DIR}
            -D VERSION_MAJOR=${CMAKE_PROJECT_VERSION_MAJOR}
            -D VERSION_MINOR=${CMAKE_PROJECT_VERSION_MINOR}
            -D VERSION_PATCH=${CMAKE_PROJECT_VERSION_PATCH}
            -D VERSION_STRING=${CMAKE_PROJECT_VERSION}
            #-D BUILDTIME=${BUILDTIME}
            -D PROJECT_NAME=${CMAKE_PROJECT_NAME}
            -D PROJECT_DESCRIPTION=${CMAKE_PROJECT_DESCRIPTION}
            -D PROJECT_URL=${CMAKE_PROJECT_HOMEPAGE_URL}
            -D ORIGINAL_FILENAME="[FIXME]" # [FIXME] $<TARGET_FILE_NAME:${PROJECT_NAME}> would be correct, but Generator Expressions can only be used at Build time, not Configuration; hmm... (in version.cmake it's also too early.)
            -D AUTHOR_NAME=${AUTHOR_NAME}
            -D AUTHOR_MAIL=${AUTHOR_MAIL}
            -D COPYRIGHT=${COPYRIGHT_NOTICE}
            -D LICENSE=${LICENSE_NOTICE}
            -P ${CMAKE_SOURCE_DIR}/cmake/version.cmake
)

# --------------------------------------------------------------------------------------------------
#                                         Post-build commands

#add_custom_command (
#    TARGET ${PROJECT_NAME}
#    POST_BUILD
#    COMMAND ${CMAKE_COMMAND}
#            -D ...
#            -P ...
#)

# --------------------------------------------------------------------------------------------------
#                                       Installation & Export
#
# Sets up the 'install' target and the configuration files, so that other CMake projects can use this with find_package().
#
# install() is used for the pre-defined targets 'INSTALL' and 'PACKAGE' (by CMake's CPack module).
# Target INSTALL depends on 'all' by default.

#set (CMAKE_INSTALL_SYSTEM_RUNTIME_DESTINATION ".")
#include (InstallRequiredSystemLibraries)

# Step 1.1: The basics...
install (
    TARGETS ${PROJECT_NAME}
	EXPORT  ${CMAKE_PROJECT_NAME}Targets                                     # Mark the targets for the export set.
	RUNTIME DESTINATION ${${CMAKE_PROJECT_NAME}_INSTALL_BINDIR}/${CMAKE_GENERATOR} # For a DLL, change to LIBDIR: A Windows DLL is considered a Runtime module.
	LIBRARY DESTINATION ${${CMAKE_PROJECT_NAME}_INSTALL_LIBDIR}/${CMAKE_GENERATOR}
	ARCHIVE DESTINATION ${${CMAKE_PROJECT_NAME}_INSTALL_LIBDIR}/${CMAKE_GENERATOR}
)

# On Debug build: Copy linker's debug symbols file (*.pdb) to the package directory.
#
# TARGET_PDB_FILE is the full path to the Microsoft debug symbols file (*.pdb) generated by the
# linker for an executable or shared library target (/pdb linker flag).
# This property does not apply to STATIC library targets because no linker is invoked to produce them.
# For static libraries, compiler-generated program database files are specified by the /Fd compiler flag.
# Use the COMPILE_PDB_OUTPUT_DIRECTORY property to specify that (see also the PDB_NAME[_<CONFIG>] and PDB_OUTPUT_DIRECTORY[_<CONFIG>] target properties).

install (
    FILES $<TARGET_PDB_FILE:${PROJECT_NAME}>
    CONFIGURATIONS Debug                      # Only run when used in a 'Debug' build.
    DESTINATION .                             # Current directory (relative path to CMAKE_INSTALL_PREFIX).
)

# Step 1.2: The public headers.
install (
    FILES
#        ../../FileA.txt                 # Note the relative path!
#        ${EXAMPLE_VAR}/FileB.txt
    DESTINATION ${${CMAKE_PROJECT_NAME}_INSTALL_INCLUDEDIR}
)

# Step 2: Installs a "*Targets.cmake" file which contains CMake code that creates imported targets for each target in the export set.
install (
    EXPORT      ${CMAKE_PROJECT_NAME}Targets
	FILE        ${CMAKE_PROJECT_NAME}Targets.cmake
	NAMESPACE   ${CMAKE_PROJECT_NAME}::
	DESTINATION ${${CMAKE_PROJECT_NAME}_INSTALL_CMAKEDIR}
)

# Step 3: Creating the CMake configuration files, so that other projects can find and use it by find_package().
include(CMakePackageConfigHelpers) # Provided by CMake.

# Step 3.1: Prepare the package configuration file (*Config.cmake).
configure_package_config_file (
	${CMAKE_SOURCE_DIR}/resources/config.cmake.in
	${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME}Config.cmake
	INSTALL_DESTINATION ${CMAKE_CURRENT_BINARY_DIR}
)

# Step 3.2: Prepare the package version file (*ConfigVersion.cmake).
write_basic_package_version_file (
	${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME}ConfigVersion.cmake
	VERSION       ${CMAKE_PROJECT_VERSION}
	COMPATIBILITY AnyNewerVersion
)

# Step 3.3: Install the generated files in the right place.
install (
    FILES
        ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME}Config.cmake
        ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME}ConfigVersion.cmake
	DESTINATION ${${CMAKE_PROJECT_NAME}_INSTALL_CMAKEDIR}
)

# Step 4: Export targets to the build-tree export set, so that an outside project can use this project without having it be installed.
#         Typically projects are built and installed before being used by an outside project.
#         However, in some cases, the targets may be used by an outside project that references the build tree with no installation involved.
export (
    TARGETS ${PROJECT_NAME}
    FILE    ${PROJECT_BINARY_DIR}/${CMAKE_PROJECT_NAME}Exports.cmake
)
