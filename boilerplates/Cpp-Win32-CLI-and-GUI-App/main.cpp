/*
    ╭──────────────────────────────────────────────────────────────────────────────────────────────╮
    │                                  <PROJECT_NAME_PLACEHOLDER>                                  │
    ╰──────────────────────────────────────────────────────────────────────────────────────────────╯
    Copyright © <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> <<PROJECT_EMAILADDRESS_PLACEHOLDER>>
    SPDX-License-Identifier: <LICENSE_PLACEHOLDER>

    - Build it with /SUBSYSTEM:WINDOWS
    - Uses WinMain() + Message loop
    - Uses AllocConsole() + AttachConsole():
      You'll get a GUI window and also a console window when starting the executable.
      
    2015-02-02 (Sascha Offe): Created.
    2022-02-27 (so)         : Small update and made it into a project boilerplate
*/

#include <windows.h>
#include <commctrl.h>
#include <tchar.h>
#include <stdio.h>
#include <iostream>
#include "res.h"

const int window_height = 256;
const int window_width = 256;

LRESULT CALLBACK WndProc (HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	switch (msg)
	{
		case WM_CLOSE:
			// if (MessageBox(hwnd, _T("Really quit?"), _T("My application"), MB_OKCANCEL) == IDOK)
			// {
			DestroyWindow(hwnd);
			// }
			return 0;
                               
		case WM_DESTROY:
			PostQuitMessage(0);
			return 0;

		case WM_QUIT:
			return 0;
       
		default:
			return DefWindowProc(hwnd, msg, wparam, lparam);
	}

	return 0;
}


int WINAPI WinMain (HINSTANCE instance, HINSTANCE previnstance, LPSTR cmdline, int cmdshow)
{
	// Attach a console window to the process; for output via stdio or iostream.
	AllocConsole();
	AttachConsole(GetCurrentProcessId());
	freopen("CON", "w", stdout);
	
	// How to close/detach the console
	// fclose(stdout);
	// FreeConsole();

	WNDCLASSEX wc;
	HWND       hwnd;
	MSG        msg;
	
	// CommonControls (COM/SHBrowseForFolder, ProgressBar, ...)
	HRESULT hr_com = CoInitializeEx(NULL, COINIT_MULTITHREADED);
	INITCOMMONCONTROLSEX icc;
	icc.dwSize = sizeof(INITCOMMONCONTROLSEX);
	icc.dwICC  = ICC_PROGRESS_CLASS;
	InitCommonControlsEx(&icc);

	// Registering the Window Class
	wc.cbSize        = sizeof(WNDCLASSEX);
	wc.style         = 0;
	wc.lpfnWndProc   = WndProc;
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = instance;
	wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = _T("MyClassName");
	wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

    if (!RegisterClassEx(&wc))
    {
        MessageBox(NULL, _T("Window Registration Failed!"), _T("Error!"), MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE,
	                      _T("MyClassName"),
						  _T("The title of my window"),
						  WS_OVERLAPPEDWINDOW,
						  CW_USEDEFAULT,
						  CW_USEDEFAULT,
						  window_height,
						  window_width,
						  NULL,
						  NULL,
						  instance,
						  NULL);

    if (hwnd == NULL)
    {
        MessageBox(NULL, _T("Window Creation Failed!"), _T("Error!"), MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    ShowWindow(hwnd, cmdshow);
    UpdateWindow(hwnd);

    // The Message Loop
    while (GetMessage(&msg, NULL, 0, 0) > 0)
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

	if (hr_com >= 0)
		CoUninitialize();

    return msg.wParam;
}