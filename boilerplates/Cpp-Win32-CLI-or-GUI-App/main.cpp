﻿/*
    ╭──────────────────────────────────────────────────────────────────────────────────────────────╮
    │                                  <PROJECT_NAME_PLACEHOLDER>                                  │
    ╰──────────────────────────────────────────────────────────────────────────────────────────────╯
    Copyright © <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> <<PROJECT_EMAILADDRESS_PLACEHOLDER>>
    SPDX-License-Identifier: <LICENSE_PLACEHOLDER>

    If you start the program with zero command line arguments, a GUI window will be created.
    (Double click or from a console.)

    If you start the program with one or more command line arguments (either from a prompt or
    double click the icon, with added arguments in its properties), instead of the GUI window
    a new console will open.

    Build it with /SUBSYSTEM:WINDOWS
      
    2018-06-03 (Sascha Offe): Created.
    2022-02-27 (so)         : Small update and made it into a project boilerplate
*/

#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <iostream>
//#include <commctrl.h>
#include "res.h"

const int window_height = 256;
const int window_width = 256;

int main (int argc, char* argv[])
{
	// Attach a console window to the process; for output via stdio or iostream.
	AllocConsole();
	AttachConsole(GetCurrentProcessId());
	FILE* stream;
	freopen_s(&stream, "CON", "w", stdout);
	
	// How to close/detach the console
	// fclose(stdout);
	// FreeConsole();
	
	std::cout << "Hello, world!\n\n";

	for (int i = 0; i < argc; ++i)
	{
		std::cout << "\tArgument: " << argv[i] << std::endl;;
	}

	std::cout << std::endl;

	system("pause");

	return 0;
}

LRESULT CALLBACK WndProc (HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	switch (msg)
	{
		case WM_CLOSE:
			// if (MessageBox(hwnd, _T("Really quit?"), _T("My application"), MB_OKCANCEL) == IDOK)
			// {
			DestroyWindow(hwnd);
			// }
			return 0;
                               
		case WM_DESTROY:
			PostQuitMessage(0);
			return 0;

		case WM_QUIT:
			return 0;
       
		default:
			return DefWindowProc(hwnd, msg, wparam, lparam);
	}

	return 0;
}


int WINAPI WinMain (HINSTANCE instance, HINSTANCE previnstance, LPSTR cmdline, int cmdshow)
{
	if (__argc > 1)
	{
		return main(__argc, __argv);
	}
	else
	{
		WNDCLASSEX wc;
		HWND       hwnd;
		MSG        msg;
	
		// Registering the Window Class
		wc.cbSize        = sizeof(WNDCLASSEX);
		wc.style         = 0;
		wc.lpfnWndProc   = WndProc;
		wc.cbClsExtra    = 0;
		wc.cbWndExtra    = 0;
		wc.hInstance     = instance;
		wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
		wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
		wc.lpszMenuName  = NULL;
		wc.lpszClassName = _T("MyClassName");
		wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

		if (!RegisterClassEx(&wc))
		{
			MessageBox(NULL, _T("Window Registration Failed!"), _T("Error!"), MB_ICONEXCLAMATION | MB_OK);
			return 0;
		}

		hwnd = CreateWindowEx(WS_EX_CLIENTEDGE,
							  _T("MyClassName"),
							  _T("The title of my window"),
							  WS_OVERLAPPEDWINDOW,
							  CW_USEDEFAULT,
							  CW_USEDEFAULT,
							  window_height,
							  window_width,
							  NULL,
							  NULL,
							  instance,
							  NULL);

		if (hwnd == NULL)
		{
			MessageBox(NULL, _T("Window Creation Failed!"), _T("Error!"), MB_ICONEXCLAMATION | MB_OK);
			return 0;
		}

		ShowWindow(hwnd, cmdshow);
		UpdateWindow(hwnd);

		// The Message Loop
		while (GetMessage(&msg, NULL, 0, 0) > 0)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		return msg.wParam;
	}
}
