# About this directory 

The files here are used to compose a viable basic foundation for project of the given type.

Files on the upper level are more genric/versatile (e.g. _ReadMe_ and _.gitignore_), while the files in
the subdirectory are more specific to the type of projects that the directory's name suggest.

You could use those files manually, but the content here is more meant for helper scripts/functions
(like my `New-saoeCPPProject.ps1`), which collect and compose the right pieces with the right
specifications automatically.

(Note that some files also have placeholder text that would be replaced by such a
helper scrip; e.g. _<PROJECT_NAME_PLACEHOLDER>_, _<COPYRIGHT_YEAR_PLACEHOLDER>_ and _<LICENSE_PLACEHOLDER>_).

**Attention:** The name of directories have meaning, so act responsible when renaming any of them.
:   For example, the directory's name should correspond with the name of type that is given to
    `New-saoeCPPProject` as the _-Type_ parameter, because it is used internally by it to create the path!

-- Sascha, 2021-05-02.

## TODOs/Issues

