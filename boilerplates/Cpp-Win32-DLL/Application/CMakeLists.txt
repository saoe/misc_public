# ╭────────────────────────────────────────────────────────────────────────────────────────────────╮
# │                                   <PROJECT_NAME_PLACEHOLDER>                                   │
# ╰────────────────────────────────────────────────────────────────────────────────────────────────╯
# Copyright © <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> <<PROJECT_EMAILADDRESS_PLACEHOLDER>>
# SPDX-License-Identifier: <LICENSE_PLACEHOLDER>

project ("Application")

add_executable (${PROJECT_NAME})

target_sources (${PROJECT_NAME}
    PRIVATE
        main.cpp
        ${CMAKE_BINARY_DIR}/version.h
        ${CMAKE_SOURCE_DIR}/resources/winres.rc
)

target_include_directories (${PROJECT_NAME}
	PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
		$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
	PRIVATE
		${CMAKE_PREFIX_PATH}
        $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}>
			# Because some auto-generated header files (i.e. version.h) are put in the binary output folder.
)

target_link_libraries (${PROJECT_NAME}
    PRIVATE
        Library
)

# Rename the output name of the executable file:
set_target_properties (${PROJECT_NAME} PROPERTIES OUTPUT_NAME "${PROJECT_NAME}-${CPU_ARCHITECTURE}")

# ------------------------------------------------------------------------------
# Post-build commands to make a 'package', i.e. putting all files necessary (like the DLLs) for running the program into a single directory.

add_custom_command (
	TARGET ${PROJECT_NAME}
	POST_BUILD
    COMMENT "Copy the generated executable and other required files into the same directory."
	COMMAND ${CMAKE_COMMAND} -E copy_if_different $<TARGET_FILE:Library> ${PROJECT_BINARY_DIR}/$<CONFIG>
    DEPENDS $<TARGET_FILE:Library>
)
