/*
    ╭──────────────────────────────────────────────────────────────────────────────────────────────╮
    │                                  <PROJECT_NAME_PLACEHOLDER>                                  │
    ╰──────────────────────────────────────────────────────────────────────────────────────────────╯
    Copyright © <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> <<PROJECT_EMAILADDRESS_PLACEHOLDER>>
    SPDX-License-Identifier: <LICENSE_PLACEHOLDER>
*/

#include "ExampleDLL.hpp"
#include "version.h"
#include <iostream>

namespace no = NiftyOddity;

int main (int argc, char* argv[])
{
	std::cout << "Hello, world!" << std::endl;
    
    no::Example ex;
    std::cout << ex.member_function("Test Text") << std::endl;
    
    std::cout << "Number: " << no::helper_function(8) << std::endl;

	return 0;
}