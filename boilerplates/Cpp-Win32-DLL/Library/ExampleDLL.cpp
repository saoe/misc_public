/*
    ╭──────────────────────────────────────────────────────────────────────────────────────────────╮
    │                                  <PROJECT_NAME_PLACEHOLDER>                                  │
    ╰──────────────────────────────────────────────────────────────────────────────────────────────╯
    Copyright © <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> <<PROJECT_EMAILADDRESS_PLACEHOLDER>>
    SPDX-License-Identifier: <LICENSE_PLACEHOLDER>
*/

#if !defined (__cplusplus)
#error "C++ compiler required"
#endif

#define DLL_EXPORT

#include "ExampleDLL.hpp"
#include <string>

#ifdef _WIN32
#include <windows.h>
#endif

namespace NiftyOddity
{

int helper_function (int foo)
{
	return foo;
}

Example::Example ()
{
}

std::string Example::member_function (std::string const& text)
{
    return text;
}

} // namespace