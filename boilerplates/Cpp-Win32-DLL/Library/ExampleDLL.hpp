/*
    ╭──────────────────────────────────────────────────────────────────────────────────────────────╮
    │                                  <PROJECT_NAME_PLACEHOLDER>                                  │
    ╰──────────────────────────────────────────────────────────────────────────────────────────────╯
    Copyright © <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> <<PROJECT_EMAILADDRESS_PLACEHOLDER>>
    SPDX-License-Identifier: <LICENSE_PLACEHOLDER>
*/

#ifndef INCLUDE_GUARD_ // TODO
#define INCLUDE_GUARD_ // TODO

#ifdef DLL_EXPORT
#define DLL __declspec(dllexport)
#else
#define DLL __declspec(dllimport)
#endif

#include <string>

namespace NiftyOddity
{

DLL int helper_function (int foo);

class DLL Example
{
    public:
        Example (); // Constructor.
        std::string member_function (std::string const& text);
};

} // namespace

#endif // include guard