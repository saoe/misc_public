/*
    <PROJECT_NAME_PLACEHOLDER>

    Copyright © <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> <<PROJECT_EMAILADDRESS_PLACEHOLDER>>
    SPDX-License-Identifier: <LICENSE_PLACEHOLDER>
*/

import QtQuick 2.12
import QtQuick.Controls 2.12

ApplicationWindow
{
    visible: true
    width: 640
    height: 480
    title: qsTr("<PROJECT_NAME_PLACEHOLDER>")
}