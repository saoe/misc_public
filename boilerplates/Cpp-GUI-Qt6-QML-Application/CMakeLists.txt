# ╭────────────────────────────────────────────────────────────────────────────────────────────────╮
# │                                   <PROJECT_NAME_PLACEHOLDER>                                   │
# ╰────────────────────────────────────────────────────────────────────────────────────────────────╯
# Copyright © <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> <<PROJECT_EMAILADDRESS_PLACEHOLDER>>
# SPDX-License-Identifier: <LICENSE_PLACEHOLDER>

# --------------------------------------------------------------------------------------------------
#                                              Setup Qt
#
# Note that find_package() expects CMAKE_PREFIX_PATH to be defined and pointing to the right directories for Qt.

set (CMAKE_INCLUDE_CURRENT_DIR ON) # Instruct CMake to always look in the binary directory for the generated Qt MOC files that need to be included.
set (CMAKE_AUTOMOC ON)             # Instruct CMake to run Qt MOC automatically when needed.
set (CMAKE_AUTOUIC ON)             # Instruct CMake to process the target with autouic (for QDesigner's *.ui files).

find_package (Qt6 REQUIRED COMPONENTS
    Gui
	QML     # "Qt Modeling|Markup|Meta Language"
    QUICK   # "Qt User Interface Creation Kit"
)

# --------------------------------------------------------------------------------------------------
#                                    The actual application target

add_executable (${PROJECT_NAME} WIN32)

qt_add_resources (RESOURCES
    ${CMAKE_CURRENT_SOURCE_DIR}/qml.qrc
)

target_sources (${PROJECT_NAME}
    PRIVATE
        main.cpp
        ${CMAKE_CURRENT_BINARY_DIR}/version.h
        ${RESOURCES}
)

target_link_libraries (${PROJECT_NAME}
	PRIVATE
        Qt6::Qml
        Qt6::Quick
)

target_include_directories (${PROJECT_NAME}
    # Note for Qt: Appropriate include directories, compile definitions etc. are automatically added by the target_link_libraries() command.
	PUBLIC
		$<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
		$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
	PRIVATE
        ${CMAKE_PREFIX_PATH}
        ${CMAKE_CURRENT_BINARY_DIR}
			# Because some auto-generated header files (i.e. version.h) are put in the binary output folder.
)

#target_compile_options (${PROJECT_NAME} (
#	PRIVATE
#)

#target_compile_definitions (${PROJECT_NAME}
#	PRIVATE
#)

target_link_options (${PROJECT_NAME}
	PRIVATE
		"/ENTRY:mainCRTStartup"         # Needed for Qt.
)

# Rename the output name of the executable file:
set_target_properties (${PROJECT_NAME} PROPERTIES OUTPUT_NAME "${CMAKE_PROJECT_NAME}-${ARCHITECTURE}")

# --------------------------------------------------------------------------------------------------
#                                          Generate version.h
# 
# If/when BUILDTIME is being used again (currently commented out), the workflow logic needs to be rethought,
# because it would be done on each build now and prolong the the build time ('cause it's an included header...).

add_custom_command (
    OUTPUT version.h
    COMMAND ${CMAKE_COMMAND}                                  # CMake unfortunately doesn't make it easy to hand over/access variables, so we have to define them here for the *.cmake file.
            -D IN=${CMAKE_SOURCE_DIR}/resources/version.h.in
            -D OUT=${CMAKE_CURRENT_BINARY_DIR}/version.h      # Put in the build directory!
            -D TOPLEVELDIR=${${CMAKE_PROJECT_NAME}_REPO_ROOT_DIR}
            -D VERSION_MAJOR=${CMAKE_PROJECT_VERSION_MAJOR}
            -D VERSION_MINOR=${CMAKE_PROJECT_VERSION_MINOR}
            -D VERSION_PATCH=${CMAKE_PROJECT_VERSION_PATCH}
            -D VERSION_STRING=${CMAKE_PROJECT_VERSION}
            #-D BUILDTIME=${BUILDTIME}
            -D PROJECT_NAME=${CMAKE_PROJECT_NAME}
            -D PROJECT_DESCRIPTION=${CMAKE_PROJECT_DESCRIPTION}
            -D PROJECT_URL=${CMAKE_PROJECT_HOMEPAGE_URL}
            -D ORIGINAL_FILENAME="[FIXME]" # [FIXME] $<TARGET_FILE_NAME:${PROJECT_NAME}> would be correct, but Generator Expressions can only be used at Build time, not Configuration; hmm... (in version.cmake it's also too early.)
            -D AUTHOR_NAME=${AUTHOR_NAME}
            -D AUTHOR_MAIL=${AUTHOR_MAIL}
            -D COPYRIGHT=${COPYRIGHT_NOTICE}
            -D LICENSE=${LICENSE_NOTICE}
            -P ${CMAKE_SOURCE_DIR}/cmake/version.cmake
)

# --------------------------------------------------------------------------------------------------
# Post-build commands to make a package, i.e. putting all files necessary for running the program into a single directory;
# e.g. all Qt runtime files to the same folder as the executable (either Release or Debug subfolder, for a multi-config compiler like MSVC).

# Copy required Qt runtime DLLs from the external directory to the destination; this includes the mandatory plugin qwindows.dll (put in 'platforms').

#    Step 1: Get the path to the Qt tools by querying for QMake, using that path then as a hint for finding WinDeployQt.exe.
get_target_property (QT_QMAKE_EXECUTABLE Qt6::qmake IMPORTED_LOCATION)
get_filename_component (QT_WINDEPLOYQT_EXECUTABLE ${QT_QMAKE_EXECUTABLE} PATH)
set (QT_WINDEPLOYQT_EXECUTABLE "${QT_WINDEPLOYQT_EXECUTABLE}/windeployqt.exe")

#    Step 2: Invoking WinDeployQt.exe. Takes care of copying all needed files.
add_custom_command (
	TARGET ${PROJECT_NAME} POST_BUILD
	COMMAND ${QT_WINDEPLOYQT_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/$<CONFIG>/$<TARGET_FILE_NAME:${PROJECT_NAME}> --qmldir ${CMAKE_CURRENT_SOURCE_DIR} --no-compiler-runtime --no-translations
        # Attention: Use --qmldir for QML/Quick applications; the directory must point to where the QMLs are (for the 'qmlimportscanner').
)

# --------------------------------------------------------------------------------------------------
#                                          For the 'INSTALL' target
#
# install() is used for the pre-defined targets 'INSTALL' and 'PACKAGE' (by CMake's CPack module).
# Target INSTALL depends on 'all' by default.

install (
    TARGETS ${PROJECT_NAME}             # The main executable.
    RUNTIME
    DESTINATION .                       # Current directory (relative path to CMAKE_INSTALL_PREFIX).
)

install (
    DIRECTORY
        ${CMAKE_CURRENT_BINARY_DIR}/$<CONFIG>/
    DESTINATION .                       # Current directory (relative path to CMAKE_INSTALL_PREFIX).
)

# On Debug build: Copy linker's debug symbols file (*.pdb) to the package directory.
#
# TARGET_PDB_FILE is the full path to the Microsoft debug symbols file (*.pdb) generated by the
# linker for an executable or shared library target (/pdb linker flag).
# This property does not apply to STATIC library targets because no linker is invoked to produce them.
# For static libraries, compiler-generated program database files are specified by the /Fd compiler flag.
# Use the COMPILE_PDB_OUTPUT_DIRECTORY property to specify that (see also the PDB_NAME[_<CONFIG>] and PDB_OUTPUT_DIRECTORY[_<CONFIG>] target properties).

install (
    FILES $<TARGET_PDB_FILE:${PROJECT_NAME}>
    DESTINATION .                             # Current directory (relative path to CMAKE_INSTALL_PREFIX).
    CONFIGURATIONS Debug                      # Only run when used in a 'Debug' build.
)
