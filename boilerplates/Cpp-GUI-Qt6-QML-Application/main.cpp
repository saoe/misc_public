/*
    <PROJECT_NAME_PLACEHOLDER>

    Copyright © <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> <<PROJECT_EMAILADDRESS_PLACEHOLDER>>
    SPDX-License-Identifier: <LICENSE_PLACEHOLDER>
*/

#include <QGuiApplication> // To be used for QML applications (instead of QApplication).
#include <QQmlApplicationEngine>

#include "version.h"

int main (int argc, char* argv[])
{
	QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

	return app.exec();
}