# <PROJECT_NAME_PLACEHOLDER>

{(short) description}

## Download

📥

----------------------------------------------------------------------------------------------------

## How to build this project

This project is currently created on and for Microsoft Windows as its primary platform in mind
and needs:

- [CMake](http://www.cmake.org), a build-system generator
- A C++ build environment, like [Microsoft Visual Studio](https://visualstudio.microsoft.com/) - [Community Edition](https://visualstudio.microsoft.com/de/vs/community/)

### Configure 🡢 Build 🡢 Install

If an optional setting is not provided, a default value (specified by either CMake or the project) will be applied.

#### Using CMake Presets

The project is now using the new CMake feature of [Presets](https://cmake.org/cmake/help/latest/manual/cmake-presets.7.html)
(fully available since CMake 3.20) for easier handling.

To get an overview of the available presets, run this:

    C:\project_dir> cd src
    C:\project_dir\src> cmake --list-presets all

Then, you can use one of those available presets to configure, build, and install the project:  
(Hint: If you can't decide which option to choose, try the _"default"_ presets.)

    C:\project_dir\src> cmake --preset <name_of_a_CONFIG_preset>
    
    C:\project_dir\src> cmake --build --preset <name_of_a_BUILD_preset>
    
    C:\project_dir\src> cmake --install <path_of_the_build_directory> [--prefix <path>]

If you want to override the installation destination path that was already configured,
you can provide a different path here with _--prefix_.


#### Using CMake with command line arguments

This information may be useful if you neither want to use the batch file, nor the CMake presets,
but want to call CMake directly; not sure for how long I will keep this info here up-to-date.

If you want to go down this road, you probably know what you're doing and could also look into the
above mentioned files and derive the required settings.

For items like _Generator_, _Toolset_ etc., CMake only allows specfic values; please consult the CMake documentation for details.

##### Configure

    cmake -D DEF=Value -S <dir> -B <dir> -G "<GeneratorName>" -A <Arch> -T <ToolsetName>
           │            │        │        │                    │         │
           [optional]   │        │        [optional]           [optional][optional]
           │            │        │        │                    │         │
           │            │        │        │                    │         ╰─ Name of the toolset that the generator should use
           │            │        │        │                    │
           │            │        │        │                    ╰─ Name of the target architecture/platform
           │            │        │        │
           │            │        │        ╰─ Name of the CMake generator
           │            │        │
           │            │        ╰─ Output-Directory for the build artifacts
           │            │
           │            ╰─ Directory with the source code files and top-level CMakeLists.txt
           │
           ╰─ Definition(s); must come first!

After the configuration is done, you can continue to use CMake for building the project(s)
(see next step), or you can open the generated files with other tools, like for example the
`*.sln` with the _Visual Studio IDE_.

##### Build (and Install)

Use the default target (or explicitly target `ALL_BUILD`) to build all dependent parts of the project.

Or use the target `INSTALL` to build and install all dependent parts of the project;
set `-D CMAKE_INSTALL_PREFIX=<InstallationPath>` during the previous configuration step to change
the project's installation directory. This target will take care of building all required dependencies in the project.

Or check the _CMakeLists.txt_ files of the sub-projects to get the specific names of other available targets.

    cmake --build <dir> --target <TargetName> --config <ConfigName> --parallel <Number>
            │             │                     │                     │
            │             [optional]            [optional]            [optional]
            │             │                     │                     │
            │             │                     │                     ╰─ Concurrent processes to use.
            │             │                     │                        If <Number> is omitted, the build tool's default is used.
            │             │                     │
            │             │                     ╰─ For multi-configuration tools (like MSVC); e.g. 'Release' or 'Debug'.
            │             │
            │             ╰─ To build <TargetName> instead of the default target.
            │                Multiple targets may be given, separated by spaces.
            │
            ╰─ The build directory from the previous configuration step.
               Mandatory and must come first!

----------------------------------------------------------------------------------------------------

## Usage

### Incorprate it as a package in your own project's CMakeLists.txt

    project(test)                   # Must come before find_package()!
    find_package(XYZ REQUIRED)
    add_executable(foo bar.cpp)
    target_link_libraries(foo XYZ::Library)

Point the project to the WPDLibConfig.cmake (etc.) files by setting CMAKE_PREFIX_PATH:

    cmake -D CMAKE_PREFIX_PATH=C:\<the_path>\<to_your>\<installation_of>\XYZ\cmake -S <YourSrcDir> -B <YourBuildDir>
    cmake --build <YourBuildDir>

## Copyright and License

Copyright © <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> \<<PROJECT_EMAILADDRESS_PLACEHOLDER>\>  
SPDX-License-Identifier: <LICENSE_PLACEHOLDER>  
(For details, see accompanying file LICENSE.txt)
