/*
    <PROJECT_NAME_PLACEHOLDER>

    Copyright © <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> <<PROJECT_EMAILADDRESS_PLACEHOLDER>>
    SPDX-License-Identifier: <LICENSE_PLACEHOLDER>
*/

#include "MainWidget.h"
#include <QLabel>
#include <QLayout>
#include <windows.h>
#include <iostream>
#include <string>

MainWidget::MainWidget ()
{
	setWindowTitle("Qt QWidgets Application");
	//setWindowIcon(QIcon(":/foo.ico"));
    
    status_bar = new QStatusBar(this);
		
	QHBoxLayout* ButtonLayout = new QHBoxLayout;
    QVBoxLayout* MainLayout = new QVBoxLayout;
	
    Button_OK = new QPushButton(tr("OK"), this);
    Button_Test = new QPushButton(tr("Test"), this);
    Button_ForwardSignal = new QPushButton(tr("Forward Signal"), this);
    
    ButtonLayout->addWidget(Button_OK, 0, Qt::AlignCenter);
    ButtonLayout->addWidget(Button_Test, 0, Qt::AlignCenter);
    ButtonLayout->addWidget(Button_ForwardSignal, 0, Qt::AlignCenter);

	MainLayout->addLayout(ButtonLayout);
	MainLayout->addWidget(status_bar, 0, Qt::AlignBottom);
	setLayout(MainLayout);

	// -- Signal/slot connections --
	
    // Button "OK": Its 'clicked' signal calls a signal handler (a slot).
    connect(Button_OK, &QPushButton::clicked, this, &MainWidget::onEvent_ButtonClick_OK);
            
    // Button "Test": Its 'clicked' signal calls a handler, which emits another signal with an added argument (the signal 'clicked' has no argument capabilities).
    connect(Button_Test, &QPushButton::clicked, this, &MainWidget::onEvent_ButtonClick_Test);
    connect(this, &MainWidget::signalTest, this, &MainWidget::onEvent_Signal_Test);
    
    // Button "Forward Signal": Its 'clicked' signal triggers to send another signal, which is then handled with another signal/slot connection.
    connect(Button_ForwardSignal, &QPushButton::clicked, this, &MainWidget::forwardSignal);
    connect(this, &MainWidget::forwardSignal, this, &MainWidget::onEvent_ForwardSignal);
}

MainWidget::~MainWidget ()
{
}

// -------------------------------------------------------------------------------------------------
// Slots (signal handler)

void MainWidget::onEvent_ButtonClick_OK ()
{
	status_bar->showMessage(tr("Button 'OK' was clicked"), 5000);
}

void MainWidget::onEvent_ButtonClick_Test ()
{
    emit signalTest(test_value++);
}

void MainWidget::onEvent_Signal_Test (int value)
{
    std::string str("Button 'Test' was clicked " + std::to_string(value) + " times; button 'OK' is toggled");
    status_bar->showMessage(str.c_str(), 5000);
    Button_OK->setEnabled(!Button_OK->isEnabled());
}

void MainWidget::onEvent_ForwardSignal ()
{
    status_bar->showMessage(tr("Signal was forwarded"), 5000);
}

/*
// -------------------------------------------------------------------------------------------------
// Reimplemented special event handler to receive native platform events (in this case, the Win32 Message Loop).
bool MainWidget::nativeEvent (const QByteArray &eventType, void* message, long* result)
{
	MSG *msg = static_cast<MSG*>(message);

	if (msg->message == WM_FOO)
	{
		switch (msg->wParam)
		{
			case FOO_BAR:
			{
				break;
			}
		}
		return true;
	}
	return false;
}
*/

// -------------------------------------------------------------------------------------------------
#include "moc_MainWidget.cpp"