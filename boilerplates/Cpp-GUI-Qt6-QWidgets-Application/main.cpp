/*
    <PROJECT_NAME_PLACEHOLDER>

    Copyright © <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> <<PROJECT_EMAILADDRESS_PLACEHOLDER>>
    SPDX-License-Identifier: <LICENSE_PLACEHOLDER>
*/

#include "MainWidget.h"
#include "version.h"

#include <QApplication>
#include <QScreen>

int main (int argc, char* argv[])
{
	QApplication app(argc, argv);

	// Setup the main application window.
	QScreen* screen = QGuiApplication::primaryScreen();
	QRect screenGeometry = screen->geometry();
	MainWidget* window = new MainWidget();
	window->resize(screenGeometry.width()/4, screenGeometry.height()/4);

	window->show();

	return app.exec();
}
