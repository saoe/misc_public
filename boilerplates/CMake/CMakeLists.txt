# ╭────────────────────────────────────────────────────────────────────────────────────────────────╮
# │                                   <PROJECT_NAME_PLACEHOLDER>                                   │
# ╰────────────────────────────────────────────────────────────────────────────────────────────────╯
# Copyright © <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> <<PROJECT_EMAILADDRESS_PLACEHOLDER>>
# SPDX-License-Identifier: <LICENSE_PLACEHOLDER>

# --------------------------------------------------------------------------------------------------
#                                         Check prerequisites                                       

cmake_minimum_required (VERSION 3.20)

# Abort script if an attempt to do an 'in-source-build' is detected.
#
# Note that by running this, the file CMakeCache.txt and the directory CMakeFiles will be generated in any case.
# There is no way to remove these from within this script, so the clean-up has to happen by other (manual) means.
#
if (${CMAKE_SOURCE_DIR} STREQUAL ${CMAKE_BINARY_DIR})
    message(FATAL_ERROR "[Fatal Error] Detected an attempt to do an in-source-build!\n"
    "Remove the file 'CMakeCache.txt' and the directory 'CMakeFiles' first, then run CMake again with an configuration for an out-of-source-build:\n"
    "Either create a separate build directory and run CMake from there, or invoke CMake with the '-B <buildir>' option.")
endif()

include (FindGit)
find_package(Git)
if(NOT Git_FOUND)
    message(FATAL_ERROR "Git not found!")
endif()

# --------------------------------------------------------------------------------------------------
#                                       Global project settings

string (TIMESTAMP CURRENT_YEAR "%Y")

project ("<PROJECT_NAME_PLACEHOLDER>"
    # The following are optional. They set variables like 'CustomProjectName_DESCRIPTION' and 'PROJECT_DESCRIPTION'.
    VERSION      1.2.3                            # [CMAKE_]PROJECT_VERSION_MAJOR, ..._MINOR, ..._PATCH and ..._TWEAK.
    LANGUAGES    CXX                              # CXX is C++.
    DESCRIPTION  "Short project description"
    HOMEPAGE_URL "http://www.saoe.net/"
)

set (APP_NAME         ${PROJECT_NAME})
set (AUTHOR_NAME      "Sascha Offe")
set (AUTHOR_MAIL      "so@saoe.net")
set (COPYRIGHT_NOTICE "Copyright (c) ${CURRENT_YEAR} ${AUTHOR_NAME}")
set (LICENSE_NOTICE   "SPDX-License-Identifier: <LICENSE_PLACEHOLDER>")

# Get the path of the directory one level above this CMakeLists.txt file (which is already the top-most that CMake knows about).
# (Comes handy at times, if you need to reference the ReadMe file or something similar...)
get_filename_component (${CMAKE_PROJECT_NAME}_REPO_ROOT_DIR ../ ABSOLUTE)

# The basename for the generated *.exe, *.dll, *.lib and *.h files.
# Based on the original project name, but sans whitespaces and converted to lower case.
string(REPLACE " " ""  PROJECT_BASENAME ${PROJECT_NAME})
string(TOLOWER ${PROJECT_BASENAME} PROJECT_BASENAME)

# You can override this on the command line: cmake -D CMAKE_INSTALL_PREFIX=C:\some\path -S ...
if (CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
    set(CMAKE_INSTALL_PREFIX "${CMAKE_BINARY_DIR}/_install" CACHE PATH "Default install path" FORCE)
    message(STATUS "[${PROJECT_NAME}] CMAKE_INSTALL_PREFIX not explicitly defined; using default value: '${CMAKE_INSTALL_PREFIX}'")
endif()

# --------------------------------------------------------------------------------------------------
#                               Global platform and compiler settings

# Popular CMake idiom to check whether the build is done for(!) a 64-bit target.
# Uses only the common x86 and x64 symbols at the moment, no IA-64, AMD64, X86_64 etc.
if (${CMAKE_SIZEOF_VOID_P} EQUAL "8")
    set (CPU_ARCHITECTURE "x64")
    set (BITNESS "64")
else ()
    set (CPU_ARCHITECTURE "x86")
    set (BITNESS "32")
endif ()

# Destination paths for the various install commands; will be applied relative to CMAKE_INSTALL_PREFIX!
set (${CMAKE_PROJECT_NAME}_INSTALL_BINDIR     bin/${CMAKE_PROJECT_NAME})
set (${CMAKE_PROJECT_NAME}_INSTALL_LIBDIR     lib/${CMAKE_PROJECT_NAME})
set (${CMAKE_PROJECT_NAME}_INSTALL_INCLUDEDIR include/${CMAKE_PROJECT_NAME})
set (${CMAKE_PROJECT_NAME}_INSTALL_CMAKEDIR   cmake/${CMAKE_PROJECT_NAME})
set (${CMAKE_PROJECT_NAME}_INSTALL_DOCDIR     doc/${CMAKE_PROJECT_NAME})

# These exist also as target-specific properties (as CXX_STANDARD etc.); <https://crascit.com/2015/03/28/enabling-cxx11-in-cmake/>
set (CMAKE_CXX_STANDARD 17)          # ISO C++ 2017
set (CMAKE_CXX_STANDARD_REQUIRED ON) # By default, CMake falls back to the latest standard the compiler supports, without any warning or error. If you don't want that, set "required" to 'true/on/yes'.
set (CMAKE_CXX_EXTENSIONS OFF)       # Stick strictly to the standard, e.g. use '-std=c++11' rather than '-std=gnu++11' (taking GCC as an example compiler here).

# Turn on the ability to create folders to organize projects in IDEs.
# Creates "CMakePredefinedTargets" folder by default and adds the CMake-defined projects like ALL_BUILD.vcproj, INSTALL.vcproj and ZERO_CHECK.vcproj to it.
set_property (GLOBAL PROPERTY USE_FOLDERS ON)

if (MSVC)                                                           # ---- Microsoft Visual C++ ----
    # Limit the MSVC configuration types to "Release" and "Debug".
    set (CMAKE_CONFIGURATION_TYPES "Release;Debug" CACHE STRING INTERNAL FORCE)

    # Set this as the start-up project for Visual Studio (otherwise it defaults to ALL_BUILD).
    set_property (DIRECTORY PROPERTY VS_STARTUP_PROJECT ${PROJECT_NAME})
    
    add_compile_definitions (
        WIN32
        _WINDOWS
        _UNICODE
        UNICODE
        _CRT_SECURE_NO_WARNINGS
    )
    
    # Global settings for the current directory/project and below/later added.
    add_compile_options (
        /MP                      # Build with Multiple Processes (optional: /MP[processMax]).
        $<$<CONFIG:Release>:/MD> # Use the multithread-specific and DLL-specific version of the run-time library.
        $<$<CONFIG:Debug>:/MDd>  # Use the debug multithread-specific and DLL-specific version of the run-time library. 
        $<$<CONFIG:Debug>:/Zi>   # Generate "complete debugging information".
        $<$<CONFIG:Debug>:/Od>   # No auto-inlining.
        $<$<CONFIG:Debug>:/Ob0>  # No optimization.
        $<$<CONFIG:Debug>:/RTC1> # Run-time checking: report when a variable is used without being initialized, and stack frame run-time error checking.
        $<$<CONFIG:Release>:/O2> # Max. speed.
    )
endif ()

# --------------------------------------------------------------------------------------------------
#                               Optional target to build documentation

add_subdirectory("Application")

if (${GenerateDocumentation})
    add_subdirectory("doc")
endif ()

# A friendly reminder.
message(FATAL_ERROR "[${CMAKE_PROJECT_NAME}] This CMakeLists.txt is a generic template -- have you added all required subdirectories for this project?!")

# --------------------------------------------------------------------------------------------------
#                             Show all targets of the project at the end
include(${CMAKE_SOURCE_DIR}/cmake/print_all_targets.cmake)
