<#
    .Synopsis
    <PROJECT_NAME_PLACEHOLDER>
    
    .Description
    ...

    .Parameter Path
    ...
    
    .Example
    > ...
    
    ...
    
    .Notes
    Copyright © <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> <<PROJECT_EMAILADDRESS_PLACEHOLDER>>
    SPDX-License-Identifier: <LICENSE_PLACEHOLDER>
#>

[CmdletBinding()]
Param
(
    [Parameter(Mandatory=$true)]
    [ValidateSet("abc", "xyz")]
    [string] $P = "abc",
    
    [Parameter(Mandatory=$true)]
    [ValidateScript({ Test-Path -Path $_ -PathType Container })]
    $Directory
)

write-host "Hello, world!"
