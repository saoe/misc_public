/*
    ╭──────────────────────────────────────────────────────────────────────────────────────────────╮
    │                                  <PROJECT_NAME_PLACEHOLDER>                                  │
    ╰──────────────────────────────────────────────────────────────────────────────────────────────╯
    Copyright © <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> <<PROJECT_EMAILADDRESS_PLACEHOLDER>>
    SPDX-License-Identifier: <LICENSE_PLACEHOLDER>

    Win32 program with several controls:   
    [_________] -> Listbox
    [_________] -> Multiline edit control (read-only)
    [_] [_] [_] -> Buttons

    The edit control serves as a sort of message log; text gets written/appended
    to it on the push of the buttons.

    2015-02-28 (Sascha Offe): Created.
    2022-02-27 (so)         : Small update and made it into a project boilerplate
*/

#include <iostream>
#include <windows.h>
#include <tchar.h>
#include <commctrl.h>
#include "res.h"

#define PROGRAM_NAME _T("Simple Win32 Application #3")
#define ID_BUTTON_001        100
#define ID_BUTTON_001_STRING _T("Button 1")
#define ID_BUTTON_002        101
#define ID_BUTTON_002_STRING _T("Button 2")
#define ID_BUTTON_003        102
#define ID_BUTTON_003_STRING _T("Button 3")
#define ID_LISTBOX_001       103
#define ID_EDITCONTROL_001   104
#define ID_CONST             -1

HWND listbox_devices, editcontrol_readonly, button_001, button_002, button_003;

// ----------------------------------------------------------------------

void appendText (std::wstring newText)
{
	newText += _T("\r\n");
	
	// Put the caret a the end.
	SendMessage(editcontrol_readonly, EM_SETSEL, 0, -1);
	SendMessage(editcontrol_readonly, EM_SETSEL, -1, -1);

	// Select all text and replace.
	DWORD l,r;
	SendMessage(editcontrol_readonly, EM_GETSEL,(WPARAM)&l,(LPARAM)&r);
	SendMessage(editcontrol_readonly, EM_SETSEL, -1, -1);
	SendMessage(editcontrol_readonly, EM_REPLACESEL, 0, (LPARAM)newText.c_str());
	SendMessage(editcontrol_readonly, EM_SETSEL,l,r);
}

// ----------------------------------------------------------------------

LRESULT CALLBACK WndProc (HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	switch (msg)
	{
		case WM_CREATE:
		{
			RECT client_area;
		    GetClientRect(hwnd, &client_area); // Get parent window's client area.

			// Get the system's current message font, because SYSTEM_FONT and DEFAULT_GUI_FONT are not what they appear to be.
			NONCLIENTMETRICS metrics;
			metrics.cbSize = sizeof(NONCLIENTMETRICS);
			SystemParametersInfo(SPI_GETNONCLIENTMETRICS, sizeof(NONCLIENTMETRICS), &metrics, 0);
			HFONT font = CreateFontIndirect(&metrics.lfMessageFont);
			
			// Create windows (i.e. buttons and other controls) and set new font.
			listbox_devices = CreateWindow(_T("LISTBOX"),
			                               0,
			                               WS_VISIBLE | WS_CHILD | WS_BORDER | WS_VSCROLL | WS_HSCROLL | LBS_NOTIFY,
			                               10, 10, // Starting point
										   (client_area.right-client_area.left)-20, 120, // Width, Height
										   hwnd, (HMENU) ID_LISTBOX_001, 0, 0);
			SendMessage(listbox_devices, WM_SETFONT, (WPARAM)font, MAKELPARAM(TRUE, 0));

			editcontrol_readonly = CreateWindow(_T("EDIT"), 0,
			                                    WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL | WS_HSCROLL | ES_READONLY | ES_LEFT | ES_MULTILINE | ES_AUTOHSCROLL | ES_AUTOVSCROLL, 
			                                    10, 150,
												(client_area.right-client_area.left)-20, 250,
			                                    hwnd, (HMENU) ID_EDITCONTROL_001, 0, 0);
			SendMessage(editcontrol_readonly, WM_SETFONT, (WPARAM)font, MAKELPARAM(TRUE, 0));

			button_001 = CreateWindow(_T("BUTTON"),
			                          ID_BUTTON_001_STRING,
			                          WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
			                          client_area.left + 10,         // x position 
			                          client_area.bottom - 40,         // y position 
			                          80,       // Button width
			                          30,       // Button height
			                          hwnd,     // Parent window
			                          (HMENU) ID_BUTTON_001,
			                          GetModuleHandle(NULL), 
			                          NULL);
			SendMessage(button_001, WM_SETFONT, (WPARAM)font, MAKELPARAM(TRUE, 0));
			
			button_001 = CreateWindow(_T("BUTTON"),
			                          ID_BUTTON_002_STRING,
			                          WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
			                          client_area.left + 100,         // x start position 
			                          client_area.bottom - 40,        // y start position 
			                          180,      // Button width
			                          30,       // Button height
			                          hwnd,     // Parent window
			                          (HMENU) ID_BUTTON_002,
			                          GetModuleHandle(NULL), 
			                          NULL);
			SendMessage(button_001, WM_SETFONT, (WPARAM)font, MAKELPARAM(TRUE, 0));
			
			button_003 = CreateWindow(_T("BUTTON"),
			                          ID_BUTTON_003_STRING,
			                          WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
			                          client_area.right - 190,        // x start position 
			                          client_area.bottom - 40,        // y start position 
			                          180,      // Button width
			                          30,       // Button height
			                          hwnd,     // Parent window
			                          (HMENU) ID_BUTTON_003,
			                          GetModuleHandle(NULL), 
			                          NULL);
			SendMessage(button_003, WM_SETFONT, (WPARAM)font, MAKELPARAM(TRUE, 0));

			break;
		}
		
		case WM_SIZE:
			// Adjusting dimensions when the window is resized.
			// MoveWindow(listbox_devices, 10, 10, LOWORD(lparam)-20, HIWORD(lparam)-50, true);
			break;

		case WM_COMMAND:
			switch(LOWORD(wparam))
			{
				case ID_BUTTON_001:
				{
					appendText(ID_BUTTON_001_STRING);
					break;
				}

				case ID_BUTTON_002:
				{
					appendText(ID_BUTTON_002_STRING);
					break;
				}

				case ID_BUTTON_003:
				{
					appendText(ID_BUTTON_003_STRING);
					break;
				}
			}
			break;

		case WM_CLOSE:
			// if (MessageBox(hwnd, _T("Really quit?"), _T("My application"), MB_OKCANCEL) == IDOK)
			// {
			DestroyWindow(hwnd);
			// }
			return 0;
                               
		case WM_DESTROY:
			PostQuitMessage(0);
			break;

		case WM_QUIT:
			break;
       
		default:
			return DefWindowProc(hwnd, msg, wparam, lparam);
	}

	return 0;
}

// -------------------------------------------------------------------------
int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	WNDCLASSEX wc;
	HWND       hwnd;
	MSG        Msg;

	HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);

	// Registering the Window Class
	wc.cbSize        = sizeof(WNDCLASSEX);
	wc.style         = 0;
	wc.lpfnWndProc   = WndProc;
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = hInstance;
	wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW);
	wc.lpszMenuName  = NULL;
	wc.lpszClassName = _T("MyClassName");
	wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

    if (!RegisterClassEx(&wc))
    {
        MessageBox(NULL, _T("Window Registration Failed!"), _T("Error!"), MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

	// Creating the Window. Fixed/unresizable dimensions (to change: Replace styles with WS_OVERLAPPEDWINDOW).
	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE, _T("MyClassName"), PROGRAM_NAME, WS_BORDER | WS_CAPTION | WS_SYSMENU, CW_USEDEFAULT, CW_USEDEFAULT, 640, 480, NULL, NULL, hInstance, NULL);

    if (hwnd == NULL)
    {
        MessageBox(NULL, _T("Window Creation Failed!"), _T("Error!"), MB_ICONEXCLAMATION | MB_OK);
        return 0;
    }

    ShowWindow(hwnd, nCmdShow);
    UpdateWindow(hwnd);

    while (GetMessage(&Msg, NULL, 0, 0) > 0)
    {
        TranslateMessage(&Msg);
        DispatchMessage(&Msg);
    }

	if (hr >= 0)
		CoUninitialize();

    return Msg.wParam;
}