"""
<PROJECT_NAME_PLACEHOLDER>

Copyright © <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> <<PROJECT_EMAILADDRESS_PLACEHOLDER>>
SPDX-License-Identifier: <LICENSE_PLACEHOLDER>
"""

import os
import sys
import argparse
import fnmatch
from pathlib import Path

if sys.version_info[0] < 3:
	sys.exit("You seem to be running an older version of Python:\n" + sys.version + "\n\nSorry, but you need Python 3 or higher for this script.")

# -- Handle command line options -------------------------------------------------------------------
# Note: There doesn't seem to be a way to configure argparse to accept case insensitive arguments on the CLI; so -f and -F are different!
# Note: By default, argparse accepts abbrevations, if they are unambiguously identifiable: -version == -ver == -v.

parser = argparse.ArgumentParser(description  = "<PROJECT_NAME_PLACEHOLDER>",
                                 prefix_chars = '-/',   # Accepting the Unix version ("-") and also the Windows prefix ("/").
                                 add_help     = True,   # We rely on the provided help by argparse.
                                 epilog       = "Copyright (c) <COPYRIGHT_YEAR_PLACEHOLDER> <PROJECT_BY_PLACEHOLDER> <<PROJECT_EMAILADDRESS_PLACEHOLDER>>. Published under the terms of the <LICENSE_PLACEHOLDER> license (see http://opensource.org/licenses/ for details)."
                                )

parser.add_argument("-version", "/version", action="version", version="Version 1.0")

parser.add_argument("-input", "/input",
                    required = True,
                    action   = "store",
                    dest     = "input_dir",
                    help     = "The directory where the script should start looking for files."
                   )

parser.add_argument("-f", "/f", "--filename-pattern",
                    required = False,
                    action   = "store",
                    dest     = "filename_pattern",
                    default  = "*.*",
                    help     = "The matching pattern for the filenames.\n Default pattern is \"%(default)s\""
                   )

parser.add_argument("-documentation", "/documentation",
                    required = False,
                    action   = "store_true",  # On/Off flag
                    help     = "Set this flag if documentation files should also be generated."
                   )

args = parser.parse_args()

input_dir              = args.input
filename_pattern       = args.filename_pattern
generate_documentation = args.documentation

# --------------------------------------------------------------------------------------------------



# --------------------------------------------------------------------------------------------------
# The main entry point for this script.
if __name__ == '__main__':
    print("Hello, world!")
