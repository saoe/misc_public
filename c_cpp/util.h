/*
	A collection of assorted utility functions in a common namespace.
	
	Author: Sascha Offe
	2005 (Initial release)
*/

#ifndef SO_UTIL_H
#define SO_UTIL_H

#include <iostream>
#include <string>
#include <time.h>
#include <vector>
#include "util_templates.h" // Defines the template functions.


void convertDecToBin (int n, std::vector<int>* dest);
int  convertBinToDec (std::string num_str);

std::string stripSpace   (std::string input, bool strip_all = false);
std::string replaceSpace (std::string input);

bool             isLeapyear            (int year);
struct timestamp mapDateToDayOfTheYear (int year, int day);
int              getDayOfTheYear       (int year, int month, int day);
int              getSwatchInternetTime ();

int generateRandomNumber (int n);
int factorial (int n);

#endif
