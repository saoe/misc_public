// 2010-02-10 00:22:55 Sascha Offe
// Based on code from Petzold's ProgWin (5th Ed.).

#include <iostream>
#include <windows.h>

LRESULT CALLBACK WndProc (HWND, UINT, WPARAM, LPARAM);

int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	static TCHAR AppName[] = TEXT("Some Name");
	
	HWND     hwnd;
	MSG      msg;
	WNDCLASS wndclass;
	
	wndclass.style         = CS_HREDRAW | CS_VREDRAW;
	wndclass.lpfnWndProc   = WndProc;
	wndclass.cbClsExtra    = 0;
	wndclass.cbWndExtra    = 0;
	wndclass.hInstance     = hInstance;
	wndclass.hIcon         = LoadIcon(0, IDI_APPLICATION);
	wndclass.hCursor       = LoadCursor(0, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH) GetStockObject(WHITE_BRUSH);
	wndclass.lpszMenuName  = 0;
	wndclass.lpszClassName = AppName;
	
	if (!RegisterClass(&wndclass))
	{
		MessageBox(0, TEXT("Error in RegisterClass"), TEXT("Caption"), MB_ICONERROR);
		return 0;
	}
   
	hwnd = CreateWindow(AppName, TEXT("Caption"),
		WS_OVERLAPPEDWINDOW, // Window style.
		CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,	// Initial x/y postion & x/y size.
		0, 0, hInstance, 0);
	
	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);
   
	while (GetMessage (&msg, 0, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
		
	return msg.wParam;
}

LRESULT CALLBACK WndProc (HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
	switch (msg)
	{
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		
		default:
			return DefWindowProc(hwnd, msg, wparam, lparam);
	}

	return 0;	
}
