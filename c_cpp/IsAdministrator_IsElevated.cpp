/* ====================================================================================================
Based on what I could gather from around the web, but note the article quoted below!
-- Sascha Offe (2016-05-17/18)

Quintessence: "... unless you know precisely what it is doing, please don’t use this.
               Because it probably doesn’t do what you think it does."

Text taken from: https://blogs.msdn.microsoft.com/cjacks/2006/10/08/how-to-determine-if-a-user-is-a-member-of-the-administrators-group-with-uac-enabled-on-windows-vista/

	> How to Determine if a User is a Member of the Administrators Group with UAC Enabled on Windows Vista
	> October 8, 2006.
	> By Chris Jackson - MSFT
	> 
	> User Account Control (UAC) on Windows Vista changes the paradigm of being an administrator on a Microsoft Windows
	> operating system. Rather than wielding full administrative privileges all of the time, the token is “split” and
	> there are two of them. If you run an application normally, it is given the token that has fewer privileges
	> (a “standard user” token, if you will, although the Administrators group is still present and set to “deny only” so
	> securable objects that an administrator is explicitly forbidden from accessing will still be denied to this user).
	> If you create a process elevated, you are prompted to approve the elevation, after which the process is provided with
	> an “unfiltered” token that grants this application full administrator credentials.
	>  
	> This is a huge win for security. However, it does break some of the paradigms that you may be used to using when developing
	> applications. One example is checking to see if the user is an administrator explicitly. Most of the tools that help identify
	> more LUA bugs (and these tools are becoming pretty indispensible now that pretty much everybody is running as a standard user
	> the majority of the time) will flag this as a potential LUA bug. You see, this is something that can be done for good
	> (you are checking to see if the user is an administrator to determine whether or not you want to offer them the option of
	> launching another process elevated to provide additional functionality), and it is something that can be done for evil
	> (you are checking to see if the user is an administrator, because it is easier just to fail than to fix the LUA bug).
	> For now, let’s assume that you are using this power for good.
	> 
	> If you happen to be using the handy shell32 API IsUserAnAdmin, you will find that it will return true if the process is
	> elevated, and false if it is not. Note that the Boolean return value doesn’t provide you with any information that will help
	> you determine if the user CAN elevate – it just tells you if you already have. What can you do if you want to know if the
	> user CAN elevate, whether or not they already have?
	> 
	> The GetTokenInformation API provides a new ability to return a TokenElevationType structure.
	> As of Windows Vista RC1, I do not see this documented in the Windows SDK, but you can find it in the Windows header files
	> (winbase.h and winnt.h). So, I whipped together a little sample that you can run from the command line to determine not only
	> whether the current process is elevated, but also whether the user happens to be a member of the administrators group:
	> 
	> [... source code omitted, see isProcessElevated()...]
	> 
	> As you can see, it’s fairly straightforward. (I have elided error handling and return value checking for clarity.)
	> If you are looking to use this information for good, I hope this helps. If you are looking to use this information for evil,
	> I hope you don’t find this post!
	> 
	> Update: Note that this technique detects if the token is split or not. In the vast majority of situations, this will
	> determine whether the user is running as an administrator. However, there are other user types with advanced permissions
	> which may generate a split token during an interactive login (for example, the Network Configuration Operators group).
	> If you are using one of these advanced permission groups, this technique will determine the elevation type, and not the
	> presence (or absence) of the administrator credentials.
	> 
	> Updated 03-March-2010
	> 
	> Apparently my previous update didn’t scare enough people away from this approach, so here goes: be very afraid of this
	> approach. It will correctly tell you if you have a split token, but that doesn’t necessarily mean anything useful.
	> For example, what happens if you disable UAC? You won’t have a split token. You’d get TokenElevationTypeDefault.
	> What happens if you are logged in as the .Administrators account? Same thing. Neither one means you’re a standard user,
	> which is a common mistake by misapplying the above logic for the “typical” case. What about if you happen to have one,
	> and only one, super privilege, and you elevated to get that into your token? Then you’d have TokenElevationTypeFull – which
	> is frequently interpreted as meaning you’re an admin.
	> 
	> The *right* thing to do is not care. If you think you need admin rights, then manifest your binary, and ask for them.
	> Does the current user’s capabilities matter? If they have a second admin account, then why does it matter if their current
	> one is a standard user?
	> 
	> And you certainly don’t want to go looking for the Administrators ACE in the token in order to authorize something.
	> Because, you know, it has a flag that says “Deny Only” on it. If you go granting access, then that’s directly violating
	> the “Deny Only” intent. Kind of like saying, “oh, I see you’re on the no gambling list – come on into the casino…”
	> 
	> In other words, despite the fact that I wrote this (back in my youth), unless you know precisely what it is doing,
	> please don’t use this. Because it probably doesn’t do what you think it does.
==================================================================================================== */

#include <windows.h>
#include <tchar.h>
#include <cstdio>
#include <Lmcons.h>


/* ----------------------------------------------------------------------------------------------------
Returns 'TRUE' if the caller's process is a member of the Administrators local group.
Caller is NOT expected to be impersonating anyone and is expected to be able to open its own process and process token.
(Note that this most probably will not return the expected value on "Windows Vista" and above due to UAC/split tokens etc.)
---------------------------------------------------------------------------------------------------- */

BOOL isUserAdministrator ()
{
	BOOL status = FALSE;

	SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
	PSID AdministratorsGroup;
	status = AllocateAndInitializeSid(&NtAuthority, 2, SECURITY_BUILTIN_DOMAIN_RID, DOMAIN_ALIAS_RID_ADMINS, 0, 0, 0, 0, 0, 0, &AdministratorsGroup);
       
	if (status)
	{
		if (!CheckTokenMembership (NULL, AdministratorsGroup, &status))
			status = FALSE;
               
		FreeSid(AdministratorsGroup);
	}

	return status;
}


/* ----------------------------------------------------------------------------------------------------
Returns 'true' if the caller's process is running elevated and copies additional text into 'info'.
---------------------------------------------------------------------------------------------------- */

bool isProcessElevated (TCHAR* info)
{
	bool elevated = false;
	HANDLE h = NULL;
 
	if (OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &h))
	{
		TOKEN_ELEVATION te;
		TOKEN_ELEVATION_TYPE tet; 
		DWORD size;// = sizeof(TOKEN_ELEVATION);
       
		if (GetTokenInformation(h, TokenElevation, &te, sizeof(te), &size))
			elevated = te.TokenIsElevated;

		if (GetTokenInformation(h, TokenElevationType, &tet, sizeof(tet), &size))
		{
			switch (tet)
			{
				case TokenElevationTypeDefault:
					_tcscpy(info, _T("TokenElevationTypeDefault: User is not using a split token."));
					break;
				
				case TokenElevationTypeFull:
					_tcscpy(info, _T("TokenElevationTypeFull: User has a split token and the process is running elevated."));
					break;
				
				case TokenElevationTypeLimited:
					_tcscpy(info, _T("TokenElevationTypeLimited: User has a split token, but the process is not running elevated."));
					break;
			} 
		}
	}
   
	if (h)
		CloseHandle(h);
   
	return elevated;
}


// ----------------------------------------------------------------------------------------------------

int main (int argc, char* argv[])
{
	#define INFO_BUFFER_SIZE 32767

	TCHAR InfoBuffer [INFO_BUFFER_SIZE];
	DWORD CharCount = INFO_BUFFER_SIZE;
 
	CharCount = INFO_BUFFER_SIZE;
	if (GetComputerName(InfoBuffer, &CharCount))
		_tprintf(_T("Computer name: %s\n"), InfoBuffer);

	CharCount = INFO_BUFFER_SIZE;
	if (GetUserName(InfoBuffer, &CharCount))
		_tprintf(_T("User name: %s\n"), InfoBuffer);
       
	if (isProcessElevated(InfoBuffer))
		_tprintf(_T("Process is running elevated.\n%s\n"), InfoBuffer);
	else
		_tprintf(_T("Process is NOT running elevated.\n%s\n"), InfoBuffer);
       
	if (isUserAdministrator())
		_tprintf(_T("User is local administrator.\n"));
	else
		_tprintf(_T("User is NOT local administrator.\n"));
    
	return 0;
}
