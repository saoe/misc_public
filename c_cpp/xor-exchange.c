/*

Given two binary integer values, x and y, the values can be exchanged without
use of a temporary by using binary XOR.

From <http://graphics.stanford.edu/~seander/bithacks.html#SwappingValuesXOR>:
	
	On January 20, 2005, Iain A. Fleming pointed out that the macro above doesn't
	work when you swap with the same memory location, such as
	SWAP(a[i], a[j]) with i == j.

	So if that may occur, consider defining the macro as
	(((a) == (b)) || (((a) ^= (b)), ((b) ^= (a)), ((a) ^= (b)))).

[Sascha] Hmm...? Why not? At least in the sample code below, there's no
problem when the values are the same (x == y).

*/

#include <stdio.h>

int main (int argc, char** argv)
{
	unsigned int x = 111;
	unsigned int y = 999;
	
	printf("x = %u\n", x);
	printf("y = %u\n", y);
	
	printf("Swapping...\n");
	
	x ^= y;   /* x' = (x^y)         */
	y ^= x;	  /* y' = (y^(x^y)) = x */
	x ^= y;   /* x' = (x^y)^x   = y */
	
	printf("x = %u\n", x);
	printf("y = %u\n", y);
	
	return 0;
}
