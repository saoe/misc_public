// Text-related functions. Copyright 2005 Sascha Offe.

// TODO:
// Support for C-Arrays (\0-terminated)?

#include "util.h"


/*
	Strips leading and trailing space in a string.
	
	input     - The string that should be cleared.
	strip_all - Set this to true if the spaces between the characters/words should be erased, too.
	Returns a string without the spaces.	
	
	BUG:
		For a spaces-only string (" ", "   ") the result is still a one-space string (" ").
*/
std::string stripSpace (std::string input, bool strip_all)
{
	if (input.empty())
	{
		return input;
	}
	else
	{
		std::string output;

		std::string::iterator pos_cur;
		std::string::iterator pos_beg = input.begin();
		std::string::iterator pos_end = input.end();		
		
		while (!isgraph(*pos_beg))
		{
			if (pos_beg != pos_end)
				++pos_beg;
			else
				break;
		}
		
		while (!isgraph(*pos_end))
		{
			if (pos_beg != pos_end)
				--pos_end;
			else
				break;
		}
		
		++pos_end; // Iterator cuts off the last character, so let's increment it.
		
		if (strip_all)
		{
			// Remove also the spaces between the characters/words.
			for (pos_cur = pos_beg; pos_cur != pos_end; ++pos_cur)
			{
				 if (isspace(*pos_cur))
				 {
				 	++pos_cur;
				 	input.erase(pos_cur-1);
				 	--pos_end;
				 }
			 }
		}
		
		while (pos_beg != pos_end)
		{
			output += *pos_beg;
			++pos_beg;
		}
		
		return output;
	}	
}


/*
	Strips leading and trailing space in a string and replaces the spaces between the characters/words with an underscore charater ("_").
	
	input - The original string.
	
	Returns a new, modified string.
*/
std::string replaceSpace (std::string input)
{
	if (input.empty())
	{
		return input;
	}
	else
	{
		
		std::string s = stripSpace(input, false);
		
		std::string::iterator pos_cur = s.begin();
		std::string::iterator pos_beg = pos_cur; // FIXME: Logically not needed -- but remove it, an the program crashes...
		std::string::iterator pos_end = s.end();
		
		std::string::size_type i;

		while (pos_cur != pos_end)
		{
			if (isspace(*pos_cur))
			{
			 	s.replace(i, 1, "_");
			}
			++i;
			++pos_cur;
		}
		
		return s;
	}	
}
