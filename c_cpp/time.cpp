// Time-related funtions. Copyright 2004, 2005 Sascha Offe.
// LOG: 2004-08-12, 2004-08-17, 2005-07, 2005-09


#include "util.h"


int days_per_month_year     [12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
int days_per_month_leapyear [12] = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

struct timestamp
{
	int year, month, day;
	int hour, minute, second;
};


// Determines wheter a given year is a leap year or not.
// Returns "true" if year is a leap-year (366 days instead of 356).	
//
bool isLeapyear (int year)
{
	if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0))
		return true;
	else
		return false;
}


// Converts "day of the year" to a standard date (Example: YYYY-1 is YYYY-01-01).
// Returns a timestamp structure.
//
struct timestamp mapDateToDayOfTheYear (int year, int day)
{
	int* days_per_month;

	struct timestamp d;
	
	d.year = year;
	d.month = 1;
	d.day = 1;
	
	if (isLeapyear(year))
	{
		days_per_month = days_per_month_leapyear;
		
		if (day > 366)
		{
			std::cout << "Error: Day out of range!" << std::endl; // throw execption instead?
			exit(1);
		}
	}
	else
	{
		days_per_month = days_per_month_year;
		
		if (day > 365)
		{
			std::cout << "Error: Day out of range!" << std::endl; // throw execption instead?
			exit(1);
		}
	}
	
	for (int i = 0; day > days_per_month[i]; ++i)
	{
		day -= days_per_month[i];
		++d.month;
	}
	
	d.day = day;
	
	return d;
}


// Returns which day of the year maps to a standard date format (Example: YYYY-01-01 is YYYY-1).
// Returns a number between 1 and 365 (or 366 in a leap-year).
//
int getDayOfTheYear (int year, int month, int day)
{
	int t = 0;
	int* days_per_month = days_per_month_year;
	
	if (isLeapyear(year))
		days_per_month = days_per_month_leapyear;
	
	if (month == 0 || month > 12)
	{
		std::cout << "Error: Month out of range!" << std::endl; // throw execption instead?
		exit(1);
	}
	
	if (day == 0 || day > days_per_month[month-1])
	{
		std::cout << "Error: Day out of range!" << std::endl; // throw execption instead?
		exit(1);
	}

	for (int i = 0; i < month-1; ++i)
	{
		t += days_per_month[i];
	}
	
	t += day;

	return t;
}

/*
	Returns the current time in the "Swatch Internet Time" format (unit: beat, notation: \@100).
	
	Swatch Internet Time is a concept introduced in 1998 and marketed by the Swatch corporation as an alternative measure of time.
	One of the goals was to simplify the way people in different time zones communicate about time, mostly by eliminating time zones
	altogether (instead, the new scale of Biel Mean Time (BMT) is used, based on the company's headquarters in Biel, Switzerland).
	Instead of hours and minutes, the 24 hour day is divided up into 1000 parts called ".beats", each .beat being 1 minute and 26.4 seconds.
	See also http://en.wikipedia.org/wiki/Swatch_Internet_Time
*/
int getSwatchInternetTime ()
{
	time_t now = time(NULL);
	struct tm* t = gmtime(&now);
	int hours = t->tm_hour;
	hours++;
    
	if (hours > 23)
		hours = 0;

	int beat = ((hours * 3600 * 1000) + (t->tm_min * 60 * 1000) + (t->tm_sec * 1000)) / 86400;
	return beat;
}
