/*
	The operand to sizeof can be one of the following:
	
	o A type name - To use sizeof with a type name, the name must be enclosed in parentheses.
	
	o An expression - When used with an expression, sizeof can be specified with or without
	                  the parentheses. The expression is not evaluated.

	gcc sizeof-array.cpp -o sizeof-array -lstdc++
*/

#include <iostream>

void char_sizes (char c [])
{
	/*
		- comp.lang.c FAQ -
		
		Q: Why doesn't sizeof properly report the size of an array when the array is a parameter to a function?
		A: The compiler pretends that the array parameter was declared as a pointer, and sizeof reports the size of the pointer. 
		   An array is never actually passed to a function. the function will in fact receive a pointer (to the array).
	
		Q: Why doesn't sizeof tell me the size of the block of memory pointed to by a pointer?
		A: sizeof tells you the size of the pointer. There is no portable way to find out the
		   size of a malloc'ed block (Remember, too, that sizeof operates at compile time).
	*/
	
	std::cout << "z - Total size         : " << sizeof c               << std::endl;
	std::cout << "z - Size of single item: " << sizeof c[0]            << std::endl;
	std::cout << "z - Count of items     : " << sizeof (c) / sizeof (c[0]) << std::endl;
}

int main (int argc, char** argv)
{
	int x [100];
	int y = 0;
	char c [] = "Hello, world!";
	
	std::cout << "x - Total size         : " << sizeof x               << std::endl;
	std::cout << "x - Size of single item: " << sizeof x[0]            << std::endl;
	std::cout << "x - Count of items     : " << sizeof x / sizeof x[0] << std::endl;
	
	std::cout << std::endl;
	
	std::cout << "y - Total size         : " << sizeof y                << std::endl;
	std::cout << "y - Size of single item: " << sizeof y                << std::endl;
	std::cout << "y - Count of items     : " << sizeof y / sizeof (int) << std::endl;
	
	std::cout << std::endl;
	
	char_sizes(c);
	
	return 0;
}
