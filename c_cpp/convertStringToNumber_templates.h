// Conversion funtions (templates).	Copyright 2005 Sascha Offe.
// *Sigh* no "export"-support for templates yet...

// See also a function version in convertStringToNumber.cpp

#ifndef SO_UTIL_TEMPLATES_H
#define SO_UTIL_TEMPLATES_H

#include <sstream>

// Converts a number to a string.
// Uses the built-in type conversion of std::ostringstream.
//
template<class T> std::string numtostr(const T& p)
{
	std::ostringstream o;
	o << p;
	return o.str();
}

// Converts a string to a number.
// Uses the built-in type conversion of std::stringstream.
//	
template<class T> T strtonum(const std::string& p)
{
	T r;
	std::stringstream s;
	s << p;
	s >> r;
	return r;
}

#endif
