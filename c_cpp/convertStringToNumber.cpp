// See also a template version in util_templates.h

#include <sstream>

std::string IntToString(int iValue)
{
    std::stringstream ssStream;
    ssStream << iValue;
    return ssStream.str();
}

int StringToInt(std::string stringValue)
{
    std::stringstream ssStream(stringValue);
    int iReturn;
    ssStream >> iReturn;

    return iReturn;
}
