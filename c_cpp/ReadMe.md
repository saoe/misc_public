# C++ and C section

Note: Some compontents from here have evolved and moved on to my repository [The Nifty Oddity Toolkit](https://bitbucket.org/saoe/theniftyodditytoolkit/).

* [convertBinToDec.cpp](convertBinToDec.cpp)  
  Convert a decimal number to binary and convert a string to a number.  

* [convertStringToNumber.cpp](convertStringToNumber.cpp)  
  Converts a string to a number and vice versa.  

* [convertStringToNumber_templates.h](convertStringToNumber_templates.h)  
  Converts a string to a number and vice versa (template version).  

* [sizeof-array.cpp](sizeof-array.cpp)  
  Demo program to show the use of the sizeof operand.  

* [text.cpp](text.cpp)  
  Several text-related functions.  

* [time.cpp](time.cpp)  
  Several time-related funtions.  

* [VeryBasicWin32App.cpp](VeryBasicWin32App.cpp)  
  Program skeleton for a simple Win32-API program.  

* [xor-exchange.c](xor-exchange.c)  
  Demo program to show that given two binary integer values, x and y, the values can be exchanged without use of a temporary by using binary XOR.  

* [IsAdministrator_IsElevated.cpp](IsAdministrator_IsElevated.cpp)  
  Simple demo program that checks if the user is an administrator or if the process is running elevated (Windows UAC).  
  But please pay attention to the text in the comments before relying upon it!  
