// Conversion funtions.	Copyright 2005 Sascha Offe.

#include "util.h"


// Converts a decimal number to a binary number.
//	
// n    - The number that is to be converted.
// dest - Where to put the converted number.
//
void convertDecToBin (int n, std::vector<int>* dest)
{
	if (n <= 1)
    {
        dest->push_back(n);
        return;
	}

	int remainder = n % 2;
	
	convertDecToBin(n >> 1, dest); // Recursive call.
	
	dest->push_back(remainder);
}


// Converts a given string to a decimal number.
//
// num_str - A string representing a number.
// Returns the decimal number.
//
int convertBinToDec (std::string num_str)
{
	unsigned int result = 0;
	int m = num_str.size() - 1;
	int z = 1;
	
	for (std::string::iterator pos = num_str.begin(); pos != num_str.end(); ++pos)
	{
		if (*pos == '1')
			result += z << m;
		else
			if (*pos != '0') return 0; // if input is invalid (neither 1 nor 0).
		
		--m;
	}
	return result;
}
